import util.configureAllKmpTargets

plugins {
    id("compose-noncommon-conventions")
    id("android-library-conventions")
    id("gitlab-publishing-conventions")
}

kotlin {
    configureAllKmpTargets()

    sourceSets {
        commonMain {
            dependencies {
                api(libs.okio)
            }
        }

        val androidMain by getting {
            dependencies {
                implementation(libs.storage)
            }
        }
    }
}

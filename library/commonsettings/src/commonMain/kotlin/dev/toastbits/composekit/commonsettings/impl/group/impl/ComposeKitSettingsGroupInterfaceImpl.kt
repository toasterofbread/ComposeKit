package dev.toastbits.composekit.commonsettings.impl.group.impl

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.GridView
import dev.toastbits.composekit.commonsettings.generated.resources.Res
import dev.toastbits.composekit.commonsettings.generated.resources.pref_interface_animate_pane_resize_title
import dev.toastbits.composekit.commonsettings.generated.resources.pref_interface_initial_pane_ratio_remember_mode_option_ratio
import dev.toastbits.composekit.commonsettings.generated.resources.pref_interface_initial_pane_ratio_remember_mode_option_size_dp
import dev.toastbits.composekit.commonsettings.generated.resources.pref_interface_initial_pane_ratio_remember_mode_title
import dev.toastbits.composekit.commonsettings.generated.resources.pref_interface_remember_initial_pane_ratios_title
import dev.toastbits.composekit.commonsettings.generated.resources.pref_interface_show_pane_resize_handles_on_hover_title
import dev.toastbits.composekit.commonsettings.generated.resources.pref_interface_show_pane_resize_handles_title
import dev.toastbits.composekit.commonsettings.generated.resources.pref_interface_ui_locale_title
import dev.toastbits.composekit.commonsettings.generated.resources.pref_theme_font_scale_title
import dev.toastbits.composekit.commonsettings.generated.resources.pref_theme_font_title
import dev.toastbits.composekit.commonsettings.generated.resources.pref_theme_ui_scale_title
import dev.toastbits.composekit.commonsettings.generated.resources.prefs_group_interface_description
import dev.toastbits.composekit.commonsettings.generated.resources.prefs_group_interface_title
import dev.toastbits.composekit.commonsettings.impl.group.ComposeKitSettingsGroupInterface
import dev.toastbits.composekit.components.utils.composable.pane.model.InitialPaneRatioSource
import dev.toastbits.composekit.components.utils.composable.pane.model.PLATFORM_DEFAULT_USE_PANE_RESIZE_ANIMATION_SPEC
import dev.toastbits.composekit.settings.ComposeKitSettingsGroupImpl
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.presentation.ui.component.item.LocaleSettingsItem
import dev.toastbits.composekit.settingsitem.presentation.ui.component.item.SliderSettingsItem
import dev.toastbits.composekit.settingsitem.presentation.ui.component.item.ToggleSettingsItem
import dev.toastbits.composekit.settingsitem.presentation.ui.item.DropdownSettingsItem
import dev.toastbits.composekit.settingsitem.presentation.ui.item.FontSelectorSettingsItem
import dev.toastbits.composekit.theme.core.model.ComposeKitFont
import dev.toastbits.composekit.util.model.ComposeLocaleList
import dev.toastbits.composekit.util.model.KImageVectorResource
import dev.toastbits.kmpresources.library.model.KmpStringResource
import dev.toastbits.composekit.util.model.Locale
import dev.toastbits.kmpresources.library.mapper.toKmpResource
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.stringResource

open class ComposeKitSettingsGroupInterfaceImpl(
    groupKey: String,
    preferences: PlatformSettings,
    protected val localeNameResource: StringResource
): ComposeKitSettingsGroupImpl(groupKey, preferences), ComposeKitSettingsGroupInterface {
    override val title: KmpStringResource =
        Res.string.prefs_group_interface_title.toKmpResource()

    override val description: KmpStringResource =
        Res.string.prefs_group_interface_description.toKmpResource()

    override val icon: KImageVectorResource =
        KImageVectorResource { Icons.Default.GridView }

    override val UI_LOCALE: PlatformSettingsProperty<Locale?> by
        nullableSerialisableProperty(
            name = Res.string.pref_interface_ui_locale_title.toKmpResource(),
            description = null,
            getDefaultValue = { null }
        )

    override val FONT: PlatformSettingsProperty<ComposeKitFont> by serialisableProperty(
        name = Res.string.pref_theme_font_title.toKmpResource(),
        description = null,
        getDefaultValue = { ComposeKitFont.DEFAULT }
    )

    override val FONT_SCALE: PlatformSettingsProperty<Float> by serialisableProperty(
        name = Res.string.pref_theme_font_scale_title.toKmpResource(),
        description = null,
        getDefaultValue = { 1f }
    )

    override val UI_SCALE: PlatformSettingsProperty<Float> by serialisableProperty(
        name = Res.string.pref_theme_ui_scale_title.toKmpResource(),
        description = null,
        getDefaultValue = { 1f }
    )

    override val SHOW_PANE_RESIZE_HANDLES: PlatformSettingsProperty<Boolean> by
        property(
            name = Res.string.pref_interface_show_pane_resize_handles_title.toKmpResource(),
            description = null,
            getDefaultValue = { true }
        )

    override val SHOW_PANE_RESIZE_HANDLES_ON_HOVER: PlatformSettingsProperty<Boolean> by
        property(
            name = Res.string.pref_interface_show_pane_resize_handles_on_hover_title.toKmpResource(),
            description = null,
            getDefaultValue = { true }
        )

    override val ANIMATE_PANE_RESIZE: PlatformSettingsProperty<Boolean> by
        property(
            name = Res.string.pref_interface_animate_pane_resize_title.toKmpResource(),
            description = null,
            getDefaultValue = { PLATFORM_DEFAULT_USE_PANE_RESIZE_ANIMATION_SPEC }
        )

    override val REMEMBER_INITIAL_PANE_RATIOS: PlatformSettingsProperty<Boolean> by
        property(
            name = Res.string.pref_interface_remember_initial_pane_ratios_title.toKmpResource(),
            description = null,
            getDefaultValue = { true }
        )

    override val REMEMBERED_INITIAL_PANE_RATIOS: PlatformSettingsProperty<Map<String, InitialPaneRatioSource>> by
        serialisableProperty(
            name = KmpStringResource.ofString(""),
            description = null,
            getDefaultValue = { emptyMap() },
            isHidden = { true }
        )

    override val INITIAL_PANE_RATIO_REMEMBER_MODE: PlatformSettingsProperty<InitialPaneRatioSource.Remembered.RememberMode> by
        enumProperty(
            name = Res.string.pref_interface_initial_pane_ratio_remember_mode_title.toKmpResource(),
            description = null,
            getDefaultValue = { InitialPaneRatioSource.Remembered.RememberMode.DEFAULT }
        )

    override fun getConfigurationItems(): List<SettingsItem> =
        listOf(
            LocaleSettingsItem(
                UI_LOCALE,
                localeList = ComposeLocaleList(localeNameResource),
                allowCustomLocale = false
            ),

            FontSelectorSettingsItem(FONT),
            SliderSettingsItem(
                FONT_SCALE,
                range = 0.1f .. 3f
            ),
            SliderSettingsItem(
                UI_SCALE,
                range = 0.1f .. 3f
            ),

            ToggleSettingsItem(SHOW_PANE_RESIZE_HANDLES),
            ToggleSettingsItem(SHOW_PANE_RESIZE_HANDLES_ON_HOVER),
            ToggleSettingsItem(ANIMATE_PANE_RESIZE),
            ToggleSettingsItem(REMEMBER_INITIAL_PANE_RATIOS),
            DropdownSettingsItem.ofEnumState(
                INITIAL_PANE_RATIO_REMEMBER_MODE,
                getItem = {
                    when (it) {
                        InitialPaneRatioSource.Remembered.RememberMode.RATIO ->
                            stringResource(Res.string.pref_interface_initial_pane_ratio_remember_mode_option_ratio)
                        InitialPaneRatioSource.Remembered.RememberMode.SIZE_DP ->
                            stringResource(Res.string.pref_interface_initial_pane_ratio_remember_mode_option_size_dp)
                    }
                }
            )
        )
}

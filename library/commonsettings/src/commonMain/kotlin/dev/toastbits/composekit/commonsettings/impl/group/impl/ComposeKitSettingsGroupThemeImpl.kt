package dev.toastbits.composekit.commonsettings.impl.group.impl

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Palette
import dev.toastbits.composekit.commonsettings.generated.resources.Res
import dev.toastbits.composekit.commonsettings.generated.resources.pref_theme_current_theme_title
import dev.toastbits.composekit.commonsettings.generated.resources.pref_theme_themes_title
import dev.toastbits.composekit.commonsettings.generated.resources.prefs_group_theme_description
import dev.toastbits.composekit.commonsettings.generated.resources.prefs_group_theme_title
import dev.toastbits.composekit.commonsettings.impl.group.ComposeKitSettingsGroupTheme
import dev.toastbits.composekit.settings.ComposeKitSettingsGroupImpl
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.presentation.ui.component.item.ThemeSelectorSettingsItem
import dev.toastbits.composekit.theme.config.ThemeTypeConfigProviderImpl
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.util.model.KImageVectorResource
import dev.toastbits.kmpresources.library.model.KmpStringResource
import dev.toastbits.kmpresources.library.mapper.toKmpResource

open class ComposeKitSettingsGroupThemeImpl(
    groupKey: String,
    preferences: PlatformSettings
): ComposeKitSettingsGroupImpl(groupKey, preferences), ComposeKitSettingsGroupTheme {
    override val title: KmpStringResource =
        Res.string.prefs_group_theme_title.toKmpResource()

    override val description: KmpStringResource =
        Res.string.prefs_group_theme_description.toKmpResource()

    override val icon: KImageVectorResource =
        KImageVectorResource { Icons.Default.Palette }

    override val CURRENT_THEME: PlatformSettingsProperty<ThemeReference> by serialisableProperty(
        name = Res.string.pref_theme_current_theme_title.toKmpResource(),
        description = null,
        getDefaultValue = { ThemeReference.SystemTheme }
    )

    override val CUSTOM_THEMES: PlatformSettingsProperty<List<NamedTheme>> by serialisableProperty(
        name = Res.string.pref_theme_themes_title.toKmpResource(),
        description = null,
        getDefaultValue = { emptyList() }
    )

    override fun getConfigurationItems(): List<SettingsItem> =
        listOf(
            ThemeSelectorSettingsItem(
                currentThemeProperty = CURRENT_THEME,
                customThemesProperty = CUSTOM_THEMES,
                themeTypeConfigProvider = ThemeTypeConfigProviderImpl
            )
        )
}

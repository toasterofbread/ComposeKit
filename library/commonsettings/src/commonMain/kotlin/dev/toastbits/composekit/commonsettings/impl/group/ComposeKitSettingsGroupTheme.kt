package dev.toastbits.composekit.commonsettings.impl.group

import com.catppuccin.kotlin.Catppuccin
import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference

interface ComposeKitSettingsGroupTheme: ComposeKitSettingsGroup {
    val CURRENT_THEME: PlatformSettingsProperty<ThemeReference>
    val CUSTOM_THEMES: PlatformSettingsProperty<List<NamedTheme>>
}

package dev.toastbits.composekit.commonsettings.impl.group.theme

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import dev.toastbits.composekit.commonsettings.impl.ComposeKitSettings
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.theme.core.AnimatedThemeManager
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.model.ThemeValuesData
import dev.toastbits.composekit.theme.core.provider.ThemeProvider
import dev.toastbits.composekit.theme.core.ui.LocalThemeProvider

open class SettingsThemeManager(
    private val settings: ComposeKitSettings
): AnimatedThemeManager(ThemeValuesData.ofSingleColour(Color.Black)) {
    @Composable
    override fun Update() {
        super.Update()

        val themeProvider: ThemeProvider = LocalThemeProvider.current
        val currentTheme: ThemeReference by settings.Theme.CURRENT_THEME.observe()

        val theme: ThemeValues =
            remember(currentTheme, themeProvider) {
                currentTheme.getTheme(themeProvider)
            }

        LaunchedEffect(theme) {
            setTheme(theme)
        }
    }
}

package dev.toastbits.composekit.commonsettings.impl

import dev.toastbits.composekit.commonsettings.impl.group.ComposeKitSettingsGroupInterface
import dev.toastbits.composekit.commonsettings.impl.group.ComposeKitSettingsGroupTheme
import dev.toastbits.composekit.commonsettings.impl.group.impl.ComposeKitSettingsGroupInterfaceImpl
import dev.toastbits.composekit.commonsettings.impl.group.impl.ComposeKitSettingsGroupThemeImpl
import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import dev.toastbits.composekit.settings.PlatformSettings
import org.jetbrains.compose.resources.StringResource

open class ComposeKitSettingsImpl(
    override val preferences: PlatformSettings,
    localeNameResource: StringResource
): ComposeKitSettings {
    override val Interface: ComposeKitSettingsGroupInterface by lazy {
        ComposeKitSettingsGroupInterfaceImpl("INTERFACE", preferences, localeNameResource)
    }

    override val Theme: ComposeKitSettingsGroupTheme by lazy {
        ComposeKitSettingsGroupThemeImpl("THEME", preferences)
    }

    override val allGroups: List<ComposeKitSettingsGroup>
        get() = listOf(Interface, Theme)
}

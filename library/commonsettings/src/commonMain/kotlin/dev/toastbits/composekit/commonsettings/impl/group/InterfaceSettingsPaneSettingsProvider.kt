package dev.toastbits.composekit.commonsettings.impl.group

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.components.utils.composable.pane.PaneSettingsProvider
import dev.toastbits.composekit.components.utils.composable.pane.model.InitialPaneRatioSource
import dev.toastbits.composekit.components.utils.composable.pane.model.ResizablePaneContainerParams

class InterfaceSettingsPaneSettingsProvider(
    private val interfaceGroup: ComposeKitSettingsGroupInterface
): PaneSettingsProvider {
    override fun getPaneRatioRememberMode(): InitialPaneRatioSource.Remembered.RememberMode =
        interfaceGroup.INITIAL_PANE_RATIO_REMEMBER_MODE.get()

    override fun getInitialPaneRatio(key: String): InitialPaneRatioSource? =
        if (!interfaceGroup.REMEMBER_INITIAL_PANE_RATIOS.get()) null
        else interfaceGroup.REMEMBERED_INITIAL_PANE_RATIOS.get()[key]

    override suspend fun updatePaneRatio(key: String, ratio: InitialPaneRatioSource) {
        if (!interfaceGroup.REMEMBER_INITIAL_PANE_RATIOS.get()) {
            return
        }

        val ratios: MutableMap<String, InitialPaneRatioSource> =
            interfaceGroup.REMEMBERED_INITIAL_PANE_RATIOS.get().toMutableMap()

        if (ratios[key] != ratio) {
            ratios[key] = ratio
            interfaceGroup.REMEMBERED_INITIAL_PANE_RATIOS.set(ratios)
        }
    }

    @Composable
    override fun getResizablePaneContainerParams(default: ResizablePaneContainerParams): ResizablePaneContainerParams? =
        interfaceGroup.getResizablePaneContainerParams(default)
}

package dev.toastbits.composekit.commonsettings.impl

import androidx.compose.runtime.ProvidableCompositionLocal
import androidx.compose.runtime.staticCompositionLocalOf
import dev.toastbits.composekit.commonsettings.impl.group.ComposeKitSettingsGroupInterface
import dev.toastbits.composekit.commonsettings.impl.group.ComposeKitSettingsGroupTheme
import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import dev.toastbits.composekit.settings.PlatformSettings

val LocalComposeKitSettings: ProvidableCompositionLocal<ComposeKitSettings?> =
    staticCompositionLocalOf { null }

interface ComposeKitSettings {
    val preferences: PlatformSettings

    val Interface: ComposeKitSettingsGroupInterface
    val Theme: ComposeKitSettingsGroupTheme

    val allGroups: List<ComposeKitSettingsGroup>
}

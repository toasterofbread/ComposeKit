package dev.toastbits.composekit.commonsettings.impl.group

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.unit.coerceAtLeast
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.utils.composable.pane.model.InitialPaneRatioSource
import dev.toastbits.composekit.components.utils.composable.pane.model.ResizablePaneContainerParams
import dev.toastbits.composekit.components.utils.composable.pane.model.ResizablePaneContainerParamsData
import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.theme.core.model.ComposeKitFont
import dev.toastbits.composekit.util.model.Locale
import dev.toastbits.composekit.util.toFloat

interface ComposeKitSettingsGroupInterface: ComposeKitSettingsGroup {
    val UI_LOCALE: PlatformSettingsProperty<Locale?>

    val FONT: PlatformSettingsProperty<ComposeKitFont>
    val FONT_SCALE: PlatformSettingsProperty<Float>
    val UI_SCALE: PlatformSettingsProperty<Float>

    val SHOW_PANE_RESIZE_HANDLES: PlatformSettingsProperty<Boolean>
    val SHOW_PANE_RESIZE_HANDLES_ON_HOVER: PlatformSettingsProperty<Boolean>
    val ANIMATE_PANE_RESIZE: PlatformSettingsProperty<Boolean>

    val REMEMBER_INITIAL_PANE_RATIOS: PlatformSettingsProperty<Boolean>
    val REMEMBERED_INITIAL_PANE_RATIOS: PlatformSettingsProperty<Map<String, InitialPaneRatioSource>>
    val INITIAL_PANE_RATIO_REMEMBER_MODE: PlatformSettingsProperty<InitialPaneRatioSource.Remembered.RememberMode>
}

@Composable
fun ComposeKitSettingsGroupInterface.getResizablePaneContainerParams(
    default: ResizablePaneContainerParams = ResizablePaneContainerParamsData()
): ResizablePaneContainerParams {
    val showPaneResizeHandles: Boolean by SHOW_PANE_RESIZE_HANDLES.observe()
    val showPaneResizeHandlesOnHover: Boolean by SHOW_PANE_RESIZE_HANDLES_ON_HOVER.observe()
    val animatePaneResize: Boolean by ANIMATE_PANE_RESIZE.observe()

    val dragHandleWidth: Float by animateFloatAsState(showPaneResizeHandles.toFloat())

    return ResizablePaneContainerParamsData(
        dragHandleSize = default.dragHandleSize * dragHandleWidth,
        dragHandlePadding =
            (default.dragHandlePadding * dragHandleWidth).coerceAtLeast(10.dp),
        hoverDragHandleSize =
            if (showPaneResizeHandles || showPaneResizeHandlesOnHover) default.dragHandleSize
            else default.dragHandleSize * dragHandleWidth,
        hoverDragHandlePadding =
            if (showPaneResizeHandles || showPaneResizeHandlesOnHover) default.dragHandlePadding
            else default.dragHandlePadding * dragHandleWidth,
        hoverable = showPaneResizeHandlesOnHover,
        resizeAnimationSpec =
            if (animatePaneResize) ResizablePaneContainerParams.DEFAULT_RESIZE_ANIMATION_SPEC
            else null
    )
}

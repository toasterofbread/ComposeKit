import util.configureAllComposeTargets

plugins {
    id("android-library-conventions")
    id("compose-conventions")
    id("gitlab-publishing-conventions")
}

kotlin {
    configureAllComposeTargets()

    sourceSets {
        commonMain {
            dependencies {
                api(projects.library.settings)
                api(projects.library.settingsitem.domain)
                implementation(projects.library.settingsitem.presentation)
                implementation(projects.library.util)
                implementation(projects.library.utilKt)
                implementation(projects.library.components)
                implementation(projects.library.theme.core)
                implementation(projects.library.theme.config)
                implementation(projects.library.context)
            }
        }
    }
}

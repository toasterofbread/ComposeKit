/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2023 Spooky Games
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package dev.toastbits.composekit.filechooser

import dev.toastbits.composekit.context.PlatformFile
import java.awt.FileDialog
import java.awt.Frame
import java.io.File
import java.io.FilenameFilter
import java.io.IOException
import java.nio.file.Files
import java.util.regex.Pattern

/**
 * Implementation of a [NativeFileChooser] for the Desktop backend of a
 * libGDX application. This implementation uses AWT's [FileDialog].
 *
 *
 *
 * A word of warning: support for the
 * [NativeFileChooserConfiguration.mimeFilter] property of given
 * [NativeFileChooserConfiguration] is experimental and slow at best. Use
 * at your own risk.
 *
 * @see .chooseFile
 * @see NativeFileChooser
 *
 * @see NativeFileChooserConfiguration
 *
 * @see NativeFileChooserCallback
 *
 *
 * @author thorthur
 */
class DesktopFileChooser : NativeFileChooser {
    /*
         * (non-Javadoc)
         *
         * @see NativeFileChooser#chooseFile(NativeFileChooserConfiguration,
         * NativeFileChooserCallback)
         */
    override fun chooseFile(
        configuration: NativeFileChooserConfiguration,
        callback: NativeFileChooserCallback,
    ) {
        chooseFile(configuration, callback, null)
    }

    fun chooseFile(
        configuration: NativeFileChooserConfiguration,
        callback: NativeFileChooserCallback,
        filenameFilter: FilenameFilter?,
    ) {
        NativeFileChooserUtils.checkNotNull(configuration, "configuration")
        NativeFileChooserUtils.checkNotNull(callback, "callback")

        // Create awt Dialog
        val fileDialog = FileDialog(
            null as Frame?,
            if (configuration.title == null) "" else configuration.title,
            if (configuration.intent === NativeFileChooserIntent.SAVE) FileDialog.SAVE else FileDialog.LOAD
        )

        val filter = filenameFilter ?: createFilenameFilter(configuration)

        if (filter != null) fileDialog.filenameFilter = filter

        // Set starting path if any
        if (configuration.directory != null) fileDialog.directory =
            configuration.directory!!.file.absolutePath

        // Present it to the world
        fileDialog.isVisible = true

        val files = fileDialog.files

        if (files == null || files.size == 0) {
            callback.onCancellation()
        } else {
            callback.onFileChosen(PlatformFile(files[0]))
        }
    }

    companion object {
        fun createFilenameFilter(configuration: NativeFileChooserConfiguration): FilenameFilter? {
            var filter: FilenameFilter? = null

            // Add MIME type filter if any
            if (configuration.mimeFilter != null) filter =
                createMimeTypeFilter(configuration.mimeFilter!!)

            // Add name filter if any
            if (configuration.nameFilter != null) {
                if (filter == null) {
                    filter = configuration.nameFilter
                } else {
                    // Combine filters!
                    val mime: FilenameFilter = filter
                    filter = FilenameFilter { dir, name ->
                        mime.accept(
                            dir,
                            name
                        ) && configuration.nameFilter!!.accept(dir, name)
                    }
                }
            }
            return filter
        }

        private fun createMimeTypeFilter(mimeType: String): FilenameFilter {
            return object : FilenameFilter {
                val mimePattern: Pattern = NativeFileChooserUtils.mimePattern(mimeType)

                @Suppress("NewApi")
                override fun accept(dir: File, name: String): Boolean {
                    // Getting a Mime type is not warranted (and may be slow!)

                    try {
                        val mime = Files.probeContentType(File(dir, name).toPath())

                        if (mime != null) {
                            // Try to get a match on Mime type
                            // That's quite faulty I know!
                            return mimePattern.matcher(mime).matches()
                        }
                    } catch (ignored: IOException) {
                    }

                    // Accept by default, in case mime probing doesn't work
                    return true
                }
            }
        }
    }
}

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2023 Spooky Games
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package dev.toastbits.composekit.filechooser

import dev.toastbits.composekit.context.PlatformFile

/**
 * The `NativeFileChooser` interface. Put this one into your core project,
 * let it sink through via the initializer in your platform-specific projects
 * and start rolling!
 *
 *
 * Be careful, not every parameter of a [NativeFileChooserConfiguration]
 * may be functional for every implementation of `NativeFileChooser`.
 *
 * @see NativeFileChooser.chooseFile
 * @see NativeFileChooserConfiguration
 *
 * @see NativeFileChooserCallback
 *
 *
 * @author thorthur
 */
interface NativeFileChooserCallback {
    /**
     * Handle the user-chosen [PlatformFile].
     *
     * @param file
     * PlatformFile chosen by user
     */
    fun onFileChosen(file: PlatformFile)

    /**
     * Handle cancellation from the user.
     *
     *
     * In this case, [.onFileChosen] will not be called.
     */
    fun onCancellation()

    /**
     * Handle exception throw during file choosing.
     *
     *
     * On Android you should be prepared to handle:
     *
     *  *
     * [IOException] if an error occurred while copying chosen resource to
     * a temporary [FileHandle].
     *
     *  *
     * `ActivityNotFoundException` if no file manager could be found on
     * the device.
     *
     *
     *
     * In this case, [.onFileChosen] will not be called.
     *
     * @param exception
     * Exception throw during file choosing
     */
    fun onError(exception: java.lang.Exception?)
}

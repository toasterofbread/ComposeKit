import util.KmpTarget
import util.configureKmpTargets

plugins {
    id("compose-conventions")
    id("gitlab-publishing-conventions")
}

kotlin {
    configureKmpTargets(KmpTarget.JVM())

    sourceSets {
        commonMain {
            dependencies {
                api(projects.library.file)
            }
        }

        val jvmMain by getting {
            dependencies {
                implementation(libs.tika.core)

                val osName: String = System.getProperty("os.name")
                val lwjglOs: String = when {
                    osName == "Linux" -> "linux"
                    osName.startsWith("Win") -> "windows"
                    osName == "Mac OS X" -> "macos"
                    else -> throw Error("Unknown OS '$osName'")
                }

                val lwjglVersion: String = libs.versions.lwjgl.get()
                for (lwjgl_library in listOf("lwjgl", "lwjgl-nfd")) {
                    implementation("org.lwjgl:$lwjgl_library:$lwjglVersion")
                    runtimeOnly("org.lwjgl:$lwjgl_library:$lwjglVersion:natives-$lwjglOs")
                }
            }
        }
    }
}

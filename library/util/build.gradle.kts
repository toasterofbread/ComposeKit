import util.configureAllComposeTargets

plugins {
    id("compose-conventions")
    id("android-library-conventions")
    id("gitlab-publishing-conventions")
}

kotlin {
    configureAllComposeTargets()

    sourceSets {
        commonMain {
            dependencies {
                api(projects.library.utilKt)
            }
        }
    }
}

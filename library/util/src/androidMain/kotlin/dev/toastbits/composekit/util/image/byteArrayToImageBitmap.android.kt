package dev.toastbits.composekit.util.image

import android.graphics.BitmapFactory
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap

actual fun ByteArray.toImageBitmap(range: IntRange): ImageBitmap =
    BitmapFactory.decodeByteArray(this, range.first, range.last - range.first + 1).asImageBitmap()

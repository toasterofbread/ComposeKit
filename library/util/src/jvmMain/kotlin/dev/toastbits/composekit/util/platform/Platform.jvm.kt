package dev.toastbits.composekit.util.platform

import java.net.InetAddress

actual fun getPlatform(): Platform =
    Platform.DESKTOP

actual fun getPlatformOSName(): String =
    System.getProperty("os.name")

actual fun getPlatformHostName(): String? =
    try {
        InetAddress.getLocalHost().hostName
    }
    catch (_: Throwable) {
        null
    }

package dev.toastbits.composekit.util.image

import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import org.jetbrains.skia.Image

actual fun ByteArray.toImageBitmap(range: IntRange): ImageBitmap =
    Image.makeFromEncoded(this.sliceArray(range)).toComposeImageBitmap()

package dev.toastbits.composekit.util.platform

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.launch

@Composable
actual fun <T> rememberResourceState(
    key1: Any?,
    getDefault: () -> T,
    block: suspend () -> T
): State<T> {
    val scope: CoroutineScope = rememberCoroutineScope()
    return remember(key1) {
        val state: MutableState<T> = mutableStateOf(getDefault())
        scope.launch(start = CoroutineStart.UNDISPATCHED) {
            state.value = block()
        }
        return@remember state
    }
}

@file:Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
package dev.toastbits.composekit.util.platform

actual fun assert(condition: Boolean) {
    kotlin.assert(condition)
}
actual fun assert(condition: Boolean, lazyMessage: () -> String) {
    kotlin.assert(condition, lazyMessage)
}

actual inline fun lazyAssert(
    noinline getMessage: (() -> String)?,
    condition: () -> Boolean
) {
    if (_Assertions.ENABLED && !condition()) {
        throw AssertionError(getMessage?.invoke() ?: "Assertion failed")
    }
}

actual fun getEnv(name: String): String? = System.getenv(name)

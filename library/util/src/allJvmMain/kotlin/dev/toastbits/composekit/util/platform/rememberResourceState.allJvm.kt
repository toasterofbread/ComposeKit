package dev.toastbits.composekit.util.platform

import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import kotlinx.coroutines.runBlocking

@Composable
actual fun <T> rememberResourceState(
    key1: Any?,
    getDefault: () -> T,
    block: suspend () -> T
): State<T> =
    remember(key1) {
        mutableStateOf(runBlocking { block() })
    }

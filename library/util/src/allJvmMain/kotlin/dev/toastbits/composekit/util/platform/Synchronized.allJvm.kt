package dev.toastbits.composekit.util.platform

import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

@OptIn(kotlin.contracts.ExperimentalContracts::class)
actual inline fun <R> synchronizedImpl(lock: Any, block: () -> R): R {
    contract {
        callsInPlace(block, InvocationKind.EXACTLY_ONCE)
    }
    return kotlin.synchronized(lock, block)
}

package dev.toastbits.composekit.util.platform

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

@OptIn(ExperimentalContracts::class)
inline fun <R> synchronized(lock: Any, block: () -> R): R {
    contract {
        callsInPlace(block, InvocationKind.EXACTLY_ONCE)
    }
    return synchronizedImpl(lock, block)
}

expect inline fun <R> synchronizedImpl(lock: Any, block: () -> R): R

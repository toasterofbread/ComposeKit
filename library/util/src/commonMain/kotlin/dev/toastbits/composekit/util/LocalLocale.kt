@file:Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
package dev.toastbits.composekit.util

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.util.model.Locale
import org.jetbrains.compose.resources.InternalResourceApi
import org.jetbrains.compose.resources.LocalComposeEnvironment

@OptIn(InternalResourceApi::class)
object LocalLocale {
    val current: Locale
        @Composable get() =
            with(LocalComposeEnvironment.current.rememberEnvironment()) {
                Locale(language.language, region.region)
            }
}

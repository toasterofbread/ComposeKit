@file:Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
package dev.toastbits.composekit.util

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.intl.Locale
import androidx.compose.ui.text.intl.platformLocaleDelegate
import androidx.compose.ui.unit.Density
import org.jetbrains.compose.resources.ComposeEnvironment
import org.jetbrains.compose.resources.DensityQualifier
import org.jetbrains.compose.resources.InternalResourceApi
import org.jetbrains.compose.resources.LanguageQualifier
import org.jetbrains.compose.resources.LocalComposeEnvironment
import org.jetbrains.compose.resources.RegionQualifier
import org.jetbrains.compose.resources.ResourceEnvironment
import org.jetbrains.compose.resources.ThemeQualifier
import dev.toastbits.composekit.util.model.Locale as ComposeKitLocale

class LocalisedComposeEnvironment(
    private val getLocale: () -> ComposeKitLocale?
): ComposeEnvironment {
    @Composable
    @OptIn(InternalResourceApi::class)
    override fun rememberEnvironment(): ResourceEnvironment {
        val locale: ComposeKitLocale? = getLocale()
        val composeTheme: Boolean = isSystemInDarkTheme()
        val composeDensity: Density = LocalDensity.current

        return remember(locale, composeTheme, composeDensity) {
            val composeLocale: Locale =
                if (locale != null) Locale(platformLocaleDelegate.parseLanguageTag(locale.toTag()))
                else Locale.current

            return@remember ResourceEnvironment(
                LanguageQualifier(composeLocale.language),
                RegionQualifier(composeLocale.region),
                ThemeQualifier.selectByValue(composeTheme),
                DensityQualifier.selectByDensity(composeDensity.density)
            )
        }
    }

    @Composable
    fun WithEnvironment(content: @Composable () -> Unit) {
        CompositionLocalProvider(LocalComposeEnvironment provides this, content)
    }
}
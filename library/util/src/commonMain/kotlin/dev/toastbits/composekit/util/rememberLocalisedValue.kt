package dev.toastbits.composekit.util

import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import dev.toastbits.composekit.util.model.Locale
import dev.toastbits.composekit.util.platform.rememberResourceState

@Composable
fun <T> rememberLocalisedValue(
    getValue: suspend (locale: Locale) -> T,
    getDefault: () -> T
): State<T> {
    val locale: Locale = LocalLocale.current
    return rememberResourceState(
        getValue,
        getDefault
    ) {
        getValue(locale)
    }
}

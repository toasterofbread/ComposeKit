package dev.toastbits.composekit.util.image

import androidx.compose.ui.graphics.ImageBitmap

expect fun ByteArray.toImageBitmap(range: IntRange): ImageBitmap

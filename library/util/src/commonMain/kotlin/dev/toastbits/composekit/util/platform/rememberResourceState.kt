package dev.toastbits.composekit.util.platform

import androidx.compose.runtime.Composable
import androidx.compose.runtime.State

@Composable
expect fun <T> rememberResourceState(
    key1: Any?,
    getDefault: () -> T,
    block: suspend () -> T
): State<T>

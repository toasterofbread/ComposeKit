package dev.toastbits.composekit.util.state

import androidx.compose.runtime.MutableState

fun <I, O> MutableState<I>.map(
    from: (I) -> O,
    to: (O) -> I
): MutableState<O> =
    object : MutableState<O> {
        override var value: O
            get() = from(this@map.value)
            set(value) { this@map.value = to(value) }

        override fun component1(): O = value
        override fun component2(): (O) -> Unit = { this@map.value = to(it) }
    }

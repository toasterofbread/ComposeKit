package dev.toastbits.composekit.util

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.compositeOver
import androidx.compose.ui.graphics.luminance
import androidx.compose.ui.graphics.toArgb
import kotlin.math.absoluteValue
import kotlin.random.Random

fun Color.Companion.red(argb: Int): Int = argb shr 16 and 0xFF
fun Color.Companion.green(argb: Int): Int = argb shr 8 and 0xFF
fun Color.Companion.blue(argb: Int): Int = argb and 0xFF

fun Color.Companion.random(randomiseAlpha: Boolean = false, rnd: Random = Random): Color {
    return Color(
        rnd.nextInt(256),
        rnd.nextInt(256),
        rnd.nextInt(256),
        if (randomiseAlpha) rnd.nextInt(256) else 255
    )
}

fun Color.blendWith(other: Color, ourRatio: Float = 0.075f): Color {
    return copy(alpha = alpha * ourRatio).compositeOver(other)
}

fun Color.offsetRGB(offset: Float, clip: Boolean = true): Color {
    var fOffset = offset
    if (clip) {
        for (value in listOf(red, green, blue)) {
            val final = value + fOffset
            if (final > 1.0) {
                fOffset = 1f - value
            }
            else if (final < 0.0) {
                fOffset = -value
            }
        }
    }

    return copy(
        red = (red + fOffset).coerceIn(0f..1f),
        green = (green + fOffset).coerceIn(0f..1f),
        blue = (blue + fOffset).coerceIn(0f..1f),
    )
}

fun Color.amplify(by: Float): Color {
    return offsetRGB(if (isDark()) -by else by, false)
}

fun Color.amplifyPercent(byPercent: Float, oppositePercent: Float = byPercent, allowReverse: Boolean = true): Color {
    val by = if (isDark()) byPercent else -byPercent
    val ret = if (by < 0f)
        Color(
            red * (1f + by),
            green * (1f + by),
            blue * (1f + by),
            alpha
        )
    else
        Color(
            red + ((1f - red) * by),
            green + ((1f - green) * by),
            blue + ((1f - blue) * by),
            alpha
        )

    if (allowReverse && compare(ret) > 0.975f) {
        return amplifyPercent(-oppositePercent, allowReverse = false)
    }

    return ret
}

fun Color.compare(against: Color): Float {
    return 1f - (((red - against.red).absoluteValue + (green - against.green).absoluteValue + (blue - against.blue).absoluteValue) / 3f)
}

fun Color.compareLuminance(against: Color): Float {
    return (luminance() - against.luminance()).absoluteValue
}

fun ImageBitmap.getThemeColour(): Color? {
    val pixelCount = width * height

    val pixels = IntArray(pixelCount)
    readPixels(pixels, 0, 0, width, height)

    var lightCount = 0
    var lightR = 0
    var lightG = 0
    var lightB = 0

    var darkR = 0
    var darkG = 0
    var darkB = 0

    for (x in 0 until width) {
        for (y in 0 until height) {
            val colour = pixels[x + y * width]

            val r = (colour shr 16 and 0xFF) / 255f
            val g = (colour shr 8 and 0xFF) / 255f
            val b = (colour and 0xFF) / 255f

            if ((0.299 * r) + (0.587 * g) + (0.114 * b) >= 0.5) {
                lightCount += 1
                lightR += (r * 255).toInt()
                lightG += (g * 255).toInt()
                lightB += (b * 255).toInt()
            }
            else {
                darkR += (r * 255).toInt()
                darkG += (g * 255).toInt()
                darkB += (b * 255).toInt()
            }
        }
    }

    val darkCount = pixelCount - lightCount
    if (darkCount == 0 && lightCount == 0) {
        return null
    }

    if (lightCount > darkCount) {
        return Color(
            lightR / lightCount,
            lightG / lightCount,
            lightB / lightCount
        )
    }
    else {
        return Color(
            darkR / darkCount,
            darkG / darkCount,
            darkB / darkCount
        )
    }
}

fun Color.isDark(): Boolean =
    luminance() < 0.2

fun Color.contrastAgainst(against: Color, by: Float = 0.5f, clip: Boolean = true): Color =
    offsetRGB(if (against.isDark()) by else -by, clip = clip)

fun Color.getContrasted(keepAlpha: Boolean = false): Color {
    val base: Color =
        if (isDark()) Color.White
        else Color.Black
    return if (keepAlpha) base.copy(alpha = alpha) else base
}

fun Color.getNeutral(): Color {
    if (isDark())
        return Color.Black
    else
        return Color.White
}

fun List<Color>.sorted(descending: Boolean = false): List<Color> {
    return if (descending) sortedByDescending { it.luminance() }
            else sortedBy { it.luminance() }
}

fun Color.generatePalette(size: Int, variance: Float = 0.2f): List<Color> {
    val ret: MutableList<Color> = mutableListOf()

    fun isColourValid(colour: Color): Boolean {
        if (ret.any { it.compare(colour) > 0.5f }) {
            return false
        }

        if (colour.compare(Color.Black) > 0.8f || colour.compare(Color.White) > 0.8f) {
            return false
        }

        return true
    }

    for (i in 0 until size) {
        var tries = 5
        while (tries-- > 0) {
            val colour = offsetRGB(Random.nextFloat() * variance * (if (Random.nextBoolean()) 1f else -1f), false)
            if (isColourValid(colour)) {
                ret.add(colour)
                break
            }
        }
    }

    return List(size) {
        offsetRGB(Random.nextFloat() * variance * (if (Random.nextBoolean()) 1f else -1f), false)
    }
}

fun Color.Companion.generatePalette(size: Int): List<Color> =
    List(size) {
        random(rnd = Random)
    }

fun Color.Companion.fromHexString(string: String): Color =
    Color(string.removePrefix("#").toLong(16) or 0x00000000FF000000)

fun Color.toRGBHexString(): String =
    '#' + (0xFFFFFF and toArgb()).toHexString().takeLast(6)

fun List<Color>.sortedByHue(): List<Color> =
    sortedBy { it.getHSV().first }

// https://www.geeksforgeeks.org/program-change-rgb-color-model-hsv-color-model/
fun Color.getHSV(): Triple<Float, Float, Float> {
    val max: Float = maxOf(red, green, blue)
    val min: Float = minOf(red, green, blue)

    if (max == min) {
        return Triple(360f, 0f, max)
    }

    val delta: Float = max - min

    val h: Float =
        when {
            max == red -> ((green - blue) / delta) % 6
            max == green -> ((blue - red) / delta) + 2
            else -> ((red - green) / delta) + 4
        } * 60

    val s: Float = if (max == 0f) 0f else delta / max
    val v: Float = max

    return Triple(h, s, v)
}

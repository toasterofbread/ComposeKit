package dev.toastbits.composekit.util.composable

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.AnimationVector
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.calculateEndPadding
import androidx.compose.foundation.layout.calculateStartPadding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.PointerEvent
import androidx.compose.ui.input.pointer.PointerEventPass
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.LayoutDirection
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.isActive
import kotlin.reflect.KProperty
import androidx.compose.runtime.getValue as composeGetValue

fun Modifier.thenIf(condition: Boolean, modifier: Modifier): Modifier = if (condition) then(modifier) else this

inline fun Modifier.thenIf(condition: Boolean, action: Modifier.() -> Modifier): Modifier = if (condition) action() else this
inline fun Modifier.thenIf(condition: Boolean, elseAction: Modifier.() -> Modifier, action: Modifier.() -> Modifier): Modifier = if (condition) action() else elseAction()

operator fun IntSize.times(other: Float): IntSize =
    IntSize(width = (width * other).toInt(), height = (height * other).toInt())

@Composable
fun PaddingValues.copy(
    start: Dp? = null,
    top: Dp? = null,
    end: Dp? = null,
    bottom: Dp? = null,
): PaddingValues {
    return PaddingValues(
        start ?: this.start,
        top ?: this.top,
        end ?: this.end,
        bottom ?: this.bottom
    )
}

fun PaddingValues.copy(
    layout_direction: LayoutDirection,
    start: Dp? = null,
    top: Dp? = null,
    end: Dp? = null,
    bottom: Dp? = null,
): PaddingValues {
    return PaddingValues(
        start ?: calculateStartPadding(layout_direction),
        top ?: calculateTopPadding(),
        end ?: calculateEndPadding(layout_direction),
        bottom ?: calculateBottomPadding()
    )
}

@Composable
operator fun PaddingValues.plus(other: PaddingValues): PaddingValues =
    PaddingValues(
        start = calculateStartPadding(LocalLayoutDirection.current) + other.calculateStartPadding(LocalLayoutDirection.current),
        top = calculateTopPadding() + other.calculateTopPadding(),
        end = calculateEndPadding(LocalLayoutDirection.current) + other.calculateEndPadding(LocalLayoutDirection.current),
        bottom = calculateBottomPadding() + other.calculateBottomPadding()
    )

val PaddingValues.top: Dp @Composable get() = calculateTopPadding()
val PaddingValues.bottom: Dp @Composable get() = calculateBottomPadding()
val PaddingValues.start: Dp @Composable get() = calculateStartPadding(LocalLayoutDirection.current)
val PaddingValues.end: Dp @Composable get() = calculateEndPadding(LocalLayoutDirection.current)

operator fun <T> State<T>?.getValue(t: T?, property: KProperty<*>): T? {
    return this?.composeGetValue(t, property)
}

suspend fun <T, V : AnimationVector> Animatable<T, V>.snapOrAnimateTo(
    targetValue: T,
    snap: Boolean,
    @Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
    animationSpec: AnimationSpec<T>? = defaultSpringSpec,
    initialVelocity: T = velocity,
    block: (Animatable<T, V>.() -> Unit)? = null
) {
    if (snap || animationSpec == null) {
        snapTo(targetValue)
    }
    else {
        animateTo(targetValue, animationSpec, initialVelocity, block)
    }
}

fun Modifier.blockGestures(): Modifier =
    pointerInput(Unit) {
        while (currentCoroutineContext().isActive) {
            awaitPointerEventScope {
                val event: PointerEvent = awaitPointerEvent(PointerEventPass.Initial)
                for (change in event.changes) {
                    change.consume()
                }
            }
        }
    }

package dev.toastbits.composekit.util.composable

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.AnimationVector1D
import androidx.compose.animation.core.spring
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshotFlow
import kotlinx.coroutines.flow.collectLatest

@Composable
fun animateFloatAsState(
    animationSpec: AnimationSpec<Float> = spring(),
    shouldSnap: () -> Boolean = { false },
    getTargetValue: () -> Float
): State<Float> {
    val animation: Animatable<Float, AnimationVector1D> = remember { Animatable(getTargetValue()) }
    LaunchedEffect(getTargetValue) {
        snapshotFlow(getTargetValue)
            .collectLatest {
                animation.snapOrAnimateTo(
                    it,
                    animationSpec = animationSpec,
                    snap = shouldSnap()
                )
            }
    }

    return remember { derivedStateOf {
        animation.value
    } }
}

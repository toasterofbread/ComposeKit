package dev.toastbits.composekit.util.platform

expect fun getPlatformForbiddenFilenameCharacters(): String

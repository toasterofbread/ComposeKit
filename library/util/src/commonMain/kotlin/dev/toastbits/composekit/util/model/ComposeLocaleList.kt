@file:Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
package dev.toastbits.composekit.util.model

import org.jetbrains.compose.resources.InternalResourceApi
import org.jetbrains.compose.resources.LanguageQualifier
import org.jetbrains.compose.resources.RegionQualifier
import org.jetbrains.compose.resources.ResourceEnvironment
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.getString
import org.jetbrains.compose.resources.getSystemResourceEnvironment

@OptIn(InternalResourceApi::class)
data class ComposeLocaleList(private val localeNameResource: StringResource): LocaleList {
    override suspend fun getLocales(): List<Locale> =
        localeNameResource.items.mapNotNull { locale ->
            if (locale.qualifiers.isEmpty()) {
                return@mapNotNull null
            }

            var language: String? = null
            var region: String? = null

            for (qualifier in locale.qualifiers) {
                when (qualifier) {
                    is LanguageQualifier -> {
                        language = qualifier.language
                    }

                    is RegionQualifier -> {
                        region = qualifier.region
                    }
                }
            }

            return@mapNotNull Locale(language!!, region)
        }

    override suspend fun getReadableLocaleName(locale: Locale): String? =
        try {
            getString(getResourceEnvironment(locale), localeNameResource)
        }
        catch (_: IllegalArgumentException) {
            null
        }

    private fun getResourceEnvironment(locale: Locale): ResourceEnvironment {
        val system_environment: ResourceEnvironment = getSystemResourceEnvironment()
        return ResourceEnvironment(
            language = LanguageQualifier(locale.language),
            region = RegionQualifier(locale.region ?: ""),
            theme = system_environment.theme,
            density = system_environment.density
        )
    }
}

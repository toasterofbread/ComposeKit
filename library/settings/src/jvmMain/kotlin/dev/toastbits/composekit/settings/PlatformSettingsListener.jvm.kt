package dev.toastbits.composekit.settings

actual fun interface PlatformSettingsListener {
    actual fun onChanged(key: String)
}

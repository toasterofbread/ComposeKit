package dev.toastbits.composekit.settings

import dev.toastbits.composekit.context.PlatformFile
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsEditor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import java.io.File

actual class PlatformSettingsImpl private constructor(
    private val file: File,
    initialData: MutableMap<String, JsonElement>,
    json: Json
): PlatformSettingsJson(
    PlatformFile(file),
    Dispatchers.IO,
    initialData,
    json
), PlatformSettings {
    actual companion object {
        private var instance: PlatformSettingsImpl? = null

        suspend fun getInstance(
            file: File,
            json: Json = createDefaultJson()
        ): PlatformSettings {
            if (instance == null) {
                val initialData: MutableMap<String, JsonElement> =
                    withContext(Dispatchers.IO) {
                        PlatformSettingsJson.loadData(PlatformFile(file), json)
                    }

                instance = PlatformSettingsImpl(file, initialData, json)
            }
            check(instance!!.file == file)
            return instance!!
        }
    }

    actual inner class EditorImpl(private val data: MutableMap<String, Any>, private val changed: MutableSet<String>): PlatformSettingsEditor {
        actual override val json: Json get() = this@PlatformSettingsImpl.json

        actual override fun putString(key: String, value: String): PlatformSettingsEditor {
            data[key] = value
            changed.add(key)
            return this
        }

        actual override fun putStringSet(
            key: String,
            values: Set<String>,
        ): PlatformSettingsEditor {
            data[key] = values
            changed.add(key)
            return this
        }

        actual override fun putInt(key: String, value: Int): PlatformSettingsEditor {
            data[key] = value
            changed.add(key)
            return this
        }

        actual override fun putLong(key: String, value: Long): PlatformSettingsEditor {
            data[key] = value
            changed.add(key)
            return this
        }

        actual override fun putFloat(key: String, value: Float): PlatformSettingsEditor {
            data[key] = value
            changed.add(key)
            return this
        }

        actual override fun putBoolean(key: String, value: Boolean): PlatformSettingsEditor {
            data[key] = value
            changed.add(key)
            return this
        }

        actual override fun <T> putSerialisable(key: String, value: T, serialiser: KSerializer<T>, json: Json): PlatformSettingsEditor {
            data[key] = json.encodeToJsonElement(serialiser, value)
            changed.add(key)
            return this
        }

        actual override fun remove(key: String): PlatformSettingsEditor {
            data.remove(key)
            return this
        }

        actual override fun clear(): PlatformSettingsEditor {
            changed.addAll(data.keys)
            data.clear()
            return this
        }
    }
}

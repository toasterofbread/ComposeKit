package dev.toastbits.composekit.settings

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

interface ComposeKitSettingsGroupWithCustomPreview {
    @Composable
    fun PreviewContent(modifier: Modifier, onSelected: () -> Unit)
}

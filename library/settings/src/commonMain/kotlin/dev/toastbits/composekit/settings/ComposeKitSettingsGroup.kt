package dev.toastbits.composekit.settings

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.util.model.KImageVectorResource
import dev.toastbits.kmpresources.library.model.KmpStringResource

interface ComposeKitSettingsGroup {
    val groupKey: String
    fun getAllProperties(): List<PlatformSettingsProperty<*>>

    val title: KmpStringResource
    val description: KmpStringResource
    val icon: KImageVectorResource

    fun getConfigurationItems(): List<SettingsItem>
}

package dev.toastbits.composekit.settings

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsEditor
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonBuilder

expect class PlatformSettingsImpl: PlatformSettings {
    override val json: Json
    override val readableStoragePath: String?
    override val storageUri: String?

    override fun addListener(listener: PlatformSettingsListener): PlatformSettingsListener
    override fun removeListener(listener: PlatformSettingsListener)

    override fun getString(key: String, default_value: String?): String?
    override fun getStringSet(key: String, default_values: Set<String>?): Set<String>?
    override fun getInt(key: String, default_value: Int?): Int?
    override fun getLong(key: String, default_value: Long?): Long?
    override fun getFloat(key: String, default_value: Float?): Float?
    override fun getBoolean(key: String, default_value: Boolean?): Boolean?
    override fun <T> getSerialisable(key: String, default_value: T, serialiser: KSerializer<T>, json: Json): T
    override operator fun contains(key: String): Boolean

    override suspend fun edit(action: suspend PlatformSettingsEditor.() -> Unit)

    companion object {}

    inner class EditorImpl: PlatformSettingsEditor {
        override val json: Json

        override fun putString(key: String, value: String): PlatformSettingsEditor
        override fun putStringSet(key: String, values: Set<String>): PlatformSettingsEditor
        override fun putInt(key: String, value: Int): PlatformSettingsEditor
        override fun putLong(key: String, value: Long): PlatformSettingsEditor
        override fun putFloat(key: String, value: Float): PlatformSettingsEditor
        override fun putBoolean(key: String, value: Boolean): PlatformSettingsEditor
        override fun <T> putSerialisable(key: String, value: T, serialiser: KSerializer<T>, json: Json): PlatformSettingsEditor
        override fun remove(key: String): PlatformSettingsEditor
        override fun clear(): PlatformSettingsEditor
    }
}

fun PlatformSettingsImpl.Companion.createDefaultJson(configure: JsonBuilder.() -> Unit = {}): Json =
    Json {
        useArrayPolymorphism = true
    }

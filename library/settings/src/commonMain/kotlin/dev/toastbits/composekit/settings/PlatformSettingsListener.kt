package dev.toastbits.composekit.settings

expect fun interface PlatformSettingsListener {
    fun onChanged(key: String)
}

package dev.toastbits.composekit.settings

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsEditor
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json

interface PlatformSettings {
    val json: Json
    val readableStoragePath: String?
    val storageUri: String?

    fun addListener(listener: PlatformSettingsListener): PlatformSettingsListener
    fun removeListener(listener: PlatformSettingsListener)

    fun getString(key: String, default_value: String?): String?
    fun getStringSet(key: String, default_values: Set<String>?): Set<String>?
    fun getInt(key: String, default_value: Int?): Int?
    fun getLong(key: String, default_value: Long?): Long?
    fun getFloat(key: String, default_value: Float?): Float?
    fun getBoolean(key: String, default_value: Boolean?): Boolean?
    fun <T> getSerialisable(key: String, default_value: T, serialiser: KSerializer<T>, json: Json = this.json): T
    operator fun contains(key: String): Boolean

    suspend fun edit(action: suspend PlatformSettingsEditor.() -> Unit)
}

package dev.toastbits.composekit.settings.ui.screen.edit.action

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.FolderOpen
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.settings.generated.resources.Res
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_action_import_file
import dev.toastbits.composekit.settings.PlatformSettings
import org.jetbrains.compose.resources.stringResource

internal object SettingsEditActionImportFile: IconButtonSettingsEditAction() {
    override val readableName: String
        @Composable
        get() = stringResource(Res.string.preferences_edit_action_import_file)

    override val icon: ImageVector
        @Composable
        get() = Icons.Default.FolderOpen

    override suspend fun execute(
        input: String,
        context: PlatformContext,
        settings: PlatformSettings
    ): String =
        context.promptUserForFileContent(setOf("application/json", "text/plain")) ?: input
}

package dev.toastbits.composekit.settings.ui.screen.edit.action

import androidx.compose.material3.FilledIconButton
import androidx.compose.material3.Icon
import androidx.compose.material3.PlainTooltip
import androidx.compose.material3.Text
import androidx.compose.material3.TooltipBox
import androidx.compose.material3.TooltipDefaults
import androidx.compose.material3.rememberTooltipState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import dev.toastbits.composekit.components.LocalContext
import dev.toastbits.composekit.components.utils.composable.BaseLoadActionButton
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settings.handler.PlatformSettingsImporter
import dev.toastbits.composekit.settings.model.PlatformSettingsExportData

internal abstract class IconButtonSettingsEditAction: SettingsEditAction {
    abstract val readableName: String
        @Composable get

    abstract val icon: ImageVector
        @Composable get

    open val tooltip: String?
        @Composable get() = null

    abstract suspend fun execute(
        input: String,
        context: PlatformContext,
        settings: PlatformSettings
    ): String?

    @Composable
    final override fun Content(
        currentInput: String,
        currentInputParseResult: Result<PlatformSettingsExportData>,
        enabled: Boolean,
        settings: PlatformSettings,
        updateInput: (String) -> Unit,
        updateConfig: (PlatformSettingsImporter.ImportConfig) -> Unit,
        modifier: Modifier
    ) {
        val context: PlatformContext = LocalContext.current
        var executing: Boolean by remember { mutableStateOf(false) }

        TooltipBox(
            TooltipDefaults.rememberPlainTooltipPositionProvider(),
            tooltip = {
                PlainTooltip {
                    Text(tooltip.orEmpty())
                }
            },
            state = rememberTooltipState(),
            enableUserInput = tooltip != null && !executing
        ) {
            BaseLoadActionButton(
                performLoad = {
                    executing = true
                    val newInput: String? =
                        execute(
                            currentInput,
                            context,
                            settings
                        )

                    if (newInput != null) {
                        updateInput(newInput)
                    }

                    executing = false
                },
                modifier = modifier
            ) { contentModifier, onClick ->
                FilledIconButton(
                    onClick = { onClick(false) },
                    contentModifier,
                    enabled = enabled
                ) {
                    Icon(icon, readableName)
                }
            }
        }
    }
}

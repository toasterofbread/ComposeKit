package dev.toastbits.composekit.settings.ui.screen.edit

import androidx.compose.animation.animateContentSize
import androidx.compose.animation.expandIn
import androidx.compose.animation.shrinkOut
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.input.TextFieldState
import androidx.compose.foundation.text.input.clearText
import androidx.compose.foundation.text.input.setTextAndPlaceCursorAtEnd
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.OpenInNew
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.LocalContext
import dev.toastbits.composekit.components.platform.composable.ScrollBarLazyColumn
import dev.toastbits.composekit.components.utils.composable.LoadActionButton
import dev.toastbits.composekit.components.utils.composable.StickyWidthRow
import dev.toastbits.composekit.components.utils.composable.animatedvisibility.NullableValueAnimatedVisibility
import dev.toastbits.composekit.components.utils.composable.crossfade.SkippableCrossfade
import dev.toastbits.composekit.components.utils.composable.crossfade.SkippableCrossfadeTransition
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settings.generated.resources.Res
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_button_apply_overlay
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_button_apply_overwrite_all
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_screen_button_open_file
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_screen_message_input_is_blank
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_screen_message_input_is_valid
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_screen_message_saved
import dev.toastbits.composekit.settings.generated.resources.`preferences_edit_screen_message_stored_at_$path`
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_screen_title
import dev.toastbits.composekit.settings.handler.PlatformSettingsExporter
import dev.toastbits.composekit.settings.handler.PlatformSettingsImporter
import dev.toastbits.composekit.settings.model.PlatformSettingsExportData
import dev.toastbits.composekit.settings.ui.screen.edit.action.SettingsEditAction
import dev.toastbits.composekit.settings.ui.screen.edit.action.SettingsEditActionApplyConfig
import dev.toastbits.composekit.settings.ui.screen.edit.action.SettingsEditActionCopy
import dev.toastbits.composekit.settings.ui.screen.edit.action.SettingsEditActionExportFile
import dev.toastbits.composekit.settings.ui.screen.edit.action.SettingsEditActionFilter
import dev.toastbits.composekit.settings.ui.screen.edit.action.SettingsEditActionImportFile
import dev.toastbits.composekit.settings.ui.screen.edit.action.SettingsEditActionPaste
import dev.toastbits.composekit.settings.ui.screen.edit.action.SettingsEditActionReset
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.util.composable.OnChangedEffect
import dev.toastbits.composekit.util.composable.end
import dev.toastbits.composekit.util.composable.start
import dev.toastbits.kmpresources.library.mapper.toKmpResource
import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.serialization.encodeToString
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.stringResource
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

private const val SAVED_MESSAGE_SHOW_DURATION_MS: Long = 1000

data class PlatformSettingsEditScreen(
    private val importer: PlatformSettingsImporter,
    private val exporter: PlatformSettingsExporter,
    private val settings: PlatformSettings,
    private val groups: List<ComposeKitSettingsGroup>
): Screen {
    private val textFieldState: TextFieldState = TextFieldState("")
    private var inputParseResult: Result<PlatformSettingsExportData> by mutableStateOf(Result.failure(IllegalStateException()))
    private var inputProcessed: Boolean by mutableStateOf(true)
    private var skipInputProcessDelay: Boolean by mutableStateOf(false)
    private var importConfig: PlatformSettingsImporter.ImportConfig by mutableStateOf(PlatformSettingsImporter.ImportConfig())

    override val title: KmpStringResource =
        Res.string.preferences_edit_screen_title.toKmpResource()

    override fun CoroutineScope.beforeOpen(): Job = launch {
        val exportData: PlatformSettingsExportData = exporter.exportPreferences(groups, false)
        val exportDataJson: String = settings.json.encodeToString(exportData)

        textFieldState.setTextAndPlaceCursorAtEnd(exportDataJson)
        inputParseResult = Result.success(exportData)
        inputProcessed = true
        skipInputProcessDelay = false
        importConfig = PlatformSettingsImporter.ImportConfig()
    }

    override fun onClosed(movingBackward: Boolean) {
        textFieldState.clearText()
        inputParseResult = Result.failure(IllegalStateException())
    }

    @Composable
    override fun Content(modifier: Modifier, contentPadding: PaddingValues) {
        var saving: Boolean by remember { mutableStateOf(false) }

        Column(
            modifier.padding(contentPadding)
        ) {
            StorageLocationText(Modifier.padding(bottom = 10.dp))

            InputField(
                enabled = !saving,
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
                    .padding(bottom = 10.dp)
            )

            InputValidationStatus(
                Modifier
                    .fillMaxWidth()
                    .animateContentSize(),
                contentPadding = PaddingValues(bottom = 15.dp)
            )

            ActionRow(
                enabled = !saving,
                modifier = Modifier.fillMaxWidth()
            ) {
                saving = it
            }
        }
    }

    @Composable
    private fun StorageLocationText(modifier: Modifier = Modifier) {
        val context: PlatformContext = LocalContext.current
        val storagePath: String? = settings.readableStoragePath
        if (storagePath != null) {
            FlowRow(
                modifier,
                horizontalArrangement = Arrangement.End
            ) {
                Box(
                    Modifier
                        .weight(1f, false)
                        .align(Alignment.CenterVertically)
                ) {
                    SelectionContainer {
                        Text(
                            stringResource(Res.string.`preferences_edit_screen_message_stored_at_$path`)
                                .replace("\$path", storagePath),
                            style = MaterialTheme.typography.titleMedium,
                            textAlign = TextAlign.End
                        )
                    }
                }

                val storageUri: String? = settings.storageUri
                if (storageUri != null && context.canOpenUrl()) {
                    IconButton(
                        { context.openUrl(storageUri) },
                        Modifier.align(Alignment.CenterVertically)
                    ) {
                        Icon(
                            Icons.AutoMirrored.Filled.OpenInNew,
                            stringResource(Res.string.preferences_edit_screen_button_open_file)
                        )
                    }
                }
            }
        }
    }

    @Composable
    private fun InputField(
        modifier: Modifier = Modifier,
        enabled: Boolean = true
    ) {
        BoxWithConstraints(modifier) {
            val density: Density = LocalDensity.current
            val textFieldInnerPadding: Dp = 18.dp
            var buttonRowHeight: Dp by remember { mutableStateOf(0.dp) }

            ScrollBarLazyColumn(
                Modifier
                    .matchParentSize()
                    .border(2.dp, LocalComposeKitTheme.current.accent, RoundedCornerShape(10.dp))
                    .padding(
                        start = textFieldInnerPadding,
                        end = textFieldInnerPadding / 2f
                    ),
                contentPadding =
                    PaddingValues(
                        top = textFieldInnerPadding,
                        bottom = textFieldInnerPadding + buttonRowHeight
                    ),
                scrollBarContentPadding =
                    PaddingValues(
                        top = textFieldInnerPadding,
                        bottom = textFieldInnerPadding
                    ),
                scrollBarSpacing = 5.dp + textFieldInnerPadding / 2f
            ) {
                item {
                    BasicTextField(
                        textFieldState,
                        Modifier
                            .fillMaxWidth()
                            .heightIn(min = this@BoxWithConstraints.maxHeight - buttonRowHeight - textFieldInnerPadding - textFieldInnerPadding),
                        enabled = enabled,
                        textStyle = MaterialTheme.typography.bodyLarge.copy(color = LocalContentColor.current),
                        cursorBrush = SolidColor(LocalContentColor.current)
                    )
                }
            }

            FlowRow(
                Modifier
                    .fillMaxWidth()
                    .align(Alignment.BottomCenter)
                    .padding(textFieldInnerPadding)
                    .onSizeChanged {
                        buttonRowHeight = with (density) { it.height.toDp() }
                    },
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                FlowRow(Modifier.align(Alignment.CenterVertically)) {
                    for (
                        action in listOf(SettingsEditActionReset(groups, exporter), SettingsEditActionFilter(groups)).filter { it.canDisplay() }
                    ) {
                        ActionButton(
                            action,
                            Modifier.align(Alignment.CenterVertically),
                            enabled = enabled
                        )
                    }
                }

                FlowRow(Modifier.align(Alignment.CenterVertically)) {
                    for (
                        action in listOf(SettingsEditActionCopy, SettingsEditActionPaste).filter { it.canDisplay() }
                    ) {
                        ActionButton(
                            action,
                            Modifier.align(Alignment.CenterVertically),
                            enabled = enabled
                        )
                    }
                }
            }
        }
    }

    @Composable
    private fun InputValidationStatus(
        modifier: Modifier = Modifier,
        contentPadding: PaddingValues = PaddingValues()
    ) {
        OnChangedEffect(textFieldState.text) {
            inputProcessed = false

            if (skipInputProcessDelay) {
                skipInputProcessDelay = false
            }
            else {
                delay(1.seconds)
            }

            inputParseResult =
                if (textFieldState.text.isBlank())
                    Result.failure(MessageParseException(Res.string.preferences_edit_screen_message_input_is_blank))
                else
                    runCatching {
                        settings.json.decodeFromString(textFieldState.text.toString())
                    }
            inputProcessed = true
        }

        NullableValueAnimatedVisibility(
            inputParseResult,
            modifier,
            enter = expandIn(),
            exit = shrinkOut()
        ) { parseResult ->
            if (parseResult == null) {
                return@NullableValueAnimatedVisibility
            }

            Box(Modifier.padding(contentPadding)) {
                parseResult.fold(
                    onSuccess = {
                        Text(stringResource(Res.string.preferences_edit_screen_message_input_is_valid))
                    },
                    onFailure = { error ->
                        SelectionContainer {
                            Text(
                                if (error is MessageParseException) stringResource(error.messageResource)
                                else error.toString(),
                                color = LocalComposeKitTheme.current.error,
                                style = MaterialTheme.typography.bodyMedium
                            )
                        }
                    }
                )
            }
        }
    }

    class MessageParseException(val messageResource: StringResource): Exception()

    @Composable
    private fun ActionRow(
        modifier: Modifier = Modifier,
        enabled: Boolean = true,
        onSavingChanged: (Boolean) -> Unit
    ) {
        val theme: ThemeValues = LocalComposeKitTheme.current
        val coroutineScope: CoroutineScope = rememberCoroutineScope()

        FlowRow(
            modifier,
            horizontalArrangement = Arrangement.SpaceBetween,
            itemVerticalAlignment = Alignment.Bottom
        ) {
            FlowRow(
                Modifier.align(Alignment.Bottom),
                horizontalArrangement = Arrangement.spacedBy(5.dp)
            ) {
                for (
                    action in listOf(SettingsEditActionImportFile, SettingsEditActionExportFile).filter { it.canDisplay() }
                ) {
                    ActionButton(
                        action,
                        Modifier.align(Alignment.CenterVertically),
                        enabled = enabled
                    )
                }
            }

            var showSavedMessage: Boolean by remember { mutableStateOf(false) }

            Box(
                Modifier
                    .align(Alignment.CenterVertically)
                    .fillMaxWidth()
                    .weight(1f, false),
                contentAlignment = Alignment.CenterEnd
            ) {
                val currentExportData: PlatformSettingsExportData? = inputParseResult.getOrNull()

                LoadActionButton(
                    modifier = Modifier.height(IntrinsicSize.Min),
                    performLoad = {
                        if (currentExportData == null) {
                            return@LoadActionButton
                        }

                        coroutineScope.coroutineContext.cancelChildren()

                        onSavingChanged(true)
                        settings.edit {
                            importer.importPreferences(currentExportData, importConfig, this, groups)
                        }
                        onSavingChanged(false)

                        coroutineScope.launch {
                            showSavedMessage = true
                            delay(SAVED_MESSAGE_SHOW_DURATION_MS.milliseconds)
                            showSavedMessage = false
                        }
                    },
                    uncancellable = true,
                    enabled = inputProcessed && currentExportData != null,
                    contentPadding = PaddingValues()
                ) {
                    Row(
                        Modifier.width(IntrinsicSize.Max),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        StickyWidthRow(
                            Modifier.fillMaxWidth().weight(1f),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            SkippableCrossfade(
                                showSavedMessage,
                                modifier =
                                    Modifier
                                        .padding(
                                            start = ButtonDefaults.ContentPadding.start,
                                            end = ButtonDefaults.ContentPadding.end / 2f
                                        ),
                                contentAlignment = Alignment.Center,
                                shouldSkipTransition = { from, _ ->
                                    SkippableCrossfadeTransition(!from)
                                }
                            ) { savedMessage ->
                                Text(
                                    if (savedMessage)
                                        stringResource(Res.string.preferences_edit_screen_message_saved)
                                    else
                                        when (importConfig.mode) {
                                            PlatformSettingsImporter.Mode.OVERWRITE_ALL ->
                                                stringResource(Res.string.preferences_edit_button_apply_overwrite_all)
                                            PlatformSettingsImporter.Mode.OVERLAY ->
                                                stringResource(Res.string.preferences_edit_button_apply_overlay)
                                        },
                                    softWrap = false
                                )
                            }
                        }

                        CompositionLocalProvider(LocalContentColor provides theme.onBackground) {
                            ActionButton(
                                remember(importConfig) { SettingsEditActionApplyConfig(importConfig) },
                                Modifier
                                    .clip(CircleShape)
                                    .aspectRatio(1f)
                                    .fillMaxHeight()
                                    .padding(2.dp)
                                    .background(theme.background, CircleShape),
                                enabled = enabled
                            )
                        }
                    }
                }
            }
        }
    }

    @Composable
    private fun ActionButton(
        action: SettingsEditAction,
        modifier: Modifier = Modifier,
        enabled: Boolean = true
    ) {
        action.Content(
            textFieldState.text.toString(),
            inputParseResult,
            enabled,
            settings,
            updateInput = { newInput ->
                skipInputProcessDelay = true
                textFieldState.edit {
                    replace(0, length, newInput)
                }
            },
            updateConfig = {
                importConfig = it
            },
            modifier = modifier
        )
    }
}

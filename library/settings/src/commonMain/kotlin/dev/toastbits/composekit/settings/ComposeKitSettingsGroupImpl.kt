package dev.toastbits.composekit.settings

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsEditor
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.booleanOrNull
import kotlinx.serialization.json.contentOrNull
import kotlinx.serialization.json.floatOrNull
import kotlinx.serialization.json.int
import kotlinx.serialization.json.intOrNull
import kotlinx.serialization.json.jsonPrimitive
import kotlinx.serialization.json.longOrNull
import kotlinx.serialization.serializer
import kotlin.properties.PropertyDelegateProvider

@Suppress("IMPLICIT_CAST_TO_ANY", "UNCHECKED_CAST")
abstract class ComposeKitSettingsGroupImpl(
    override val groupKey: String,
    val settings: PlatformSettings
): ComposeKitSettingsGroup {
    protected open fun getUnregisteredProperties(): List<PlatformSettingsProperty<*>> = emptyList()

    override fun getAllProperties(): List<PlatformSettingsProperty<*>> = all_properties + getUnregisteredProperties()

    protected inline fun <reified T: Any> property(
        name: KmpStringResource,
        description: KmpStringResource?,
        noinline getDefaultValue: () -> T,
        noinline isHidden: () -> Boolean = { false }
    ): PropertyDelegateProvider<Any?, PlatformSettingsProperty<T>> {
        val defaultValueProvider: () -> T = getDefaultValue
        return PropertyDelegateProvider { _, property ->
            check(T::class !is Enum<*>) { "Enum property '$property' must use enumProperty()" }

            val property: PlatformSettingsProperty<T> =
                object : PrefsProperty<T>(key = property.name) {
                    override val name: KmpStringResource = name
                    override val description: KmpStringResource? = description

                    override fun getDefaultValue(): T = defaultValueProvider()
                    override fun isHidden(): Boolean = isHidden()
                }
            onPropertyAdded(property)
            return@PropertyDelegateProvider property
        }
    }

    protected inline fun <reified T: Any> resourceDefaultValueProperty(
        name: KmpStringResource,
        description: KmpStringResource?,
        noinline getDefaultValue: () -> T,
        noinline isHidden: () -> Boolean = { false }
    ): PropertyDelegateProvider<Any?, PlatformSettingsProperty<T>> {
        return PropertyDelegateProvider { _, property ->
            check(T::class !is Enum<*>) { "Enum property '$property' must use enumProperty()" }

            val property: PlatformSettingsProperty<T> =
                object : PrefsProperty<T>(key = property.name) {
                    override val name: KmpStringResource = name
                    override val description: KmpStringResource? = description

                    override fun getDefaultValue(): T = getDefaultValue()
                    override fun isHidden(): Boolean = isHidden()
                }
            onPropertyAdded(property)
            return@PropertyDelegateProvider property
        }
    }

    protected inline fun <reified T: Enum<T>> enumProperty(
        name: KmpStringResource,
        description: KmpStringResource?,
        noinline getDefaultValue: () -> T,
        noinline isHidden: () -> Boolean = { false }
    ): PropertyDelegateProvider<Any?, PlatformSettingsProperty<T>> {
        val defaultValueProvider: () -> T = getDefaultValue
        return PropertyDelegateProvider { _, property ->
            val property: PlatformSettingsProperty<T> =
                object : EnumPrefsProperty<T>(
                    key = property.name,
                    entries = enumValues<T>().toList()
                ) {
                    override val name: KmpStringResource = name
                    override val description: KmpStringResource? = description

                    override fun getDefaultValue(): T = defaultValueProvider()
                    override fun isHidden(): Boolean = isHidden()
                }
            onPropertyAdded(property)
            return@PropertyDelegateProvider property
        }
    }

    protected inline fun <reified T: Any> serialisableProperty(
        name: KmpStringResource,
        description: KmpStringResource?,
        noinline getDefaultValue: () -> T,
        noinline isHidden: () -> Boolean = { false },
        json: Json? = null
    ): PropertyDelegateProvider<Any?, PlatformSettingsProperty<T>> {
        val defaultValueProvider: () -> T = getDefaultValue
        return PropertyDelegateProvider { _, property ->
            val property: PlatformSettingsProperty<T> =
                object : SerialisablePrefsProperty<T>(
                    key = property.name,
                    serialiser = serializer<T>(),
                    jsonOverride = json
                ) {
                    override val name: KmpStringResource = name
                    override val description: KmpStringResource? = description

                    override fun getDefaultValue(): T = defaultValueProvider()
                    override fun isHidden(): Boolean = isHidden()
                }
            onPropertyAdded(property)
            return@PropertyDelegateProvider property
        }
    }

    protected inline fun <reified T: Any> nullableSerialisableProperty(
        name: KmpStringResource,
        description: KmpStringResource?,
        noinline getDefaultValue: () -> T?,
        noinline isHidden: () -> Boolean = { false },
        json: Json? = null
    ): PropertyDelegateProvider<Any?, PlatformSettingsProperty<T?>> {
        val defaultValueProvider: () -> T? = getDefaultValue
        return PropertyDelegateProvider { _, property ->
            val property: PlatformSettingsProperty<T?> =
                object : SerialisablePrefsProperty<T?>(
                    key = property.name,
                    serialiser = serializer<T?>(),
                    jsonOverride = json
                ) {
                    override val name: KmpStringResource = name
                    override val description: KmpStringResource? = description

                    override fun getDefaultValue(): T? = defaultValueProvider()
                    override fun isHidden(): Boolean = isHidden()
                }
            onPropertyAdded(property)
            return@PropertyDelegateProvider property
        }
    }

    private val all_properties: MutableList<PlatformSettingsProperty<*>> = mutableListOf()

    fun onPropertyAdded(property: PlatformSettingsProperty<*>) {
        all_properties.add(property)
    }

    private fun formatPropertyKey(property_key: String): String =
        groupKey + "_" + property_key

    @Suppress("UNCHECKED_CAST")
    protected abstract inner class PrefsProperty<T>(key: String): PlatformSettingsProperty<T> {
        override val key: String = formatPropertyKey(key)

        override fun get(): T =
            when (val default_value: T = getDefaultValue()) {
                is Boolean -> settings.getBoolean(key, default_value)
                is Float -> settings.getFloat(key, default_value)
                is Int -> settings.getInt(key, default_value)
                is Long -> settings.getLong(key, default_value)
                is String -> settings.getString(key, default_value)
                is Set<*> -> settings.getStringSet(key, default_value as Set<String>)
                is Enum<*> -> throw IllegalStateException("Use EnumPrefsProperty")
                else -> throw NotImplementedError("$key $default_value ${default_value!!::class.simpleName}")
            } as T

        @Suppress("UNNECESSARY_NOT_NULL_ASSERTION")
        override suspend fun set(value: T, editor: PlatformSettingsEditor?) =
            (editor ?: settings).edit {
                when (value) {
                    null -> remove(key)
                    is Boolean -> putBoolean(key, value)
                    is Float -> putFloat(key, value)
                    is Int -> putInt(key, value)
                    is Long -> putLong(key, value)
                    is String -> putString(key, value)
                    is Set<*> -> putStringSet(key, value as Set<String>)
                    is Enum<*> -> throw IllegalStateException("Use EnumPrefsProperty")
                    else -> throw NotImplementedError("$key ${value!!::class.simpleName}")
                }
            }

        override suspend fun set(data: JsonElement, editor: PlatformSettingsEditor?) =
            when (data) {
                is JsonArray -> set(data.map { it.jsonPrimitive.content }.toSet() as T, editor)
                is JsonPrimitive -> {
                    val value: T = (
                        data.booleanOrNull
                        ?: data.intOrNull
                        ?: data.longOrNull
                        ?: data.floatOrNull
                        ?: data.contentOrNull
                    ) as T

                    set(value, editor)
                }
                is JsonObject -> throw IllegalStateException("PrefsProperty ($this) data is JsonObject ($data)")
            }

        override suspend fun reset(editor: PlatformSettingsEditor?) {
            if (editor != null) {
                editor.remove(key)
            }
            else {
                settings.edit {
                    remove(key)
                }
            }
        }

        override fun serialise(value: Any?): JsonElement =
            when (value) {
                null -> JsonPrimitive(null)
                is Boolean -> JsonPrimitive(value)
                is Float -> JsonPrimitive(value)
                is Int -> JsonPrimitive(value)
                is Long -> JsonPrimitive(value)
                is String -> JsonPrimitive(value)
                is Set<*> -> JsonArray((value as Set<String>).map { JsonPrimitive(it) })
                is Enum<*> -> throw IllegalStateException("Use EnumPrefsProperty")
                else -> throw NotImplementedError("$key ${value::class.simpleName}")
            }

        override fun asFlow(): StateFlow<T> {
            val flow: MutableStateFlow<T> = MutableStateFlow(get())

            val listener: PlatformSettingsListener =
                PlatformSettingsListener { key ->
                    if (key != this@PrefsProperty.key) {
                        return@PlatformSettingsListener
                    }

                    flow.value = get()
                }

            settings.addListener(listener)
            flow.onCompletion {
                settings.removeListener(listener)
            }

            return flow
        }

        override fun toString(): String =
            "PrefsProperty<T>(key=$key)"
    }

    protected abstract inner class EnumPrefsProperty<T: Enum<T>>(
        key: String,
        val entries: List<T>
    ): PrefsProperty<T>(key) {
        override fun get(): T =
            entries[settings.getInt(key, getDefaultValue().ordinal)!!]

        override suspend fun set(value: T, editor: PlatformSettingsEditor?) =
            (editor ?: settings).edit {
                putInt(key, value.ordinal)
            }

        override suspend fun set(data: JsonElement, editor: PlatformSettingsEditor?) =
            set(entries[data.jsonPrimitive.int], editor)

        override fun serialise(value: Any?): JsonElement =
            JsonPrimitive((value as T?)?.ordinal)

        override fun toString(): String =
            "EnumPrefsProperty(key=$key)"
    }

    protected abstract inner class SerialisablePrefsProperty<T>(
        key: String,
        val serialiser: KSerializer<T>,
        private val jsonOverride: Json?
    ): PrefsProperty<T>(key) {
        private val json: Json
            get() = jsonOverride ?: settings.json

        override fun get(): T =
            settings.getSerialisable(key, getDefaultValue(), serialiser, json)

        override suspend fun set(value: T, editor: PlatformSettingsEditor?) =
            (editor ?: settings).edit {
                putSerialisable(key, value, serialiser, this@SerialisablePrefsProperty.json)
            }

        override suspend fun set(data: JsonElement, editor: PlatformSettingsEditor?) {
            val value: T

            if (data is JsonPrimitive) {
                value = json.decodeFromString(serialiser, data.content)
            }
            else {
                value = json.decodeFromJsonElement(serialiser, data)
            }

            set(value, editor)
        }

        override fun serialise(value: Any?): JsonElement =
            json.encodeToJsonElement(serialiser, value as T)

        override fun toString(): String =
            "SerialisablePrefsProperty(key=$key)"
    }
}

private suspend fun Any.edit(action: PlatformSettingsEditor.() -> Unit) {
    if (this is PlatformSettingsEditor) {
        action(this)
    }
    else if (this is PlatformSettings) {
        this.edit {
            action(this)
        }
    }
    else {
        throw NotImplementedError(this::class.toString())
    }
}

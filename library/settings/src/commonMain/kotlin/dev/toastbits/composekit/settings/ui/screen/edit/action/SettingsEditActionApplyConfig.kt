package dev.toastbits.composekit.settings.ui.screen.edit.action

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import dev.toastbits.composekit.components.utils.composable.LargeDropdownMenu
import dev.toastbits.composekit.settings.generated.resources.Res
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_apply_config_dialog_apply_mode
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_apply_config_dialog_apply_mode_option_overlay
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_apply_config_dialog_apply_mode_option_overwrite_all
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settings.handler.PlatformSettingsImporter
import dev.toastbits.composekit.settings.model.PlatformSettingsExportData
import org.jetbrains.compose.resources.stringResource

internal class SettingsEditActionApplyConfig(
    private val initialConfig: PlatformSettingsImporter.ImportConfig
): SettingsEditAction {
    @Composable
    override fun Content(
        currentInput: String,
        currentInputParseResult: Result<PlatformSettingsExportData>,
        enabled: Boolean,
        settings: PlatformSettings,
        updateInput: (String) -> Unit,
        updateConfig: (PlatformSettingsImporter.ImportConfig) -> Unit,
        modifier: Modifier
    ) {
        var showConfigDialog: Boolean by remember { mutableStateOf(false) }

        Box(
            modifier.clickable {
                showConfigDialog = true
            },
            contentAlignment = Alignment.Center
        ) {
            Icon(Icons.Default.Settings, stringResource(Res.string.preferences_edit_apply_config_dialog_apply_mode))
        }

        ApplyConfigDialog(showConfigDialog) { newConfig ->
            if (newConfig != null) {
                updateConfig(newConfig)
            }
            showConfigDialog = false
        }
    }

    @Composable
    private fun ApplyConfigDialog(
        show: Boolean,
        modifier: Modifier = Modifier,
        onFinished: (PlatformSettingsImporter.ImportConfig?) -> Unit
    ) {
        LargeDropdownMenu(
            isOpen = show,
            title = stringResource(Res.string.preferences_edit_apply_config_dialog_apply_mode),
            items = PlatformSettingsImporter.Mode.entries,
            selectedItem = initialConfig.mode,
            onDismissRequest = { onFinished(null) },
            onSelected = { _, mode ->
                onFinished(initialConfig.copy(mode = mode))
            },
            modifier = modifier
        ) { mode ->
            Text(
                when (mode) {
                    PlatformSettingsImporter.Mode.OVERWRITE_ALL -> stringResource(Res.string.preferences_edit_apply_config_dialog_apply_mode_option_overwrite_all)
                    PlatformSettingsImporter.Mode.OVERLAY -> stringResource(Res.string.preferences_edit_apply_config_dialog_apply_mode_option_overlay)
                }
            )
        }
    }
}

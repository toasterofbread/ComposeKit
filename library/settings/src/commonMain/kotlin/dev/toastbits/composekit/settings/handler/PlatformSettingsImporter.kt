package dev.toastbits.composekit.settings.handler

import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import dev.toastbits.composekit.settings.model.PlatformSettingsExportData
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsEditor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.JsonElement

object PlatformSettingsImporter {
    data class ImportResult(
        val importedProperties: Int
    )

    data class ImportConfig(val mode: Mode = Mode.DEFAULT)

    enum class Mode {
        OVERWRITE_ALL,
        OVERLAY;

        companion object {
            val DEFAULT: Mode = OVERWRITE_ALL
        }
    }

    suspend fun importPreferences(
        data: PlatformSettingsExportData,
        config: ImportConfig,
        editor: PlatformSettingsEditor,
        allGroups: List<ComposeKitSettingsGroup>
    ): ImportResult = withContext(Dispatchers.Default) {
        var importedProperties: Int = 0

        for (group in allGroups) {
            for (property in group.getAllProperties()) {
                val value: JsonElement? = data[property.key]
                if (value == null) {
                    if (config.mode == Mode.OVERWRITE_ALL) {
                        property.reset(editor)
                    }
                    continue
                }

                property.set(value, editor)
                importedProperties++
            }
        }

        return@withContext ImportResult(importedProperties)
    }
}
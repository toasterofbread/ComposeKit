package dev.toastbits.composekit.settings.handler

import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import dev.toastbits.composekit.settings.model.PlatformSettingsExportData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject

object PlatformSettingsExporter {
    suspend fun exportPreferences(
        groups: List<ComposeKitSettingsGroup>,
        includeDefaults: Boolean
    ): PlatformSettingsExportData = withContext(Dispatchers.Default) {
        val values: MutableMap<String, JsonElement> = mutableMapOf()

        for (group in groups) {
            for (property in group.getAllProperties()) {
                val value: Any? = property.get()
                if (!includeDefaults && value == property.getDefaultValue()) {
                    continue
                }

                values[property.key] = property.serialise(value)
            }
        }

        return@withContext JsonObject(values)
    }
}
package dev.toastbits.composekit.settings.ui.screen.edit.action

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settings.handler.PlatformSettingsImporter
import dev.toastbits.composekit.settings.model.PlatformSettingsExportData

internal interface SettingsEditAction {
    @Composable
    fun canDisplay(): Boolean = true

    @Composable
    fun Content(
        currentInput: String,
        currentInputParseResult: Result<PlatformSettingsExportData>,
        enabled: Boolean,
        settings: PlatformSettings,
        updateInput: (String) -> Unit,
        updateConfig: (PlatformSettingsImporter.ImportConfig) -> Unit,
        modifier: Modifier = Modifier
    )
}

package dev.toastbits.composekit.settings.ui.screen.edit.action

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.FilterAlt
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.FilledIconButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.platform.composable.ScrollBarLazyColumn
import dev.toastbits.composekit.components.utils.modifier.horizontal
import dev.toastbits.composekit.navigation.compositionlocal.LocalNavigator
import dev.toastbits.composekit.navigation.util.AlertDialog
import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settings.generated.resources.Res
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_action_filter
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_filter_dialog_button_apply
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_filter_dialog_button_cancel
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_filter_dialog_groups_disable_all
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_filter_dialog_groups_enable_all
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_filter_dialog_groups_to_include
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_filter_dialog_title
import dev.toastbits.composekit.settings.handler.PlatformSettingsImporter
import dev.toastbits.composekit.settings.model.PlatformSettingsExportData
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.JsonObject
import org.jetbrains.compose.resources.stringResource

internal class SettingsEditActionFilter(
    private val groups: List<ComposeKitSettingsGroup>
): SettingsEditAction {
    @Composable
    override fun Content(
        currentInput: String,
        currentInputParseResult: Result<PlatformSettingsExportData>,
        enabled: Boolean,
        settings: PlatformSettings,
        updateInput: (String) -> Unit,
        updateConfig: (PlatformSettingsImporter.ImportConfig) -> Unit,
        modifier: Modifier
    ) {
        var showOptionsDialog: Boolean by remember { mutableStateOf(false) }
        val exportData: PlatformSettingsExportData? = currentInputParseResult.getOrNull()

        LaunchedEffect(exportData) {
            if (exportData == null) {
                showOptionsDialog = false
            }
        }

        FilledIconButton(
            { showOptionsDialog = true },
            modifier,
            enabled = exportData != null
        ) {
            Icon(Icons.Default.FilterAlt, stringResource(Res.string.preferences_edit_action_filter))
        }

        FilterOptionsDialog(showOptionsDialog && exportData != null) { options ->
            if (options != null && exportData != null) {
                val newExportData: PlatformSettingsExportData = applyOptionsToExportData(exportData, options)
                updateInput(settings.json.encodeToString(newExportData))
            }

            showOptionsDialog = false
        }
    }

    private fun applyOptionsToExportData(
        exportData: PlatformSettingsExportData,
        filterOptions: FilterOptions
    ) =
        JsonObject(
            exportData.filterKeys { key ->
                filterOptions.groups.any { group -> group.getAllProperties().any { property -> property.key == key } }
            }
        )

    @Composable
    private fun FilterOptionsDialog(
        show: Boolean,
        modifier: Modifier = Modifier,
        onFinished: (FilterOptions?) -> Unit
    ) {
        var filterOptions: FilterOptions by remember { mutableStateOf(FilterOptions(groups)) }

        LocalNavigator.current.AlertDialog(
            show,
            confirmButton = {
                Button({ onFinished(filterOptions) }) {
                    Text(stringResource(Res.string.preferences_edit_filter_dialog_button_apply))
                }
            },
            dismissButton = {
                OutlinedButton({ onFinished(null) }) {
                    Text(stringResource(Res.string.preferences_edit_filter_dialog_button_cancel))
                }
            },
            onDismissRequest = { onFinished(null) },
            modifier = modifier,
            title = { Text(stringResource(Res.string.preferences_edit_filter_dialog_title)) },
            text = {
                FilterOptionsList(
                    filterOptions,
                    Modifier.padding(top = 15.dp)
                ) {
                    filterOptions = it
                }
            }
        )
    }

    @Composable
    private fun FilterOptionsList(
        currentOptions: FilterOptions,
        modifier: Modifier = Modifier,
        onChange: (FilterOptions) -> Unit
    ) {
        Column(
            modifier,
            verticalArrangement = Arrangement.spacedBy(5.dp)
        ) {
            Text(
                stringResource(Res.string.preferences_edit_filter_dialog_groups_to_include),
                Modifier.alpha(0.8f),
                style = MaterialTheme.typography.labelLarge
            )

            ScrollBarLazyColumn(Modifier.fillMaxHeight().weight(1f)) {
                item {
                    GroupsToggleList(currentOptions.groups) {
                        onChange(currentOptions.copy(groups = it))
                    }
                }
            }

            Row(
                Modifier
                    .padding(horizontal = 25.dp)
                    .padding(top = 10.dp),
                horizontalArrangement = Arrangement.spacedBy(15.dp)
            ) {
                OutlinedButton(
                    { onChange(currentOptions.copy(groups = groups)) },
                    Modifier.fillMaxWidth(0.5f),
                    contentPadding = ButtonDefaults.ContentPadding.horizontal
                ) {
                    Text(stringResource(Res.string.preferences_edit_filter_dialog_groups_enable_all))
                }

                OutlinedButton(
                    { onChange(currentOptions.copy(groups = emptyList())) },
                    Modifier.fillMaxWidth(),
                    contentPadding = ButtonDefaults.ContentPadding.horizontal
                ) {
                    Text(stringResource(Res.string.preferences_edit_filter_dialog_groups_disable_all))
                }
            }
        }
    }

    @Composable
    private fun GroupsToggleList(
        currentGroups: List<ComposeKitSettingsGroup>,
        modifier: Modifier = Modifier,
        onChange: (List<ComposeKitSettingsGroup>) -> Unit
    ) {
        Column(modifier) {
            for (group in groups) {
                val groupEnabled: Boolean = currentGroups.contains(group)
                GroupToggle(
                    group,
                    groupEnabled,
                    Modifier.fillMaxWidth()
                ) { enableGroup ->
                    if (enableGroup) {
                        onChange(currentGroups + group)
                    }
                    else {
                        onChange(currentGroups.filter { it != group })
                    }
                }
            }
        }
    }

    @Composable
    private fun GroupToggle(
        group: ComposeKitSettingsGroup,
        groupEnabled: Boolean,
        modifier: Modifier = Modifier,
        onChange: (Boolean) -> Unit
    ) {
        FlowRow(
            modifier,
            horizontalArrangement = Arrangement.SpaceBetween,
            itemVerticalAlignment = Alignment.CenterVertically
        ) {
            Text(group.title.getComposable())
            Switch(groupEnabled, onChange)
        }
    }

    private data class FilterOptions(
        val groups: List<ComposeKitSettingsGroup>
    )
}

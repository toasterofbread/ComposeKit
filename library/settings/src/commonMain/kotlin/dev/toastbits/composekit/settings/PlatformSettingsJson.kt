package dev.toastbits.composekit.settings

import dev.toastbits.composekit.context.PlatformFile
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsEditor
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import okio.buffer
import okio.use

open class PlatformSettingsJson(
    private val file: PlatformFile,
    private val ioDispatcher: CoroutineDispatcher,
    initialData: MutableMap<String, JsonElement>,
    json: Json = PlatformSettingsImpl.createDefaultJson()
): InMemoryPlatformSettings(json) {
    override val readableStoragePath: String? get() = file.absolute_path
    override val storageUri: String? get() = "file://${file.absolute_path}"

    override val data: MutableMap<String, JsonElement> = initialData

    private val writeLock: Mutex = Mutex()
    private suspend fun saveData() = withContext(ioDispatcher) {
        writeLock.withLock {
            file.createFile()
            file.outputStream().buffer().use { writer ->
                writer.writeUtf8(json.encodeToString(data))
                writer.flush()
            }
        }
    }

    override suspend fun edit(action: suspend PlatformSettingsEditor.() -> Unit) {
        super.edit(action)
        saveData()
    }

    companion object {
        suspend fun load(
            file: PlatformFile,
            ioDispatcher: CoroutineDispatcher,
            json: Json = PlatformSettingsImpl.createDefaultJson()
        ) = withContext(ioDispatcher) {
            return@withContext PlatformSettingsJson(file, ioDispatcher, loadData(file, json), json)
        }

        fun loadData(
            file: PlatformFile,
            json: Json
        ): MutableMap<String, JsonElement> {
            if (!file.exists) {
                return mutableMapOf()
            }

            return file.inputStream().use { stream ->
                val text: String = stream.buffer().readUtf8()
                if (text.isBlank()) mutableMapOf()
                else json.decodeFromString(text)
            }
        }
    }
}

package dev.toastbits.composekit.settings.ui.screen.edit.action

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ContentPaste
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import dev.toastbits.composekit.components.LocalContext
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.settings.generated.resources.Res
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_action_paste
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_screen_tooltip_paste_permission_prompt
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.util.platform.Platform
import org.jetbrains.compose.resources.stringResource

internal object SettingsEditActionPaste: IconButtonSettingsEditAction() {
    override val readableName: String
        @Composable
        get() = stringResource(Res.string.preferences_edit_action_paste)

    override val icon: ImageVector
        @Composable
        get() = Icons.Default.ContentPaste

    override val tooltip: String?
        @Composable
        get() =
            when (Platform.current) {
                Platform.WEB -> stringResource(Res.string.preferences_edit_screen_tooltip_paste_permission_prompt)
                Platform.ANDROID,
                Platform.DESKTOP -> null
            }

    @Composable
    override fun canDisplay(): Boolean =
        LocalContext.current.canPasteText()

    override suspend fun execute(
        input: String,
        context: PlatformContext,
        settings: PlatformSettings
    ): String =
        context.pasteText() ?: input
}

package dev.toastbits.composekit.settings.ui.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CardColors
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.utils.composable.wave.WaveLineArea
import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import dev.toastbits.composekit.settings.ComposeKitSettingsGroupWithCustomPreview
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.util.blendWith

@Composable
fun SettingsGroupPreview(
    group: ComposeKitSettingsGroup,
    modifier: Modifier = Modifier,
    highlight: Boolean = false,
    onSelected: () -> Unit
) {
    if (group is ComposeKitSettingsGroupWithCustomPreview) {
        group.PreviewContent(modifier, onSelected)
        return
    }

    val theme: ThemeValues = LocalComposeKitTheme.current

    val shape: Shape = MaterialTheme.shapes.medium
    val colours: CardColors =
        CardDefaults.elevatedCardColors(
            containerColor = theme.accent.blendWith(theme.background, 0.03f),
            contentColor = theme.onBackground
        )

    ElevatedCard(
        Modifier
            .clip(shape)
            .then(modifier)
            .clickable(onClick = onSelected),
        shape = shape,
        colors = colours
    ) {
        WaveLineArea(showWaves = highlight) {
            Row(
                Modifier.padding(15.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(20.dp)
            ) {
                Icon(group.icon.get(), null)

                Column(verticalArrangement = Arrangement.spacedBy(5.dp)) {
                    Text(group.title.getComposable(), style = MaterialTheme.typography.titleLarge)
                    Text(group.description.getComposable(), style = MaterialTheme.typography.bodySmall, modifier = Modifier.alpha(0.7f))
                }
            }
        }
    }
}

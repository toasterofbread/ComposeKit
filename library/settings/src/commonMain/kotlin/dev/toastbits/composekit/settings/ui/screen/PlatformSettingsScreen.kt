package dev.toastbits.composekit.settings.ui.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.platform.composable.ScrollBarLazyColumn
import dev.toastbits.composekit.components.utils.composable.pane.model.InitialPaneRatioSource
import dev.toastbits.composekit.components.utils.composable.pane.model.ResizablePaneContainerParamsProvider
import dev.toastbits.composekit.components.utils.composable.pauseableInfiniteRepeatableAnimation
import dev.toastbits.composekit.components.utils.composable.wave.LocalWaveLineAreaState
import dev.toastbits.composekit.components.utils.composable.wave.WaveLineAreaState
import dev.toastbits.composekit.navigation.component.StandardButton
import dev.toastbits.composekit.navigation.screen.ResponsiveTwoPaneNavigatorScreen
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.navigation.screen.ScreenButton
import dev.toastbits.composekit.navigation.screen.ScreenWithExtraButtons
import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settings.generated.resources.Res
import dev.toastbits.composekit.settings.generated.resources.preferences_screen_title
import dev.toastbits.composekit.settings.handler.PlatformSettingsExporter
import dev.toastbits.composekit.settings.handler.PlatformSettingsImporter
import dev.toastbits.composekit.settings.ui.component.SettingsGroupPreview
import dev.toastbits.composekit.settings.ui.screen.edit.SettingsEditButton
import dev.toastbits.kmpresources.library.model.KmpStringResource
import dev.toastbits.kmpresources.library.mapper.toKmpResource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlin.math.roundToInt

open class PlatformSettingsScreen(
    private val settings: PlatformSettings,
    private val groups: List<ComposeKitSettingsGroup>,
    override val paneParams: ResizablePaneContainerParamsProvider = ResizablePaneContainerParamsProvider.default(),
    override val initialStartPaneRatioSource: InitialPaneRatioSource = InitialPaneRatioSource.Ratio(0.4f),
    private val displayExtraButtonsAboveGroups: Boolean = false
): ResponsiveTwoPaneNavigatorScreen(), ScreenWithExtraButtons {
    private var waveMillis: Int = 0
    private var firstLaunch: Boolean = true

    override val title: KmpStringResource =
        Res.string.preferences_screen_title.toKmpResource()

    protected open var GroupsListHeaderContent: (@Composable (Modifier) -> Unit)? = null
    protected open var GroupsListFooterContent: (@Composable (Modifier) -> Unit)? = null

    override val extraButtons: StateFlow<List<ScreenButton>> =
        MutableStateFlow(
            listOf(
                SettingsEditButton(
                    settings,
                    groups,
                    PlatformSettingsExporter,
                    PlatformSettingsImporter
                )
            )
        )

    fun reset() {
        if (isDisplayingBothPanes.value && groups.isNotEmpty()) {
            val firstGroup: ComposeKitSettingsGroup? =
                groups.firstOrNull { it.getConfigurationItems().isNotEmpty() }
            openGroup(firstGroup)
            alwaysShowEndPane = true
        }
        else {
            openGroup(null)
            alwaysShowEndPane = false
        }
    }

    final override var alwaysShowEndPane: Boolean = true
        private set

    @Composable
    override fun PrimaryPane(data: Screen?, contentPadding: PaddingValues, modifier: Modifier) {
        val wavePeriod: Int = 2000
        val waveState: WaveLineAreaState =
            pauseableInfiniteRepeatableAnimation(
                start = 0f,
                end = 1f,
                period = wavePeriod,
                initialOffsetMillis = waveMillis
            )

        waveMillis = (waveState.value * wavePeriod).roundToInt()

        LaunchedEffect(Unit) {
            if (!firstLaunch) {
                alwaysShowEndPane = false
                return@LaunchedEffect
            }
            firstLaunch = false
            reset()
        }

        CompositionLocalProvider(LocalWaveLineAreaState provides waveState) {
            val extraButtons: List<ScreenButton> =
                if (displayExtraButtonsAboveGroups) this.extraButtons.collectAsState().value
                else emptyList()

            ScrollBarLazyColumn(
                modifier,
                verticalArrangement = Arrangement.spacedBy(10.dp),
                contentPadding = contentPadding
            ) {
                if (extraButtons.isNotEmpty()) {
                    item {
                        TopRow(extraButtons)
                    }
                }

                GroupsListHeaderContent?.also { headerContent ->
                    item {
                        headerContent(Modifier)
                    }
                }

                val activeGroup: ComposeKitSettingsGroup? =
                    (internalNavigator.getMostRecentOfOrNull { it is PlatformSettingsGroupScreen } as PlatformSettingsGroupScreen?)?.group

                itemsIndexed(groups) { index, group ->
                    SettingsGroupPreview(
                        group,
                        highlight = group == activeGroup,
                        modifier = Modifier.fillMaxWidth(),
                        onSelected = {
                            val currentGroup: ComposeKitSettingsGroup? =
                                (internalNavigator.currentScreen.value as? PlatformSettingsGroupScreen)?.group
                            if (currentGroup == group) {
                                return@SettingsGroupPreview
                            }
                            else {
                                openGroup(group)
                            }
                        }
                    )
                }

                GroupsListFooterContent?.also { headerContent ->
                    item {
                        headerContent(Modifier)
                    }
                }
            }
        }
    }

    @Composable
    private fun TopRow(extraButtons: List<ScreenButton>, modifier: Modifier = Modifier) {
        FlowRow(
            modifier,
            horizontalArrangement = Arrangement.spacedBy(10.dp),
            itemVerticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                title.getComposable(),
                Modifier.fillMaxWidth().weight(1f),
                style = MaterialTheme.typography.headlineMedium
            )
            Row {
                for (button in extraButtons) {
                    button.StandardButton()
                }
            }
        }
    }

    private fun openGroup(group: ComposeKitSettingsGroup?) {
        val newScreen: Screen =
            if (group != null) PlatformSettingsGroupScreen(group)
            else Screen.EMPTY
        internalNavigator.replaceScreenUpTo(newScreen) { newScreen::class.isInstance(it) }
    }
}
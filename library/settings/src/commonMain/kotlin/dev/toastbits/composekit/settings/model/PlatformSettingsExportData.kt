package dev.toastbits.composekit.settings.model

import kotlinx.serialization.json.JsonObject

typealias PlatformSettingsExportData = JsonObject

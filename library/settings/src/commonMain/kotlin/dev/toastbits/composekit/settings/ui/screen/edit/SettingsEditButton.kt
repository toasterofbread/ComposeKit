package dev.toastbits.composekit.settings.ui.screen.edit

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import dev.toastbits.composekit.navigation.navigator.Navigator
import dev.toastbits.composekit.navigation.screen.ScreenButton
import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settings.generated.resources.Res
import dev.toastbits.composekit.settings.generated.resources.preferences_button_edit
import dev.toastbits.composekit.settings.handler.PlatformSettingsExporter
import dev.toastbits.composekit.settings.handler.PlatformSettingsImporter
import dev.toastbits.composekit.util.model.KImageVectorResource
import dev.toastbits.kmpresources.library.mapper.toKmpResource
import dev.toastbits.kmpresources.library.model.KmpStringResource

internal class SettingsEditButton(
    private val settings: PlatformSettings,
    private val groups: List<ComposeKitSettingsGroup>,
    private val exporter: PlatformSettingsExporter,
    private val importer: PlatformSettingsImporter
): ScreenButton {
    override val icon: KImageVectorResource =
        KImageVectorResource {
            Icons.Default.Edit
        }
    override val readableName: KmpStringResource =
        Res.string.preferences_button_edit.toKmpResource()

    override suspend fun onClick(navigator: Navigator) {
        navigator.pushScreen(
            PlatformSettingsEditScreen(
                importer,
                exporter,
                settings,
                groups
            )
        )
    }
}

package dev.toastbits.composekit.settings.ui.screen.edit.action

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.settings.generated.resources.Res
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_action_reset
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settings.handler.PlatformSettingsExporter
import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import kotlinx.serialization.encodeToString
import org.jetbrains.compose.resources.stringResource

internal class SettingsEditActionReset(
    private val groups: List<ComposeKitSettingsGroup>,
    private val exporter: PlatformSettingsExporter
): IconButtonSettingsEditAction() {
    override val readableName: String
        @Composable
        get() = stringResource(Res.string.preferences_edit_action_reset)

    override val icon: ImageVector
        @Composable
        get() = Icons.Default.Refresh

    override suspend fun execute(
        input: String,
        context: PlatformContext,
        settings: PlatformSettings
    ): String =
        settings.json.encodeToString(
            exporter.exportPreferences(groups, includeDefaults = false)
        )
}

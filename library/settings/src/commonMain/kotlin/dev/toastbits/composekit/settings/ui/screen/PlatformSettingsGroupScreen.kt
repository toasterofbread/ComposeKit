package dev.toastbits.composekit.settings.ui.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.platform.composable.ScrollBarLazyColumn
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.settings.ComposeKitSettingsGroup
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.kmpresources.library.model.KmpStringResource

data class PlatformSettingsGroupScreen(val group: ComposeKitSettingsGroup): Screen {
    override val title: KmpStringResource get() = group.title

    private val settingsItems: List<SettingsItem> by lazy { group.getConfigurationItems() }

    override fun onClosed(movingBackward: Boolean) {
        for (item in settingsItems) {
            item.resetUiState()
        }
    }

    @Composable
    override fun Content(modifier: Modifier, contentPadding: PaddingValues) {
        ScrollBarLazyColumn(
            modifier,
            contentPadding = contentPadding,
            verticalArrangement = Arrangement.spacedBy(20.dp)
        ) {
            items(settingsItems) { item ->
                item.Item(Modifier.fillMaxWidth())
            }
        }
    }
}
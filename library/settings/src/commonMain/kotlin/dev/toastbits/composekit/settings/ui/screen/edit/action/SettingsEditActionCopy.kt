package dev.toastbits.composekit.settings.ui.screen.edit.action

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ContentCopy
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import dev.toastbits.composekit.components.LocalContext
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.settings.generated.resources.Res
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_action_copy
import dev.toastbits.composekit.settings.PlatformSettings
import org.jetbrains.compose.resources.stringResource

internal object SettingsEditActionCopy: IconButtonSettingsEditAction() {
    override val readableName: String
        @Composable
        get() = stringResource(Res.string.preferences_edit_action_copy)

    override val icon: ImageVector
        @Composable
        get() = Icons.Default.ContentCopy

    @Composable
    override fun canDisplay(): Boolean =
        LocalContext.current.canCopyText()

    override suspend fun execute(
        input: String,
        context: PlatformContext,
        settings: PlatformSettings
    ): String? {
        context.copyText(input)
        return null
    }
}

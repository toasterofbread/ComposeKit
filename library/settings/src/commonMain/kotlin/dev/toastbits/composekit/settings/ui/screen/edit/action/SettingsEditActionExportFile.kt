package dev.toastbits.composekit.settings.ui.screen.edit.action

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.FileDownload
import androidx.compose.material.icons.filled.SaveAlt
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.settings.generated.resources.Res
import dev.toastbits.composekit.settings.generated.resources.preferences_edit_action_export_file
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.util.platform.Platform
import org.jetbrains.compose.resources.stringResource

internal object SettingsEditActionExportFile: IconButtonSettingsEditAction() {
    override val readableName: String
        @Composable
        get() = stringResource(Res.string.preferences_edit_action_export_file)

    override val icon: ImageVector
        @Composable
        get() = when (Platform.current) {
            Platform.ANDROID,
            Platform.DESKTOP -> Icons.Default.SaveAlt
            Platform.WEB -> Icons.Default.FileDownload
        }

    override suspend fun execute(
        input: String,
        context: PlatformContext,
        settings: PlatformSettings
    ): String? {
        context.promptUserForDirectFileCreation(
            "application/json",
            "json",
            "settings",
            input
        )

        return null
    }
}

package dev.toastbits.composekit.settings

import android.content.SharedPreferences

actual fun interface PlatformSettingsListener: SharedPreferences.OnSharedPreferenceChangeListener {
    override fun onSharedPreferenceChanged(prefs: SharedPreferences, key: String?) {
        if (key != null) {
            onChanged(key)
        }
    }
    actual fun onChanged(key: String)
}

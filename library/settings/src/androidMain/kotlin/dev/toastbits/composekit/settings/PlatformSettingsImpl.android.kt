package dev.toastbits.composekit.settings

import android.content.Context
import android.content.SharedPreferences
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsEditor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json

actual class PlatformSettingsImpl private constructor(
    private val prefs: SharedPreferences,
    actual override val json: Json
): PlatformSettings {
    actual companion object {
        private var instance: PlatformSettings? = null

        fun getInstance(context: Context, json: Json = createDefaultJson()): PlatformSettings =
            getInstance(
                context.getSharedPreferences(
                    "dev.toastbits.composekit.PREFERENCES",
                    Context.MODE_PRIVATE
                ),
                json
            )

        fun getInstance(prefs: SharedPreferences, json: Json = createDefaultJson()): PlatformSettings {
            if (instance == null) {
                instance = PlatformSettingsImpl(prefs, json)
            }
            return instance!!
        }
    }

    actual override val readableStoragePath: String? = null
    actual override val storageUri: String? = null

    actual override fun getString(key: String, default_value: String?): String? =
        prefs.getString(key, default_value)

    actual override fun getStringSet(key: String, default_values: Set<String>?): Set<String>? =
        prefs.getStringSet(key, default_values)

    actual override fun getInt(key: String, default_value: Int?): Int? {
        if (!prefs.contains(key)) {
            return default_value
        }
        return prefs.getInt(key, 0)
    }

    actual override fun getLong(key: String, default_value: Long?): Long? {
        if (!prefs.contains(key)) {
            return default_value
        }
        return prefs.getLong(key, 0)
    }

    actual override fun getFloat(key: String, default_value: Float?): Float? {
        if (!prefs.contains(key)) {
            return default_value
        }
        return prefs.getFloat(key, 0f)
    }

    actual override fun getBoolean(key: String, default_value: Boolean?): Boolean? {
        if (!prefs.contains(key)) {
            return default_value
        }
        return prefs.getBoolean(key, false)
    }

    actual override fun <T> getSerialisable(key: String, default_value: T, serialiser: KSerializer<T>, json: Json): T {
        val data: String = prefs.getString(key, null) ?: return default_value
        try {
            return json.decodeFromString(serialiser, data)
        }
        catch (e: Throwable) {
            throw RuntimeException("Deserialising prefs key '$key' with value '$data' failed", e)
        }
    }

    actual override operator fun contains(key: String): Boolean = prefs.contains(key)

    actual override fun addListener(listener: PlatformSettingsListener): PlatformSettingsListener {
        prefs.registerOnSharedPreferenceChangeListener(listener as SharedPreferences.OnSharedPreferenceChangeListener)
        return listener
    }

    actual override fun removeListener(listener: PlatformSettingsListener) {
        prefs.unregisterOnSharedPreferenceChangeListener(listener as SharedPreferences.OnSharedPreferenceChangeListener)
    }

    actual override suspend fun edit(action: suspend PlatformSettingsEditor.() -> Unit): Unit =
        withContext(Dispatchers.IO) {
            prefs.edit().apply {
                action(EditorImpl(this))
                commit()
            }
        }

    actual inner class EditorImpl(private val upstream: SharedPreferences.Editor): PlatformSettingsEditor {
        actual override val json: Json get() = this@PlatformSettingsImpl.json

        actual override fun putString(key: String, value: String): PlatformSettingsEditor {
            upstream.putString(key, value)
            return this
        }

        actual override fun putStringSet(
            key: String,
            values: Set<String>
        ): PlatformSettingsEditor {
            upstream.putStringSet(key, values)
            return this
        }

        actual override fun putInt(key: String, value: Int): PlatformSettingsEditor {
            upstream.putInt(key, value)
            return this
        }

        actual override fun putLong(key: String, value: Long): PlatformSettingsEditor {
            upstream.putLong(key, value)
            return this
        }

        actual override fun putFloat(key: String, value: Float): PlatformSettingsEditor {
            upstream.putFloat(key, value)
            return this
        }

        actual override fun putBoolean(key: String, value: Boolean): PlatformSettingsEditor {
            upstream.putBoolean(key, value)
            return this
        }

        actual override fun <T> putSerialisable(key: String, value: T, serialiser: KSerializer<T>, json: Json): PlatformSettingsEditor {
            upstream.putString(key, json.encodeToString(serialiser, value))
            return this
        }

        actual override fun remove(key: String): PlatformSettingsEditor {
            upstream.remove(key)
            return this
        }

        actual override fun clear(): PlatformSettingsEditor {
            upstream.clear()
            return this
        }
    }
}

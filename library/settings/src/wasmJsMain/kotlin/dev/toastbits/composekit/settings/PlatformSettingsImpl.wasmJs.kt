package dev.toastbits.composekit.settings

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsEditor
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json

actual class PlatformSettingsImpl(actual override val json: Json): PlatformSettings {
    actual override val readableStoragePath: String? = null
    actual override val storageUri: String? = null

    actual override fun addListener(listener: PlatformSettingsListener): PlatformSettingsListener {
        return listener
    }

    actual override fun removeListener(listener: PlatformSettingsListener) {}

    actual override fun getString(
        key: String,
        default_value: String?
    ): String? = default_value

    actual override fun getStringSet(
        key: String,
        default_values: Set<String>?
    ): Set<String>? = default_values

    actual override fun getInt(key: String, default_value: Int?): Int? = default_value

    actual override fun getLong(key: String, default_value: Long?): Long? = default_value

    actual override fun getFloat(key: String, default_value: Float?): Float? = default_value

    actual override fun getBoolean(
        key: String,
        default_value: Boolean?
    ): Boolean? = default_value

    actual override fun <T> getSerialisable(
        key: String,
        default_value: T,
        serialiser: KSerializer<T>,
        json: Json
    ): T = default_value

    actual override fun contains(key: String): Boolean = false

    actual override suspend fun edit(action: suspend PlatformSettingsEditor.() -> Unit) {}

    actual companion object {}

    actual inner class EditorImpl : PlatformSettingsEditor {
        actual override val json: Json get() = this@PlatformSettingsImpl.json

        actual override fun putString(
            key: String,
            value: String
        ): PlatformSettingsEditor = this

        actual override fun putStringSet(
            key: String,
            values: Set<String>
        ): PlatformSettingsEditor = this

        actual override fun putInt(
            key: String,
            value: Int
        ): PlatformSettingsEditor = this

        actual override fun putLong(
            key: String,
            value: Long
        ): PlatformSettingsEditor = this

        actual override fun putFloat(
            key: String,
            value: Float
        ): PlatformSettingsEditor = this

        actual override fun putBoolean(
            key: String,
            value: Boolean
        ): PlatformSettingsEditor = this

        actual override fun <T> putSerialisable(
            key: String,
            value: T,
            serialiser: KSerializer<T>,
            json: Json
        ): PlatformSettingsEditor = this

        actual override fun remove(key: String): PlatformSettingsEditor = this

        actual override fun clear(): PlatformSettingsEditor = this
    }
}

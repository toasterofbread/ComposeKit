package dev.toastbits.composekit.settings.cookies

import dev.toastbits.composekit.settings.InMemoryPlatformSettings
import dev.toastbits.composekit.settings.PlatformSettingsImpl
import dev.toastbits.composekit.settings.createDefaultJson
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement

class CookiesPlatformSettings(
    private val cookies: Cookies,
    json: Json = PlatformSettingsImpl.createDefaultJson()
): InMemoryPlatformSettings(json) {
    override val readableStoragePath: String? = null

    override val data: MutableMap<String, JsonElement> =
        mutableMapOf<String, JsonElement>().also { map ->
            for ((key, value) in cookies) {
                val decoded: JsonElement =
                    try {
                        json.decodeFromString(value)
                    }
                    catch (e: Throwable) {
                        RuntimeException("Decoding cookie ($key=$value) failed, ignoring", e).printStackTrace()
                        continue
                    }

                map[key] = decoded
            }
        }

    override fun onKeyChanged(key: String) {
        super.onKeyChanged(key)
        cookies[key] = json.encodeToString(data[key])
    }
}

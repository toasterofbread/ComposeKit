package dev.toastbits.composekit.settings.cookies.model

data class Cookie(val key: String, val value: String)

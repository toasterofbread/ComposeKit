package dev.toastbits.composekit.settings.cookies

import dev.toastbits.composekit.settings.cookies.model.Cookie

interface Cookies: Iterable<Cookie> {
    operator fun get(key: String): String?
    operator fun set(key: String, value: String)
}

package dev.toastbits.composekit.settings.cookies

import dev.toastbits.composekit.settings.cookies.model.Cookie
import kotlinx.browser.document
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.datetime.format
import kotlinx.datetime.format.DateTimeComponents
import kotlin.time.Duration.Companion.days

private external fun encodeURIComponent(uriComponent: String): String
private external fun decodeURIComponent(encodedURI: String): String

class BrowserCookies(
    private val getExpiry: () -> Instant? = {
        Clock.System.now().plus(3650.days)
    }
): Cookies {
    override fun iterator(): Iterator<Cookie> =
        document.cookie.splitToSequence(';').mapNotNull {
            if (it.isBlank()) {
                return@mapNotNull null
            }

            val cookie: String = it.trim()

            val split: Int = cookie.indexOf('=')
            if (split == -1) {
                RuntimeException("Ignoring malformed cookie '$cookie'").printStackTrace()
                return@mapNotNull null
            }

            val value: String = cookie.substring(split + 1).decode()
            if (value == "null") {
                return@mapNotNull null
            }

            return@mapNotNull Cookie(
                key = cookie.substring(0, split).decode(),
                value = cookie.substring(split + 1).decode()
            )
        }.iterator()

    override operator fun get(key: String): String? =
        firstOrNull { it.key == key }?.value

    override operator fun set(key: String, value: String) {
        document.cookie =
            buildString {
                append("${key.encode()}=${value.encode()}; SameSite=Strict; path=/")

                val expiryDate: String? = getExpiry()?.format(DateTimeComponents.Formats.RFC_1123)
                if (expiryDate != null) {
                    append("; expires=$expiryDate")
                }
            }
    }

    private fun String.encode(): String = encodeURIComponent(this)
    private fun String.decode(): String = decodeURIComponent(this)
}

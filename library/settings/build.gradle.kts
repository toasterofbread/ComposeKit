import util.configureAllComposeTargets

plugins {
    id("android-library-conventions")
    id("compose-conventions")
    id("gitlab-publishing-conventions")

    alias(libs.plugins.kotlinx.serialization)
}

kotlin {
    configureAllComposeTargets()

    sourceSets {
        commonMain {
            dependencies {
                api(libs.kotlinx.serialization.json)
                api(projects.library.settingsitem.domain)
                implementation(projects.library.context)
                implementation(projects.library.util)
                implementation(projects.library.utilKt)
                implementation(projects.library.theme.core)
                implementation(projects.library.navigation)
                implementation(projects.library.components)
                implementation(projects.library.file)
            }
        }

        val jvmMain by getting {
            dependencies {
            }
        }

        val androidMain by getting {
            dependencies {
            }
        }

        val wasmJsMain by getting {
            dependencies {
                api(libs.kotlinx.datetime)
            }
        }
    }
}

import util.configureAllKmpTargets

plugins {
    id("compose-noncommon-conventions")
    id("android-library-conventions")
    id("gitlab-publishing-conventions")
}

kotlin {
    configureAllKmpTargets()

    sourceSets {
        commonMain {
            dependencies {
                api(projects.library.file)
                implementation(libs.kotlinx.coroutines.core)
            }
        }

        val composeMain by getting {
            dependencies {
                implementation(projects.library.util)
            }
        }

        val jvmMain by getting {
            dependencies {
                implementation(projects.library.filechooser)
                implementation(libs.two.slices)
            }
        }

        val androidMain by getting {
            dependencies {
                api(libs.androidx.activity.compose)
                implementation(libs.storage)
            }
        }
    }
}

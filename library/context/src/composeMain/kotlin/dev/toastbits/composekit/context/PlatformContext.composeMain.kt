package dev.toastbits.composekit.context

import androidx.compose.material3.ColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

@Composable
expect fun PlatformContext.isSystemInDarkTheme(): Boolean?

expect fun PlatformContext.setStatusBarColour(colour: Color?)

expect fun PlatformContext.setNavigationBarColour(colour: Color?)

expect fun PlatformContext.getLightColorScheme(): ColorScheme

expect fun PlatformContext.getDarkColorScheme(): ColorScheme

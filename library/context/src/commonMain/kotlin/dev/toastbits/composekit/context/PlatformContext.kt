package dev.toastbits.composekit.context

import kotlinx.coroutines.CoroutineScope

expect class PlatformContext {
    val coroutineScope: CoroutineScope

    fun getFilesDir(): PlatformFile?
    fun getCacheDir(): PlatformFile?

    suspend fun promptUserForDirectory(persist: Boolean = false): PlatformFile?
    suspend fun promptUserForFile(mimeTypes: Set<String>, persist: Boolean = false): PlatformFile?
    suspend fun promptUserForFileContent(mimeTypes: Set<String>): String?
    suspend fun promptUserForFileCreation(
        mimeType: String,
        filenameSuggestion: String?,
        persist: Boolean = false
    ): PlatformFile?

    suspend fun promptUserForDirectFileCreation(
        mimeType: String,
        extension: String,
        filename: String,
        content: String
    ): Boolean

    fun getUserDirectoryFile(uri: String): PlatformFile?

    fun isAppInForeground(): Boolean

    fun isDisplayingAboveNavigationBar(): Boolean

    fun canShare(): Boolean
    fun shareText(text: String, title: String? = null)

    fun canOpenUrl(): Boolean
    fun openUrl(url: String)

    fun canCopyText(): Boolean
    fun copyText(text: String)

    fun canPasteText(): Boolean
    suspend fun pasteText(): String?

    fun canSendNotifications(): Boolean
    fun sendNotification(title: String, body: String)
    fun sendNotification(throwable: Throwable)

    fun sendToast(text: String, long: Boolean = false)

    fun vibrate(duration: Double)

    fun isConnectionMetered(): Boolean
}

fun PlatformContext.vibrateShort() {
    vibrate(0.01)
}

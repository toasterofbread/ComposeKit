package dev.toastbits.composekit.context

import kotlinx.coroutines.CoroutineScope

actual fun createPlatformContext(
    appName: String,
    coroutineScope: CoroutineScope
) = PlatformContext(coroutineScope)

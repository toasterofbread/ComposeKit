package dev.toastbits.composekit.context

import androidx.compose.material3.ColorScheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import com.sshtools.twoslices.Toast
import com.sshtools.twoslices.ToastType
import dev.toastbits.composekit.filechooser.DesktopFileChooser
import dev.toastbits.composekit.filechooser.NativeFileChooserCallback
import dev.toastbits.composekit.filechooser.NativeFileChooserConfiguration
import dev.toastbits.composekit.filechooser.NativeFileChooserIntent
import dev.toastbits.composekit.util.platform.getEnv
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okio.buffer
import okio.use
import org.jetbrains.skiko.OS
import org.jetbrains.skiko.hostOs
import java.awt.Desktop
import java.awt.Toolkit
import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.DataFlavor
import java.awt.datatransfer.StringSelection
import java.io.File
import java.net.URI
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

actual open class PlatformContext(
    private val app_name: String,
    actual val coroutineScope: CoroutineScope
) {
    open suspend fun getIconImageData(): ByteArray? = null

    private val file_chooser: DesktopFileChooser = DesktopFileChooser()
    private fun getFileChooserConfiguration(): NativeFileChooserConfiguration =
        NativeFileChooserConfiguration().apply {
            directory = PlatformFile(getHomeDir())
        }

    actual fun getFilesDir(): PlatformFile? =
        PlatformFile.fromFile(getDesktopFilesDir(app_name), this)

    actual fun getCacheDir(): PlatformFile? {
        val subdir: String = when (hostOs) {
            OS.Linux -> ".cache"
            OS.Windows -> return (getFilesDir() ?: return null).resolve("cache")
            else -> throw NotImplementedError(hostOs.name)
        }

        val file: File = getHomeDir().resolve(subdir).resolve(app_name.lowercase())
        return PlatformFile.fromFile(file, this)
    }

    private fun getTempDir(): File {
        val dir: File = when (hostOs) {
            OS.Linux -> File("/tmp")
            OS.Windows -> getHomeDir().resolve("AppData/Local/Temp")
            else -> throw NotImplementedError(hostOs.name)
        }
        return dir.resolve(app_name.lowercase())
    }

    actual fun isAppInForeground(): Boolean = true // TODO

    actual fun canOpenUrl(): Boolean =
        Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)

    actual fun openUrl(url: String) {
        check(canOpenUrl())
        Desktop.getDesktop().browse(URI(url))
    }

    actual fun canCopyText(): Boolean = true
    actual fun copyText(text: String) {
        val clipboard: Clipboard = Toolkit.getDefaultToolkit().systemClipboard
        clipboard.setContents(StringSelection(text), null)
    }

    actual fun canPasteText(): Boolean = true
    actual suspend fun pasteText(): String? = withContext(Dispatchers.IO) {
        val clipboard: Clipboard = Toolkit.getDefaultToolkit().systemClipboard
        return@withContext clipboard.getContents(null).getTransferData(DataFlavor.stringFlavor) as String
    }

    actual fun canSendNotifications(): Boolean = true

    actual fun sendNotification(title: String, body: String) {
        val icon_path: String = getIconFile()?.absolutePath ?: ""
        Toast.toast(ToastType.INFO, icon_path, title, body)
    }

    actual fun sendNotification(throwable: Throwable) {
        throwable.printStackTrace()
        sendNotification(throwable::class.simpleName ?: "Exception", throwable.stackTraceToString())
    }

    actual fun sendToast(text: String, long: Boolean) {
        sendNotification(app_name, text)
    }

    actual suspend fun promptUserForDirectory(
        persist: Boolean
    ): PlatformFile? = withContext(Dispatchers.Main) {
        suspendCoroutine { continuation ->
            val configuration: NativeFileChooserConfiguration = getFileChooserConfiguration()
            configuration.intent = NativeFileChooserIntent.SAVE

            // Not perfect, but as close as I can get with the native dialog
            file_chooser.chooseFile(
                configuration,
                createFileChooserCallback(continuation),
                { dir: File, file: String ->
                    dir.resolve(file).isDirectory
                }
            )
        }
    }

    actual suspend fun promptUserForFile(
        mimeTypes: Set<String>,
        persist: Boolean
    ): PlatformFile? = withContext(Dispatchers.Main) {
        suspendCoroutine { continuation ->
            file_chooser.chooseFile(
                getFileChooserConfiguration(),
                createFileChooserCallback(continuation)
            )
        }
    }

    actual suspend fun promptUserForFileContent(mimeTypes: Set<String>): String? =
        promptUserForFile(mimeTypes, false)?.inputStream()?.buffer()?.use {
            it.readUtf8()
        }

    actual suspend fun promptUserForFileCreation(
        mimeType: String,
        filenameSuggestion: String?,
        persist: Boolean
    ): PlatformFile? = withContext(Dispatchers.Main) {
        suspendCoroutine { continuation ->
            val configuration: NativeFileChooserConfiguration = getFileChooserConfiguration()
            configuration.intent = NativeFileChooserIntent.SAVE
            configuration.mimeFilter = mimeType
            file_chooser.chooseFile(configuration, createFileChooserCallback(continuation))
        }
    }

    actual suspend fun promptUserForDirectFileCreation(
        mimeType: String,
        extension: String,
        filename: String,
        content: String
    ): Boolean {
        val file: PlatformFile =
            promptUserForFileCreation(mimeType, filename, false)
                ?: return false

        file.outputStream().buffer().use {
            it.writeUtf8(content)
            it.flush()
        }

        return true
    }

    actual fun getUserDirectoryFile(uri: String): PlatformFile? {
        return PlatformFile(File(uri))
    }

    // Unsupported and/or too much work
    actual fun isConnectionMetered(): Boolean = false
    actual fun isDisplayingAboveNavigationBar(): Boolean = false
    actual fun vibrate(duration: Double) {}
    actual fun canShare(): Boolean = false
    actual fun shareText(text: String, title: String?): Unit = throw NotImplementedError()

    @Suppress("NewApi")
    private fun getIconFile(): File? {
        val file: File = getTempDir().resolve("ic_$app_name.png")
        if (!file.isFile) {
            val image_data: ByteArray =
                runBlocking { getIconImageData() } ?: return null

            file.parentFile.mkdirs()
            file.writeBytes(image_data)
        }
        return file
    }

    private fun createFileChooserCallback(continuation: Continuation<PlatformFile?>) =
        object : NativeFileChooserCallback {
            override fun onFileChosen(file: PlatformFile) {
                continuation.resume(file)
            }

            override fun onCancellation() {
                continuation.resume(null)
            }

            override fun onError(exception: Exception?) {
                exception?.printStackTrace()
                continuation.resume(null)
            }
        }
}

private fun getHomeDir(): File = File(System.getProperty("user.home"))

actual fun PlatformFile.Companion.fromFile(file: File, context: PlatformContext): PlatformFile =
    PlatformFile(file)

fun getDesktopFilesDir(app_name: String): File {
    val subdir: String =
        when (hostOs) {
            OS.Linux -> ".local/share"
            OS.Windows -> "AppData/Local/"
            else -> throw NotImplementedError(hostOs.name)
        }

    return getHomeDir().resolve(subdir).resolve(app_name.lowercase())
}

@Composable
actual fun PlatformContext.isSystemInDarkTheme(): Boolean? = remember {
    val gtkTheme: String = getEnv("GTK_THEME")?.lowercase().orEmpty()
    if (gtkTheme.contains("dark")) {
        return@remember true
    }
    else if (gtkTheme.contains("light")) {
        return@remember false
    }
    return@remember null
}

actual fun PlatformContext.getLightColorScheme(): ColorScheme = lightColorScheme()

actual fun PlatformContext.getDarkColorScheme(): ColorScheme = darkColorScheme()

actual fun PlatformContext.setNavigationBarColour(colour: Color?) {}

actual fun PlatformContext.setStatusBarColour(colour: Color?) {}

actual fun createPlatformContext(
    appName: String,
    coroutineScope: CoroutineScope
) = PlatformContext(appName, coroutineScope)

package dev.toastbits.composekit.context

import kotlinx.coroutines.CoroutineScope

expect fun createPlatformContext(appName: String, coroutineScope: CoroutineScope): PlatformContext

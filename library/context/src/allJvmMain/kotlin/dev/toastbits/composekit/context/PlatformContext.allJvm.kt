package dev.toastbits.composekit.context

import java.io.File

expect fun PlatformFile.Companion.fromFile(file: File, context: PlatformContext): PlatformFile

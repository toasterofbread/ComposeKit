package dev.toastbits.composekit.context

import kotlinx.coroutines.CoroutineScope

actual class PlatformContext(
    actual val coroutineScope: CoroutineScope
) {
    actual fun getFilesDir(): PlatformFile? = null
    actual fun getCacheDir(): PlatformFile? = null

    actual suspend fun promptUserForDirectory(persist: Boolean): PlatformFile? = null

    actual suspend fun promptUserForFile(
        mimeTypes: Set<String>,
        persist: Boolean
    ): PlatformFile? = null

    actual suspend fun promptUserForFileContent(mimeTypes: Set<String>): String? = null

    actual suspend fun promptUserForFileCreation(
        mimeType: String,
        filenameSuggestion: String?,
        persist: Boolean
    ): PlatformFile? = null

    actual suspend fun promptUserForDirectFileCreation(
        mimeType: String,
        extension: String,
        filename: String,
        content: String
    ): Boolean = false

    actual fun getUserDirectoryFile(uri: String): PlatformFile? = null

    actual fun isAppInForeground(): Boolean = true

    actual fun isDisplayingAboveNavigationBar(): Boolean = false

    actual fun canShare(): Boolean = false

    actual fun shareText(text: String, title: String?): Unit = throw IllegalStateException()

    actual fun canOpenUrl(): Boolean = false

    actual fun openUrl(url: String): Unit = throw IllegalStateException()

    actual fun canCopyText(): Boolean = false

    actual fun copyText(text: String): Unit = throw IllegalStateException()

    actual fun canPasteText(): Boolean = false

    actual suspend fun pasteText(): String? = throw IllegalStateException()

    actual fun canSendNotifications(): Boolean = false

    actual fun sendNotification(title: String, body: String): Unit = throw IllegalStateException()

    actual fun sendNotification(throwable: Throwable): Unit = throw IllegalStateException()

    actual fun sendToast(text: String, long: Boolean) {}

    actual fun vibrate(duration: Double) {}

    actual fun isConnectionMetered(): Boolean = false
}

package dev.toastbits.composekit.context

import androidx.compose.material3.ColorScheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.await
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLInputElement
import org.w3c.files.File
import org.w3c.files.FileReader
import org.w3c.files.get
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.math.roundToInt

actual open class PlatformContext(
    actual val coroutineScope: CoroutineScope
) {
    actual fun getFilesDir(): PlatformFile? = null
    actual fun getCacheDir(): PlatformFile? = null

    actual suspend fun promptUserForDirectory(
        persist: Boolean
    ): PlatformFile? = null

    actual suspend fun promptUserForFile(
        mimeTypes: Set<String>,
        persist: Boolean
    ): PlatformFile? = null

    actual suspend fun promptUserForFileContent(
        mimeTypes: Set<String>
    ): String? = suspendCoroutine { continuation ->
        val input: HTMLInputElement = document.createElement("input") as HTMLInputElement
        input.setAttribute("type", "file")
        input.click()

        input.onchange = {
            val file: File? = input.files?.get(0)
            if (file == null) {
                continuation.resume(null)
            }
            else {
                val reader: FileReader = FileReader()
                reader.onload = {
                    continuation.resume(reader.result?.toString())
                }
                reader.onabort = {
                    continuation.resume(null)
                }
                reader.readAsText(file)
            }
        }

        input.oncancel = {
            continuation.resume(null)
        }
    }

    actual suspend fun promptUserForFileCreation(
        mimeType: String,
        filenameSuggestion: String?,
        persist: Boolean
    ): PlatformFile? = null

    actual suspend fun promptUserForDirectFileCreation(
        mimeType: String,
        extension: String,
        filename: String,
        content: String
    ): Boolean {
        val a: HTMLElement = document.createElement("a") as HTMLElement
        a.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(content))
        a.setAttribute("download", "$filename.$extension")
        a.setAttribute("style", "display:none;")
        a.click()
        return true
    }

    actual fun getUserDirectoryFile(uri: String): PlatformFile? = null

    actual fun isAppInForeground(): Boolean {
        TODO("Not yet implemented")
    }

    actual fun isDisplayingAboveNavigationBar(): Boolean = false

    actual fun canShare(): Boolean = false

    actual fun shareText(text: String, title: String?) {
        throw IllegalStateException()
    }

    actual fun canOpenUrl(): Boolean = true

    actual fun openUrl(url: String) {
        window.open(url, "_blank")?.focus()
    }

    actual fun canCopyText(): Boolean = true

    actual fun copyText(text: String) {
        window.navigator.clipboard.writeText(text)
    }

    actual fun canPasteText(): Boolean = true

    actual suspend fun pasteText(): String? =
        window.navigator.clipboard.readText().await<JsString?>()?.toString()?.ifEmpty { null }

    actual fun canSendNotifications(): Boolean = true

    actual fun sendNotification(title: String, body: String) {
        window.alert("$title - $body")
    }

    actual fun sendNotification(throwable: Throwable) {
        sendNotification(throwable::class.simpleName ?: "Error", throwable.message ?: "")
    }

    actual fun sendToast(text: String, long: Boolean) {
        sendNotification(text, "")
    }

    actual fun vibrate(duration: Double) {
        window.navigator.vibrate((duration * 1000).roundToInt())
    }

    actual fun isConnectionMetered(): Boolean = false
}

private external fun encodeURIComponent(uriComponent: String): String

actual fun PlatformContext.setStatusBarColour(colour: Color?) {}

actual fun PlatformContext.setNavigationBarColour(colour: Color?) {}

@Composable
actual fun PlatformContext.isSystemInDarkTheme(): Boolean? =
    window.matchMedia("(prefers-color-scheme: dark)").matches

actual fun PlatformContext.getLightColorScheme(): ColorScheme = lightColorScheme()

actual fun PlatformContext.getDarkColorScheme(): ColorScheme = darkColorScheme()

actual fun createPlatformContext(
    appName: String,
    coroutineScope: CoroutineScope
) = PlatformContext(coroutineScope)

import util.configureAllComposeTargets

plugins {
    id("android-library-conventions")
    id("compose-conventions")
    id("gitlab-publishing-conventions")

    alias(libs.plugins.kotlinx.serialization)
}

kotlin {
    configureAllComposeTargets()

    sourceSets {
        commonMain {
            dependencies {
                implementation(projects.library.context)
                implementation(projects.library.util)
                implementation(projects.library.utilKt)
                implementation(projects.library.settingsitem.domain)

                implementation(libs.kotlinx.serialization.json)
                api(libs.catppuccin.kotlin)
            }
        }
    }
}

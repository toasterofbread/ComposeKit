package dev.toastbits.composekit.theme.core.provider

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.ThemeValuesData

interface ThemeProvider {
    @Composable
    fun rememberBuiltInSystemTheme(): ThemeValuesData

    @Composable
    fun rememberIsSystemInDarkMode(): Boolean?

    fun getCustomThemes(): List<NamedTheme>
    fun getCustomTheme(index: Int): SerialisableTheme?
}

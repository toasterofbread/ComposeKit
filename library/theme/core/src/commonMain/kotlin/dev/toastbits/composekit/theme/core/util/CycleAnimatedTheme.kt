package dev.toastbits.composekit.theme.core.util

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.graphics.Color
import dev.toastbits.composekit.theme.core.ThemeValues
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

abstract class CycleAnimatedTheme: BaseAnimatedTheme() {
    private val mutex: Mutex = Mutex()

    open val animationSpec: AnimationSpec<Color> = tween(1000)

    @Composable
    abstract fun rememberCycleAnimationThemes(): List<ThemeValues>

    @Composable
    override fun Update() {
        val currentThemes: List<ThemeValues> = rememberCycleAnimationThemes()

        LaunchedEffect(mutex, currentThemes, animationSpec) {
            if (currentThemes.isEmpty()) {
                return@LaunchedEffect
            }
            mutex.withLock {
                var i: Int = 0
                while (true) {
                    updateColours(currentThemes[i], animationSpec)
                    i = (i + 1) % currentThemes.size
                }
            }
        }
    }
}

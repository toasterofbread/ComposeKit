package dev.toastbits.composekit.theme.core.provider

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.context.getDarkColorScheme
import dev.toastbits.composekit.context.getLightColorScheme
import dev.toastbits.composekit.context.isSystemInDarkTheme
import dev.toastbits.composekit.theme.core.model.ThemeValuesData

abstract class ContextThemeProvider(private val context: PlatformContext): ThemeProvider {
    @Composable
    override fun rememberBuiltInSystemTheme(): ThemeValuesData {
        val localDarkTheme: Boolean = context.isSystemInDarkTheme() ?: true
        return remember(localDarkTheme) {
            ThemeValuesData.fromColourScheme(
                if (localDarkTheme) context.getDarkColorScheme()
                else context.getLightColorScheme()
            )
        }
    }

    @Composable
    override fun rememberIsSystemInDarkMode(): Boolean? =
        context.isSystemInDarkTheme()
}

package dev.toastbits.composekit.theme.core

import androidx.compose.ui.graphics.Color

interface ThemeManager: ThemeValues {
    fun onContextualColourChanged(thumbnailColour: Color?)
}

package dev.toastbits.composekit.theme.core.provider

import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.type.ThemeTypeConfig

interface ThemeTypeConfigProvider {
    fun <T: ThemeValues> getConfig(type: ThemeType<T>): ThemeTypeConfig<T>
}

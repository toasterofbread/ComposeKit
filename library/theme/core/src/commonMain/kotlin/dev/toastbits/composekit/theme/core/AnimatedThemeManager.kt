package dev.toastbits.composekit.theme.core

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import dev.toastbits.composekit.theme.core.model.AnimatedThemeValues
import dev.toastbits.composekit.theme.core.model.ThemeValuesData

open class AnimatedThemeManager(initialTheme: ThemeValues): ThemeManager, AnimatedThemeValues(initialTheme){
    private var currentTheme: ThemeValues by mutableStateOf(initialTheme)

    private var themeSet: Boolean = false
    private var fallbackTheme: ThemeValues? by mutableStateOf(null)

    // TODO | This should be moved to SpMp or removed
    private var currentContextualColour: Color? by mutableStateOf(null)

    override val background: Color
        get() =
        currentTheme.let { current ->
            if (current.isAnimated && current.isInitialised) current.background
            else if (isInitialised) super.background
            else fallbackTheme?.background ?: super.background
        }
    override val onBackground: Color
        get() =
        currentTheme.let { current ->
            if (current.isAnimated && current.isInitialised) current.onBackground
            else if (isInitialised) super.onBackground
            else fallbackTheme?.onBackground ?: super.onBackground
        }
    override val card: Color
        get() =
        currentTheme.let { current ->
            if (current.isAnimated && current.isInitialised) current.card
            else if (isInitialised) super.card
            else fallbackTheme?.card ?: super.card
        }
    override val accent: Color
        get() =
        currentTheme.let { current ->
            if (current.isAnimated && current.isInitialised) current.accent
            else if (isInitialised) super.accent
            else fallbackTheme?.accent ?: super.accent
        }
    override val error: Color
        get() =
        currentTheme.let { current ->
            if (current.isAnimated && current.isInitialised) current.error
            else if (isInitialised) super.error
            else fallbackTheme?.error ?: super.error
        }

    open fun selectAccentColour(values: ThemeValues, contextualColour: Color?): Color =
        contextualColour ?: values.accent

    protected fun setTheme(newTheme: ThemeValues) {
        if (themeSet) {
            fallbackTheme = currentTheme
        }
        else {
            themeSet = true
        }
        currentTheme = newTheme
    }

    override fun onContextualColourChanged(thumbnailColour: Color?) {
        currentContextualColour = thumbnailColour
    }

    @Composable
    override fun Update() {
        currentTheme.Update()

        LaunchedEffect(currentTheme, currentTheme.key, currentContextualColour) {
            updateColours()
        }
    }

    private suspend fun updateColours() {
        if (currentTheme.isAnimated) {
            return
        }

        val accentColour: Color = selectAccentColour(currentTheme, currentContextualColour)
        val themeValues: ThemeValues = ThemeValuesData.of(currentTheme).copy(accent = accentColour)
        updateColours(themeValues)
    }

    override fun toString(): String =
        "ThemeManager(currentTheme=$currentTheme)"
}

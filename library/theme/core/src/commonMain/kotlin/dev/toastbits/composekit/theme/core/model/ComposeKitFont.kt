package dev.toastbits.composekit.theme.core.model

import androidx.compose.runtime.Composable
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.ZenMaruGothic_Medium
import dev.toastbits.composekit.theme.core.generated.resources.`font_default_name_$default`
import dev.toastbits.composekit.theme.core.generated.resources.font_system_name
import dev.toastbits.composekit.theme.core.generated.resources.font_zen_maru_gothic_name
import kotlinx.serialization.Serializable
import org.jetbrains.compose.resources.Font
import org.jetbrains.compose.resources.FontResource
import org.jetbrains.compose.resources.stringResource

@Serializable
sealed interface ComposeKitFont {
    @Composable
    fun rememberFontFamily(): FontFamily

    @Serializable
    data object Default: ComposeKitFont {
        val font: ComposeKitFont = BuiltIn.ZEN_MARU_GOTHIC

        @Composable
        override fun rememberFontFamily(): FontFamily =
            font.rememberFontFamily()
    }

    @Serializable
    data object System: ComposeKitFont {
        @Composable
        override fun rememberFontFamily(): FontFamily = FontFamily.Default
    }

    @Serializable
    enum class BuiltIn(val fontResource: FontResource): ComposeKitFont, ResolvableFont {
        ZEN_MARU_GOTHIC(Res.font.ZenMaruGothic_Medium);

        @Composable
        override fun rememberFont(): Font =
            Font(fontResource)

        @Composable
        override fun rememberFontFamily(): FontFamily =
            FontFamily(rememberFont())
    }

    @Serializable
    data class Composite(val fonts: List<ResolvableFont>): ComposeKitFont {
        @Composable
        override fun rememberFontFamily(): FontFamily =
            FontFamily(fonts.map { it.rememberFont() })
    }

    interface ResolvableFont: ComposeKitFont {
        @Composable
        fun rememberFont(): Font
    }

    @Composable
    @Suppress("SimplifiableCallChain")
    fun getDisplayName(): String =
        when (this) {
            Default ->
                stringResource(Res.string.`font_default_name_$default`)
                    .replace("\$default", Default.font.getDisplayName())
            System -> stringResource(Res.string.font_system_name)
            BuiltIn.ZEN_MARU_GOTHIC -> stringResource(Res.string.font_zen_maru_gothic_name)
            is Composite -> fonts.map { it.getDisplayName() }.joinToString(" + ")
            is ResolvableFont -> throw IllegalStateException(this::class.toString())
        }

    companion object {
        val DEFAULT: ComposeKitFont = Default
    }
}

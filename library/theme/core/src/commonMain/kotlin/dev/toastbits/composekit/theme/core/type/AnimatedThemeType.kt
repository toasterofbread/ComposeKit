package dev.toastbits.composekit.theme.core.type

import androidx.compose.runtime.Composable
import com.catppuccin.kotlin.Catppuccin
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.theme_name_animated
import dev.toastbits.composekit.theme.core.generated.resources.theme_type_preview_name_animated
import dev.toastbits.composekit.theme.core.model.AnimatedTheme
import dev.toastbits.composekit.theme.core.model.CatppuccinTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.stringResource

data object AnimatedThemeType: ThemeType<AnimatedTheme> {
    override val themeName: StringResource
        get() = Res.string.theme_name_animated

    override val isPure: Boolean = false

    override val creationPreviewReadableName: String
        @Composable
        get() = stringResource(Res.string.theme_type_preview_name_animated)

    override fun getConstantTheme(): AnimatedTheme? = null

    override fun isInstance(themeReference: ThemeReference): Boolean =
        (themeReference as? ThemeReference.StaticTheme)?.theme is AnimatedTheme

    override fun getDefaultTheme(context: PlatformContext): AnimatedTheme =
        AnimatedTheme(
            null,
            Catppuccin.Accent.entries.map { accent ->
                ThemeReference.StaticTheme(CatppuccinTheme(accentDefinition = accent))
            }
        )
}

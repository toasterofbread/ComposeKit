/*
MIT License

Copyright (c) 2023 Rosé Pine

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package dev.toastbits.composekit.theme.core.external

import androidx.compose.ui.graphics.Color

object RosePine {
    enum class Flavour {
        BASE,
        MOON,
        DAWN;

        val base: Color
            get() = when (this) {
                BASE -> Color(0xff191724)
                MOON -> Color(0xff232136)
                DAWN -> Color(0xfffaf4ed)
            }

        val surface: Color
            get() = when (this) {
                BASE -> Color(0xff1f1d2e)
                MOON -> Color(0xff2a273f)
                DAWN -> Color(0xfffffaf3)
            }

        val overlay: Color
            get() = when (this) {
                BASE -> Color(0xff26233a)
                MOON -> Color(0xff393552)
                DAWN -> Color(0xfff2e9e1)
            }

        val muted: Color
            get() = when (this) {
                BASE -> Color(0xff6e6a86)
                MOON -> Color(0xff6e6a86)
                DAWN -> Color(0xff9893a5)
            }

        val subtle: Color
            get() = when (this) {
                BASE -> Color(0xff908caa)
                MOON -> Color(0xff908caa)
                DAWN -> Color(0xff797593)
            }

        val text: Color
            get() = when (this) {
                BASE -> Color(0xffe0def4)
                MOON -> Color(0xffe0def4)
                DAWN -> Color(0xff575279)
            }
    }

    enum class Accent {
        LOVE,
        GOLD,
        ROSE,
        PINE,
        FOAM,
        IRIS;

        infix fun of(flavour: Flavour): Color =
            when (flavour) {
                Flavour.BASE ->
                    when (this) {
                        LOVE -> Color(0xffeb6f92)
                        GOLD -> Color(0xfff6c177)
                        ROSE -> Color(0xffebbcba)
                        PINE -> Color(0xff31748f)
                        FOAM -> Color(0xff9ccfd8)
                        IRIS -> Color(0xffc4a7e7)
                    }
                Flavour.MOON ->
                    when (this) {
                        LOVE -> Color(0xffeb6f92)
                        GOLD -> Color(0xfff6c177)
                        ROSE -> Color(0xffea9a97)
                        PINE -> Color(0xff3e8fb0)
                        FOAM -> Color(0xff9ccfd8)
                        IRIS -> Color(0xffc4a7e7)
                    }
                Flavour.DAWN ->
                    when (this) {
                        LOVE -> Color(0xffb4637a)
                        GOLD -> Color(0xffea9d34)
                        ROSE -> Color(0xffd7827e)
                        PINE -> Color(0xff286983)
                        FOAM -> Color(0xff286983)
                        IRIS -> Color(0xff907aa9)
                    }
            }
    }
}

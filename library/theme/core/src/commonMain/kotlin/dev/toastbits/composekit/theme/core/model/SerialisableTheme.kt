package dev.toastbits.composekit.theme.core.model

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.theme.core.ThemeValues
import kotlinx.serialization.Serializable

@Serializable
sealed interface SerialisableTheme: ThemeValues {
    val readableName: String @Composable get
    suspend fun getReadableName(): String

    override fun toSerialisable(): SerialisableTheme =
        this
}

package dev.toastbits.composekit.theme.core

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.theme_colour_slot_accent
import dev.toastbits.composekit.theme.core.generated.resources.theme_colour_slot_background
import dev.toastbits.composekit.theme.core.generated.resources.theme_colour_slot_card
import dev.toastbits.composekit.theme.core.generated.resources.theme_colour_slot_card_vibrant_accent
import dev.toastbits.composekit.theme.core.generated.resources.theme_colour_slot_error
import dev.toastbits.composekit.theme.core.generated.resources.theme_colour_slot_on_accent
import dev.toastbits.composekit.theme.core.generated.resources.theme_colour_slot_on_background
import dev.toastbits.composekit.theme.core.generated.resources.theme_colour_slot_on_card
import dev.toastbits.composekit.theme.core.generated.resources.theme_colour_slot_vibrant_accent
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.ThemeValuesData
import dev.toastbits.composekit.util.compare
import dev.toastbits.composekit.util.contrastAgainst
import dev.toastbits.composekit.util.getContrasted
import org.jetbrains.compose.resources.stringResource

interface ThemeValues {
    val background: Color
    val onBackground: Color
    val card: Color
    val accent: Color
    val error: Color

    val isInitialised: Boolean get() = true
    val isAnimated: Boolean get() = false
    val key: Any? get() = Unit

    @Composable
    fun Update() {}

    fun toSerialisable(): SerialisableTheme =
        ThemeValuesData.of(this)

    sealed interface Slot {
        enum class BuiltIn: Slot {
            BACKGROUND,
            ON_BACKGROUND,
            CARD,
            ACCENT,
            ERROR;
        }

        enum class Extension: Slot {
            ON_ACCENT,
            ON_CARD,
            VIBRANT_ACCENT,
            CARD_VIBRANT_ACCENT
        }

        companion object {
            val entries: List<Slot>
                get() = BuiltIn.entries + Extension.entries
        }
    }
}

val ThemeValues.Slot.readableName: String
    @Composable
    get() = when (this) {
        ThemeValues.Slot.BuiltIn.BACKGROUND -> stringResource(Res.string.theme_colour_slot_background)
        ThemeValues.Slot.BuiltIn.ON_BACKGROUND -> stringResource(Res.string.theme_colour_slot_on_background)
        ThemeValues.Slot.BuiltIn.CARD -> stringResource(Res.string.theme_colour_slot_card)
        ThemeValues.Slot.BuiltIn.ACCENT -> stringResource(Res.string.theme_colour_slot_accent)
        ThemeValues.Slot.BuiltIn.ERROR -> stringResource(Res.string.theme_colour_slot_error)
        ThemeValues.Slot.Extension.ON_ACCENT -> stringResource(Res.string.theme_colour_slot_on_accent)
        ThemeValues.Slot.Extension.ON_CARD -> stringResource(Res.string.theme_colour_slot_on_card)
        ThemeValues.Slot.Extension.VIBRANT_ACCENT -> stringResource(Res.string.theme_colour_slot_vibrant_accent)
        ThemeValues.Slot.Extension.CARD_VIBRANT_ACCENT -> stringResource(Res.string.theme_colour_slot_card_vibrant_accent)
    }

operator fun ThemeValues.get(slot: ThemeValues.Slot): Color =
    when (slot) {
        ThemeValues.Slot.BuiltIn.BACKGROUND -> background
        ThemeValues.Slot.BuiltIn.ON_BACKGROUND -> onBackground
        ThemeValues.Slot.BuiltIn.CARD -> card
        ThemeValues.Slot.BuiltIn.ACCENT -> accent
        ThemeValues.Slot.BuiltIn.ERROR -> error
        ThemeValues.Slot.Extension.ON_ACCENT -> onAccent
        ThemeValues.Slot.Extension.ON_CARD -> onCard
        ThemeValues.Slot.Extension.VIBRANT_ACCENT -> vibrantAccent
        ThemeValues.Slot.Extension.CARD_VIBRANT_ACCENT -> cardVibrantAccent
    }

val ThemeValues.onAccent: Color get() = accent.getContrasted()
val ThemeValues.onCard: Color get() = card.getContrasted()
val ThemeValues.vibrantAccent: Color get() = makeVibrant(accent, background)
val ThemeValues.cardVibrantAccent: Color get() = makeVibrant(accent, card)

const val VIBRANT_ACCENT_CONTRAST: Float = 0.4f

fun ThemeValues.makeVibrant(
    colour: Color,
    against: Color = background,
    contrast: Float = VIBRANT_ACCENT_CONTRAST
): Color {
    if (colour.compare(background) > 0.7f) {
        return colour.contrastAgainst(against, contrast, clip = false)
    }
    return colour
}

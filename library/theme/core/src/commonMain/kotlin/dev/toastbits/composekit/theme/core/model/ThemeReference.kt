package dev.toastbits.composekit.theme.core.model

import dev.toastbits.composekit.theme.core.provider.ThemeProvider
import kotlinx.serialization.Serializable

@Serializable
sealed interface ThemeReference {
    fun getTheme(themeProvider: ThemeProvider): SerialisableTheme

    @Serializable
    data class CustomTheme(val customThemeIndex: Int): ThemeReference {
        override fun getTheme(themeProvider: ThemeProvider): SerialisableTheme =
            themeProvider.getCustomTheme(customThemeIndex)
                ?: dev.toastbits.composekit.theme.core.model.SystemTheme
    }

    @Serializable
    data class StaticTheme(val theme: SerialisableTheme): ThemeReference {
        override fun getTheme(themeProvider: ThemeProvider): SerialisableTheme = theme
    }

    @Serializable
    data object SystemTheme: ThemeReference {
        override fun getTheme(themeProvider: ThemeProvider): SerialisableTheme =
            dev.toastbits.composekit.theme.core.model.SystemTheme
    }

    companion object {
        val DEFAULT: ThemeReference = SystemTheme
    }
}

package dev.toastbits.composekit.theme.core.util

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.rosepine_accent_love_name
import dev.toastbits.composekit.theme.core.generated.resources.rosepine_accent_gold_name
import dev.toastbits.composekit.theme.core.generated.resources.rosepine_accent_rose_name
import dev.toastbits.composekit.theme.core.generated.resources.rosepine_accent_pine_name
import dev.toastbits.composekit.theme.core.generated.resources.rosepine_accent_foam_name
import dev.toastbits.composekit.theme.core.generated.resources.rosepine_accent_iris_name
import dev.toastbits.composekit.theme.core.generated.resources.rosepine_flavour_base_name
import dev.toastbits.composekit.theme.core.generated.resources.rosepine_flavour_moon_name
import dev.toastbits.composekit.theme.core.generated.resources.rosepine_flavour_dawn_name
import dev.toastbits.composekit.theme.core.external.RosePine
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.stringResource

val RosePine.Flavour.nameResource: StringResource
    get() = when (this) {
        RosePine.Flavour.BASE -> Res.string.rosepine_flavour_base_name
        RosePine.Flavour.MOON -> Res.string.rosepine_flavour_moon_name
        RosePine.Flavour.DAWN -> Res.string.rosepine_flavour_dawn_name
    }

val RosePine.Flavour.readableName: String
    @Composable
    get() = stringResource(nameResource)

val RosePine.Accent.nameResource: StringResource
    get() = when (this) {
        RosePine.Accent.LOVE -> Res.string.rosepine_accent_love_name
        RosePine.Accent.GOLD -> Res.string.rosepine_accent_gold_name
        RosePine.Accent.ROSE -> Res.string.rosepine_accent_rose_name
        RosePine.Accent.PINE -> Res.string.rosepine_accent_pine_name
        RosePine.Accent.FOAM -> Res.string.rosepine_accent_foam_name
        RosePine.Accent.IRIS -> Res.string.rosepine_accent_iris_name
    }

val RosePine.Accent.readableName: String
    @Composable
    get() = stringResource(nameResource)

package dev.toastbits.composekit.theme.core.model

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import dev.toastbits.composekit.theme.core.ui.LocalThemeProvider
import dev.toastbits.composekit.theme.core.util.BaseAnimatedTheme
import dev.toastbits.composekit.util.platform.getEnv
import org.jetbrains.compose.resources.getString

object SystemTheme: BaseAnimatedTheme(), SerialisableTheme {
    override val readableName: String
        @Composable
        get() = rememberSystemTheme().readableName

    override suspend fun getReadableName(): String =
        getCurrentEnvironmentTheme().getOrNull()?.getReadableName()
            ?: getString(NamedTheme.Type.SYSTEM.nameResource)

    @Composable
    override fun Update() {
        val systemTheme: SerialisableTheme = rememberSystemTheme()
        LaunchedEffect(instanceKey, systemTheme) {
            updateColours(systemTheme)
        }
    }

    @Composable
    private fun rememberSystemTheme(): SerialisableTheme {
        val environmentTheme: SerialisableTheme? =
            remember { getCurrentEnvironmentTheme().getOrNull() }

        if (environmentTheme != null) {
            return NamedTheme(
                NamedTheme.Type.SYSTEM_ENVIRONMENT,
                null,
                environmentTheme
            )
        }

        val builtInTheme: ThemeValuesData = LocalThemeProvider.current.rememberBuiltInSystemTheme()
        return NamedTheme(
            NamedTheme.Type.SYSTEM,
            null,
            builtInTheme
        )
    }

    private fun getCurrentEnvironmentTheme(): Result<SerialisableTheme?> = runCatching {
        val gtkTheme: String? = getEnv("GTK_THEME")?.lowercase()

        if (gtkTheme?.startsWith("catppuccin-") == true) {
            val split: List<String> = gtkTheme.substring(11).split("-", limit = 4)
            if (split.size >= 3) {
                val flavour: String = split[0]
                val accent: String = split[2]

                return@runCatching CatppuccinTheme.getByName(flavour, accent)
            }
        }

        return@runCatching null
    }
}

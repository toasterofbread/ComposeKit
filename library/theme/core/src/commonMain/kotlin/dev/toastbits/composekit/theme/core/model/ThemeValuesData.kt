package dev.toastbits.composekit.theme.core.model

import androidx.compose.material3.ColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import dev.toastbits.composekit.theme.core.ThemeValues
import kotlinx.serialization.Serializable

@Serializable
data class ThemeValuesData(
    val _background: Int,
    val _onBackground: Int,
    val _card: Int,
    val _accent: Int,
    val _error: Int
): SerialisableTheme {
    override val readableName: String
        @Composable
        get() = toString()

    override suspend fun getReadableName(): String = toString()

    override val background: Color get() = Color(_background)
    override val onBackground: Color get() = Color(_onBackground)
    override val card: Color get() = Color(_card)
    override val accent: Color get() = Color(_accent)
    override val error: Color get() = Color(_error)

    fun setSlotColour(slot: ThemeValues.Slot.BuiltIn, colour: Color): ThemeValuesData =
        when (slot) {
            ThemeValues.Slot.BuiltIn.BACKGROUND -> copy(background = colour)
            ThemeValues.Slot.BuiltIn.ON_BACKGROUND -> copy(onBackground = colour)
            ThemeValues.Slot.BuiltIn.CARD -> copy(card = colour)
            ThemeValues.Slot.BuiltIn.ACCENT -> copy(accent = colour)
            ThemeValues.Slot.BuiltIn.ERROR -> copy(error = colour)
        }

    fun copy(
        background: Color? = null,
        onBackground: Color? = null,
        card: Color? = null,
        accent: Color? = null,
        error: Color? = null
    ): ThemeValuesData =
        ThemeValuesData(
            background ?: this.background,
            onBackground ?: this.onBackground,
            card ?: this.card,
            accent ?: this.accent,
            error ?: this.error
        )

    companion object {
        fun of(other: ThemeValues): ThemeValuesData {
            if (other is ThemeValuesData) {
                return other
            }

            return ThemeValuesData(
                other.background,
                other.onBackground,
                other.card,
                other.accent,
                other.error
            )
        }

        fun fromColourScheme(colourScheme: ColorScheme): ThemeValuesData =
            ThemeValuesData(
                background = colourScheme.background,
                onBackground = colourScheme.onBackground,
                card = colourScheme.surface,
                accent = colourScheme.primary,
                error = colourScheme.error
            )

        fun ofSingleColour(colour: Color): ThemeValuesData =
            ThemeValuesData(
                background = colour,
                onBackground = colour,
                card = colour,
                accent = colour,
                error = colour
            )

        operator fun invoke(
            background: Color,
            onBackground: Color,
            card: Color,
            accent: Color,
            error: Color
        ): ThemeValuesData =
            ThemeValuesData(
                background.toArgb(),
                onBackground.toArgb(),
                card.toArgb(),
                accent.toArgb(),
                error.toArgb()
            )
    }
}

package dev.toastbits.composekit.theme.core.type

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.theme_name_catppuccin
import dev.toastbits.composekit.theme.core.generated.resources.theme_type_preview_name_catppuccin
import dev.toastbits.composekit.theme.core.model.CatppuccinTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.stringResource

data object CatppuccinThemeType: ThemeType<CatppuccinTheme> {
    override val isPure: Boolean = true

    override val themeName: StringResource
        get() = Res.string.theme_name_catppuccin

    override val creationPreviewReadableName: String
        @Composable
        get() = stringResource(Res.string.theme_type_preview_name_catppuccin)

    override fun getConstantTheme(): CatppuccinTheme? = null

    override fun isInstance(themeReference: ThemeReference): Boolean =
        (themeReference as? ThemeReference.StaticTheme)?.theme is CatppuccinTheme

    override fun getDefaultTheme(context: PlatformContext): CatppuccinTheme =
        CatppuccinTheme()
}

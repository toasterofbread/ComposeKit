package dev.toastbits.composekit.theme.core.type

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.context.getDarkColorScheme
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.theme_name_custom
import dev.toastbits.composekit.theme.core.generated.resources.theme_type_preview_name_custom
import dev.toastbits.composekit.theme.core.model.AnimatedTheme
import dev.toastbits.composekit.theme.core.model.CatppuccinTheme
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.RosePineTheme
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.SystemLightDarkTheme
import dev.toastbits.composekit.theme.core.model.SystemTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.model.ThemeValuesData
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.stringResource

data class CustomThemeType(
    val defaultDarkModeStatus: Boolean = true,
    val showPureOnly: Boolean = false
): ThemeType<CustomThemeType.IndexedTheme> {
    override val themeName: StringResource
        get() = Res.string.theme_name_custom

    override val isPure: Boolean = true

    fun shouldShowTheme(theme: NamedTheme): Boolean =
        if (showPureOnly) theme.isPure() else true

    override val creationPreviewReadableName: String
        @Composable
        get() = stringResource(Res.string.theme_type_preview_name_custom)

    override fun isInstance(themeReference: ThemeReference): Boolean =
        themeReference is ThemeReference.CustomTheme

    override fun getConstantTheme(): IndexedTheme? = null

    override fun canSetThemeName(theme: IndexedTheme): Boolean = true
    override fun setThemeName(theme: IndexedTheme, newName: String): IndexedTheme =
        theme.copy(theme = theme.theme.copy(name = newName))

    override fun canSetSlotColour(theme: IndexedTheme, slot: ThemeValues.Slot): Boolean =
        slot is ThemeValues.Slot.BuiltIn && theme.theme.theme is ThemeValuesData

    override fun setSlotColour(
        theme: IndexedTheme,
        slot: ThemeValues.Slot,
        colour: Color
    ): IndexedTheme {
        require(slot is ThemeValues.Slot.BuiltIn) { slot }
        require(theme.theme.theme is ThemeValuesData) { theme }
        return theme.copy(
            theme = theme.theme.copy(
                theme = theme.theme.theme.setSlotColour(slot, colour)
            )
        )
    }

    @Composable
    override fun rememberThemeName(theme: IndexedTheme): String =
        theme.theme.readableName

    override suspend fun getThemeName(theme: IndexedTheme): String =
        theme.theme.getReadableName()

    override fun getDefaultTheme(context: PlatformContext): IndexedTheme =
        IndexedTheme(
            null,
            NamedTheme(
                NamedTheme.Type.CUSTOM,
                null,
                ThemeValuesData.fromColourScheme(context.getDarkColorScheme())
            )
        )

    private fun NamedTheme.isPure(): Boolean =
        when (theme) {
            is AnimatedTheme,
            is SystemLightDarkTheme -> false
            SystemTheme,
            is CatppuccinTheme,
            is RosePineTheme,
            is ThemeValuesData -> true
            is NamedTheme -> theme.isPure()
        }

    data class IndexedTheme(
        val index: Int?,
        val theme: NamedTheme
    ): ThemeValues by theme {
        override fun toSerialisable(): SerialisableTheme = theme
    }
}

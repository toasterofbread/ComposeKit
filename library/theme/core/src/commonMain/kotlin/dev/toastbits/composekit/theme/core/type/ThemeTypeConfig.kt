package dev.toastbits.composekit.theme.core.type

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.theme.core.ThemeValues
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest

abstract class ThemeTypeConfig<T: ThemeValues> {
    abstract val type: ThemeType<T>

    @Composable
    fun rememberConfigItems(
        theme: T,
        onChanged: (T) -> Unit
    ): List<SettingsItem> {
        val currentThemeState: MutableStateFlow<T> = remember { MutableStateFlow(theme) }
        LaunchedEffect(currentThemeState) {
            currentThemeState.collectLatest {
                onChanged(it)
            }
        }

        return rememberConfigItems(currentThemeState)
    }

    @Composable
    protected abstract fun rememberConfigItems(
        state: MutableStateFlow<T>
    ): List<SettingsItem>
}

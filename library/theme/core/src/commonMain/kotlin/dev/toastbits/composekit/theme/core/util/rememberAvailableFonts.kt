package dev.toastbits.composekit.theme.core.util

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.theme.core.model.ComposeKitFont

@Composable
fun ComposeKitFont.Companion.rememberAvailableFonts(): List<ComposeKitFont> =
    listOf(ComposeKitFont.Default, ComposeKitFont.System) + ComposeKitFont.BuiltIn.entries

package dev.toastbits.composekit.theme.core.type

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.theme_name_rosepine
import dev.toastbits.composekit.theme.core.generated.resources.theme_type_preview_name_rosepine
import dev.toastbits.composekit.theme.core.model.RosePineTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.stringResource

data object RosePineThemeType: ThemeType<RosePineTheme> {
    val defaultTheme: RosePineTheme = RosePineTheme()

    override val isPure: Boolean = true

    override val themeName: StringResource
        get() = Res.string.theme_name_rosepine

    override val creationPreviewReadableName: String
        @Composable
        get() = stringResource(Res.string.theme_type_preview_name_rosepine)

    override fun getConstantTheme(): RosePineTheme? = null

    override fun isInstance(themeReference: ThemeReference): Boolean =
        (themeReference as? ThemeReference.StaticTheme)?.theme is RosePineTheme

    override fun getDefaultTheme(context: PlatformContext): RosePineTheme =
        defaultTheme
}

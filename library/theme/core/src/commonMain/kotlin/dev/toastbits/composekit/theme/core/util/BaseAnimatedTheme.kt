package dev.toastbits.composekit.theme.core.util

import androidx.compose.ui.graphics.Color
import dev.toastbits.composekit.theme.core.model.AnimatedThemeValues
import dev.toastbits.composekit.theme.core.model.ThemeValuesData

abstract class BaseAnimatedTheme: AnimatedThemeValues(ThemeValuesData.ofSingleColour(Color.Black))

package dev.toastbits.composekit.theme.core.model

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.theme_name_custom
import dev.toastbits.composekit.theme.core.generated.resources.theme_name_system
import kotlinx.serialization.Serializable
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.getString
import org.jetbrains.compose.resources.stringResource

@Serializable
data class NamedTheme(
    val type: Type,
    val name: String?,
    val theme: SerialisableTheme
): SerialisableTheme by theme {
    override val readableName: String
        @Composable
        get() = formatThemeName(name ?: stringResource(type.nameResource)) { theme.readableName }

    override suspend fun getReadableName(): String =
        formatThemeName(name ?: getString(type.nameResource)) { theme.getReadableName() }

    private inline fun formatThemeName(name: String, getThemeName: () -> String): String =
        buildString {
            append(name)
            if (type.appendThemeName) {
                append(" - ")
                append(getThemeName())
            }
        }

    enum class Type(val appendThemeName: Boolean = false) {
        SYSTEM,
        SYSTEM_ENVIRONMENT(true),
        CUSTOM;

        val nameResource: StringResource
            get() = when (this) {
                SYSTEM -> Res.string.theme_name_system
                SYSTEM_ENVIRONMENT -> Res.string.theme_name_system
                CUSTOM -> Res.string.theme_name_custom
            }
    }
}

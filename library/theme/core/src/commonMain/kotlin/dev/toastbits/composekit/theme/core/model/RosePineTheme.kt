package dev.toastbits.composekit.theme.core.model

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.`rosepine_theme_name_$flavour_$accent`
import dev.toastbits.composekit.theme.core.external.RosePine
import dev.toastbits.composekit.theme.core.util.nameResource
import kotlinx.serialization.Serializable
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.getString
import org.jetbrains.compose.resources.stringResource

@Serializable
data class RosePineTheme(
    val flavour: RosePine.Flavour = RosePine.Flavour.BASE,
    val accentDefinition: RosePine.Accent = RosePine.Accent.ROSE
): SerialisableTheme {
    override val background: Color
        get() = flavour.base
    override val onBackground: Color
        get() = flavour.text
    override val card: Color
        get() = flavour.overlay
    override val accent: Color
        get() = accentDefinition of flavour
    override val error: Color
        get() = accentDefinition.errorColour of flavour

    private val RosePine.Accent.errorColour: RosePine.Accent
        get() =
            if (this == RosePine.Accent.LOVE) RosePine.Accent.GOLD
            else RosePine.Accent.LOVE

    override val readableName: String
        @Composable
        get() =
            stringResource(Res.string.`rosepine_theme_name_$flavour_$accent`)
                .formatName { stringResource(it) }

    override suspend fun getReadableName(): String =
        getString(Res.string.`rosepine_theme_name_$flavour_$accent`)
            .formatName { getString(it) }

    private inline fun String.formatName(getString: (StringResource) -> String): String =
        replace("\$flavour", getString(flavour.nameResource))
        .replace("\$accent", getString(accentDefinition.nameResource))

    companion object {
        fun getByName(targetFlavour: String, targetAccent: String): RosePineTheme? {
            val flavour: RosePine.Flavour =
                RosePine.Flavour.entries.firstOrNull { it.name.lowercase() == targetFlavour }
                    ?: return null

            val accent: RosePine.Accent =
                RosePine.Accent.entries.firstOrNull { it.name.lowercase() == targetAccent.lowercase() }
                    ?: return null

            return RosePineTheme(flavour, accent)
        }
    }
}

package dev.toastbits.composekit.theme.core.model

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.theme_name_animated
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.provider.ThemeProvider
import dev.toastbits.composekit.theme.core.ui.LocalThemeProvider
import dev.toastbits.composekit.theme.core.util.CycleAnimatedTheme
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import org.jetbrains.compose.resources.getString
import org.jetbrains.compose.resources.stringResource
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

@Serializable
data class AnimatedTheme(
    val name: String?,
    val animationThemes: List<ThemeReference> = emptyList(),
    val animationDuration: Duration = 1.seconds
): CycleAnimatedTheme(), SerialisableTheme {
    override val readableName: String
        @Composable
        get() = name ?: stringResource(Res.string.theme_name_animated)

    override suspend fun getReadableName(): String =
        name ?: getString(Res.string.theme_name_animated)

    @Transient
    override val animationSpec: AnimationSpec<Color> =
        tween(animationDuration.inWholeMilliseconds.toInt())

    @Composable
    override fun rememberCycleAnimationThemes(): List<ThemeValues> {
        val themeProvider: ThemeProvider = LocalThemeProvider.current
        return remember(animationThemes, themeProvider) {
            animationThemes.map { it.getTheme(themeProvider) }
        }
    }
}

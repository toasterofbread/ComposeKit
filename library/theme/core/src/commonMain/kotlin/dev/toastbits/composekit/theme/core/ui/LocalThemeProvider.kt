package dev.toastbits.composekit.theme.core.ui

import androidx.compose.runtime.ProvidableCompositionLocal
import androidx.compose.runtime.compositionLocalOf
import dev.toastbits.composekit.theme.core.provider.ThemeProvider

val LocalThemeProvider: ProvidableCompositionLocal<ThemeProvider> =
    compositionLocalOf { throw IllegalStateException("LocalThemeProvider not provided") }

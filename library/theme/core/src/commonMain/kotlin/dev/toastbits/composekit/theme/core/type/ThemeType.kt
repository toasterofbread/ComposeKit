package dev.toastbits.composekit.theme.core.type

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.model.AnimatedTheme
import dev.toastbits.composekit.theme.core.model.CatppuccinTheme
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.RosePineTheme
import dev.toastbits.composekit.theme.core.model.SystemLightDarkTheme
import dev.toastbits.composekit.theme.core.model.SystemTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.model.ThemeValuesData
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.getString
import org.jetbrains.compose.resources.stringResource

sealed interface ThemeType<T: ThemeValues> {
    val themeName: StringResource
    val isPure: Boolean

    val creationPreviewReadableName: String
        @Composable get

    fun isInstance(themeReference: ThemeReference): Boolean

    fun getConstantTheme(): T?

    fun canSetThemeName(theme: T): Boolean = false
    fun setThemeName(theme: T, newName: String): T = theme

    fun canSetSlotColour(theme: T, slot: ThemeValues.Slot): Boolean = false
    fun setSlotColour(theme: T, slot: ThemeValues.Slot, colour: Color): T = theme

    @Composable
    fun rememberThemeName(theme: T): String =
        stringResource(themeName)

    suspend fun getThemeName(theme: T): String =
        getString(themeName)

    fun getDefaultTheme(context: PlatformContext): T

    data class ThemeAndType<T: ThemeValues>(
        val theme: T,
        val type: ThemeType<T>
    )

    companion object {
        // Remember to handle new themes in of() below!
        fun getAll(): List<ThemeType<*>> =
            listOf(
                SystemThemeType,
                AnimatedThemeType,
                SystemLightDarkThemeType,
                CatppuccinThemeType,
                RosePineThemeType,
                CustomThemeType()
            )

        fun getAllPure(): List<ThemeType<*>> =
            getAll()
                .filter { it.isPure }
                .map {
                    if (it is CustomThemeType) it.copy(showPureOnly = true)
                    else it
                }

        fun <T: ThemeValues> of(theme: T): ThemeAndType<*>? =
            when (theme) {
                is AnimatedTheme -> ThemeAndType(theme, AnimatedThemeType)
                is CatppuccinTheme -> ThemeAndType(theme, CatppuccinThemeType)
                is RosePineTheme -> ThemeAndType(theme, RosePineThemeType)
                is SystemLightDarkTheme -> ThemeAndType(theme, SystemLightDarkThemeType)
                is NamedTheme ->
                    of(theme.theme) ?: ThemeAndType(CustomThemeType.IndexedTheme(null, theme), CustomThemeType())
                is ThemeValuesData -> null
                is SystemTheme -> ThemeAndType(theme, SystemThemeType)
                else -> throw NotImplementedError(theme::class.toString())
            }
    }
}

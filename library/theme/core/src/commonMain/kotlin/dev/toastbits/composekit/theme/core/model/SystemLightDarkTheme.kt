package dev.toastbits.composekit.theme.core.model

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import com.catppuccin.kotlin.Catppuccin
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.theme_name_system_light_dark
import dev.toastbits.composekit.theme.core.provider.ThemeProvider
import dev.toastbits.composekit.theme.core.ui.LocalThemeProvider
import dev.toastbits.composekit.theme.core.util.BaseAnimatedTheme
import kotlinx.serialization.Serializable
import org.jetbrains.compose.resources.getString
import org.jetbrains.compose.resources.stringResource

@Serializable
data class SystemLightDarkTheme(
    val name: String? = null,
    val lightTheme: ThemeReference = defaultLightTheme,
    val darkTheme: ThemeReference = defaultDarkTheme,
    val defaultDarkMode: Boolean = true
): BaseAnimatedTheme(), SerialisableTheme {
    override val readableName: String
        @Composable
        get() = name ?: stringResource(Res.string.theme_name_system_light_dark)

    override suspend fun getReadableName(): String =
        name ?: getString(Res.string.theme_name_system_light_dark)

    @Composable
    override fun Update() {
        val themeProvider: ThemeProvider = LocalThemeProvider.current
        val darkMode: Boolean = themeProvider.rememberIsSystemInDarkMode() ?: defaultDarkMode

        val currentTheme: SerialisableTheme =
            remember(themeProvider, darkMode) {
                (
                    if (darkMode) darkTheme
                    else lightTheme
                ).getTheme(themeProvider)
            }

        LaunchedEffect(instanceKey, currentTheme) {
            updateColours(currentTheme)
        }
    }

    companion object {
        val defaultLightTheme: ThemeReference.StaticTheme
            get() = ThemeReference.StaticTheme(
                CatppuccinTheme(Catppuccin.Flavour.LATTE)
            )

        val defaultDarkTheme: ThemeReference.StaticTheme
            get() = ThemeReference.StaticTheme(
                CatppuccinTheme(Catppuccin.Flavour.MOCHA)
            )
    }
}

package dev.toastbits.composekit.theme.core.model

import androidx.compose.animation.Animatable
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.AnimationVector4D
import androidx.compose.animation.core.SpringSpec
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.isSpecified
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.get
import dev.toastbits.composekit.util.composable.snapOrAnimateTo
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

open class AnimatedThemeValues(initialState: ThemeValues): ThemeValues {
    protected val instanceKey: Any? get() = backgroundState

    override val isAnimated: Boolean = true

    override val background: Color get() = getAnimatable(ThemeValues.Slot.BuiltIn.BACKGROUND).value
    override val onBackground: Color get() = getAnimatable(ThemeValues.Slot.BuiltIn.ON_BACKGROUND).value
    override val card: Color get() = getAnimatable(ThemeValues.Slot.BuiltIn.CARD).value
    override val accent: Color get() = getAnimatable(ThemeValues.Slot.BuiltIn.ACCENT).value
    override val error: Color get() = getAnimatable(ThemeValues.Slot.BuiltIn.ERROR).value

    final override var isInitialised: Boolean by mutableStateOf(false)
        private set

    private val backgroundState: Animatable<Color, AnimationVector4D> = Animatable(initialState.background)
    private val onBackgroundState: Animatable<Color, AnimationVector4D> = Animatable(initialState.onBackground)
    private val cardState: Animatable<Color, AnimationVector4D> = Animatable(initialState.card)
    private val accentState: Animatable<Color, AnimationVector4D> = Animatable(initialState.accent)
    private val errorState: Animatable<Color, AnimationVector4D> = Animatable(initialState.error)

    private fun getAnimatable(slot: ThemeValues.Slot.BuiltIn): Animatable<Color, AnimationVector4D> =
        when (slot) {
            ThemeValues.Slot.BuiltIn.BACKGROUND -> backgroundState
            ThemeValues.Slot.BuiltIn.ON_BACKGROUND -> onBackgroundState
            ThemeValues.Slot.BuiltIn.CARD -> cardState
            ThemeValues.Slot.BuiltIn.ACCENT -> accentState
            ThemeValues.Slot.BuiltIn.ERROR -> errorState
        }

    suspend fun updateColours(
        values: ThemeValues,
        animationSpec: AnimationSpec<Color>? = SpringSpec()
    ) {
        coroutineScope {
            for (slot in ThemeValues.Slot.BuiltIn.entries) {
                launch {
                    getAnimatable(slot).snapOrAnimateTo(
                        values[slot],
                        snap = !isInitialised,
                        animationSpec = animationSpec
                    )
                }
            }
        }

        if (!isInitialised) {
            delay(10)
            isInitialised = ThemeValues.Slot.BuiltIn.entries.all { values[it].isSpecified }
        }
    }
}

package dev.toastbits.composekit.theme.core.model

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.catppuccin.kotlin.Catppuccin
import com.catppuccin.kotlin.ColorDefinition
import com.catppuccin.kotlin.color.base
import com.catppuccin.kotlin.color.mantle
import com.catppuccin.kotlin.color.of
import com.catppuccin.kotlin.color.text
import com.catppuccin.kotlin.red
import com.catppuccin.kotlin.yellow
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.`catppuccin_theme_name_$flavour_$accent`
import dev.toastbits.composekit.theme.core.util.nameResource
import dev.toastbits.composekit.theme.core.util.toColour
import kotlinx.serialization.Serializable
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.getString
import org.jetbrains.compose.resources.stringResource

@Serializable
data class CatppuccinTheme(
    val flavour: Catppuccin.Flavour = Catppuccin.Flavour.MOCHA,
    val accentDefinition: Catppuccin.Accent = Catppuccin.Accent.MAUVE
): SerialisableTheme {
    override val background: Color
        get() = flavour.palette.base.toColour()
    override val onBackground: Color
        get() = flavour.palette.text.toColour()
    override val card: Color
        get() = flavour.palette.mantle.toColour()
    override val accent: Color
        get() = (accentDefinition.definition of flavour.palette).toColour()
    override val error: Color
        get() = (accentDefinition.definition.errorColour of flavour.palette).toColour()

    private val ColorDefinition.errorColour: ColorDefinition
        get() =
            if (this == red) yellow
            else red

    override val readableName: String
        @Composable
        get() = stringResource(Res.string.`catppuccin_theme_name_$flavour_$accent`)
                    .formatName { stringResource(it) }

    override suspend fun getReadableName(): String =
        getString(Res.string.`catppuccin_theme_name_$flavour_$accent`)
            .formatName { getString(it) }

    private inline fun String.formatName(getString: (StringResource) -> String): String =
        replace("\$flavour", getString(flavour.nameResource))
        .replace("\$accent", getString(accentDefinition.nameResource))

    companion object {
        fun getByName(targetFlavour: String, targetAccent: String): CatppuccinTheme? {
            val flavour: Catppuccin.Flavour =
                Catppuccin.Flavour.entries.firstOrNull { it.palette.name.lowercase() == targetFlavour.lowercase() }
                    ?: return null

            val accent: Catppuccin.Accent =
                Catppuccin.Accent.entries.firstOrNull { it.definition.label.lowercase() == targetAccent.lowercase() }
                    ?: return null

            return CatppuccinTheme(flavour, accent)
        }
    }
}

package dev.toastbits.composekit.theme.core.util

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.catppuccin.kotlin.Catppuccin
import com.catppuccin.kotlin.ColorValueContainer
import dev.toastbits.composekit.theme.core.generated.resources.Res
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_blue_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_flamingo_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_green_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_lavender_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_maroon_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_mauve_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_peach_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_pink_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_red_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_rosewater_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_sapphire_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_sky_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_teal_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_accent_yellow_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_flavour_frappe_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_flavour_latte_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_flavour_macchiato_name
import dev.toastbits.composekit.theme.core.generated.resources.catppuccin_flavour_mocha_name
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.stringResource

fun ColorValueContainer.toColour(): Color =
    Color(hex.formatted.toLong(16) or 0x00000000FF000000)

val Catppuccin.Flavour.nameResource: StringResource
    get() = when (this) {
        Catppuccin.Flavour.LATTE -> Res.string.catppuccin_flavour_latte_name
        Catppuccin.Flavour.FRAPPE -> Res.string.catppuccin_flavour_frappe_name
        Catppuccin.Flavour.MACCHIATO -> Res.string.catppuccin_flavour_macchiato_name
        Catppuccin.Flavour.MOCHA -> Res.string.catppuccin_flavour_mocha_name
    }

val Catppuccin.Flavour.readableName: String
    @Composable
    get() = stringResource(nameResource)

val Catppuccin.Accent.nameResource: StringResource
    get() = when (this) {
        Catppuccin.Accent.MAUVE -> Res.string.catppuccin_accent_mauve_name
        Catppuccin.Accent.LAVENDER -> Res.string.catppuccin_accent_lavender_name
        Catppuccin.Accent.RED -> Res.string.catppuccin_accent_red_name
        Catppuccin.Accent.YELLOW -> Res.string.catppuccin_accent_yellow_name
        Catppuccin.Accent.GREEN -> Res.string.catppuccin_accent_green_name
        Catppuccin.Accent.TEAL -> Res.string.catppuccin_accent_teal_name
        Catppuccin.Accent.PINK -> Res.string.catppuccin_accent_pink_name
        Catppuccin.Accent.SAPPHIRE -> Res.string.catppuccin_accent_sapphire_name
        Catppuccin.Accent.ROSEWATER -> Res.string.catppuccin_accent_rosewater_name
        Catppuccin.Accent.PEACH -> Res.string.catppuccin_accent_peach_name
        Catppuccin.Accent.SKY -> Res.string.catppuccin_accent_sky_name
        Catppuccin.Accent.MAROON -> Res.string.catppuccin_accent_maroon_name
        Catppuccin.Accent.BLUE -> Res.string.catppuccin_accent_blue_name
        Catppuccin.Accent.FLAMINGO -> Res.string.catppuccin_accent_flamingo_name
    }

val Catppuccin.Accent.readableName: String
    @Composable
    get() = stringResource(nameResource)

package dev.toastbits.composekit.theme.core.ui

import androidx.compose.material3.darkColorScheme
import androidx.compose.runtime.ProvidableCompositionLocal
import androidx.compose.runtime.staticCompositionLocalOf
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.model.ThemeValuesData

val LocalComposeKitTheme: ProvidableCompositionLocal<ThemeValues> =
    staticCompositionLocalOf { ThemeValuesData.fromColourScheme(darkColorScheme()) }

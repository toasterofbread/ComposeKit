package dev.toastbits.composekit.theme.config

import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.provider.ThemeTypeConfigProvider
import dev.toastbits.composekit.theme.core.type.AnimatedThemeType
import dev.toastbits.composekit.theme.core.type.CatppuccinThemeType
import dev.toastbits.composekit.theme.core.type.CustomThemeType
import dev.toastbits.composekit.theme.core.type.RosePineThemeType
import dev.toastbits.composekit.theme.core.type.SystemLightDarkThemeType
import dev.toastbits.composekit.theme.core.type.SystemThemeType
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.type.ThemeTypeConfig

object ThemeTypeConfigProviderImpl: ThemeTypeConfigProvider {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ThemeValues> getConfig(type: ThemeType<T>): ThemeTypeConfig<T> =
        when (type) {
            SystemThemeType -> EmptyThemeTypeConfig(type)
            is CustomThemeType -> CustomThemeTypeConfig(type)
            AnimatedThemeType -> AnimatedThemeTypeConfig
            SystemLightDarkThemeType -> SystemLightDarkThemeTypeConfig
            CatppuccinThemeType -> CatppuccinThemeTypeConfig
            RosePineThemeType -> RosePineThemeTypeConfig
        } as ThemeTypeConfig<T>
}

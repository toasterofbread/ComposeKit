package dev.toastbits.composekit.theme.config.util

import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisallowComposableCalls
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.settingsitem.domain.MutableStateFlowSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.presentation.ui.item.DropdownSettingsItem
import dev.toastbits.composekit.settingsitem.presentation.util.getConvertedProperty
import dev.toastbits.composekit.theme.config.ui.component.AccentPreview
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.kmpresources.library.mapper.toKmpResource
import kotlinx.coroutines.flow.MutableStateFlow
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.stringResource
import kotlin.enums.enumEntries

@Composable
inline fun <T: ThemeValues, reified F: Enum<F>, reified A: Enum<A>> rememberFlavourAccentComboThemeConfigItems(
    state: MutableStateFlow<T>,
    defaultTheme: T,
    flavourOptionName: StringResource,
    accentOptionName: StringResource,
    noinline getFlavour: @DisallowComposableCalls (T) -> F,
    noinline setFlavour: @DisallowComposableCalls (T, F) -> T,
    noinline getFlavourName: @DisallowComposableCalls (F) -> StringResource,
    noinline getAccent: @DisallowComposableCalls (T) -> A,
    noinline setAccent: @DisallowComposableCalls (T, A) -> T,
    noinline getAccentName: @DisallowComposableCalls (A) -> StringResource,
    noinline getAccentColour: @DisallowComposableCalls (A, F) -> Color
): List<SettingsItem> =
    remember {
        listOf(
            DropdownSettingsItem.ofEnumState(
                MutableStateFlowSettingsProperty(
                    state,
                    propertyName = flavourOptionName.toKmpResource(),
                    propertyDescription = null,
                    getPropertyDefaultValue = { defaultTheme }
                ).getConvertedProperty(
                    fromProperty = getFlavour,
                    toProperty = { setFlavour(state.value, it) }
                )
            ) {
                stringResource(getFlavourName(it))
            },
            DropdownSettingsItem.ofEnumState(
                MutableStateFlowSettingsProperty(
                    state,
                    propertyName = accentOptionName.toKmpResource(),
                    propertyDescription = null,
                    getPropertyDefaultValue = { defaultTheme },
                ).getConvertedProperty(
                    fromProperty = getAccent,
                    toProperty = { setAccent(state.value, it) }
                ),
                itemContent = { index, name ->
                    val accent: A = enumEntries<A>()[index]
                    val flavour: F = getFlavour(state.value)
                    AccentPreview(name, getAccentColour(accent, flavour))
                },
                columns = GridCells.Adaptive(200.dp)
            ) {
                stringResource(getAccentName(it))
            }
        )
    }

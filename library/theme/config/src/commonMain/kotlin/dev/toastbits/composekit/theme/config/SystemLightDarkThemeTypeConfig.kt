package dev.toastbits.composekit.theme.config

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import dev.toastbits.composekit.settingsitem.domain.MutableStateFlowSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.presentation.ui.component.item.ThemeSettingsItem
import dev.toastbits.composekit.settingsitem.presentation.util.getConvertedProperty
import dev.toastbits.composekit.theme.config.generated.resources.Res
import dev.toastbits.composekit.theme.config.generated.resources.theme_option_system_light_dark_dark_theme
import dev.toastbits.composekit.theme.config.generated.resources.theme_option_system_light_dark_dark_theme_picker_dialog_title
import dev.toastbits.composekit.theme.config.generated.resources.theme_option_system_light_dark_light_theme
import dev.toastbits.composekit.theme.config.generated.resources.theme_option_system_light_dark_light_theme_picker_dialog_title
import dev.toastbits.composekit.theme.core.model.SystemLightDarkTheme
import dev.toastbits.composekit.theme.core.type.SystemLightDarkThemeType
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.type.ThemeTypeConfig
import dev.toastbits.kmpresources.library.mapper.toKmpResource
import kotlinx.coroutines.flow.MutableStateFlow
import org.jetbrains.compose.resources.stringResource

internal data object SystemLightDarkThemeTypeConfig: ThemeTypeConfig<SystemLightDarkTheme>() {
    override val type: ThemeType<SystemLightDarkTheme> = SystemLightDarkThemeType

    private fun getThemeTypes(): List<ThemeType<*>> =
        ThemeType.getAllPure()

    @Composable
    override fun rememberConfigItems(
        state: MutableStateFlow<SystemLightDarkTheme>
    ): List<SettingsItem> =
        remember {
            listOf(
                ThemeSettingsItem(
                    MutableStateFlowSettingsProperty(
                        state,
                        Res.string.theme_option_system_light_dark_light_theme.toKmpResource(),
                        null,
                        { SystemLightDarkTheme() }
                    ).getConvertedProperty(
                        fromProperty = { it.lightTheme },
                        toProperty = { state.value.copy(lightTheme = it) }
                    ),
                    themeTypeConfigProvider = ThemeTypeConfigProviderImpl,
                    pickerDialogTitle = {
                        Text(stringResource(Res.string.theme_option_system_light_dark_light_theme_picker_dialog_title))
                    },
                    themeTypes = getThemeTypes()
                ),
                ThemeSettingsItem(
                    MutableStateFlowSettingsProperty(
                        state,
                        Res.string.theme_option_system_light_dark_dark_theme.toKmpResource(),
                        null,
                        { SystemLightDarkTheme() }
                    ).getConvertedProperty(
                        fromProperty = { it.darkTheme },
                        toProperty = { state.value.copy(darkTheme = it) }
                    ),
                    themeTypeConfigProvider = ThemeTypeConfigProviderImpl,
                    pickerDialogTitle = {
                        Text(stringResource(Res.string.theme_option_system_light_dark_dark_theme_picker_dialog_title))
                    },
                    themeTypes = getThemeTypes()
                )
            )
        }
}

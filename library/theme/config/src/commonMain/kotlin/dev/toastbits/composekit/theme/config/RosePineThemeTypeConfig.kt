package dev.toastbits.composekit.theme.config

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.theme.config.generated.resources.Res
import dev.toastbits.composekit.theme.config.generated.resources.theme_option_rosepine_accent
import dev.toastbits.composekit.theme.config.generated.resources.theme_option_rosepine_flavour
import dev.toastbits.composekit.theme.config.util.rememberFlavourAccentComboThemeConfigItems
import dev.toastbits.composekit.theme.core.external.RosePine
import dev.toastbits.composekit.theme.core.model.RosePineTheme
import dev.toastbits.composekit.theme.core.type.RosePineThemeType
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.type.ThemeTypeConfig
import dev.toastbits.composekit.theme.core.util.nameResource
import kotlinx.coroutines.flow.MutableStateFlow

internal data object RosePineThemeTypeConfig: ThemeTypeConfig<RosePineTheme>() {
    override val type: ThemeType<RosePineTheme> = RosePineThemeType

    @Composable
    override fun rememberConfigItems(
        state: MutableStateFlow<RosePineTheme>
    ): List<SettingsItem> =
        rememberFlavourAccentComboThemeConfigItems(
            state = state,
            defaultTheme = RosePineTheme(),
            flavourOptionName = Res.string.theme_option_rosepine_flavour,
            accentOptionName = Res.string.theme_option_rosepine_accent,
            getFlavour = RosePineTheme::flavour,
            setFlavour = { theme, flavour -> theme.copy(flavour = flavour) },
            getFlavourName = RosePine.Flavour::nameResource,
            getAccent = RosePineTheme::accentDefinition,
            setAccent = { theme, accent -> theme.copy(accentDefinition = accent) },
            getAccentName = RosePine.Accent::nameResource,
            getAccentColour = RosePine.Accent::of
        )
}

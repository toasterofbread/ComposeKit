package dev.toastbits.composekit.theme.config

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.provider.ThemeTypeConfigProvider
import dev.toastbits.composekit.theme.core.type.CustomThemeType
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.type.ThemeTypeConfig
import kotlinx.coroutines.flow.MutableStateFlow

internal data class CustomThemeTypeConfig(
    override val type: CustomThemeType
): ThemeTypeConfig<CustomThemeType.IndexedTheme>() {
    @Composable
    override fun rememberConfigItems(
        state: MutableStateFlow<CustomThemeType.IndexedTheme>
    ): List<SettingsItem> =
        state.value.theme.rememberConfigItems(state)

    @Composable
    private fun <T: SerialisableTheme> T.rememberConfigItems(
        state: MutableStateFlow<CustomThemeType.IndexedTheme>
    ): List<SettingsItem> {
        val themeType: ThemeType.ThemeAndType<*> =
            ThemeType.of(this) ?: return emptyList()

        if (themeType.type is CustomThemeType) {
            return emptyList()
        }

        return themeType.rememberConfigItems(ThemeTypeConfigProviderImpl) {
            state.value = state.value.copy(theme = state.value.theme.copy(theme = it.toSerialisable()))
        }
    }
}

@Composable
private fun <T: ThemeValues> ThemeType.ThemeAndType<T>.rememberConfigItems(
    configProvider: ThemeTypeConfigProvider,
    onChanged: (T) -> Unit
): List<SettingsItem> =
    configProvider.getConfig(type).rememberConfigItems(theme, onChanged)

package dev.toastbits.composekit.theme.config

import androidx.compose.runtime.Composable
import com.catppuccin.kotlin.Catppuccin
import com.catppuccin.kotlin.color.of
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.theme.config.generated.resources.Res
import dev.toastbits.composekit.theme.config.generated.resources.theme_option_catppuccin_accent
import dev.toastbits.composekit.theme.config.generated.resources.theme_option_catppuccin_flavour
import dev.toastbits.composekit.theme.config.util.rememberFlavourAccentComboThemeConfigItems
import dev.toastbits.composekit.theme.core.model.CatppuccinTheme
import dev.toastbits.composekit.theme.core.type.CatppuccinThemeType
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.type.ThemeTypeConfig
import dev.toastbits.composekit.theme.core.util.nameResource
import dev.toastbits.composekit.theme.core.util.toColour
import kotlinx.coroutines.flow.MutableStateFlow

internal data object CatppuccinThemeTypeConfig: ThemeTypeConfig<CatppuccinTheme>() {
    override val type: ThemeType<CatppuccinTheme> = CatppuccinThemeType

    @Composable
    override fun rememberConfigItems(
        state: MutableStateFlow<CatppuccinTheme>
    ): List<SettingsItem> =
        rememberFlavourAccentComboThemeConfigItems(
            state = state,
            defaultTheme = CatppuccinTheme(),
            flavourOptionName = Res.string.theme_option_catppuccin_flavour,
            accentOptionName = Res.string.theme_option_catppuccin_accent,
            getFlavour = CatppuccinTheme::flavour,
            setFlavour = { theme, flavour -> theme.copy(flavour = flavour) },
            getFlavourName = Catppuccin.Flavour::nameResource,
            getAccent = CatppuccinTheme::accentDefinition,
            setAccent = { theme, accent -> theme.copy(accentDefinition = accent) },
            getAccentName = Catppuccin.Accent::nameResource,
            getAccentColour = { accent, flavour -> (accent.definition of flavour.palette).toColour() }
        )
}

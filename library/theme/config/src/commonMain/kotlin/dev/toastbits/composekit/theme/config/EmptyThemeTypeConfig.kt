package dev.toastbits.composekit.theme.config

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.type.ThemeTypeConfig
import kotlinx.coroutines.flow.MutableStateFlow

internal data class EmptyThemeTypeConfig<T: ThemeValues>(override val type: ThemeType<T>): ThemeTypeConfig<T>() {
    @Composable
    override fun rememberConfigItems(
        state: MutableStateFlow<T>
    ): List<SettingsItem> = emptyList()
}

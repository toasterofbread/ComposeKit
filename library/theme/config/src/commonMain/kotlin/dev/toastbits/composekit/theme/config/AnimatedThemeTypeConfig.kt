package dev.toastbits.composekit.theme.config

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import dev.toastbits.composekit.settingsitem.domain.MutableStateFlowSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.presentation.ui.component.item.SliderSettingsItem
import dev.toastbits.composekit.settingsitem.presentation.ui.component.item.ThemeListSettingsItem
import dev.toastbits.composekit.settingsitem.presentation.util.getConvertedProperty
import dev.toastbits.composekit.theme.config.generated.resources.Res
import dev.toastbits.composekit.theme.config.generated.resources.theme_option_animated_animation_duration_s
import dev.toastbits.composekit.theme.config.generated.resources.theme_option_animated_animation_themes
import dev.toastbits.composekit.theme.config.generated.resources.theme_option_animated_animation_themes_picker_dialog_title
import dev.toastbits.composekit.theme.core.model.AnimatedTheme
import dev.toastbits.composekit.theme.core.type.AnimatedThemeType
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.type.ThemeTypeConfig
import dev.toastbits.kmpresources.library.mapper.toKmpResource
import kotlinx.coroutines.flow.MutableStateFlow
import org.jetbrains.compose.resources.stringResource
import kotlin.time.Duration.Companion.seconds

internal data object AnimatedThemeTypeConfig: ThemeTypeConfig<AnimatedTheme>() {
    override val type: ThemeType<AnimatedTheme> = AnimatedThemeType

    @Composable
    override fun rememberConfigItems(
        state: MutableStateFlow<AnimatedTheme>
    ): List<SettingsItem> =
        remember {
            listOf(
                SliderSettingsItem(
                    MutableStateFlowSettingsProperty(
                        state,
                        propertyName = Res.string.theme_option_animated_animation_duration_s.toKmpResource(),
                        propertyDescription = null,
                        getPropertyDefaultValue = { AnimatedTheme("") }
                    ).getConvertedProperty(
                        fromProperty = { state.value.animationDuration.inWholeMilliseconds / 1000f },
                        toProperty = { state.value.copy(animationDuration = it.toDouble().seconds) }
                    ),
                    range = 0.1f .. 100f
                ),
                ThemeListSettingsItem(
                    MutableStateFlowSettingsProperty(
                        state,
                        propertyName = Res.string.theme_option_animated_animation_themes.toKmpResource(),
                        propertyDescription = null,
                        getPropertyDefaultValue = { AnimatedTheme("") }
                    ).getConvertedProperty(
                        fromProperty = { state.value.animationThemes },
                        toProperty = { state.value.copy(animationThemes = it) }
                    ),
                    pickerDialogTitle = {
                        Text(stringResource(Res.string.theme_option_animated_animation_themes_picker_dialog_title))
                    },
                    themeTypeConfigProvider = ThemeTypeConfigProviderImpl,
                    themeTypes = ThemeType.getAllPure()
                )
            )
        }
}

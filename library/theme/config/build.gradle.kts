import util.configureAllComposeTargets

plugins {
    id("android-library-conventions")
    id("compose-conventions")
    id("gitlab-publishing-conventions")

    alias(libs.plugins.kotlinx.serialization)
}

kotlin {
    configureAllComposeTargets()

    sourceSets {
        commonMain {
            dependencies {
                implementation(projects.library.util)
                implementation(projects.library.components)
                implementation(projects.library.navigation)
                implementation(projects.library.context)
                implementation(projects.library.theme.core)
                implementation(projects.library.settingsitem.domain)
                implementation(projects.library.settingsitem.presentation)
            }
        }
    }
}

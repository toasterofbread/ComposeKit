import util.configureAllKmpTargets

plugins {
    id("android-library-conventions")
    id("compose-noncommon-conventions")
    id("gitlab-publishing-conventions")

    alias(libs.plugins.kotlinx.serialization)
}

kotlin {
    configureAllKmpTargets()

    sourceSets {
        commonMain {
            dependencies {
                api(projects.library.utilKt)
                api(libs.kmpresources)
                api(libs.kotlinx.coroutines.core)
                api(libs.kotlinx.serialization.json)
            }
        }
    }
}

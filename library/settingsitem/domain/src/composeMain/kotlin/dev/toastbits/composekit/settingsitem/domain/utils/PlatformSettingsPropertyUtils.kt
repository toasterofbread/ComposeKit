package dev.toastbits.composekit.settingsitem.domain.utils

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshotFlow
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest

@Composable
fun <T> PlatformSettingsProperty<T>.observe(): MutableState<T> {
    val inState: Flow<T> = remember(this) { asFlow() }
    val outState: MutableState<T> = remember(this) { mutableStateOf(get()) }

    LaunchedEffect(inState) {
        inState.collectLatest { value ->
            outState.value = value
        }
    }
    LaunchedEffect(outState) {
        snapshotFlow { outState.value }
            .collectLatest { value ->
                if (value != get()) {
                    set(value)
                }
            }
    }

    return outState
}

package dev.toastbits.composekit.settingsitem.domain

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

actual abstract class SettingsItem {
    actual abstract suspend fun resetValues()
    actual abstract fun getProperties(): List<PlatformSettingsProperty<*>>
    actual abstract fun resetUiState()

    actual open val showItem: Flow<Boolean> =
        flowOf(false)

    actual companion object
}

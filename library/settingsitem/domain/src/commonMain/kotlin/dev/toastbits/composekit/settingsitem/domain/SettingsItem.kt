@file:Suppress("MemberVisibilityCanBePrivate")

package dev.toastbits.composekit.settingsitem.domain

import kotlinx.coroutines.flow.Flow

expect abstract class SettingsItem() {
    abstract suspend fun resetValues()

    abstract fun getProperties(): List<PlatformSettingsProperty<*>>

    abstract fun resetUiState()

    open val showItem: Flow<Boolean>

    companion object
}

package dev.toastbits.composekit.settingsitem.domain

import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.json.JsonElement
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

interface PlatformSettingsProperty<T>: ReadOnlyProperty<Any?, PlatformSettingsProperty<T>> {
    val key: String

    val name: KmpStringResource
    val description: KmpStringResource?

    fun get(): T
    suspend fun set(value: T, editor: PlatformSettingsEditor? = null)
    suspend fun set(data: JsonElement, editor: PlatformSettingsEditor? = null)
    suspend fun reset(editor: PlatformSettingsEditor? = null)

    fun serialise(value: Any?): JsonElement

    fun getDefaultValue(): T

    fun isHidden(): Boolean = false

    fun asFlow(): StateFlow<T>

    override fun getValue(thisRef: Any?, property: KProperty<*>): PlatformSettingsProperty<T> = this
}

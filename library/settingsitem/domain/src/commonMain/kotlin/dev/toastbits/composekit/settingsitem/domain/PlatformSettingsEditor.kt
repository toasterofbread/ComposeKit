package dev.toastbits.composekit.settingsitem.domain

import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json

interface PlatformSettingsEditor {
    val json: Json

    fun putString(key: String, value: String): PlatformSettingsEditor
    fun putStringSet(key: String, values: Set<String>): PlatformSettingsEditor
    fun putInt(key: String, value: Int): PlatformSettingsEditor
    fun putLong(key: String, value: Long): PlatformSettingsEditor
    fun putFloat(key: String, value: Float): PlatformSettingsEditor
    fun putBoolean(key: String, value: Boolean): PlatformSettingsEditor
    fun <T> putSerialisable(key: String, value: T, serialiser: KSerializer<T>, json: Json = this.json): PlatformSettingsEditor
    fun remove(key: String): PlatformSettingsEditor
    fun clear(): PlatformSettingsEditor
}

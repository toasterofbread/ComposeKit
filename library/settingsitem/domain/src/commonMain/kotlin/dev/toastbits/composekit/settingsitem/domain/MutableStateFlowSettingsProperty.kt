package dev.toastbits.composekit.settingsitem.domain

import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.json.JsonElement

class MutableStateFlowSettingsProperty<T>(
    private val state: MutableStateFlow<T>,
    private val propertyName: KmpStringResource,
    private val propertyDescription: KmpStringResource?,
    private val getPropertyDefaultValue: () -> T = { throw IllegalStateException("Default value not provided to MutableStatePreferencesProperty") }
): PlatformSettingsProperty<T> {
    override val key: String get() = throw IllegalStateException()

    override fun get(): T = state.value

    override fun getDefaultValue(): T = getPropertyDefaultValue()

    override val name: KmpStringResource
        get() = propertyName

    override val description: KmpStringResource?
        get() = propertyDescription

    override fun asFlow(): MutableStateFlow<T> = state

    override suspend fun reset(editor: PlatformSettingsEditor?) = throw IllegalStateException()

    override fun serialise(value: Any?): JsonElement = throw IllegalStateException()

    override suspend fun set(data: JsonElement, editor: PlatformSettingsEditor?) = throw IllegalStateException()

    override suspend fun set(value: T, editor: PlatformSettingsEditor?) {
        state.value = value
    }
}
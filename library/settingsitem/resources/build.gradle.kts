import util.configureAllComposeTargets

plugins {
    id("android-library-conventions")
    id("compose-conventions")
    id("gitlab-publishing-conventions")
}

kotlin {
    configureAllComposeTargets()
}

compose {
    resources {
        publicResClass = true
    }
}

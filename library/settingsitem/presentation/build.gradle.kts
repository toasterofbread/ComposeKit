import util.configureAllKmpTargets

plugins {
    id("android-library-conventions")
    id("compose-noncommon-conventions")
    id("gitlab-publishing-conventions")

    alias(libs.plugins.kotlinx.serialization)
}

kotlin {
    configureAllKmpTargets()

    sourceSets {
        commonMain {
            dependencies {
                api(projects.library.settingsitem.domain)
                implementation(projects.library.utilKt)
                implementation(projects.library.navigation)
            }
        }

        val composeMain by getting {
            dependencies {
                implementation(projects.library.settingsitem.resources)

                implementation(projects.library.components)
                implementation(projects.library.context)
                implementation(projects.library.theme.core)
                implementation(projects.library.util)
            }
        }
    }
}

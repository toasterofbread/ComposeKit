package dev.toastbits.composekit.settingsitem.presentation.util

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsEditor
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.util.mapState
import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.json.JsonElement

fun <T, O> PlatformSettingsProperty<T>.getConvertedProperty(fromProperty: (T) -> O, toProperty: (O) -> T): PlatformSettingsProperty<O> {
    val base: PlatformSettingsProperty<T> = this
    return object : PlatformSettingsProperty<O> {
        override val key: String get() = base.key
        override val name: KmpStringResource get() = base.name
        override val description: KmpStringResource? get() = base.description

        override fun get(): O = fromProperty(base.get())
        override suspend fun set(value: O, editor: PlatformSettingsEditor?) { base.set(toProperty(value), editor) }
        override suspend fun set(data: JsonElement, editor: PlatformSettingsEditor?) { base.set(data, editor) }
        override suspend fun reset(editor: PlatformSettingsEditor?) { base.reset(editor) }

        @Suppress("UNCHECKED_CAST")
        override fun serialise(value: Any?): JsonElement = base.serialise(toProperty(value as O))

        override fun getDefaultValue(): O = fromProperty(base.getDefaultValue())

        override fun isHidden(): Boolean = base.isHidden()

        override fun asFlow(): StateFlow<O> =
            base.asFlow().mapState {
                fromProperty(it)
            }
    }
}

package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem

expect class SubpageSettingsItem(
    title: String,
    subtitle: String?,
    targetPage: Screen
): BaseSubpageSettingsItem {
    override fun resetUiState()
}

abstract class BaseSubpageSettingsItem(
    val title: String,
    val subtitle: String?,
    val targetPage: Screen
): SettingsItem() {
    override fun getProperties(): List<PlatformSettingsProperty<*>> = emptyList()
    override suspend fun resetValues() {}
}

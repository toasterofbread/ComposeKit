package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.util.model.Locale
import dev.toastbits.composekit.util.model.LocaleList

expect class LocaleSettingsItem(
    state: PlatformSettingsProperty<Locale?>,
    localeList: LocaleList,
    allowCustomLocale: Boolean
): BaseLocaleSettingsItem {
    override fun resetUiState()
}

abstract class BaseLocaleSettingsItem(
    val state: PlatformSettingsProperty<Locale?>,
    val localeList: LocaleList,
    val allowCustomLocale: Boolean
): SettingsItem() {
    override suspend fun resetValues() {
        state.reset()
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> = listOf(state)
}

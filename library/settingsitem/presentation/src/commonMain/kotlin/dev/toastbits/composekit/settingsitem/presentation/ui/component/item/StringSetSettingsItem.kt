package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.kmpresources.library.model.KmpStringResource
import dev.toastbits.composekit.util.model.Kdp
import dev.toastbits.composekit.util.model.kdp

expect class StringSetSettingsItem(
    state: PlatformSettingsProperty<Set<String>>,
    add_dialog_title: KmpStringResource,
    msg_item_already_added: KmpStringResource,
    msg_set_empty: KmpStringResource,
    single_line_content: Boolean = true,
    height: Kdp = 300.kdp,
    itemToText: (String) -> KmpStringResource = { KmpStringResource.ofString(it) },
    textToItem: (String) -> String = { it }
): BaseStringSetSettingsItem {
    override fun resetUiState()
}

abstract class BaseStringSetSettingsItem(
    val state: PlatformSettingsProperty<Set<String>>,
    val addDialogTitle: KmpStringResource,
    val msgItemAlreadyAdded: KmpStringResource,
    val msgSetEmpty: KmpStringResource,
    val single_line_content: Boolean = true,
    val height: Kdp = 300.kdp,
    val itemToText: (String) -> KmpStringResource = { KmpStringResource.ofString(it) },
    val textToItem: (String) -> String = { it }
): SettingsItem() {
    override suspend fun resetValues() {
        state.reset()
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> = listOf(state)
}

package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.presentation.util.getConvertedProperty
import dev.toastbits.kmpresources.library.model.KmpStringResource

expect class MultipleChoiceSettingsItem(
    state: PlatformSettingsProperty<Int>,
    choiceAmount: Int,
    getChoiceText: (Int) -> KmpStringResource
): BaseMultipleChoiceSettingsItem {
    override fun resetUiState()
}

abstract class BaseMultipleChoiceSettingsItem(
    val state: PlatformSettingsProperty<Int>,
    val choiceAmount: Int,
    val getChoiceText: (Int) -> KmpStringResource
): SettingsItem() {
    override suspend fun resetValues() {
        state.reset()
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> = listOf(state)
}

inline fun <reified T: Enum<T>> MultipleChoiceSettingsItem(
    state: PlatformSettingsProperty<T>,
    noinline getChoiceText: (T) -> KmpStringResource,
): MultipleChoiceSettingsItem =
    MultipleChoiceSettingsItem(
        state.getConvertedProperty(
            fromProperty = { it.ordinal },
            toProperty = { enumValues<T>()[it] }
        ),
        choiceAmount = enumValues<T>().size,
        getChoiceText = {
            getChoiceText(enumValues<T>()[it])
        }
    )

package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.kmpresources.library.model.KmpStringResource
import dev.toastbits.composekit.util.roundTo

expect class SliderSettingsItem(
    state: PlatformSettingsProperty<out Number>,
    minLabel: KmpStringResource? = null,
    maxLabel: KmpStringResource? = null,
    steps: Int = 0,
    range: ClosedFloatingPointRange<Float> = 0f .. 1f,
    getValueText: ((value: Number) -> String?)? = {
        if (it is Float) it.roundTo(2).toString()
        else it.toString()
    }
): BaseSliderSettingsItem {
    override fun resetUiState()
    override fun setValue(value: Float)
    override suspend fun saveValue()
    override fun getValue(): Float
}

abstract class BaseSliderSettingsItem(
    val state: PlatformSettingsProperty<out Number>,
    val minLabel: KmpStringResource? = null,
    val maxLabel: KmpStringResource? = null,
    val steps: Int = 0,
    val range: ClosedFloatingPointRange<Float> = 0f .. 1f,
    val getValueText: ((value: Number) -> String?)? = {
        if (it is Float) it.roundTo(2).toString()
        else it.toString()
    }
): SettingsItem() {
    abstract fun setValue(value: Float)

    abstract suspend fun saveValue()

    abstract fun getValue(): Float

    override suspend fun resetValues() {
        state.reset()
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> = listOf(state)
}

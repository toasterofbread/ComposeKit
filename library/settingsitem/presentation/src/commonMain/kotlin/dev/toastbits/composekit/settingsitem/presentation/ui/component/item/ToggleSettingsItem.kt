package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

expect class ToggleSettingsItem(
    state: PlatformSettingsProperty<Boolean>,
    enabled: StateFlow<Boolean> = MutableStateFlow(true),
    valueOverride: StateFlow<Boolean?> = MutableStateFlow(null),
    subtitleOverride: StateFlow<String?> = MutableStateFlow(null),
    checker: ((target: Boolean, setLoading: (Boolean) -> Unit, (allow_change: Boolean) -> Unit) -> Unit)? = null
): BaseToggleSettingsItem {
    override fun resetUiState()
}

abstract class BaseToggleSettingsItem(
    val state: PlatformSettingsProperty<Boolean>,
    val enabled: StateFlow<Boolean> = MutableStateFlow(true),
    val valueOverride: StateFlow<Boolean?> = MutableStateFlow(null),
    val subtitleOverride: StateFlow<String?> = MutableStateFlow(null),
    val checker: ((target: Boolean, setLoading: (Boolean) -> Unit, (allow_change: Boolean) -> Unit) -> Unit)? = null
): SettingsItem() {
    override suspend fun resetValues() {
        state.reset()
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> =
        listOf(state)
}

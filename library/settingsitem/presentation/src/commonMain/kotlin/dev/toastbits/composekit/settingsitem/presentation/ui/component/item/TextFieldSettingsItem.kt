package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

expect class TextFieldSettingsItem(
    state: PlatformSettingsProperty<String>,
    singleLine: Boolean = true,
    errorProvider: StateFlow<TextFieldErrorMessageProvider?> = MutableStateFlow(null)
): BaseTextFieldSettingsItem {
    override fun resetUiState()
}

abstract class BaseTextFieldSettingsItem(
    val state: PlatformSettingsProperty<String>,
    val singleLine: Boolean = true,
    val errorProvider: StateFlow<TextFieldErrorMessageProvider?> = MutableStateFlow(null)
): SettingsItem() {
    override suspend fun resetValues() {
        state.reset()
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> = listOf(state)
}

typealias TextFieldErrorMessageProvider = (String) -> String?

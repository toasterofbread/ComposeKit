package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.kmpresources.library.model.KmpStringResource

expect class InfoTextSettingsItem(
    text: KmpStringResource
): BaseInfoTextSettingsItem {
    override fun resetUiState()
}

abstract class BaseInfoTextSettingsItem(val text: KmpStringResource): SettingsItem() {
    override fun getProperties(): List<PlatformSettingsProperty<*>> = emptyList()
    override suspend fun resetValues() {}
}

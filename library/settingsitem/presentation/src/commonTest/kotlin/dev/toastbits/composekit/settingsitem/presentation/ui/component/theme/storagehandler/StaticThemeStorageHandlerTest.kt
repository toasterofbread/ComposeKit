package dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler

import androidx.compose.ui.graphics.Color
import assertk.assertThat
import assertk.assertions.isFalse
import assertk.assertions.isNotNull
import dev.mokkery.answering.returns
import dev.mokkery.everySuspend
import dev.mokkery.matcher.any
import dev.mokkery.mock
import dev.mokkery.verify.VerifyMode
import dev.mokkery.verifySuspend
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.model.ThemeValuesData
import kotlinx.coroutines.test.runTest
import kotlin.test.BeforeTest
import kotlin.test.Test

class StaticThemeStorageHandlerTest {
    private lateinit var storageHandler: StaticThemeStorageHandler<SerialisableTheme>

    private lateinit var currentThemeProperty: PlatformSettingsProperty<ThemeReference>
    private lateinit var customThemesProperty: PlatformSettingsProperty<List<NamedTheme>>

    private val testTheme: SerialisableTheme =
        NamedTheme(NamedTheme.Type.CUSTOM, "test", ThemeValuesData.ofSingleColour(Color.Black))

    @BeforeTest
    fun setUp() {
        currentThemeProperty = mock {
            everySuspend { set(any<ThemeReference>()) } returns Unit
        }
        customThemesProperty = mock()
        storageHandler = StaticThemeStorageHandler(currentThemeProperty, customThemesProperty)
    }

    @Test
    fun selectTheme_staticThemeSet() = runTest {
        // WHEN
        storageHandler.selectTheme(testTheme)

        // EXPECT
        verifySuspend(VerifyMode.exhaustiveOrder) {
            currentThemeProperty.set(ThemeReference.StaticTheme(testTheme), any())
        }
    }

    @Test
    fun deleteTheme_exceptionThrown() = runTest {
        // WHEN
        val exception = runCatching {
            storageHandler.deleteTheme(testTheme)
        }.exceptionOrNull()

        // EXPECT
        assertThat(exception).isNotNull()
    }

    @Test
    fun canDeleteTheme_resultFalse() = runTest {
        // WHEN
        val result: Boolean = storageHandler.canDeleteTheme(testTheme)

        // EXPECT
        assertThat(result).isFalse()
    }
}

package dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler

import androidx.compose.ui.graphics.Color
import assertk.assertThat
import assertk.assertions.isFalse
import assertk.assertions.isNotNull
import assertk.assertions.isTrue
import dev.mokkery.answering.returns
import dev.mokkery.every
import dev.mokkery.everySuspend
import dev.mokkery.matcher.any
import dev.mokkery.mock
import dev.mokkery.verify.VerifyMode
import dev.mokkery.verifySuspend
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.model.ThemeValuesData
import dev.toastbits.composekit.theme.core.type.CustomThemeType
import kotlinx.coroutines.test.runTest
import kotlin.test.BeforeTest
import kotlin.test.Test

class CustomThemeStorageHandlerTest {
    private lateinit var storageHandler: CustomThemeStorageHandler

    private lateinit var currentThemeProperty: PlatformSettingsProperty<ThemeReference>
    private lateinit var customThemesProperty: PlatformSettingsProperty<List<NamedTheme>>

    private val testThemeA = NamedTheme(NamedTheme.Type.CUSTOM, "testA", ThemeValuesData.ofSingleColour(Color.Black))
    private val testThemeB = NamedTheme(NamedTheme.Type.CUSTOM, "testB", ThemeValuesData.ofSingleColour(Color.White))

    @BeforeTest
    fun setUp() {
        currentThemeProperty = mock {
            everySuspend { set(any<ThemeReference>()) } returns Unit
            everySuspend { reset() } returns Unit
        }
        customThemesProperty = mock {
            everySuspend { set(any<List<NamedTheme>>()) } returns Unit
        }
        storageHandler = CustomThemeStorageHandler(
            currentThemeProperty,
            customThemesProperty
        )
    }

    @Test
    fun selectTheme_existingTheme_themeListUpdated() = runTest {
        // GIVEN
        val initialThemes = listOf(testThemeB, testThemeA)
        val newTheme = CustomThemeType.IndexedTheme(0, testThemeA)
        val finalThemes = listOf(testThemeA, testThemeA)
        every {
            customThemesProperty.get()
        } returns initialThemes

        // WHEN
        storageHandler.selectTheme(newTheme)

        // EXPECT
        verifySuspend(VerifyMode.exhaustiveOrder) {
            customThemesProperty.get()
            customThemesProperty.set(finalThemes, any())
        }
    }

    @Test
    fun selectTheme_newTheme_themeListAppended() = runTest {
        // GIVEN
        val initialThemes = listOf(testThemeB, testThemeA)
        val newTheme = CustomThemeType.IndexedTheme(null, testThemeB)
        val finalThemes = listOf(testThemeB, testThemeA, testThemeB)
        every {
            customThemesProperty.get()
        } returns initialThemes

        // WHEN
        storageHandler.selectTheme(newTheme)

        // EXPECT
        verifySuspend(VerifyMode.exhaustiveOrder) {
            customThemesProperty.get()
            customThemesProperty.set(finalThemes, any())
            currentThemeProperty.set(ThemeReference.CustomTheme(finalThemes.size - 1))
        }
    }

    @Test
    fun selectTheme_outOfBoundsTheme_exceptionThrown() = runTest {
        // GIVEN
        val initialThemes = listOf(testThemeB, testThemeA)
        val newTheme = CustomThemeType.IndexedTheme(2, testThemeA)
        every {
            customThemesProperty.get()
        } returns initialThemes

        // WHEN
        val exception: Throwable? = runCatching {
            storageHandler.selectTheme(newTheme)
        }.exceptionOrNull()

        // EXPECT
        assertThat(exception).isNotNull()
        verifySuspend(VerifyMode.not) {
            customThemesProperty.set(any<List<NamedTheme>>(), any())
        }
    }

    @Test
    fun deleteTheme_themeWithIndex_themeDeletedAndOthersUpdated() = runTest {
        // GIVEN
        val initialThemes = listOf(testThemeB, testThemeA, testThemeB)
        val finalThemes = listOf(testThemeB, testThemeB)
        val themeToDelete = CustomThemeType.IndexedTheme(1, testThemeA)
        val initialTheme = ThemeReference.CustomTheme(2)
        val finalTheme = ThemeReference.CustomTheme(1)
        every {
            currentThemeProperty.get()
        } returns initialTheme
        every {
            customThemesProperty.get()
        } returns initialThemes

        // WHEN
        storageHandler.deleteTheme(themeToDelete)

        // EXPECT
        verifySuspend(VerifyMode.exhaustiveOrder) {
            customThemesProperty.get()
            currentThemeProperty.get()
            currentThemeProperty.set(finalTheme, any())
            customThemesProperty.set(finalThemes, any())
        }
    }

    @Test
    fun deleteTheme_themeWithCurrentIndex_themeDeletedAndSystemThemeSelected() = runTest {
        // GIVEN
        val initialThemes = listOf(testThemeB, testThemeA, testThemeB)
        val finalThemes = listOf(testThemeB, testThemeB)
        val themeToDelete = CustomThemeType.IndexedTheme(1, testThemeA)
        val initialTheme = ThemeReference.CustomTheme(1)
        every {
            currentThemeProperty.get()
        } returns initialTheme
        every {
            customThemesProperty.get()
        } returns initialThemes

        // WHEN
        storageHandler.deleteTheme(themeToDelete)

        // EXPECT
        verifySuspend(VerifyMode.exhaustiveOrder) {
            customThemesProperty.get()
            currentThemeProperty.get()
            currentThemeProperty.reset()
            customThemesProperty.set(finalThemes, any())
        }
    }

    @Test
    fun deleteTheme_themeWithoutIndex_exceptionThrown() = runTest {
        // GIVEN
        val themeToDelete = CustomThemeType.IndexedTheme(null, testThemeB)

        // WHEN
        val exception: Throwable? = runCatching {
            storageHandler.deleteTheme(themeToDelete)
        }.exceptionOrNull()

        // EXPECT
        assertThat(exception).isNotNull()
        verifySuspend(VerifyMode.not) {
            currentThemeProperty.get()
            currentThemeProperty.set(any<ThemeReference>(), any())
            customThemesProperty.get()
            customThemesProperty.set(any<List<NamedTheme>>(), any())
        }
    }

    @Test
    fun canDeleteTheme_themeWithIndex_resultTrue() = runTest {
        for (index in 0 until 10) {
            // GIVEN
            val theme = CustomThemeType.IndexedTheme(index, testThemeB)

            // WHEN
            val result: Boolean = storageHandler.canDeleteTheme(theme)

            // EXPECT
            assertThat(result).isTrue()
        }
    }

    @Test
    fun canDeleteTheme_themeWithoutIndex_resultFalse() = runTest {
        // GIVEN
        val theme = CustomThemeType.IndexedTheme(null, testThemeB)

        // WHEN
        val result: Boolean = storageHandler.canDeleteTheme(theme)

        // EXPECT
        assertThat(result).isFalse()
    }
}

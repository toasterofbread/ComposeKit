package dev.toastbits.composekit.settingsitem.presentation.ui.screen

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.input.TextFieldLineLimits
import androidx.compose.foundation.text.input.TextFieldState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.LocalComposeKitDialogWrapper
import dev.toastbits.composekit.components.platform.composable.ScrollBarColumn
import dev.toastbits.composekit.navigation.compositionlocal.LocalNavigator
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.navigation.util.AlertDialog
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.ThemePaletteEditor
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler.ThemeStorageHandler
import dev.toastbits.composekit.settingsitem.resources.generated.resources.Res
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_confirmation_screen_button_cancel
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_confirmation_screen_button_delete_theme
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_confirmation_screen_button_edit_theme_name
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_confirmation_screen_button_set_theme
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_confirmation_screen_button_theme_name_apply
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_confirmation_screen_button_theme_save_as
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_confirmation_screen_button_theme_save_as_save
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_confirmation_screen_heading_configure
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_confirmation_screen_title
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.type.ThemeTypeConfig
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.util.composable.plus
import dev.toastbits.composekit.util.getContrasted
import dev.toastbits.composekit.util.platform.rememberResourceState
import dev.toastbits.kmpresources.library.mapper.toKmpResource
import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.jetbrains.compose.resources.stringResource

class ThemeConfirmationScreen<T: ThemeValues>(
    val initialTheme: T,
    val themeConfiguration: ThemeTypeConfig<T>,
    val storageHandler: ThemeStorageHandler<T>,
    val canEditName: Boolean = true,
    val onFinished: suspend () -> Unit
): Screen {
    private var currentTheme: T by mutableStateOf(initialTheme)
    private var editingThemeNameTextFieldState: TextFieldState? by mutableStateOf(null)

    override val title: KmpStringResource =
        Res.string.theme_confirmation_screen_title.toKmpResource()

    override fun onClosed(movingBackward: Boolean) {
        currentTheme = initialTheme
        editingThemeNameTextFieldState = null
    }

    @Composable
    override fun Content(modifier: Modifier, contentPadding: PaddingValues) {
        val density: Density = LocalDensity.current
        var bottomBarHeight: Dp by remember { mutableStateOf(0.dp) }

        ThemeNameEditDialog()

        Box(modifier) {
            ScrollBarColumn(
                contentPadding = contentPadding + PaddingValues(bottom = bottomBarHeight + 10.dp),
                verticalArrangement = Arrangement.spacedBy(15.dp)
            ) {
                ThemePaletteEditor(
                    currentTheme,
                    slots = ThemeValues.Slot.BuiltIn.entries,
                    canEditSlot = { slot ->
                        themeConfiguration.type.canSetSlotColour(currentTheme, slot)
                    },
                    onEdit = { slot, colour ->
                        currentTheme = themeConfiguration.type.setSlotColour(currentTheme, slot, colour)
                    },
                    titleContent = {
                        ThemeTitle()
                    }
                )

                OptionsColumn(
                    Modifier
                        .fillMaxWidth()
                        .padding(top = 20.dp)
                )
            }

            BottomRow(
                Modifier
                    .fillMaxWidth()
                    .padding(contentPadding)
                    .align(Alignment.BottomCenter)
                    .onSizeChanged {
                        with (density) {
                            bottomBarHeight = it.height.toDp()
                        }
                    }
            )
        }
    }

    private fun commitEditedThemeName() {
        editingThemeNameTextFieldState?.also {
            currentTheme = themeConfiguration.type.setThemeName(currentTheme, it.text.toString().trim())
        }
        editingThemeNameTextFieldState = null
    }

    @Composable
    fun ThemeNameEditDialog() {
        val canSetThemeName: Boolean = themeConfiguration.type.canSetThemeName(currentTheme)
        LaunchedEffect(canSetThemeName) {
            if (!canSetThemeName) {
                editingThemeNameTextFieldState = null
            }
        }

        LocalNavigator.current.AlertDialog(
            show = editingThemeNameTextFieldState != null,
            onDismissRequest = { editingThemeNameTextFieldState = null },
            confirmButton = {
                Button(::commitEditedThemeName) {
                    Text(stringResource(Res.string.theme_confirmation_screen_button_theme_name_apply))
                }
            },
            text = {
                OutlinedTextField(
                    state = editingThemeNameTextFieldState ?: TextFieldState(),
                    modifier = Modifier.fillMaxWidth(),
                    lineLimits = TextFieldLineLimits.SingleLine,
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                    onKeyboardAction = {
                        commitEditedThemeName()
                    }
                )
            }
        )
    }

    @Composable
    private fun ThemeTitle(modifier: Modifier = Modifier) {
        Row(
            modifier,
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            val themeName: String = themeConfiguration.type.rememberThemeName(currentTheme)
            Text(
                themeName,
                style = MaterialTheme.typography.headlineMedium
            )

            AnimatedVisibility(canEditName && themeConfiguration.type.canSetThemeName(currentTheme)) {
                IconButton({
                    editingThemeNameTextFieldState = TextFieldState(themeName)
                }) {
                    Icon(
                        Icons.Default.Edit,
                        stringResource(Res.string.theme_confirmation_screen_button_edit_theme_name),
                        Modifier.alpha(0.7f)
                    )
                }
            }
        }
    }

    @Composable
    private fun OptionsColumn(modifier: Modifier = Modifier) {
        val configItems: List<SettingsItem> =
            themeConfiguration.rememberConfigItems(
                currentTheme,
                onChanged = {
                    currentTheme = it
                }
            )

        if (configItems.isNotEmpty()) {
            Column(
                modifier,
                verticalArrangement = Arrangement.spacedBy(15.dp)
            ) {
                Text(
                    stringResource(Res.string.theme_confirmation_screen_heading_configure),
                    style = MaterialTheme.typography.headlineSmall
                )

                for (item in configItems) {
                    item.Item(Modifier)
                }
            }
        }
    }

    @Composable
    private fun BottomRow(modifier: Modifier = Modifier) {
        val theme: ThemeValues = LocalComposeKitTheme.current
        val coroutineScope: CoroutineScope = rememberCoroutineScope()

        var showSaveAsDialog: Boolean by remember { mutableStateOf(false) }
        LocalComposeKitDialogWrapper.current.DialogWrapper(showSaveAsDialog) {
            SaveAsDialog(
                onDismissRequest = { showSaveAsDialog = false }
            )
        }

        FlowRow(
            modifier,
            horizontalArrangement = Arrangement.spacedBy(10.dp)
        ) {
            if (storageHandler.canDeleteTheme(currentTheme)) {
                Button(
                    onClick = {
                        coroutineScope.launch {
                            storageHandler.deleteTheme(currentTheme)
                            onFinished()
                        }
                    },
                    colors =
                        ButtonDefaults.buttonColors(
                            containerColor = theme.error,
                            contentColor = theme.error.getContrasted()
                        )
                ) {
                    Text(stringResource(Res.string.theme_confirmation_screen_button_delete_theme))
                }
            }

            if (storageHandler.canSaveAsNewTheme(currentTheme)) {
                Button({
                    showSaveAsDialog = true
                }) {
                    Text(stringResource(Res.string.theme_confirmation_screen_button_theme_save_as))
                }
            }

            Spacer(Modifier.fillMaxWidth().weight(1f))

            FlowRow(
                horizontalArrangement = Arrangement.spacedBy(10.dp)
            ) {
                OutlinedButton(
                    {
                        coroutineScope.launch {
                            onFinished()
                        }
                    },
                    colors = ButtonDefaults.outlinedButtonColors(containerColor = theme.background)
                ) {
                    Text(stringResource(Res.string.theme_confirmation_screen_button_cancel))
                }

                Button({
                    coroutineScope.launch {
                        storageHandler.selectTheme(currentTheme)
                        onFinished()
                    }
                }) {
                    Text(stringResource(Res.string.theme_confirmation_screen_button_set_theme))
                }
            }
        }
    }

    @Composable
    private fun SaveAsDialog(
        onDismissRequest: () -> Unit
    ) {
        val coroutineScope: CoroutineScope = rememberCoroutineScope()
        val nameFieldState: TextFieldState? by
            rememberResourceState(Unit, { null }) {
                TextFieldState(themeConfiguration.type.getThemeName(currentTheme))
            }

        AlertDialog(
            onDismissRequest = onDismissRequest,
            confirmButton = {
                Button({
                    coroutineScope.launch {
                        storageHandler.saveAsNewTheme(currentTheme, nameFieldState?.text?.toString().orEmpty(), NamedTheme.Type.CUSTOM)
                        onFinished()
                    }
                }) {
                    Text(stringResource(Res.string.theme_confirmation_screen_button_theme_save_as_save))
                }
            },
            text = {
                OutlinedTextField(
                    state = nameFieldState ?: TextFieldState(),
                    modifier = Modifier.fillMaxWidth()
                )
            }
        )
    }
}

package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.LocalComposeKitDialogWrapper
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.settingsitem.presentation.ui.component.DropdownButton
import dev.toastbits.composekit.settingsitem.presentation.ui.component.dialog.ThemePickerDialog
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemText
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemTitleText
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.provider.ThemeTypeConfigProvider
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.ui.LocalThemeProvider

data class ThemeSettingsItem(
    private val state: PlatformSettingsProperty<ThemeReference>,
    private val themeTypes: List<ThemeType<*>>,
    private val themeTypeConfigProvider: ThemeTypeConfigProvider,
    private val pickerDialogTitle: @Composable () -> Unit
): SettingsItem() {
    override suspend fun resetValues() {
        state.reset()
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> = emptyList()

    private var showThemePicker: Boolean by mutableStateOf(false)

    override fun resetUiState() {
        showThemePicker = false
    }

    @Composable
    override fun Item(modifier: Modifier) {
        val currentTheme: SerialisableTheme = state.observe().value.getTheme(LocalThemeProvider.current)

        LocalComposeKitDialogWrapper.current.DialogWrapper(showThemePicker) {
            ThemePickerDialog(
                themeTypes = themeTypes,
                themeTypeConfigProvider = themeTypeConfigProvider,
                titleContent = pickerDialogTitle,
                onDismissRequest = {
                    showThemePicker = false
                },
                onSelected = {
                    state.set(it)
                    showThemePicker = false
                }
            )
        }

        FlowRow(
            modifier,
            verticalArrangement = Arrangement.spacedBy(5.dp),
            itemVerticalAlignment = Alignment.CenterVertically
        ) {
            Column(
                Modifier
                    .fillMaxWidth()
                    .weight(1f)
            ) {
                ItemTitleText(state.name.getComposable())
                ItemText(state.description?.getComposable())
            }

            DropdownButton(
                text = currentTheme.readableName,
                onClick = { showThemePicker = true },
                onCycle = null,
                modifier = Modifier.requiredHeight(40.dp)
            )
        }
    }
}

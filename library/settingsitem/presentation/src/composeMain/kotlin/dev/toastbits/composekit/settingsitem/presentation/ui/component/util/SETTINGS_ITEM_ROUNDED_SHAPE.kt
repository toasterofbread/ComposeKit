package dev.toastbits.composekit.settingsitem.presentation.ui.component.util

import androidx.compose.foundation.shape.CornerBasedShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import dev.toastbits.composekit.util.platform.Platform

val SETTINGS_ITEM_ROUNDED_SHAPE: CornerBasedShape
    @Composable
    get() = when (Platform.current) {
        Platform.ANDROID -> MaterialTheme.shapes.extraLarge
        Platform.DESKTOP,
        Platform.WEB -> MaterialTheme.shapes.small
    }

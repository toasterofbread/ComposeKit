package dev.toastbits.composekit.settingsitem.presentation.ui.item

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.utils.composable.LargeDropdownMenu
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.settingsitem.presentation.ui.component.DropdownButton
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemText
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemTitleText
import dev.toastbits.composekit.settingsitem.presentation.util.getConvertedProperty

class DropdownSettingsItem(
    val state: PlatformSettingsProperty<Int>,
    val itemCount: Int,
    val columns: GridCells = GridCells.Fixed(1),
    val getButtonItem: (@Composable (Int) -> String)? = null,
    val itemContent: (@Composable (Int, String) -> Unit)? = null,
    val hoverPreviewContent: (@Composable (Int?) -> Unit)? = null,
    dropdownStartsOpen: Boolean = false,
    val getItem: @Composable (Int) -> String
): SettingsItem() {
    var dropdownOpen: Boolean by mutableStateOf(dropdownStartsOpen)
        private set

    override suspend fun resetValues() {
        state.reset()
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> = listOf(state)

    override fun resetUiState() {
        dropdownOpen = false
    }

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        var currentValue: Int by state.observe()
        var hoveringItem: Int? by remember { mutableStateOf(null) }

        FlowRow(
            modifier,
            verticalArrangement = Arrangement.spacedBy(5.dp)
        ) {
            Column(
                Modifier
                    .fillMaxWidth()
                    .weight(1f)
                    .align(Alignment.CenterVertically)
            ) {
                ItemTitleText(state.name.getComposable())
                ItemText(state.description?.getComposable())
            }

            DropdownButton(
                text = getButtonItem?.invoke(currentValue) ?: getItem(currentValue),
                onClick = { dropdownOpen = true },
                onCycle = {
                    currentValue = (currentValue + it).coerceIn(0, itemCount - 1)
                },
                modifier =
                    Modifier
                        .requiredHeight(40.dp)
                        .align(Alignment.CenterVertically)

            )

            LargeDropdownMenu(
                title = state.name.getComposable(),
                isOpen = dropdownOpen,
                onDismissRequest = { dropdownOpen = false },
                columns = columns,
                items = remember(itemCount) { (0 until itemCount).toList() },
                selectedItem = currentValue,
                onSelected = { _, index ->
                    currentValue = index
                    dropdownOpen = false
                },
                onItemHovered = {
                    hoveringItem = it?.second
                },
                additionalTopContent = {
                    hoverPreviewContent?.invoke(hoveringItem)
                }
            ) { index ->
                val itemText: String = getItem(index)
                if (itemContent != null) {
                    itemContent.invoke(index, itemText)
                }
                else {
                    Text(itemText)
                }
            }
        }
    }

    companion object {
        inline fun <reified T: Enum<T>> ofEnumState(
            state: PlatformSettingsProperty<T>,
            columns: GridCells = GridCells.Fixed(1),
            noinline getButtonItem: (@Composable (T) -> String)? = null,
            noinline itemContent: (@Composable (Int, String) -> Unit)? = null,
            noinline hoverPreviewContent: (@Composable (Int?) -> Unit)? = null,
            noinline getItem: @Composable (T) -> String
        ): DropdownSettingsItem =
            DropdownSettingsItem(
                state.getConvertedProperty(
                    fromProperty = { it.ordinal },
                    toProperty = { enumValues<T>()[it] }
                ),
                itemCount = enumValues<T>().size,
                columns = columns,
                getButtonItem = getButtonItem?.let { lambda -> { lambda(enumValues<T>()[it]) } },
                itemContent = itemContent,
                hoverPreviewContent = hoverPreviewContent,
                getItem = { getItem(enumValues<T>()[it]) }
            )
    }
}

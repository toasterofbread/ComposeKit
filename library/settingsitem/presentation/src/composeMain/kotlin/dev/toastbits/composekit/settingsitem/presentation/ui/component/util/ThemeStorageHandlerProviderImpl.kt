package dev.toastbits.composekit.settingsitem.presentation.ui.component.util

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.provider.ThemeStorageHandlerProvider
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler.CustomThemeStorageHandler
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler.StaticThemeStorageHandler
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler.SystemThemeStorageHandler
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler.ThemeStorageHandler
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.type.AnimatedThemeType
import dev.toastbits.composekit.theme.core.type.CatppuccinThemeType
import dev.toastbits.composekit.theme.core.type.CustomThemeType
import dev.toastbits.composekit.theme.core.type.RosePineThemeType
import dev.toastbits.composekit.theme.core.type.SystemLightDarkThemeType
import dev.toastbits.composekit.theme.core.type.SystemThemeType
import dev.toastbits.composekit.theme.core.type.ThemeType

class ThemeStorageHandlerProviderImpl(
    private val currentThemeProperty: PlatformSettingsProperty<ThemeReference>,
    private val customThemesProperty: PlatformSettingsProperty<List<NamedTheme>>
): ThemeStorageHandlerProvider {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ThemeValues> invoke(themeType: ThemeType<T>): ThemeStorageHandler<T> =
        when (themeType) {
            is CustomThemeType ->
                CustomThemeStorageHandler(currentThemeProperty, customThemesProperty)

            SystemThemeType ->
                SystemThemeStorageHandler(currentThemeProperty, customThemesProperty)

            AnimatedThemeType,
            SystemLightDarkThemeType,
            RosePineThemeType,
            CatppuccinThemeType ->
                StaticThemeStorageHandler(currentThemeProperty, customThemesProperty)
        } as ThemeStorageHandler<T>
}
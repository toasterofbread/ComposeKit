package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import dev.toastbits.composekit.components.utils.composable.LinkifyText
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent
import dev.toastbits.kmpresources.library.model.KmpStringResource

actual class InfoTextSettingsItem actual constructor(
    text: KmpStringResource
): BaseInfoTextSettingsItem(text) {
    actual override fun resetUiState() {}

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        LinkifyText(
            text.getComposable(),
            LocalComposeKitTheme.current.vibrantAccent,
            modifier
        )
    }
}

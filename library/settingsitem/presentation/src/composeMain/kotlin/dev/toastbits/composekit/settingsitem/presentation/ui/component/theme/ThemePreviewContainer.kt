package dev.toastbits.composekit.settingsitem.presentation.ui.component.theme

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.hoverable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsHoveredAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.utils.composable.animatedvisibility.NullableValueAnimatedVisibility
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.makeVibrant
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent
import dev.toastbits.composekit.util.composable.animateFloatAsState
import dev.toastbits.composekit.util.thenWith
import dev.toastbits.composekit.util.toFloat

@Composable
fun <T: ThemeValues> ThemePreviewContainer(
    label: String?,
    theme: T,
    modifier: Modifier = Modifier,
    cornerContent: (@Composable RowScope.() -> Unit)? = null,
    onClicked: ((T) -> Unit)? = null,
    onSelected: ((T) -> Unit)? = null,
    isSelected: Boolean = false,
    hoverContent: @Composable BoxScope.() -> Unit = {},
    content: @Composable BoxScope.(T) -> Unit
) {
    val shape: Shape = MaterialTheme.shapes.medium

    val interactionSource: MutableInteractionSource = remember { MutableInteractionSource() }
    val hovering: Boolean by interactionSource.collectIsHoveredAsState()
    val hoverContentAlpha: Float by animateFloatAsState { hovering.toFloat() }

    Column(modifier) {
        Box(
            Modifier
                .border(2.dp, LocalComposeKitTheme.current.makeVibrant(theme.accent), shape)
                .background(theme.background, shape)
                .clip(shape)
                .thenWith(theme, onClicked) { loadedTheme, lambda ->
                    clickable(
                        onClick = {
                            lambda(loadedTheme)
                        },
                        interactionSource = interactionSource,
                        indication = null
                    )
                    .hoverable(interactionSource)
                }
                .fillMaxSize()
                .weight(1f)
        ) {
            Box(
                Modifier
                    .padding(15.dp)
                    .matchParentSize()
            ) {
                content(theme)
            }

            Box(
                Modifier
                    .graphicsLayer {
                        alpha = hoverContentAlpha
                    }
                    .background(
                        theme.vibrantAccent.copy(alpha = 0.35f)
                    )
                    .matchParentSize()
            ) {
                CompositionLocalProvider(LocalContentColor provides theme.onBackground) {
                    hoverContent()
                }
            }
        }

        NullableValueAnimatedVisibility(
            if (label != null) label
            else if (onSelected != null || cornerContent != null) ""
            else null,
            enter = slideInVertically(),
            exit = slideOutVertically()
        ) { currentLabel ->
            Row(
                Modifier.heightIn(min = 48.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    currentLabel.orEmpty(),
                    Modifier.fillMaxWidth().weight(1f),
                    style = MaterialTheme.typography.titleMedium
                )

                cornerContent?.invoke(this)

                AnimatedVisibility(
                    isSelected || onSelected != null,
                    enter = fadeIn() + slideInHorizontally { it / 2 },
                    exit = fadeOut() + slideOutHorizontally { it / 2 }
                ) {
                    RadioButton(
                        selected = isSelected,
                        onClick = {
                            onSelected?.invoke(theme)
                        }
                    )
                }
            }
        }
    }
}

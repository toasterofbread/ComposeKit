package dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.provider

import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler.ThemeStorageHandler
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.type.ThemeType

interface ThemeStorageHandlerProvider {
    operator fun <T: ThemeValues> invoke(themeType: ThemeType<T>): ThemeStorageHandler<T>
}

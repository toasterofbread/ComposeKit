package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.input.TextFieldState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.FilledTonalIconButton
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.utils.composable.LargeDropdownMenu
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.settingsitem.presentation.ui.component.DropdownButton
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemText
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemTitleText
import dev.toastbits.composekit.settingsitem.resources.generated.resources.Res
import dev.toastbits.composekit.settingsitem.resources.generated.resources.locale_selector_custom_field_button_done
import dev.toastbits.composekit.settingsitem.resources.generated.resources.locale_selector_custom_field_placeholder
import dev.toastbits.composekit.settingsitem.resources.generated.resources.locale_selector_custom_field_title
import dev.toastbits.composekit.settingsitem.resources.generated.resources.locale_selector_header_available_locales
import dev.toastbits.composekit.settingsitem.resources.generated.resources.locale_selector_system_locale_name
import dev.toastbits.composekit.util.model.Locale
import dev.toastbits.composekit.util.model.LocaleList
import dev.toastbits.composekit.util.platform.rememberResourceState
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import org.jetbrains.compose.resources.stringResource

actual class LocaleSettingsItem actual constructor(
    state: PlatformSettingsProperty<Locale?>,
    localeList: LocaleList,
    allowCustomLocale: Boolean
): BaseLocaleSettingsItem(
    state, localeList, allowCustomLocale
) {
    @Composable
    private fun Locale?.getReadableName(): String =
        if (this == null) stringResource(Res.string.locale_selector_system_locale_name)
        else
            rememberResourceState(localeList to this, { "" }) {
                localeList.getReadableLocaleName(this@getReadableName) ?: this@getReadableName.toString()
            }.value

    private var showingDropdown: Boolean by mutableStateOf(false)

    actual override fun resetUiState() {
        showingDropdown = false
    }

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        var currentLocale: Locale? by state.observe()

        val localeListLocales: List<Locale>? by
            rememberResourceState(localeList, { null }) {
                localeList.getLocales()
            }
        val availableLocales: List<Locale?>? =
            localeListLocales?.let { listOf(null) + it }

        FlowRow(
            modifier,
            verticalArrangement = Arrangement.spacedBy(5.dp),
            itemVerticalAlignment = Alignment.CenterVertically
        ) {
            Column(
                Modifier
                    .fillMaxWidth()
                    .weight(1f)
            ) {
                ItemTitleText(state.name.getComposable())
                ItemText(state.description?.getComposable())
            }

            DropdownButton(
                text =
                currentLocale.let { locale ->
                    if (locale == null || availableLocales?.contains(locale) == true) locale.getReadableName()
                    else locale.toTag()
                },
                onClick = { showingDropdown = true },
                onCycle = { delta ->
                    val locales: List<Locale?> =
                        availableLocales ?: return@DropdownButton

                    val currentLocaleIndex: Int? =
                        locales.indexOf(currentLocale).takeIf { it != -1 }

                    if (currentLocaleIndex == null && delta <= 0) {
                        return@DropdownButton
                    }

                    currentLocale =
                        locales.getOrNull((currentLocaleIndex ?: -1) + delta)
                            ?: return@DropdownButton
                }
            )
        }

        LocaleSelectionPopup(
            isOpen = showingDropdown,
            currentLocale = currentLocale,
            availableLocales = availableLocales,
            onLocaleSelected = {
                currentLocale = it
                showingDropdown = false
            },
            onDismissRequest = {
                showingDropdown = false
            }
        )
    }

    @Composable
    private fun LocaleSelectionPopup(
        isOpen: Boolean,
        currentLocale: Locale?,
        availableLocales: List<Locale?>?,
        onLocaleSelected: (Locale?) -> Unit,
        onDismissRequest: () -> Unit,
        modifier: Modifier = Modifier
    ) {
        LargeDropdownMenu(
            title = stringResource(Res.string.locale_selector_header_available_locales),
            isOpen = isOpen,
            onDismissRequest = onDismissRequest,
            items = availableLocales.orEmpty(),
            selectedItem = currentLocale,
            onSelected = { _, locale ->
                onLocaleSelected(locale)
            },
            additionalTopContent = {
                if (allowCustomLocale) {
                    LocaleInputField(onLocaleSelected)
                }
            },
            modifier = modifier
        ) { locale ->
            Text(locale.getReadableName())
        }
    }

    @Composable
    private fun LocaleInputField(onLocaleSelected: (Locale) -> Unit, modifier:  Modifier = Modifier) {
        val textFieldState: TextFieldState = remember { TextFieldState() }
        val textIsBlank: Boolean by remember { derivedStateOf { textFieldState.text.isBlank() } }
        var selectedLocale: Locale? by remember { mutableStateOf(null) }

        LaunchedEffect(Unit) {
            snapshotFlow { textFieldState.text.toString() }
                .collectLatest { text ->
                    delay(100)
                    selectedLocale =
                        try {
                            Locale.parse(text)
                        }
                        catch (_: Throwable) {
                            null
                        }
                }
        }

        FlowRow(
            modifier,
            itemVerticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(10.dp, Alignment.End)
        ) {
            OutlinedTextField(
                textFieldState,
                Modifier.fillMaxWidth().weight(1f),
                isError = !textIsBlank && selectedLocale == null,
                label = { Text(stringResource(Res.string.locale_selector_custom_field_title)) },
                placeholder = {
                    Text(
                        stringResource(Res.string.locale_selector_custom_field_placeholder),
                        Modifier.alpha(0.5f)
                    )
                }
            )

            FilledTonalIconButton(
                { selectedLocale?.also { onLocaleSelected(it) } },
                enabled = selectedLocale != null
            ) {
                Icon(Icons.Default.Done, stringResource(Res.string.locale_selector_custom_field_button_done))
            }
        }
    }
}

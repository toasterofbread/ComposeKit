package dev.toastbits.composekit.settingsitem.presentation.ui.item

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.settingsitem.presentation.util.getConvertedProperty
import dev.toastbits.composekit.theme.core.model.ComposeKitFont
import dev.toastbits.composekit.theme.core.util.rememberAvailableFonts

class FontSelectorSettingsItem(
    val themeProperty: PlatformSettingsProperty<ComposeKitFont>
): SettingsItem() {
    override suspend fun resetValues() {
        themeProperty.reset()
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> =
        listOf(themeProperty)

    private var dropdownOpen: Boolean by mutableStateOf(false)

    override fun resetUiState() {
        dropdownOpen = false
    }

    @Composable
    override fun Item(modifier: Modifier) {
        val currentFont: ComposeKitFont by themeProperty.observe()
        val availableFonts: List<ComposeKitFont> = ComposeKitFont.rememberAvailableFonts()

        val intProperty: PlatformSettingsProperty<Int> =
            themeProperty.getConvertedProperty(
                fromProperty = { availableFonts.indexOf(currentFont) },
                toProperty = { availableFonts[it] }
            )

        val dropdownItem: DropdownSettingsItem =
            remember(themeProperty) {
                DropdownSettingsItem(
                    intProperty,
                    itemCount = availableFonts.size,
                    dropdownStartsOpen = dropdownOpen
                ) {
                    availableFonts[it].getDisplayName()
                }
            }

        LaunchedEffect(dropdownItem.dropdownOpen) {
            dropdownOpen = dropdownItem.dropdownOpen
        }

        dropdownItem.Item(modifier)
    }
}

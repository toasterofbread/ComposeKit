package dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler

import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.model.NamedTheme

interface ThemeStorageHandler<T: ThemeValues> {
    suspend fun selectTheme(theme: T)

    fun canSaveAsNewTheme(theme: T): Boolean
    suspend fun saveAsNewTheme(theme: T, name: String?, type: NamedTheme.Type)

    fun canDeleteTheme(theme: T): Boolean
    suspend fun deleteTheme(theme: T)
}

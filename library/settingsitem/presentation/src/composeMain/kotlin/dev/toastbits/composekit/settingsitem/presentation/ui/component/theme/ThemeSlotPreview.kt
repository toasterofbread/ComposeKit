package dev.toastbits.composekit.settingsitem.presentation.ui.component.theme

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.FlowRowOverflow
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.settingsitem.resources.generated.resources.Res
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_auto_colour_label
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.get
import dev.toastbits.composekit.theme.core.onAccent
import dev.toastbits.composekit.theme.core.readableName
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent
import dev.toastbits.composekit.util.toRGBHexString
import org.jetbrains.compose.resources.stringResource

@Composable
fun ThemeSlotPreview(
    theme: ThemeValues,
    slot: ThemeValues.Slot,
    modifier: Modifier = Modifier,
    customButtonContent: @Composable RowScope.() -> Unit = {}
) {
    val currentTheme: ThemeValues = LocalComposeKitTheme.current
    val slotColour: Color = theme[slot]

    val horizontalArrangement: Arrangement.Horizontal = Arrangement.spacedBy(7.dp)

    FlowRow(
        modifier,
        itemVerticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = horizontalArrangement,
        overflow = FlowRowOverflow.Visible
    ) {
        Row(
            Modifier.fillMaxWidth().weight(1f),
            horizontalArrangement = horizontalArrangement,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                slot.readableName,
                style = MaterialTheme.typography.labelLarge,
                softWrap = false
            )

            if (slot is ThemeValues.Slot.Extension) {
                Box(
                    Modifier
                        .background(currentTheme.vibrantAccent.copy(alpha = 0.7f), MaterialTheme.shapes.small)
                        .padding(horizontal = 5.dp)
                ) {
                    Text(
                        stringResource(Res.string.theme_auto_colour_label),
                        softWrap = false,
                        color = currentTheme.onAccent,
                        style = MaterialTheme.typography.labelSmall
                    )
                }
            }
        }

        Row(
            Modifier.height(30.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = horizontalArrangement
        ) {
            customButtonContent()

            Box(
                Modifier
                    .border(1.dp, currentTheme.onBackground, CircleShape)
                    .background(slotColour, CircleShape)
                    .size(20.dp)
            )

            SelectionContainer {
                Text(
                    slotColour.toRGBHexString(),
                    Modifier.alpha(0.7f),
                    fontFamily = FontFamily.Monospace
                )
            }
        }
    }
}

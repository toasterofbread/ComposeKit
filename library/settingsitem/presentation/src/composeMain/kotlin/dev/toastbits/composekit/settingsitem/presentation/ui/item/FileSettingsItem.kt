package dev.toastbits.composekit.settingsitem.presentation.ui.item

import androidx.compose.animation.Crossfade
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Folder
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.utils.composable.SubtleLoadingIndicator
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemText
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemTitleText
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.onAccent
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.util.composable.WidthShrinkText
import dev.toastbits.kmpresources.library.model.KmpStringResource
import dev.toastbits.composekit.util.toFloat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.job
import kotlinx.coroutines.launch

class FileSettingsItem(
    val state: PlatformSettingsProperty<String>,
    val getPathLabel: (String) -> KmpStringResource,
    val onSelectRequested: suspend (
        setValue: suspend (String) -> Unit,
        showDialog: suspend (Dialog) -> Unit
    ) -> Unit,
    val extraContent: (@Composable (PlatformSettingsProperty<String>) -> Unit)? = null
): SettingsItem() {
    data class Dialog(
        val title: String,
        val body: String,
        val accept_button: String,
        val deny_button: String?,
        val onSelected: suspend (accepted: Boolean) -> Unit
    )

    override suspend fun resetValues() {
        state.reset()
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> = listOf(state)

    private var actionInProgress: Boolean by mutableStateOf(false)
    private var currentDialog: Dialog? by mutableStateOf(null)
    private val coroutineScope: CoroutineScope = CoroutineScope(Job())
    private val selectionCoroutineScope: CoroutineScope = CoroutineScope(Job())

    override fun resetUiState() {
        actionInProgress = false
        currentDialog = null
        coroutineScope.coroutineContext.cancelChildren()
        selectionCoroutineScope.coroutineContext.cancelChildren()
    }

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        val theme: ThemeValues = LocalComposeKitTheme.current

        LaunchedEffect(currentDialog) {
            actionInProgress = false
            coroutineScope.coroutineContext.job.cancelChildren()
        }

        currentDialog?.also { dialog ->
            AlertDialog(
                { currentDialog = null },
                title = {
                    WidthShrinkText(dialog.title)
                },
                text = {
                    Text(dialog.body)
                },
                confirmButton = {
                    Button(
                        {
                            coroutineScope.launch(Dispatchers.Default) {
                                actionInProgress = true
                                dialog.onSelected(true)
                                actionInProgress = false

                                if (currentDialog == dialog) {
                                    currentDialog = null
                                }
                            }
                        },
                        enabled = !actionInProgress
                    ) {
                        Box(contentAlignment = Alignment.Center) {
                            val alpha: Float by animateFloatAsState(actionInProgress.toFloat())
                            SubtleLoadingIndicator(Modifier.alpha(alpha))
                            Text(
                                dialog.accept_button,
                                Modifier.alpha(1f - alpha)
                            )
                        }
                    }
                },
                dismissButton = dialog.deny_button?.let { deny_button -> {
                    Crossfade(!actionInProgress) { enabled ->
                        Button(
                            {
                                if (!enabled) {
                                    return@Button
                                }

                                coroutineScope.launch(Dispatchers.Default) {
                                    actionInProgress = true
                                    dialog.onSelected(false)
                                    actionInProgress = false

                                    if (currentDialog == dialog) {
                                        currentDialog = null
                                    }
                                }
                            },
                            enabled = enabled
                        ) {
                            Text(deny_button)
                        }
                    }
                }}
            )
        }

        Column(
            modifier.fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(10.dp)
        ) {
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(5.dp)
            ) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .weight(1f),
                    verticalArrangement = Arrangement.spacedBy(5.dp)
                ) {
                    ItemTitleText(state.name.getComposable())
                    ItemText(state.description?.getComposable())
                }

                IconButton({
                    selectionCoroutineScope.launch {
                        onSelectRequested(
                            { path ->
                                state.set(path)
                            },
                            { dialog ->
                                currentDialog = dialog
                            }
                        )
                    }
                }) {
                    Icon(Icons.Default.Folder, null)
                }

                extraContent?.invoke(state)
            }

            Box(
                Modifier
                    .fillMaxWidth()
                    .background(theme.accent, RoundedCornerShape(16.dp))
                    .padding(10.dp)
            ) {
                val value: String? by state.observe()

                Text(
                    value?.let { getPathLabel(it).getComposable() } ?: "",
                    style = MaterialTheme.typography.bodySmall,
                    color = theme.onAccent
                )
            }
        }
    }
}

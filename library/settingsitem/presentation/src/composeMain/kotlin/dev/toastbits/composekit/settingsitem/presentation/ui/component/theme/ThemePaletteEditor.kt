package dev.toastbits.composekit.settingsitem.presentation.ui.component.theme

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.layout.positionInParent
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.utils.composable.ColourPicker
import dev.toastbits.composekit.components.utils.composable.animatedvisibility.NullableValueAnimatedVisibility
import dev.toastbits.composekit.settingsitem.resources.generated.resources.Res
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_palette_editor_edit_colour
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.get
import dev.toastbits.composekit.theme.core.readableName
import dev.toastbits.composekit.util.composable.thenIf
import kotlinx.coroutines.flow.collectLatest
import org.jetbrains.compose.resources.stringResource

@Composable
fun ThemePaletteEditor(
    theme: ThemeValues,
    canEditSlot: (ThemeValues.Slot) -> Boolean,
    onEdit: (ThemeValues.Slot, Color) -> Unit,
    modifier: Modifier = Modifier,
    slots: List<ThemeValues.Slot> = ThemeValues.Slot.entries,
    infoMinWidth: Dp = 350.dp,
    previewMinWidth: Dp = 250.dp,
    previewHeight: Dp = 200.dp,
    titleContent: @Composable () -> Unit = {}
) {
    val density: Density = LocalDensity.current

    FlowRow(
        modifier,
        horizontalArrangement = Arrangement.spacedBy(15.dp, Alignment.CenterHorizontally),
        verticalArrangement = Arrangement.spacedBy(15.dp),
        itemVerticalAlignment = Alignment.Top,
        maxItemsInEachRow = 2
    ) {
        var generalInfoBoxHeight: Int by remember { mutableIntStateOf(0) }
        var generalInfoBoxContentHeight: Int by remember { mutableIntStateOf(0) }
        val generalInfoBoxOverflowHeight: Dp by remember {
            derivedStateOf {
                with (density) {
                    (generalInfoBoxContentHeight - generalInfoBoxHeight).coerceAtLeast(0).toDp()
                }
            }
        }
        
        var previewIsOnFirstRow: Boolean by remember { mutableStateOf(false) }

        var editingSlot: ThemeValues.Slot? by remember {
            mutableStateOf(slots.firstOrNull { canEditSlot(it) })
        }

        LaunchedEffect(Unit) {
            snapshotFlow { editingSlot?.let { canEditSlot(it) } }
                .collectLatest {
                    if (it == false) {
                        editingSlot = null
                    }
                }
        }

        Column(
            Modifier
                .widthIn(min = infoMinWidth)
                .onSizeChanged {
                    generalInfoBoxHeight = it.height
                }
                .wrapContentHeight(
                    align = Alignment.Top,
                    unbounded = true
                )
                .thenIf(previewIsOnFirstRow) {
                    heightIn(min = previewHeight)
                }
                .fillMaxWidth()
                .weight(1f)
                .onSizeChanged {
                    generalInfoBoxContentHeight = it.height
                }
        ) {
            titleContent()

            Spacer(
                Modifier
                    .fillMaxHeight()
                    .weight(1f)
            )

            Spacer(Modifier.height(10.dp))

            for (slot in slots) {
                ThemeSlotPreview(
                    theme,
                    slot,
                    customButtonContent = {
                        if (canEditSlot(slot)) {
                            IconButton(
                                {
                                    editingSlot = slot
                                },
                                modifier = Modifier.size(30.dp)
                            ) {
                                Icon(Icons.Default.Edit, stringResource(Res.string.theme_palette_editor_edit_colour))
                            }
                        }
                    }
                )
            }
        }

        ThemePreview(
            label = null,
            theme = theme,
            modifier =
            Modifier
                .fillMaxWidth()
                .weight(1f)
                .padding(top = generalInfoBoxOverflowHeight)
                .widthIn(min = previewMinWidth)
                .height(previewHeight)
                .onGloballyPositioned {
                    previewIsOnFirstRow = it.positionInParent().y == 0f
                }
        )

        NullableValueAnimatedVisibility(editingSlot) { slot ->
            if (slot == null) {
                return@NullableValueAnimatedVisibility
            }

            Column(
                Modifier.fillMaxWidth(),
                verticalArrangement = Arrangement.spacedBy(10.dp)
            ) {
                Text(slot.readableName)

                ColourPicker(
                    editingSlot,
                    { theme[slot] },
                    Modifier
                        .heightIn(max = 300.dp),
                    onSelected = { colour ->
                        onEdit(slot, colour)
                    },
                    square = false
                )
            }
        }
    }
}

package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemText
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemTitleText
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

actual class MultipleChoiceSettingsItem actual constructor(
    state: PlatformSettingsProperty<Int>,
    choiceAmount: Int,
    getChoiceText: (Int) -> KmpStringResource
): BaseMultipleChoiceSettingsItem(
    state, choiceAmount, getChoiceText
) {

    actual override fun resetUiState() {}

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        val theme: ThemeValues = LocalComposeKitTheme.current
        val currentValue: Int by state.observe()
        val coroutineScope: CoroutineScope = rememberCoroutineScope()

        Column(modifier) {
            Column(Modifier.fillMaxWidth()) {
                ItemTitleText(state.name.getComposable(), Modifier.padding(bottom = 7.dp).fillMaxWidth())
                ItemText(state.description?.getComposable())

                Spacer(Modifier.height(10.dp))

                Column(
                    Modifier.padding(start = 15.dp)
                ) {
                    for (i in 0 until choiceAmount) {
                        Row(
                            Modifier
                                .clickable(
                                    remember { MutableInteractionSource() },
                                    null
                                ) {
                                    coroutineScope.launch {
                                        state.set(i)
                                    }
                                },
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.spacedBy(10.dp)
                        ) {
                            Text(
                                getChoiceText(i).getComposable(),
                                color = theme.onBackground.copy(alpha = 0.8f),
                                modifier = Modifier.fillMaxWidth().weight(1f)
                            )

                            RadioButton(
                                selected = currentValue == i,
                                onClick = {
                                    coroutineScope.launch {
                                        state.set(i)
                                    }
                                }
                            )
                        }
                    }
                }
            }
        }
    }
}

package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.navigation.compositionlocal.LocalNavigator
import dev.toastbits.composekit.navigation.navigator.Navigator
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.settingsitem.presentation.ui.component.DropdownButton
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.provider.ThemeStorageHandlerProvider
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemText
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemTitleText
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ThemeStorageHandlerProviderImpl
import dev.toastbits.composekit.settingsitem.presentation.ui.screen.ThemePickerScreen
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.provider.ThemeProvider
import dev.toastbits.composekit.theme.core.provider.ThemeTypeConfigProvider
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.ui.LocalThemeProvider

data class ThemeSelectorSettingsItem(
    private val currentThemeProperty: PlatformSettingsProperty<ThemeReference>,
    private val customThemesProperty: PlatformSettingsProperty<List<NamedTheme>>,
    private val themeTypeConfigProvider: ThemeTypeConfigProvider,
    private val themeStorageHandlerProvider: ThemeStorageHandlerProvider = ThemeStorageHandlerProviderImpl(currentThemeProperty, customThemesProperty),
    private val themeTypes: List<ThemeType<*>> = ThemeType.getAll(),
    private val canSelectThemesDirectly: Boolean = true,
    private val extraThemes: List<ThemeReference> = emptyList(),
    private val onExtraThemeEdited: (Int) -> Unit = {},
    private val pickerResultHandler: ThemePickerScreen.ResultHandler = ThemePickerScreen.ResultHandler.DEFAULT
): SettingsItem() {
    private val themePickerScreen: ThemePickerScreen =
        ThemePickerScreen(
            currentThemeProperty = currentThemeProperty,
            themeTypes = themeTypes,
            themeTypeConfigProvider = themeTypeConfigProvider,
            themeStorageHandlerProvider = themeStorageHandlerProvider,
            canSelectThemesDirectly = canSelectThemesDirectly,
            extraThemes = extraThemes,
            onExtraThemeEdited = onExtraThemeEdited,
            resultHandler = pickerResultHandler
        )

    override suspend fun resetValues() {
        currentThemeProperty.reset()
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> = listOf(currentThemeProperty)

    override fun resetUiState() {}

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        val navigator: Navigator = LocalNavigator.current
        val themeProvider: ThemeProvider = LocalThemeProvider.current

        val currentThemeReference: ThemeReference by currentThemeProperty.observe()
        val currentTheme: SerialisableTheme =
            remember(currentThemeReference, themeProvider) {
                currentThemeReference.getTheme(themeProvider)
            }

        FlowRow(
            modifier,
            verticalArrangement = Arrangement.spacedBy(5.dp),
            itemVerticalAlignment = Alignment.CenterVertically
        ) {
            Column(
                Modifier
                    .fillMaxWidth()
                    .weight(1f)
            ) {
                ItemTitleText(currentThemeProperty.name.getComposable())
                ItemText(currentThemeProperty.description?.getComposable())
            }

            DropdownButton(
                text = currentTheme.readableName,
                onClick = {
                    navigator.pushScreen(themePickerScreen)
                },
                onCycle = null,
                modifier = Modifier.requiredHeight(40.dp)
            )
        }
    }
}

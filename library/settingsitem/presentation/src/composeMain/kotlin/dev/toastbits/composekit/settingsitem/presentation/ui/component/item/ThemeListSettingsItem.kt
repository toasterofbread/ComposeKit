package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.LocalComposeKitDialogWrapper
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.settingsitem.presentation.ui.component.dialog.ThemePickerDialog
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.ThemePreview
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemTitleText
import dev.toastbits.composekit.settingsitem.presentation.ui.screen.DEFAULT_THEME_PREVIEW_SIZE
import dev.toastbits.composekit.settingsitem.resources.generated.resources.Res
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_list_button_add_theme
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_list_button_remove_theme
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.provider.ThemeProvider
import dev.toastbits.composekit.theme.core.provider.ThemeTypeConfigProvider
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.ui.LocalThemeProvider
import org.jetbrains.compose.resources.stringResource

data class ThemeListSettingsItem(
    private val state: PlatformSettingsProperty<List<ThemeReference>>,
    private val themeTypes: List<ThemeType<*>>,
    private val themeTypeConfigProvider: ThemeTypeConfigProvider,
    private val pickerDialogTitle: @Composable () -> Unit
): SettingsItem() {
    override suspend fun resetValues() {
        state.reset()
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> = emptyList()

    private var showThemePicker: Boolean by mutableStateOf(false)

    override fun resetUiState() {
        showThemePicker = false
    }

    @Composable
    override fun Item(modifier: Modifier) {
        val themeProvider: ThemeProvider = LocalThemeProvider.current

        LocalComposeKitDialogWrapper.current.DialogWrapper(showThemePicker) {
            ThemePickerDialog(
                themeTypeConfigProvider = themeTypeConfigProvider,
                titleContent = pickerDialogTitle,
                onDismissRequest = {
                    showThemePicker = false
                },
                onSelected = {
                    state.set(listOf(it) + state.get())
                    showThemePicker = false
                },
                themeTypes = themeTypes
            )
        }

        Column {
            Row(verticalAlignment = Alignment.CenterVertically) {
                ItemTitleText(
                    state.name.getComposable(),
                    Modifier
                )
                Spacer(Modifier.fillMaxWidth().weight(1f))

                IconButton({ showThemePicker = true }) {
                    Icon(
                        Icons.Default.Add,
                        stringResource(Res.string.theme_list_button_add_theme)
                    )
                }
            }

            FlowRow(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.spacedBy(10.dp),
                verticalArrangement = Arrangement.spacedBy(10.dp)
            ) {
                var themes: List<ThemeReference> by state.observe()

                for ((index, themeReference) in themes.withIndex()) {
                    val theme: SerialisableTheme =
                        remember(themeReference, themeProvider) { themeReference.getTheme(themeProvider) }

                    ThemePreview(
                        theme.readableName,
                        theme,
                        Modifier
                            .fillMaxWidth()
                            .weight(1f)
                            .size(DEFAULT_THEME_PREVIEW_SIZE),
                        cornerContent = {
                            IconButton({
                                themes = themes.toMutableList().apply {
                                    removeAt(index)
                                }
                            }) {
                                Icon(
                                    Icons.Default.Delete,
                                    stringResource(Res.string.theme_list_button_remove_theme)
                                )
                            }
                        }
                    )
                }

                // Prevent gap in middle of bottom row with 2 elements
                Spacer(
                    Modifier
                        .fillMaxWidth()
                        .weight(1f)
                        .width(DEFAULT_THEME_PREVIEW_SIZE.width)
                )
            }
        }
    }
}

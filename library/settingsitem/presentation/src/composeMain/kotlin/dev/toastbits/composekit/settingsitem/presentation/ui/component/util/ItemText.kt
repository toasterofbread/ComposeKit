package dev.toastbits.composekit.settingsitem.presentation.ui.component.util

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import dev.toastbits.composekit.components.utils.composable.LinkifyText
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme

@Composable
fun SettingsItem.Companion.ItemTitleText(
    text: String?,
    modifier: Modifier = Modifier.fillMaxWidth(),
    theme: ThemeValues = LocalComposeKitTheme.current
) {
    if (text?.isNotBlank() == true) {
        Text(
            text,
            modifier,
            style = MaterialTheme.typography.titleMedium.copy(color = theme.onBackground)
        )
    }
}

@Composable
fun SettingsItem.Companion.ItemText(
    text: String?,
    modifier: Modifier = Modifier,
    theme: ThemeValues = LocalComposeKitTheme.current,
    colour: Color = theme.onBackground.copy(alpha = 0.75f),
    linkify: Boolean = true
) {
    if (text?.isNotBlank() == true) {
        val style: TextStyle = MaterialTheme.typography.bodySmall.copy(color = colour)
        if (linkify) LinkifyText(text, theme.accent, modifier, style = style)
        else Text(text, modifier, style = style)
    }
}

package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import dev.toastbits.composekit.navigation.compositionlocal.LocalNavigator
import dev.toastbits.composekit.navigation.navigator.Navigator
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemText
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.onAccent
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent

actual class SubpageSettingsItem actual constructor(
    title: String,
    subtitle: String?,
    targetPage: Screen
) : BaseSubpageSettingsItem(
    title, subtitle, targetPage
) {
    actual override fun resetUiState() {}

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        val theme: ThemeValues = LocalComposeKitTheme.current
        val navigator: Navigator = LocalNavigator.current

        Button(
            { navigator.pushScreen(targetPage) },
            modifier.fillMaxWidth(),
            colors = ButtonDefaults.buttonColors(
                containerColor = theme.vibrantAccent,
                contentColor = theme.onAccent
            )
        ) {
            Column(Modifier.weight(1f)) {
                Text(title, color = theme.onAccent)
                ItemText(subtitle)
            }
        }
    }
}

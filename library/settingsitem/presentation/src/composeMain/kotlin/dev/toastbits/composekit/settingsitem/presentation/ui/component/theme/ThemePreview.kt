package dev.toastbits.composekit.settingsitem.presentation.ui.component.theme

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.focusProperties
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.settingsitem.resources.generated.resources.Res
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_picker_screen_button_configure_theme
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.get
import dev.toastbits.composekit.theme.core.onAccent
import dev.toastbits.composekit.theme.core.onCard
import dev.toastbits.composekit.theme.core.readableName
import org.jetbrains.compose.resources.stringResource
import kotlin.random.Random

@Composable
fun <T: ThemeValues> ThemePreview(
    label: String?,
    theme: T,
    modifier: Modifier = Modifier,
    cornerContent: (@Composable RowScope.() -> Unit)? = null,
    onEdited: ((T) -> Unit)? = null,
    onSelected: ((T) -> Unit)? = null,
    isSelected: Boolean = false
) {
    theme.Update()

    ThemePreviewContainer(
        label,
        theme,
        modifier = modifier,
        cornerContent = cornerContent,
        onClicked = onEdited,
        onSelected = onSelected,
        isSelected = isSelected,
        hoverContent = {
            Icon(
                Icons.Default.Settings,
                stringResource(Res.string.theme_picker_screen_button_configure_theme),
                Modifier
                    .align(Alignment.Center)
                    .size(60.dp)
            )
        }
    ) { loadedTheme ->
        Row(horizontalArrangement = Arrangement.spacedBy(20.dp)) {
            Column(
                Modifier.fillMaxWidth(0.4f)
            ) {
                ThemeValues.Slot.BuiltIn.BACKGROUND.Text(
                    loadedTheme,
                    colour = loadedTheme.onBackground
                )

                LineArea(
                    loadedTheme.onBackground,
                    label?.hashCode() ?: 0,
                    Modifier
                        .fillMaxSize()
                        .padding(
                            end = 30.dp,
                            bottom = 50.dp
                        )
                )
            }

            Column(
                Modifier
                    .fillMaxSize()
                    .background(loadedTheme.card, MaterialTheme.shapes.medium)
                    .width(IntrinsicSize.Min)
                    .padding(10.dp)
            ) {
                ThemeValues.Slot.BuiltIn.CARD.Text(
                    loadedTheme,
                    colour = loadedTheme.onCard
                )

                LineArea(
                    loadedTheme.onCard,
                    (label?.hashCode() ?: 0) + 1,
                    Modifier
                        .fillMaxWidth()
                        .padding(end = 30.dp)
                        .height(50.dp)
                )

                Spacer(Modifier.fillMaxHeight().weight(1F))

                Button(
                    {},
                    Modifier
                        .fillMaxWidth()
                        .align(Alignment.End)
                        .focusProperties {
                            canFocus = false
                        }
                        .height(25.dp),
                    colors =
                        ButtonDefaults.buttonColors(
                            containerColor = loadedTheme.accent,
                            contentColor = loadedTheme.onAccent
                        ),
                    contentPadding = PaddingValues(horizontal = 10.dp)
                ) {
                    Text(
                        ThemeValues.Slot.BuiltIn.ACCENT.readableName,
                        style = MaterialTheme.typography.labelSmall,
                        softWrap = false
                    )
                }
            }
        }
    }
}

@Composable
private fun LineArea(
    colour: Color,
    seed: Int,
    modifier: Modifier = Modifier
) {
    val random: Random = remember(seed) { Random(seed) }
    val endPadding: MutableMap<Int, Float> = remember(seed) { mutableMapOf() }

    Canvas(modifier) {
        val spacing: Float = 10.dp.toPx()
        val thickness: Float = 2.dp.toPx()

        var i: Int = 0

        while (true) {
            val y: Float = spacing / 2f + (i * (thickness + spacing))
            if (y + (spacing / 2f) >= size.height) {
                break
            }

            val padding: Float =
                endPadding.getOrPut(i++) { random.nextFloat() } * size.width * 0.7f

            drawLine(
                colour,
                Offset(0f, y),
                Offset(size.width - padding, y),
                strokeWidth = thickness
            )
        }
    }
}

@Composable
private fun ThemeValues.Slot.Text(
    theme: ThemeValues,
    modifier: Modifier = Modifier,
    colour: Color = theme[this]
) {
    Text(
        readableName,
        modifier,
        color = colour,
        style = MaterialTheme.typography.labelSmall
    )
}

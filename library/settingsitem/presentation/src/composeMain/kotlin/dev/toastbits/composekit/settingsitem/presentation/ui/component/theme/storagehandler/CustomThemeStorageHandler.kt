package dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.type.CustomThemeType

internal class CustomThemeStorageHandler(
    override val currentThemeProperty: PlatformSettingsProperty<ThemeReference>,
    override val customThemesProperty: PlatformSettingsProperty<List<NamedTheme>>,
): BaseThemeStorageHandler<CustomThemeType.IndexedTheme>() {
    override fun getThemeSerialisablePart(theme: CustomThemeType.IndexedTheme): SerialisableTheme =
        theme.theme.theme

    override suspend fun selectTheme(theme: CustomThemeType.IndexedTheme) {
        val themeIndex: Int? = theme.index
        if (themeIndex == null) {
            saveAsNewTheme(theme, theme.theme.name, NamedTheme.Type.CUSTOM)
            return
        }

        customThemesProperty.set(
            customThemesProperty.get().toMutableList().apply {
                set(themeIndex, theme.theme)
            }
        )
        currentThemeProperty.set(ThemeReference.CustomTheme(themeIndex))
    }

    override fun canDeleteTheme(theme: CustomThemeType.IndexedTheme): Boolean =
        theme.index != null

    override suspend fun deleteTheme(theme: CustomThemeType.IndexedTheme) {
        val themeIndex: Int? = theme.index
        requireNotNull(themeIndex) { "Cannot delete custom theme without index ($theme)" }

        val currentCustomThemes: List<NamedTheme> = customThemesProperty.get()
        if (themeIndex >= currentCustomThemes.size) {
            return
        }

        val currentTheme: ThemeReference = currentThemeProperty.get()
        if (currentTheme is ThemeReference.CustomTheme) {
            if (currentTheme.customThemeIndex == themeIndex) {
                currentThemeProperty.reset()
            }
            else if (currentTheme.customThemeIndex > themeIndex) {
                currentThemeProperty.set(currentTheme.copy(customThemeIndex = currentTheme.customThemeIndex - 1))
            }
        }

        customThemesProperty.set(
            currentCustomThemes.toMutableList().apply {
                removeAt(themeIndex)
            }
        )
    }
}

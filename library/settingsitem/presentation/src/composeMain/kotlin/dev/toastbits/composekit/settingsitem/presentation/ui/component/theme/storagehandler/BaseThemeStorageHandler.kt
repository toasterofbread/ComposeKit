package dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference

abstract class BaseThemeStorageHandler<T: ThemeValues>: ThemeStorageHandler<T> {
    protected abstract val currentThemeProperty: PlatformSettingsProperty<ThemeReference>
    protected abstract val customThemesProperty: PlatformSettingsProperty<List<NamedTheme>>

    protected abstract fun getThemeSerialisablePart(theme: T): SerialisableTheme

    override fun canSaveAsNewTheme(theme: T): Boolean = true
    override suspend fun saveAsNewTheme(theme: T, name: String?, type: NamedTheme.Type) {
        val customThemes: MutableList<NamedTheme> = customThemesProperty.get().toMutableList()

        val themeIndex = customThemes.size
        customThemes.add(
            NamedTheme(type, name, getThemeSerialisablePart(theme))
        )

        customThemesProperty.set(customThemes)
        currentThemeProperty.set(ThemeReference.CustomTheme(themeIndex))
    }
}

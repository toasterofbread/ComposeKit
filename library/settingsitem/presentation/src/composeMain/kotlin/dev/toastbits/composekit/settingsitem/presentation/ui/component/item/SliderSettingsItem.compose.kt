package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import androidx.compose.animation.Crossfade
import androidx.compose.animation.core.SpringSpec
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalViewConfiguration
import androidx.compose.ui.platform.ViewConfiguration
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import com.github.krottv.compose.sliders.DefaultThumb
import com.github.krottv.compose.sliders.DefaultTrack
import com.github.krottv.compose.sliders.ListenOnPressed
import com.github.krottv.compose.sliders.SliderValueHorizontal
import dev.toastbits.composekit.components.utils.composable.MeasureUnconstrainedView
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemText
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemTitleText
import dev.toastbits.composekit.settingsitem.resources.generated.resources.Res
import dev.toastbits.composekit.settingsitem.resources.generated.resources.slider_value_input_cancel
import dev.toastbits.composekit.settingsitem.resources.generated.resources.slider_value_input_done
import dev.toastbits.composekit.settingsitem.resources.generated.resources.slider_value_not_float
import dev.toastbits.composekit.settingsitem.resources.generated.resources.slider_value_not_int
import dev.toastbits.composekit.settingsitem.resources.generated.resources.`slider_value_out_of_$range`
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent
import dev.toastbits.composekit.util.getContrasted
import dev.toastbits.kmpresources.library.model.KmpStringResource
import dev.toastbits.composekit.util.platform.Platform
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.jetbrains.compose.resources.getString
import org.jetbrains.compose.resources.stringResource
import kotlin.math.roundToInt

actual class SliderSettingsItem actual constructor(
    state: PlatformSettingsProperty<out Number>,
    minLabel: KmpStringResource?,
    maxLabel: KmpStringResource?,
    steps: Int,
    range: ClosedFloatingPointRange<Float>,
    getValueText: ((value: Number) -> String?)?
) : BaseSliderSettingsItem(
    state,
    minLabel,
    maxLabel,
    steps,
    range,
    getValueText
) {
    private var showEditDialog: Boolean by mutableStateOf(false)
    private var currentInputText: String by mutableStateOf("")
    private var error: String? by mutableStateOf(null)
    private var valueState: Float? by mutableStateOf(null)

    actual override fun resetUiState() {
        showEditDialog = false
        currentInputText = ""
        error = null
    }

    override suspend fun resetValues() {
        super.resetValues()
        valueState = state.get().toFloat()
    }

    actual override fun setValue(value: Float) {
        valueState = value
    }

    @Suppress("UNCHECKED_CAST")
    actual override suspend fun saveValue() {
        if (isInt()) {
            (state as PlatformSettingsProperty<Int>).set(getValue().roundToInt())
        }
        else {
            (state as PlatformSettingsProperty<Float>).set(getValue())
        }
    }

    actual override fun getValue(): Float =
        valueState ?: range.start

    private fun isInt(): Boolean =
        when (val default: Number = state.getDefaultValue()) {
            is Float -> false
            is Int -> true
            else -> throw NotImplementedError(default::class.toString())
        }

    private fun getTypedValue(isInt: Boolean): Number =
        if (isInt) getValue().roundToInt()
        else getValue()

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        val coroutineScope: CoroutineScope = rememberCoroutineScope()
        val theme: ThemeValues = LocalComposeKitTheme.current
        var isInt: Boolean by rememberSaveable { mutableStateOf(false) }

        LaunchedEffect(this) {
            launch {
                valueState = state.get().toFloat()
            }
            isInt = isInt()
        }

        fun saveInput() {
            coroutineScope.launch {
                try {
                    setValue(if (isInt) currentInputText.toInt().toFloat() else currentInputText.toFloat())
                    saveValue()
                    showEditDialog = false
                }
                catch (_: NumberFormatException) {}
            }
        }

        if (showEditDialog) {
            AlertDialog(
                {
                    showEditDialog = false
                },
                confirmButton = {
                    FilledTonalButton(
                        ::saveInput,
                        enabled = error == null
                    ) {
                        Text(stringResource(Res.string.slider_value_input_done))
                    }
                },
                dismissButton = {
                    TextButton({ showEditDialog = false }) {
                        Text(stringResource(Res.string.slider_value_input_cancel))
                    }
                },
                title = { ItemTitleText(state.name.getComposable()) },
                text = {
                    OutlinedTextField(
                        value = currentInputText,
                        isError = error != null,
                        label = {
                            Crossfade(error) { errorText ->
                                if (errorText != null) {
                                    Text(errorText)
                                }
                            }
                        },
                        onValueChange = {
                            currentInputText = it

                            try {
                                val value: Float = if (isInt) currentInputText.toInt().toFloat() else currentInputText.toFloat()
                                if (!range.contains(value)) {
                                    coroutineScope.launch {
                                        error = getString(Res.string.`slider_value_out_of_$range`)
                                            .replace("\$range", range.toString())
                                    }
                                    return@OutlinedTextField
                                }

                                error = null
                            }
                            catch (_: NumberFormatException) {
                                coroutineScope.launch {
                                    error = getString(
                                        if (isInt) Res.string.slider_value_not_int
                                        else Res.string.slider_value_not_float
                                    )
                                }
                            }
                        },
                        singleLine = true,
                        modifier = Modifier.fillMaxWidth(),
                        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                        keyboardActions = KeyboardActions {
                            saveInput()
                        }
                    )
                }
            )
        }

        Column(modifier.fillMaxWidth().padding(bottom = 10.dp)) {
            Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {
                ItemTitleText(state.name.getComposable(), Modifier.fillMaxWidth().weight(1f))

                IconButton(
                    {
                        currentInputText = (if (isInt()) getValue().roundToInt() else getValue()).toString()
                        showEditDialog = true
                    }, Modifier.size(25.dp)
                ) {
                    Icon(Icons.Filled.Edit, null)
                }
            }

            ItemText(state.description?.getComposable())

            Spacer(Modifier.requiredHeight(10.dp))

            Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)) {
                if (minLabel != null) {
                    ItemText(minLabel.getComposable())
                }

                val viewConfiguration: ViewConfiguration = LocalViewConfiguration.current
                CompositionLocalProvider(
                    LocalViewConfiguration provides remember {
                        object : ViewConfiguration {
                            override val doubleTapMinTimeMillis get() = viewConfiguration.doubleTapMinTimeMillis
                            override val doubleTapTimeoutMillis get() = viewConfiguration.doubleTapTimeoutMillis
                            override val longPressTimeoutMillis get() = viewConfiguration.longPressTimeoutMillis

                            override val touchSlop: Float
                                get() {
                                    return viewConfiguration.touchSlop * 2f
                                }
                        }
                    }
                ) {
                    SliderValueHorizontal(
                        value = getValue(),
                        onValueChange = { setValue(it) },
                        onValueChangeFinished = {
                            coroutineScope.launch {
                                saveValue()
                            }
                        },
                        thumbSizeInDp = DpSize(12.dp, 12.dp),
                        track = { a, b, c, d, e ->
                            DefaultTrack(a, b, c, d, e,
                                theme.vibrantAccent.copy(alpha = 0.5f),
                                theme.vibrantAccent,
                                colorTickProgress = theme.vibrantAccent.getContrasted().copy(alpha = 0.5f)
                            )
                        },
                        track_press_enabled = !Platform.ANDROID.isCurrent(),
                        thumb = { modifier, offset, interactionSource, enabled, thumbSize ->
                            val colour: Color = theme.vibrantAccent
                            val scaleOnPress: Float = 1.15f
                            val animationSpec: SpringSpec<Float> = SpringSpec(0.65f)
                            var valueText: String? by remember { mutableStateOf(null) }

                            val typedValue: Number = getTypedValue(isInt)
                            LaunchedEffect(typedValue, getValueText) {
                                valueText = getValueText?.invoke(typedValue)
                            }

                            if (valueText != null) {
                                MeasureUnconstrainedView({ ItemText(valueText) }) { size ->
                                    var isPressed by remember { mutableStateOf(false) }
                                    interactionSource.ListenOnPressed { isPressed = it }
                                    val scale: Float by animateFloatAsState(
                                        if (isPressed) scaleOnPress else 1f,
                                        animationSpec = animationSpec
                                    )

                                    Column(
                                        Modifier
                                            .offset(
                                                x = with(LocalDensity.current) { offset - (size.width.toDp() / 2) + 12.dp },
                                                y = (-5).dp
                                            )
                                            .requiredHeight(55.dp)
                                            .graphicsLayer(scale, scale),
                                        verticalArrangement = Arrangement.Bottom,
                                        horizontalAlignment = Alignment.CenterHorizontally
                                    ) {
                                        Spacer(
                                            Modifier
                                                .size(12.dp)
                                                .background(
                                                    if (enabled) colour else
                                                        colour.copy(alpha = 0.6f), CircleShape
                                                )
                                        )
                                        ItemText(valueText, linkify = false)
                                    }
                                }
                            }
                            else {
                                DefaultThumb(
                                    modifier,
                                    offset,
                                    interactionSource,
                                    true,
                                    thumbSize,
                                    colour,
                                    scaleOnPress,
                                    animationSpec
                                )
                            }
                        },
                        steps = steps,
                        modifier = Modifier.weight(1f),
                        valueRange = range
                    )
                }

                if (maxLabel != null) {
                    ItemText(maxLabel.getComposable())
                }
            }
        }
    }
}

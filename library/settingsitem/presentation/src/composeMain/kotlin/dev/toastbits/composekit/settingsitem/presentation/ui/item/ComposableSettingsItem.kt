package dev.toastbits.composekit.settingsitem.presentation.ui.item

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

class ComposableSettingsItem(
    val settingsProperties: List<PlatformSettingsProperty<*>> = emptyList(),
    val resetComposeUiState: () -> Unit,
    val resetSettingsValues: () -> Unit = {},
    override val showItem: Flow<Boolean> = flowOf(),
    val content: @Composable (Modifier) -> Unit
): SettingsItem() {
    override fun getProperties(): List<PlatformSettingsProperty<*>> = settingsProperties

    override suspend fun resetValues() {
        resetSettingsValues()
    }

    override fun resetUiState() {
        resetComposeUiState()
    }

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        content(modifier)
    }
}

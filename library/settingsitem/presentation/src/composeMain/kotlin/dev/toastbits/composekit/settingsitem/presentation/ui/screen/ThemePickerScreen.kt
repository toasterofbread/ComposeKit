package dev.toastbits.composekit.settingsitem.presentation.ui.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.LocalContext
import dev.toastbits.composekit.components.platform.composable.ScrollBarLazyVerticalGrid
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.navigation.compositionlocal.LocalNavigator
import dev.toastbits.composekit.navigation.navigator.Navigator
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.ThemePreview
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.ThemePreviewContainer
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.provider.ThemeStorageHandlerProvider
import dev.toastbits.composekit.settingsitem.resources.generated.resources.Res
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_picker_screen_title
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.provider.ThemeProvider
import dev.toastbits.composekit.theme.core.provider.ThemeTypeConfigProvider
import dev.toastbits.composekit.theme.core.type.CustomThemeType
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.type.ThemeTypeConfig
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.ui.LocalThemeProvider
import dev.toastbits.kmpresources.library.model.KmpStringResource
import dev.toastbits.kmpresources.library.mapper.toKmpResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

val DEFAULT_THEME_PREVIEW_SIZE: DpSize = DpSize(220.dp, 195.dp)

data class ThemePickerScreen(
    val currentThemeProperty: PlatformSettingsProperty<out ThemeReference?>,
    val themeTypeConfigProvider: ThemeTypeConfigProvider,
    val themeStorageHandlerProvider: ThemeStorageHandlerProvider,
    val themeTypes: List<ThemeType<*>> = ThemeType.getAll(),
    val extraThemes: List<ThemeReference> = emptyList(),
    val onExtraThemeEdited: (Int) -> Unit = {},
    val canSelectThemesDirectly: Boolean = true,
    val resultHandler: ResultHandler = ResultHandler.DEFAULT
): Screen {
    private val gridSpacing: Dp = 25.dp

    override val title: KmpStringResource =
        Res.string.theme_picker_screen_title.toKmpResource()

    @Composable
    override fun Content(modifier: Modifier, contentPadding: PaddingValues) {
        val navigator: Navigator = LocalNavigator.current
        val themeProvider: ThemeProvider = LocalThemeProvider.current
        val coroutineScope: CoroutineScope = rememberCoroutineScope()

        val currentTheme: ThemeReference? by currentThemeProperty.observe()
        val customThemes: List<NamedTheme> = themeProvider.getCustomThemes()

        val (customThemeType: CustomThemeType?, nonCustomThemeTypes: List<ThemeType<*>>) =
            remember(themeTypes) {
                themeTypes.firstNotNullOfOrNull { it as? CustomThemeType } to themeTypes.filter { it !is CustomThemeType }
            }

        ScrollBarLazyVerticalGrid(
            modifier = modifier,
            columns = GridCells.Adaptive(DEFAULT_THEME_PREVIEW_SIZE.width),
            horizontalArrangement = Arrangement.spacedBy(gridSpacing, Alignment.Start),
            verticalArrangement = Arrangement.spacedBy(gridSpacing),
            contentPadding = contentPadding
        ) {
            if (customThemeType != null) {
                item {
                    ThemeCreationBox(customThemeType)
                }
            }

            itemsIndexed(extraThemes) { index, extraThemeReference ->
                val theme: SerialisableTheme =
                    remember(themeProvider, extraThemeReference) {
                        extraThemeReference.getTheme(themeProvider)
                    }
                ThemePreview(
                    theme.readableName,
                    theme,
                    Modifier.size(DEFAULT_THEME_PREVIEW_SIZE),
                    onEdited = {
                        onExtraThemeEdited(index)
                    }
                )
            }

            items(nonCustomThemeTypes) { type ->
                ThemeTypePreview(type)
            }

            if (customThemeType != null) {
                itemsIndexed(
                    customThemes.filter(customThemeType::shouldShowTheme)
                ) { index, theme ->
                    ThemePreview(
                        theme.readableName,
                        theme,
                        Modifier.size(DEFAULT_THEME_PREVIEW_SIZE),
                        onEdited = {
                            resultHandler.onResult(
                                navigator,
                                CustomThemeType.IndexedTheme(index, theme),
                                themeTypeConfigProvider.getConfig(customThemeType),
                                themeStorageHandlerProvider
                            )
                        },
                        onSelected =
                            if (canSelectThemesDirectly) {{
                                coroutineScope.launch {
                                    themeStorageHandlerProvider(customThemeType).selectTheme(
                                        CustomThemeType.IndexedTheme(index, theme)
                                    )
                                }
                            }}
                            else null,
                        isSelected =
                            canSelectThemesDirectly && (currentTheme as? ThemeReference.CustomTheme)?.customThemeIndex == index
                    )
                }
            }
        }
    }

    @Composable
    private fun <T: ThemeValues> ThemeTypePreview(type: ThemeType<T>, modifier: Modifier = Modifier) {
        val context: PlatformContext = LocalContext.current
        val navigator: Navigator = LocalNavigator.current
        val themeProvider: ThemeProvider = LocalThemeProvider.current
        val coroutineScope: CoroutineScope = rememberCoroutineScope()

        @Suppress("UNCHECKED_CAST") // Type checked with type.isInstance
        val instanceTheme: T? =
            currentThemeProperty.observe().value
                ?.takeIf { type.isInstance(it) }
                ?.getTheme(themeProvider) as T?

        ThemePreview(
            label = type.creationPreviewReadableName,
            theme = instanceTheme ?: remember(type, context) { type.getDefaultTheme(context) },
            modifier = modifier.size(DEFAULT_THEME_PREVIEW_SIZE),
            onEdited = {
                resultHandler.onResult(
                    navigator,
                    instanceTheme ?: it,
                    themeTypeConfigProvider.getConfig(type),
                    themeStorageHandlerProvider
                )
            },
            onSelected =
                type.getConstantTheme()
                    ?.takeIf { canSelectThemesDirectly }
                    ?.let { theme ->
                        {
                            coroutineScope.launch {
                                themeStorageHandlerProvider(type).selectTheme(theme)
                            }
                        }
                    },
            isSelected = canSelectThemesDirectly && instanceTheme != null
        )
    }

    @Composable
    private fun ThemeCreationBox(type: CustomThemeType, modifier: Modifier = Modifier) {
        val navigator: Navigator = LocalNavigator.current
        val context: PlatformContext = LocalContext.current
        val defaultTheme: CustomThemeType.IndexedTheme = type.getDefaultTheme(context)

        ThemePreviewContainer(
            type.creationPreviewReadableName,
            LocalComposeKitTheme.current,
            onClicked = {
                resultHandler.onResult(
                    navigator,
                    defaultTheme,
                    themeTypeConfigProvider.getConfig(type),
                    themeStorageHandlerProvider
                )
            },
            onSelected = null,
            isSelected = false,
            modifier = modifier.size(DEFAULT_THEME_PREVIEW_SIZE)
        ) {
            Icon(
                Icons.Default.Add,
                null,
                Modifier
                    .size(40.dp)
                    .align(Alignment.Center)
            )
        }
    }

    interface ResultHandler {
        fun <T: ThemeValues> onResult(
            navigator: Navigator,
            initialTheme: T,
            config: ThemeTypeConfig<T>,
            themeStorageHandlerProvider: ThemeStorageHandlerProvider
        )

        data object PushScreenToNavigator: ResultHandler {
            override fun <T : ThemeValues> onResult(
                navigator: Navigator,
                initialTheme: T,
                config: ThemeTypeConfig<T>,
                themeStorageHandlerProvider: ThemeStorageHandlerProvider
            ) {
                navigator.pushScreen(
                    ThemeConfirmationScreen(
                        initialTheme,
                        config,
                        themeStorageHandlerProvider(config.type),
                        onFinished = {
                            navigator.navigateBackward(1)
                        }
                    )
                )
            }
        }

        companion object {
            val DEFAULT: ResultHandler = PushScreenToNavigator
        }
    }
}
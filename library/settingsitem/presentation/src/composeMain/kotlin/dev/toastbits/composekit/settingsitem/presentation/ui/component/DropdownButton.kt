package dev.toastbits.composekit.settingsitem.presentation.ui.component

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.KeyEventType
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.input.key.type
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.SETTINGS_ITEM_ROUNDED_SHAPE
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.onAccent
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent
import dev.toastbits.composekit.util.thenWith

@Composable
internal fun DropdownButton(
    text: String,
    onClick: () -> Unit,
    onCycle: ((Int) -> Unit)?,
    modifier: Modifier = Modifier
) {
    val theme: ThemeValues = LocalComposeKitTheme.current

    Button(
        onClick,
        modifier
            .thenWith(onCycle) { lambda ->
                onKeyEvent {
                    if (it.type == KeyEventType.KeyDown) {
                        if (it.type == KeyEventType.KeyDown && it.key == Key.DirectionUp) {
                            lambda(-1)
                            return@onKeyEvent true
                        } else if (it.key == Key.DirectionDown) {
                            lambda(1)
                            return@onKeyEvent true
                        }
                    }

                    return@onKeyEvent false
                }
            },
        shape = SETTINGS_ITEM_ROUNDED_SHAPE,
        colors = ButtonDefaults.buttonColors(
            containerColor = theme.vibrantAccent,
            contentColor = theme.onAccent
        )
    ) {
        Text(text)
        Icon(
            Icons.Filled.ArrowDropDown,
            null,
            tint = theme.onAccent
        )
    }
}

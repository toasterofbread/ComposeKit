package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.text.input.TextFieldLineLimits
import androidx.compose.foundation.text.input.TextFieldState
import androidx.compose.foundation.text.input.setTextAndPlaceCursorAtEnd
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.PointerIcon
import androidx.compose.ui.input.pointer.pointerHoverIcon
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemText
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemTitleText
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest

actual class TextFieldSettingsItem actual constructor(
    state: PlatformSettingsProperty<String>,
    singleLine: Boolean,
    errorProvider: StateFlow<TextFieldErrorMessageProvider?>
): BaseTextFieldSettingsItem(
    state,
    singleLine,
    errorProvider
) {
    private var currentInput: TextFieldState by mutableStateOf(TextFieldState(state.get()))
    private var inputError: String? by mutableStateOf(null)
    private var textReset: Boolean = false

    actual override fun resetUiState() {
        textReset = true
        currentInput = TextFieldState(state.get())
        inputError = null
    }

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        val stringErrorProvider: TextFieldErrorMessageProvider? by errorProvider.collectAsState()

        LaunchedEffect(currentInput) {
            snapshotFlow {
                val text: String = currentInput.text.toString()
                text to stringErrorProvider?.invoke(text)
            }.collectLatest { (input, error) ->
                inputError = error

                if (input.isNotEmpty()) {
                    textReset = false
                }
                else if (textReset) {
                    return@collectLatest
                }

                if (inputError == null) {
                    state.set(input)
                }
            }
        }

        Column(modifier.fillMaxWidth(), verticalArrangement = Arrangement.spacedBy(5.dp)) {
            ItemTitleText(state.name.getComposable())
            ItemText(state.description?.getComposable())

            LaunchedEffect(state) {
                currentInput.setTextAndPlaceCursorAtEnd(state.get())
            }

            OutlinedTextField(
                currentInput,
                Modifier
                    .fillMaxWidth()
                    .pointerHoverIcon(PointerIcon.Text)
                    .height(70.dp),
                lineLimits =
                    if (singleLine) TextFieldLineLimits.SingleLine
                    else TextFieldLineLimits.MultiLine(),
                isError = inputError != null,
                label = {
                    inputError?.also {
                        Text(it)
                    }
                }
            )
        }
    }
}

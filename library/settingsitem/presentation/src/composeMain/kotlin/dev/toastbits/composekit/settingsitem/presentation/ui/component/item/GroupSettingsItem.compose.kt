package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent
import dev.toastbits.kmpresources.library.model.KmpStringResource

actual class GroupSettingsItem actual constructor(
    title: KmpStringResource?
): BaseGroupSettingsItem(title) {
    actual override fun resetUiState() {}

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        title?.also {
            Text(
                it.getComposable(),
                modifier.padding(top = 25.dp),
                color = LocalComposeKitTheme.current.vibrantAccent,
                fontSize = 20.sp,
                fontWeight = FontWeight.Light
            )
        }
    }
}

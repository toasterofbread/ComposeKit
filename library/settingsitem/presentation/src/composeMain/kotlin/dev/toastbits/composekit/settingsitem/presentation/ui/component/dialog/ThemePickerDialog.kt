package dev.toastbits.composekit.settingsitem.presentation.ui.component.dialog

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import dev.toastbits.composekit.components.platform.composable.BackHandler
import dev.toastbits.composekit.navigation.navigator.Navigator
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.settingsitem.domain.MutableStateFlowSettingsProperty
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.provider.ThemeStorageHandlerProvider
import dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler.ThemeStorageHandler
import dev.toastbits.composekit.settingsitem.presentation.ui.screen.ThemeConfirmationScreen
import dev.toastbits.composekit.settingsitem.presentation.ui.screen.ThemePickerScreen
import dev.toastbits.composekit.settingsitem.resources.generated.resources.Res
import dev.toastbits.composekit.settingsitem.resources.generated.resources.theme_picker_dialog_button_cancel
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.provider.ThemeProvider
import dev.toastbits.composekit.theme.core.provider.ThemeTypeConfigProvider
import dev.toastbits.composekit.theme.core.type.AnimatedThemeType
import dev.toastbits.composekit.theme.core.type.CatppuccinThemeType
import dev.toastbits.composekit.theme.core.type.CustomThemeType
import dev.toastbits.composekit.theme.core.type.RosePineThemeType
import dev.toastbits.composekit.theme.core.type.SystemLightDarkThemeType
import dev.toastbits.composekit.theme.core.type.SystemThemeType
import dev.toastbits.composekit.theme.core.type.ThemeType
import dev.toastbits.composekit.theme.core.type.ThemeTypeConfig
import dev.toastbits.composekit.theme.core.ui.LocalThemeProvider
import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.flow.MutableStateFlow
import org.jetbrains.compose.resources.stringResource

@Composable
fun ThemePickerDialog(
    themeTypes: List<ThemeType<*>>,
    themeTypeConfigProvider: ThemeTypeConfigProvider,
    titleContent: @Composable () -> Unit,
    onDismissRequest: () -> Unit,
    onSelected: suspend (ThemeReference) -> Unit
) {
    val themeProvider: ThemeProvider = LocalThemeProvider.current

    var confirmationScreen: Screen? by remember { mutableStateOf(null) }
    val pickerScreen: Screen =
        remember(themeProvider) {
            ThemePickerScreen(
                MutableStateFlowSettingsProperty(
                    MutableStateFlow(null),
                    KmpStringResource.ofString(""),
                    null
                ),
                canSelectThemesDirectly = false,
                themeTypes = themeTypes,
                themeTypeConfigProvider = themeTypeConfigProvider,
                themeStorageHandlerProvider =
                    object : ThemeStorageHandlerProvider {
                        override fun <T : ThemeValues> invoke(themeType: ThemeType<T>): ThemeStorageHandler<T> =
                            object : ThemeStorageHandler<T> {
                                override suspend fun selectTheme(theme: T) {
                                    when (themeType) {
                                        AnimatedThemeType,
                                        SystemLightDarkThemeType -> throw IllegalStateException()

                                        SystemThemeType ->
                                            onSelected(ThemeReference.SystemTheme)

                                        is CustomThemeType -> {
                                            check(theme is CustomThemeType.IndexedTheme)

                                            val index: Int? = theme.index
                                            val existingTheme: SerialisableTheme? =
                                                index
                                                    ?.let { themeProvider.getCustomTheme(index) as? NamedTheme }
                                                    ?.theme

                                            if (existingTheme == theme.theme.theme) {
                                                onSelected(ThemeReference.CustomTheme(index))
                                            }
                                            else {
                                                onSelected(ThemeReference.StaticTheme(theme.theme))
                                            }
                                        }

                                        RosePineThemeType,
                                        CatppuccinThemeType ->
                                            onSelected(ThemeReference.StaticTheme(theme as SerialisableTheme))
                                    }
                                }

                                override fun canSaveAsNewTheme(theme: T): Boolean = false
                                override suspend fun saveAsNewTheme(theme: T, name: String?, type: NamedTheme.Type) =
                                    throw IllegalStateException()

                                override fun canDeleteTheme(theme: T): Boolean = false
                                override suspend fun deleteTheme(theme: T) =
                                    throw IllegalStateException()
                            }
                    },
                resultHandler = object : ThemePickerScreen.ResultHandler {
                    override fun <T : ThemeValues> onResult(
                        navigator: Navigator,
                        initialTheme: T,
                        config: ThemeTypeConfig<T>,
                        themeStorageHandlerProvider: ThemeStorageHandlerProvider,
                    ) {
                        confirmationScreen =
                            ThemeConfirmationScreen(
                                initialTheme,
                                config,
                                themeStorageHandlerProvider(config.type),
                                onFinished = onDismissRequest,
                                canEditName = false
                            )
                    }
                }
            )
        }

    AlertDialog(
        onDismissRequest = onDismissRequest,
        properties = DialogProperties(dismissOnBackPress = false),
        confirmButton = {},
        title = {
            titleContent()
        },
        text = {
            val density: Density = LocalDensity.current

            BackHandler {
                if (confirmationScreen != null) {
                    confirmationScreen = null
                }
                else {
                    onDismissRequest()
                }
            }

            Crossfade(confirmationScreen ?: pickerScreen) { screen ->
                Column {
                    var bottomBarHeight by remember { mutableStateOf(0.dp) }
                    screen.Content(Modifier, PaddingValues(bottom = bottomBarHeight + 10.dp))

                    Row(
                        Modifier
                            .fillMaxWidth()
                            .onSizeChanged {
                                with (density) {
                                    bottomBarHeight = it.height.toDp()
                                }
                            },
                        horizontalArrangement = Arrangement.End
                    ) {
                        if (screen == pickerScreen) {
                            Button(onDismissRequest) {
                                Text(stringResource(Res.string.theme_picker_dialog_button_cancel))
                            }
                        }
                    }
                }
            }
        }
    )
}

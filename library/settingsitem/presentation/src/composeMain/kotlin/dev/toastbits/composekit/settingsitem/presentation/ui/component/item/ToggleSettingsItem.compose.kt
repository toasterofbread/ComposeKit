package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemText
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemTitleText
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent
import kotlinx.coroutines.flow.StateFlow

actual class ToggleSettingsItem actual constructor(
    state: PlatformSettingsProperty<Boolean>,
    enabled: StateFlow<Boolean>,
    valueOverride: StateFlow<Boolean?>,
    subtitleOverride: StateFlow<String?>,
    checker: ((target: Boolean, setLoading: (Boolean) -> Unit, (allow_change: Boolean) -> Unit) -> Unit)?
) : BaseToggleSettingsItem(
    state,
    enabled,
    valueOverride,
    subtitleOverride,
    checker
) {
    private var loading: Boolean by mutableStateOf(false)

    actual override fun resetUiState() {
        loading = false
    }

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        val theme: ThemeValues = LocalComposeKitTheme.current
        val enabled: Boolean by this.enabled.collectAsState()
        val valueOverride: Boolean? by this.valueOverride.collectAsState()
        var currentValue: Boolean by state.observe()

        Row(
            modifier,
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(5.dp)
        ) {
            Column(
                Modifier
                    .fillMaxWidth()
                    .weight(1f),
                verticalArrangement = Arrangement.spacedBy(5.dp)
            ) {
                ItemTitleText(state.name.getComposable())
                ItemText(subtitleOverride.collectAsState().value ?: state.description?.getComposable())
            }

            Crossfade(loading) {
                if (it) {
                    CircularProgressIndicator(color = theme.onBackground)
                }
                else {
                    Switch(
                        valueOverride ?: currentValue,
                        onCheckedChange = null,
                        enabled = enabled,
                        modifier =
                        Modifier.clickable(
                            interactionSource = remember { MutableInteractionSource() },
                            indication = null,
                            enabled = enabled
                        ) {
                            if (checker == null) {
                                currentValue = !currentValue
                                return@clickable
                            }

                            checker.invoke(
                                !currentValue,
                                { l ->
                                    loading = l
                                }
                            ) { allow_change ->
                                if (allow_change) {
                                    currentValue = !currentValue
                                }
                                loading = false
                            }
                        },
                        colors = SwitchDefaults.colors(
                            checkedThumbColor = theme.vibrantAccent,
                            checkedTrackColor = theme.vibrantAccent.copy(alpha = 0.5f)
                        )
                    )
                }
            }
        }
    }
}

package dev.toastbits.composekit.settingsitem.presentation.ui.item

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.utils.composable.SubtleLoadingIndicator
import dev.toastbits.composekit.components.utils.modifier.background
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.SettingsItem
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.onAccent
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent
import dev.toastbits.composekit.util.composable.WidthShrinkText
import dev.toastbits.composekit.util.composable.getValue
import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch

class LargeToggleSettingsItem(
    val state: PlatformSettingsProperty<Boolean>,
    val enabledText: KmpStringResource? = null,
    val disabledText: KmpStringResource? = null,
    val enableButton: KmpStringResource,
    val disableButton: KmpStringResource,
    val enabledContent: (@Composable (Modifier) -> Unit)? = null,
    val disabledContent: (@Composable (Modifier) -> Unit)? = null,
    val prerequisiteValue: PlatformSettingsProperty<Boolean>? = null,
    val warningDialog: (@Composable (dismiss: () -> Unit) -> Unit)? = null,
    val infoButton: (@Composable (enabled: Boolean, showingExtraState: MutableState<Boolean>) -> Unit)? = null,
    val extraItems: List<SettingsItem> = emptyList(),
    val showButton: Boolean = true,
    val onClicked: (target: Boolean, setEnabled: (Boolean) -> Unit, setLoading: (Boolean) -> Unit) -> Unit =
        { target, setEnabled, _ -> setEnabled(target) }
): SettingsItem() {
    override suspend fun resetValues() {
        state.reset()
        for (item in extraItems) {
            item.resetValues()
        }
    }

    override fun getProperties(): List<PlatformSettingsProperty<*>> = listOf(state) + extraItems.flatMap { it.getProperties() }

    private var loading: Boolean by mutableStateOf(false)
    private val coroutineScope: CoroutineScope = CoroutineScope(Job())
    private val showingExtraState: MutableState<Boolean> = mutableStateOf(false)
    private var showingDialog: (@Composable (dismiss: () -> Unit) -> Unit)? by mutableStateOf(null)

    override fun resetUiState() {
        loading = false
        coroutineScope.coroutineContext.cancelChildren()
        showingExtraState.value = false
        showingDialog = null
    }

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        val theme: ThemeValues = LocalComposeKitTheme.current
        val shape: RoundedCornerShape = RoundedCornerShape(25.dp)

        showingDialog?.invoke { showingDialog = null }

        val stateValue: Boolean? by state.observe()
        val prerequisiteValueValue: Boolean? by prerequisiteValue?.observe()

        LaunchedEffect(stateValue) {
            if (stateValue == false) {
                showingExtraState.value = false
            }
        }

        AnimatedVisibility(
            prerequisiteValueValue != false,
            modifier,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            Crossfade(stateValue ?: false) { enabled ->
                CompositionLocalProvider(LocalContentColor provides if (!enabled) theme.onBackground else theme.onAccent) {
                    Column(
                        Modifier
                            .background(
                                if (!enabled) theme.background else theme.vibrantAccent,
                                shape
                            )
                            .border(2.dp, theme.vibrantAccent, shape)
                            .padding(horizontal = 10.dp, vertical = 5.dp)
                            .fillMaxSize()
                            .animateContentSize(),
                        verticalArrangement = Arrangement.Top
                    ) {
                        Row(
                            Modifier.height(IntrinsicSize.Max),
                            horizontalArrangement = Arrangement.spacedBy(6.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            (if (enabled) enabledContent else disabledContent)?.invoke(Modifier.weight(1f).padding(vertical = 5.dp))
                            (if (enabled) enabledText else disabledText)?.also { WidthShrinkText(it.getComposable(), Modifier.fillMaxWidth().weight(1f)) }

                            AnimatedVisibility(showButton) {
                                Button(
                                    {
                                        if (!enabled && warningDialog != null) {
                                            showingDialog = warningDialog
                                        }
                                        else {
                                            onClicked(
                                                !enabled,
                                                {
                                                    coroutineScope.launch {
                                                        state.set(it)
                                                    }
                                                },
                                                { loading = it }
                                            )
                                        }
                                    },
                                    colors = ButtonDefaults.buttonColors(
                                        containerColor = if (enabled) theme.background else theme.vibrantAccent,
                                        contentColor = if (enabled) theme.onBackground else theme.onAccent
                                    )
                                ) {
                                    Box(contentAlignment = Alignment.Center) {
                                        this@Row.AnimatedVisibility(loading, enter = fadeIn(), exit = fadeOut()) {
                                            SubtleLoadingIndicator()
                                        }

                                        val textAlpha = animateFloatAsState(if (loading) 0f else 1f)
                                        Text(
                                            (if (enabled) disableButton else enableButton).getComposable(),
                                            Modifier.graphicsLayer { alpha = textAlpha.value }
                                        )
                                    }
                                }
                            }

                            infoButton?.invoke(enabled, showingExtraState)
                        }

                        if (showingExtraState.value) {
                            Column(
                                Modifier
                                    .padding(vertical = 5.dp)
                                    .background(shape) { theme.background }
                                    .padding(horizontal = 10.dp, vertical = 20.dp),
                                verticalArrangement = Arrangement.spacedBy(10.dp)
                            ) {
                                CompositionLocalProvider(LocalContentColor provides theme.onBackground) {
                                    for (item in extraItems) {
                                        item.Item(Modifier)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

package dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference
import dev.toastbits.composekit.theme.core.model.ThemeValuesData

internal class SystemThemeStorageHandler(
    override val currentThemeProperty: PlatformSettingsProperty<ThemeReference>,
    override val customThemesProperty: PlatformSettingsProperty<List<NamedTheme>>
): BaseThemeStorageHandler<ThemeValues>() {
    override fun getThemeSerialisablePart(theme: ThemeValues): SerialisableTheme =
        ThemeValuesData.of(theme)

    override suspend fun selectTheme(theme: ThemeValues) {
        currentThemeProperty.set(ThemeReference.SystemTheme)
    }

    override fun canDeleteTheme(theme: ThemeValues): Boolean = false

    override suspend fun deleteTheme(theme: ThemeValues) {
        throw IllegalStateException()
    }
}

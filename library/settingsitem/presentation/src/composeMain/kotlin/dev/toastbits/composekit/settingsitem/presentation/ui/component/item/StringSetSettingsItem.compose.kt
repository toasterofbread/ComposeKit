package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Remove
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.platform.composable.ScrollabilityIndicatorColumn
import dev.toastbits.composekit.components.utils.composable.ShapedIconButton
import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemText
import dev.toastbits.composekit.settingsitem.presentation.ui.component.util.ItemTitleText
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.onAccent
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.kmpresources.library.model.KmpStringResource
import dev.toastbits.composekit.util.model.Kdp
import dev.toastbits.composekit.util.model.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

actual class StringSetSettingsItem actual constructor(
    state: PlatformSettingsProperty<Set<String>>,
    add_dialog_title: KmpStringResource,
    msg_item_already_added: KmpStringResource,
    msg_set_empty: KmpStringResource,
    single_line_content: Boolean,
    height: Kdp,
    itemToText: (String) -> KmpStringResource,
    textToItem: (String) -> String
) : BaseStringSetSettingsItem(
    state,
    add_dialog_title,
    msg_item_already_added,
    msg_set_empty,
    single_line_content,
    height,
    itemToText
) {
    private var showAddItemDialog: Boolean by mutableStateOf(false)

    actual override fun resetUiState() {
        showAddItemDialog = false
    }

    @Composable
    override fun Item(
        modifier: Modifier
    ) {
        val coroutineScope: CoroutineScope = rememberCoroutineScope()
        val value: Set<String> by state.observe()
        val theme: ThemeValues = LocalComposeKitTheme.current
        val iconButtonColours = IconButtonDefaults.iconButtonColors(
            containerColor = theme.accent,
            contentColor = theme.onAccent,
            disabledContainerColor = theme.accent.copy(alpha = 0.5f)
        )

        if (showAddItemDialog) {
            var new_item_content: String by remember { mutableStateOf("") }
            val item_already_added = value.contains(new_item_content)
            val can_add_item = new_item_content.isNotEmpty() && !item_already_added

            AlertDialog(
                onDismissRequest = { showAddItemDialog = false },
                confirmButton = {
                    Crossfade(can_add_item) { enabled ->
                        ShapedIconButton(
                            {
                                coroutineScope.launch {
                                    state.set(value.plus(textToItem(new_item_content)))
                                }
                                showAddItemDialog = false
                            },
                            colours = iconButtonColours,
                            enabled = enabled
                        ) {
                            Icon(Icons.Default.Done, null)
                        }
                    }
                },
                dismissButton = {
                    ShapedIconButton(
                        {
                            showAddItemDialog = false
                        },
                        colours = iconButtonColours
                    ) {
                        Icon(Icons.Default.Close, null)
                    }
                },
                title = { Text(addDialogTitle.getComposable()) },
                text = {
                    TextField(
                        new_item_content,
                        { new_item_content = it },
                        Modifier,
                        singleLine = single_line_content,
                        isError = item_already_added,
                        label =
                        if (item_already_added) {{ Text(msgItemAlreadyAdded.getComposable()) }}
                        else null
                    )
                }
            )
        }

        Column(modifier.height(height.dp)) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .weight(1f),
                    verticalArrangement = Arrangement.spacedBy(5.dp)
                ) {
                    ItemTitleText(state.name.getComposable())
                    ItemText(state.description?.getComposable())
                }

                ShapedIconButton(
                    { showAddItemDialog = true },
                    colours = iconButtonColours
                ) {
                    Icon(Icons.Default.Add, null)
                }
            }

            Crossfade(value, Modifier.fillMaxWidth()) { set ->
                if (set.isEmpty()) {
                    Text(
                        msgSetEmpty.getComposable(),
                        Modifier.fillMaxWidth().padding(top = 20.dp),
                        textAlign = TextAlign.Center
                    )
                }
                else {
                    val scroll_state = rememberLazyListState()
                    ScrollabilityIndicatorColumn(scroll_state) {
                        LazyColumn(state = scroll_state) {
                            for (item in set) {
                                item {
                                    Row(verticalAlignment = Alignment.CenterVertically) {
                                        Text(itemToText(item).getComposable(), Modifier.fillMaxWidth().weight(1f))

                                        IconButton(
                                            {
                                                coroutineScope.launch {
                                                    state.set(set.minus(item))
                                                }
                                            }
                                        ) {
                                            Icon(Icons.Default.Remove, null, tint = theme.onBackground)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

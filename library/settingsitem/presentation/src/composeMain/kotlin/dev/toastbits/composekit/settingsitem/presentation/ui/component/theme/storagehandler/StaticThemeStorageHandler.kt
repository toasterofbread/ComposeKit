package dev.toastbits.composekit.settingsitem.presentation.ui.component.theme.storagehandler

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.model.ThemeReference

internal class StaticThemeStorageHandler<T: SerialisableTheme>(
    override val currentThemeProperty: PlatformSettingsProperty<ThemeReference>,
    override val customThemesProperty: PlatformSettingsProperty<List<NamedTheme>>
): BaseThemeStorageHandler<T>() {
    override fun getThemeSerialisablePart(theme: T): SerialisableTheme = theme

    override suspend fun selectTheme(theme: T) {
        currentThemeProperty.set(ThemeReference.StaticTheme(theme))
    }

    override fun canDeleteTheme(theme: T): Boolean = false

    override suspend fun deleteTheme(theme: T) {
        throw IllegalStateException()
    }
}

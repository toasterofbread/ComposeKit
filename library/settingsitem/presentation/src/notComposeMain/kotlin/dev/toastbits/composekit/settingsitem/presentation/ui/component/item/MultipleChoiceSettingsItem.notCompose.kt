package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.kmpresources.library.model.KmpStringResource

actual class MultipleChoiceSettingsItem actual constructor(
    state: PlatformSettingsProperty<Int>,
    choiceAmount: Int,
    getChoiceText: (Int) -> KmpStringResource
): BaseMultipleChoiceSettingsItem(
    state, choiceAmount, getChoiceText
) {
    actual override fun resetUiState() {}
}

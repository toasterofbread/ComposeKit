package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.kmpresources.library.model.KmpStringResource
import dev.toastbits.composekit.util.model.Kdp

actual class StringSetSettingsItem actual constructor(
    state: PlatformSettingsProperty<Set<String>>,
    add_dialog_title: KmpStringResource,
    msg_item_already_added: KmpStringResource,
    msg_set_empty: KmpStringResource,
    single_line_content: Boolean,
    height: Kdp,
    itemToText: (String) -> KmpStringResource,
    textToItem: (String) -> String
) : BaseStringSetSettingsItem(
    state,
    add_dialog_title,
    msg_item_already_added,
    msg_set_empty,
    single_line_content,
    height,
    itemToText,
    textToItem
) {
    actual override fun resetUiState() {}
}

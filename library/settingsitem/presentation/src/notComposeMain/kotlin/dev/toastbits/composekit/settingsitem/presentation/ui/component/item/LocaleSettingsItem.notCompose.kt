package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.composekit.util.model.Locale
import dev.toastbits.composekit.util.model.LocaleList

actual class LocaleSettingsItem actual constructor(
    state: PlatformSettingsProperty<Locale?>,
    localeList: LocaleList,
    allowCustomLocale: Boolean
): BaseLocaleSettingsItem(
    state, localeList, allowCustomLocale
) {
    actual override fun resetUiState() {}
}

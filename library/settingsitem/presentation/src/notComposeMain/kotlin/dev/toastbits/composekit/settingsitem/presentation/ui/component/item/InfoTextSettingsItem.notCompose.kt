package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.kmpresources.library.model.KmpStringResource

actual class InfoTextSettingsItem actual constructor(
    text: KmpStringResource
): BaseInfoTextSettingsItem(text) {
    actual override fun resetUiState() {}
}

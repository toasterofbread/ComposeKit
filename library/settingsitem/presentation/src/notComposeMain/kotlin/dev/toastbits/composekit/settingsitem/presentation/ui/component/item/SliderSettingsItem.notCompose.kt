package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import dev.toastbits.kmpresources.library.model.KmpStringResource

actual class SliderSettingsItem actual constructor(
    state: PlatformSettingsProperty<out Number>,
    minLabel: KmpStringResource?,
    maxLabel: KmpStringResource?,
    steps: Int,
    range: ClosedFloatingPointRange<Float>,
    getValueText: ((value: Number) -> String?)?
): BaseSliderSettingsItem(
    state,
    minLabel,
    maxLabel,
    steps,
    range,
    getValueText
) {
    actual override fun resetUiState() {}

    actual override fun setValue(value: Float) {
        TODO("Not yet implemented")
    }

    actual override suspend fun saveValue() {
        TODO("Not yet implemented")
    }

    actual override fun getValue(): Float {
        TODO("Not yet implemented")
    }
}

package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.navigation.screen.Screen

actual class SubpageSettingsItem actual constructor(
    title: String,
    subtitle: String?,
    targetPage: Screen
) : BaseSubpageSettingsItem(
    title, subtitle, targetPage
) {
    actual override fun resetUiState() {}
}

package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import kotlinx.coroutines.flow.StateFlow

actual class ToggleSettingsItem actual constructor(
    state: PlatformSettingsProperty<Boolean>,
    enabled: StateFlow<Boolean>,
    valueOverride: StateFlow<Boolean?>,
    subtitleOverride: StateFlow<String?>,
    checker: ((target: Boolean, setLoading: (Boolean) -> Unit, (allow_change: Boolean) -> Unit) -> Unit)?
) : BaseToggleSettingsItem(
    state,
    enabled,
    valueOverride,
    subtitleOverride,
    checker
) {
    actual override fun resetUiState() {}
}

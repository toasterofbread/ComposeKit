package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.kmpresources.library.model.KmpStringResource

actual class GroupSettingsItem actual constructor(
    title: KmpStringResource?
): BaseGroupSettingsItem(title) {
    actual override fun resetUiState() {}
}

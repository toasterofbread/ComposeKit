package dev.toastbits.composekit.settingsitem.presentation.ui.component.item

import dev.toastbits.composekit.settingsitem.domain.PlatformSettingsProperty
import kotlinx.coroutines.flow.StateFlow

actual class TextFieldSettingsItem actual constructor(
    state: PlatformSettingsProperty<String>,
    singleLine: Boolean,
    errorProvider: StateFlow<TextFieldErrorMessageProvider?>
): BaseTextFieldSettingsItem(
    state,
    singleLine,
    errorProvider
) {
    actual override fun resetUiState() {}
}

import util.configureAllComposeTargets

plugins {
    id("android-library-conventions")
    id("compose-conventions")
    id("gitlab-publishing-conventions")

    alias(libs.plugins.kotlinx.serialization)
}

kotlin {
    configureAllComposeTargets()

    sourceSets {
        commonMain {
            dependencies {
                api(projects.library.context)
                implementation(projects.library.util)
                implementation(projects.library.utilKt)
                implementation(projects.library.theme.core)
                implementation(compose.material)
                implementation(libs.compose.color.picker)
                api(libs.kotlinx.serialization.json)
            }
        }

        val jvmMain by getting {
            dependencies {
            }
        }

        val androidMain by getting {
            dependencies {
                implementation(libs.accompanist.swiperefresh)
            }
        }
    }
}

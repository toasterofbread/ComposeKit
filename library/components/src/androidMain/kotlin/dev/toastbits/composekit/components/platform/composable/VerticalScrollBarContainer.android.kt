package dev.toastbits.composekit.components.platform.composable

import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.ScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp

@Composable
actual fun <S: ScrollableState> VerticalScrollBarContainer(
    state: S,
    modifier: Modifier,
    showScrollbar: Boolean,
    contentPadding: PaddingValues,
    scrollBarContentPadding: PaddingValues,
    horizontalAlignment: Alignment.Horizontal,
    userScrollEnabled: Boolean,
    scrollBarColour: Color,
    verticalAlignment: Alignment.Vertical,
    reverseScrollBarLayout: Boolean,
    scrollBarSpacing: Dp,
    scrollBarThickness: Dp,
    onScrollDelta: ((delta: Float, continuous: Boolean) -> Unit)?,
    content: @Composable (Modifier, PaddingValues) -> Unit,
) {
    content(
        modifier.scrollable(
            state,
            Orientation.Vertical,
            reverseDirection = true
        ),
        contentPadding
    )
}

@Composable
actual fun areScrollBarsVisible(): Boolean = false

package dev.toastbits.composekit.components.platform.composable

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.context.PlatformContext

@Composable
actual fun BackHandler(
    enabled: Boolean,
    priority: Int,
    action: () -> Unit
) {
    androidx.activity.compose.BackHandler(enabled, action)
}

actual fun onWindowBackPressed(context: PlatformContext): Boolean {
    context.applicationContext?.simulateBackPress()
    return true
}

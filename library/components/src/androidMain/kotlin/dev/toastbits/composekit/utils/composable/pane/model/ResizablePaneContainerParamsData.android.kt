package dev.toastbits.composekit.components.utils.composable.pane.model

actual val PLATFORM_DEFAULT_USE_PANE_RESIZE_ANIMATION_SPEC: Boolean
    get() = true

@file:Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
package dev.toastbits.composekit.components.utils.composable

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.launch
import kotlin.coroutines.EmptyCoroutineContext

@Composable
fun BaseLoadActionButton(
    performLoad: suspend (Boolean) -> Unit,
    modifier: Modifier = Modifier,
    uncancellable: Boolean = false,
    loadOnLaunch: Boolean = false,
    contentAlignment: Alignment = Alignment.Center,
    content: @Composable (modifier: Modifier, onClick: (Boolean) -> Unit) -> Unit,
) {
    val coroutineScope = rememberCoroutineScope()
    var loadInProgress: Boolean by remember { mutableStateOf(false) }

    LaunchedEffect(Unit) {
        if (!loadOnLaunch || loadInProgress) {
            return@LaunchedEffect
        }

        coroutineScope.launch(
            if (uncancellable) NonCancellable
            else EmptyCoroutineContext
        ) {
            loadInProgress = true
            performLoad(false)
            loadInProgress = false
        }
    }

    val contentOpacity: Float by animateFloatAsState(if (loadInProgress) 0f else 1f)

    Box(modifier, contentAlignment = contentAlignment) {
        SubtleLoadingIndicator(Modifier.graphicsLayer { alpha = 1f - contentOpacity })

        content(Modifier.graphicsLayer { alpha = contentOpacity }) {
            if (loadInProgress) {
                return@content
            }

            coroutineScope.launch(
                if (uncancellable) NonCancellable
                else EmptyCoroutineContext
            ) {
                loadInProgress = true
                performLoad(it)
                loadInProgress = false
            }
        }
    }
}

package dev.toastbits.composekit.components.utils.composable

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationVector1D
import androidx.compose.animation.core.FastOutLinearInEasing
import androidx.compose.animation.core.InfiniteTransition
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.utils.modifier.background
import kotlinx.coroutines.delay
import kotlin.random.Random
import kotlin.time.Duration

@Composable
fun SubtleLoadingIndicator(
    modifier: Modifier = Modifier,
    message: String? = null,
    size: Dp = 20.dp,
    containerModifier: Modifier = Modifier,
    appearAfter: Duration = Duration.ZERO,
	initialOffset: Float = remember { Random.nextFloat() },
    getColour: (() -> Color)? = null,
) {
    val contentColour: Color = LocalContentColor.current
	val infTransition: InfiniteTransition = rememberInfiniteTransition()
	val animatedOffset: Float by infTransition.animateFloat(
		initialValue = 0f,
		targetValue = 1f,
		animationSpec = infiniteRepeatable(
            animation = tween(1500, easing = FastOutLinearInEasing),
            repeatMode = RepeatMode.Restart
        )
	)

    val appearAnimation: Animatable<Float, AnimationVector1D>? =
        remember {
            if (appearAfter == Duration.ZERO) null else Animatable(0f)
        }
    LaunchedEffect(Unit) {
        appearAnimation?.apply {
            delay(appearAfter)
            animateTo(1f)
        }
    }

    Column(
        containerModifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(7.dp)
    ) {
        Box(
            Modifier.sizeIn(minWidth = size, minHeight = size).then(modifier),
            contentAlignment = Alignment.Center
        ) {
            val currentOffset: Float =
                (animatedOffset + initialOffset).let {
                    if (it > 1f) it - 1f else it
                }

            val sizePercent: Float = if (currentOffset < 0.5f) currentOffset else 1f - currentOffset
            Spacer(
                Modifier
                    .graphicsLayer {
                        alpha = appearAnimation?.value ?: 1f
                    }
                    .background(CircleShape, getColour ?: { contentColour })
                    .size(size * sizePercent)
            )
        }

        if (message != null) {
            Text(message, style = MaterialTheme.typography.labelLarge, color = LocalContentColor.current.copy(alpha = 0.85f))
        }
    }
}

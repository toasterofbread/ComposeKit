package dev.toastbits.composekit.components.utils.composable.pane

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.platform.composable.ScrollBarColumn
import dev.toastbits.composekit.components.utils.composable.MeasureUnconstrainedView
import dev.toastbits.composekit.components.utils.composable.pane.model.InitialPaneRatioSource
import dev.toastbits.composekit.components.utils.composable.pane.model.ResizablePaneContainerParams
import dev.toastbits.composekit.components.utils.composable.pane.model.ResizablePaneContainerParamsData
import dev.toastbits.composekit.util.composable.copy

@Composable
fun <T> ResizableSyncedSplitColumn(
    items: List<T>,
    modifier: Modifier = Modifier,
    state: ScrollState = rememberScrollState(),
    horizontalAlignment: Alignment.Horizontal = Alignment.Start,
    verticalArrangement: Arrangement.Vertical = Arrangement.Top,
    getItemContentAlignment: (T) -> Alignment = { Alignment.TopStart },
    showEndPane: Boolean = true,
    contentPadding: PaddingValues = PaddingValues(),
    scrollBarContentPadding: PaddingValues = contentPadding,
    initialStartPaneRatioSource: InitialPaneRatioSource = InitialPaneRatioSource.Ratio(0.5f),
    params: ResizablePaneContainerParams = ResizablePaneContainerParamsData(),
    content: @Composable (isStart: Boolean, item: T) -> Unit
) {
    val startHeights: MutableMap<T, Dp> = remember { mutableStateMapOf() }
    val endHeights: MutableMap<T, Dp> = remember { mutableStateMapOf() }

    fun getItemHeight(item: T): Dp =
        maxOf(startHeights[item] ?: 0.dp, endHeights[item] ?: 0.dp)

    ResizableTwoPaneRow(
        startPaneContent = {
            Column(
                Modifier
                    .verticalScroll(state)
                    .padding(contentPadding.copy(end = 0.dp)),
                horizontalAlignment = horizontalAlignment,
                verticalArrangement = verticalArrangement
            ) {
                Content(
                    items = items,
                    content = { content(true, it) },
                    getItemContentAlignment = getItemContentAlignment,
                    setItemHeight = { item, height -> startHeights[item] = height },
                    getItemHeight = ::getItemHeight
                )
            }
        },
        endPaneContent = {
            ScrollBarColumn(
                state = state,
                contentPadding = contentPadding.copy(start = 0.dp),
                scrollBarContentPadding = scrollBarContentPadding,
                horizontalAlignment = horizontalAlignment,
                verticalArrangement = verticalArrangement
            ) {
                Content(
                    items = items,
                    content = { content(false, it) },
                    getItemContentAlignment = getItemContentAlignment,
                    setItemHeight = { item, height -> endHeights[item] = height },
                    getItemHeight = ::getItemHeight
                )
            }
        },
        modifier = modifier,
        showEndPane = showEndPane,
        contentPadding = contentPadding,
        initialStartPaneRatioSource = initialStartPaneRatioSource,
        params = params
    )
}

@Composable
private fun <T> Content(
    items: List<T>,
    content: @Composable (T) -> Unit,
    getItemContentAlignment: (T) -> Alignment,
    setItemHeight: (T, Dp) -> Unit,
    getItemHeight: (T) -> Dp
) {
    val density: Density = LocalDensity.current

    for (item in items) {
        BoxWithConstraints(
            Modifier
                .heightIn(min = getItemHeight(item))
                .fillMaxWidth(),
            contentAlignment = getItemContentAlignment(item)
        ) {
            MeasureUnconstrainedView({
                Box(Modifier.widthIn(max = maxWidth)) {
                    content(item)
                }
            }) { unconstrainedSize ->
                with(density) {
                    setItemHeight(item, unconstrainedSize.height.toDp())
                }

                content(item)
            }
        }
    }
}

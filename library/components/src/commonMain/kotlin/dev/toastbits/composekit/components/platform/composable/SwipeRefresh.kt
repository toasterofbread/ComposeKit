package dev.toastbits.composekit.components.platform.composable

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
expect fun SwipeRefresh(
    state: Boolean,
    onRefresh: () -> Unit,
    modifier: Modifier = Modifier,
    swipe_enabled: Boolean = true,
    indicator: Boolean = true,
    indicator_padding: PaddingValues = PaddingValues(),
    content: @Composable () -> Unit
)
package dev.toastbits.composekit.components.utils.composable.pane.model

import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.encodeToJsonElement
import kotlinx.serialization.serializer

object InitialPaneRatioSourceSerialiser: KSerializer<InitialPaneRatioSource> {
    @Serializable
    private data class InitialPaneRatioSourceData(val type: InitialPaneRatioSourceType, val data: JsonElement)

    @Serializable
    private enum class InitialPaneRatioSourceType {
        Ratio,
        SizeDp,
        Remembered
    }

    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("InitialPaneRatioSource", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): InitialPaneRatioSource {
        if (decoder !is JsonDecoder) {
            throw UnsupportedOperationException("Decoder is ${decoder::class}, but expected JsonDecoder")
        }

        val obj: InitialPaneRatioSourceData = decoder.decodeSerializableValue(InitialPaneRatioSourceData.serializer())

        val serialiser: KSerializer<out InitialPaneRatioSource> =
            when (obj.type) {
                InitialPaneRatioSourceType.Ratio -> InitialPaneRatioSource.Ratio.serializer()
                InitialPaneRatioSourceType.SizeDp -> InitialPaneRatioSource.SizeDp.serializer()
                InitialPaneRatioSourceType.Remembered -> InitialPaneRatioSource.Remembered.serializer()
            }

        return Json.decodeFromJsonElement(serialiser, obj.data)
    }

    @OptIn(InternalSerializationApi::class)
    override fun serialize(encoder: Encoder, value: InitialPaneRatioSource) {
        encoder.encodeSerializableValue(
            InitialPaneRatioSourceData::class.serializer(),
            InitialPaneRatioSourceData(
                when (value) {
                    is InitialPaneRatioSource.Ratio -> InitialPaneRatioSourceType.Ratio
                    is InitialPaneRatioSource.Remembered -> InitialPaneRatioSourceType.Remembered
                    is InitialPaneRatioSource.SizeDp -> InitialPaneRatioSourceType.SizeDp
                },
                when (value) {
                    is InitialPaneRatioSource.Ratio -> Json.encodeToJsonElement<InitialPaneRatioSource.Ratio>(value)
                    is InitialPaneRatioSource.Remembered -> Json.encodeToJsonElement<InitialPaneRatioSource.Remembered>(value)
                    is InitialPaneRatioSource.SizeDp -> Json.encodeToJsonElement<InitialPaneRatioSource.SizeDp>(value)
                }
            )
        )
    }
}

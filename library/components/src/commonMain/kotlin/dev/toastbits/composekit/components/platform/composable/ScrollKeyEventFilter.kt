package dev.toastbits.composekit.components.platform.composable

import androidx.compose.runtime.ProvidableCompositionLocal
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.input.key.KeyEvent

fun interface ScrollKeyEventFilter {
    operator fun invoke(keyEvent: KeyEvent): Boolean
}

val LocalScrollKeyEventFilter: ProvidableCompositionLocal<ScrollKeyEventFilter> =
    staticCompositionLocalOf {
        ScrollKeyEventFilter {
            return@ScrollKeyEventFilter true
        }
    }

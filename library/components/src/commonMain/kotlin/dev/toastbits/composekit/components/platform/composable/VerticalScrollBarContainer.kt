package dev.toastbits.composekit.components.platform.composable

import androidx.compose.foundation.gestures.ScrollableState
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
expect fun <S: ScrollableState> VerticalScrollBarContainer(
    state: S,
    modifier: Modifier = Modifier,
    showScrollbar: Boolean = true,
    contentPadding: PaddingValues = PaddingValues(0.dp),
    scrollBarContentPadding: PaddingValues = contentPadding,
    horizontalAlignment: Alignment.Horizontal = Alignment.Start,
    userScrollEnabled: Boolean = true,
    scrollBarColour: Color = Color.Unspecified,
    verticalAlignment: Alignment.Vertical = Alignment.Top,
    reverseScrollBarLayout: Boolean = false,
    scrollBarSpacing: Dp = 10.dp,
    scrollBarThickness: Dp = 8.dp,
    onScrollDelta: ((delta: Float, continuous: Boolean) -> Unit)? = null,
    content: @Composable (Modifier, PaddingValues) -> Unit
)

@Composable
expect fun areScrollBarsVisible(): Boolean

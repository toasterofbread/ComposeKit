@file:Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
package dev.toastbits.composekit.components.utils.composable

//@Composable
//fun ResizableOutlinedTextField(
//    state: TextFieldState,
//    modifier: Modifier = Modifier,
//    enabled: Boolean = true,
//    readOnly: Boolean = false,
//    showKeyboard: Boolean = false,
//    textStyle: TextStyle = LocalTextStyle.current,
//    label: @Composable (() -> Unit)? = null,
//    placeholder: @Composable (() -> Unit)? = null,
//    leadingIcon: @Composable (() -> Unit)? = null,
//    trailingIcon: @Composable (() -> Unit)? = null,
//    supportingText: @Composable (() -> Unit)? = null,
//    isError: Boolean = false,
//    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
//    onKeyboardAction: KeyboardActionHandler? = null,
//    singleLine: Boolean = false,
//    maxLines: Int = Int.MAX_VALUE,
//    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
//    shape: Shape = OutlinedTextFieldDefaults.shape,
//    colors: TextFieldColors = OutlinedTextFieldDefaults.colors()
//) {
//    val textColor = textStyle.color.takeOrElse {
//        val focused: Boolean = interactionSource.collectIsFocusedAsState().value
//        colors.textColor(enabled, isError, focused)
//    }
//    val mergedTextStyle = textStyle.merge(TextStyle(color = textColor))
//
//    val focus_requester = remember { FocusRequester() }
//
//    LaunchedEffect(showKeyboard) {
//        if (showKeyboard) {
//            focus_requester.requestFocus()
//        }
//    }
//
//    CompositionLocalProvider(LocalTextSelectionColors provides colors.textSelectionColors) {
//        BasicTextField(
//            state,
//            modifier = modifier
//                .focusRequester(focus_requester)
//                .defaultMinSize(
//                    minWidth = TextFieldDefaults.MinWidth,
//                    minHeight = TextFieldDefaults.MinHeight
//                )
//                .thenIf(label != null) {
//                    semantics(mergeDescendants = true) {}
//                },
//            enabled = enabled,
//            readOnly = readOnly,
//            textStyle = mergedTextStyle,
//            cursorBrush = SolidColor(colors.cursorColor(isError)),
//            keyboardOptions = keyboardOptions,
//            onKeyboardAction = onKeyboardAction,
//            interactionSource = interactionSource,
//            maxLines = maxLines,
//            decorationBox = @Composable { innerTextField ->
//                OutlinedTextFieldDefaults.DecorationBox(
//                    value = value,
//                    visualTransformation = visualTransformation,
//                    innerTextField = innerTextField,
//                    placeholder = placeholder,
//                    label = label,
//                    leadingIcon = leadingIcon,
//                    trailingIcon = trailingIcon,
//                    supportingText = supportingText,
//                    singleLine = singleLine,
//                    enabled = enabled,
//                    isError = isError,
//                    interactionSource = interactionSource,
//                    colors = colors,
//                    container = {
//                        OutlinedTextFieldDefaults.ContainerBox(
//                            enabled,
//                            isError,
//                            interactionSource,
//                            colors,
//                            shape
//                        )
//                    },
//                    contentPadding = OutlinedTextFieldDefaults.contentPadding(
//                        top = 0.dp, bottom = 0.dp
//                    )
//                )
//            }
//        )
//    }
//}

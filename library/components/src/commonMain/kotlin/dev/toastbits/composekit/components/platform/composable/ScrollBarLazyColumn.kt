package dev.toastbits.composekit.components.platform.composable

import androidx.compose.foundation.gestures.FlingBehavior
import androidx.compose.foundation.gestures.ScrollableDefaults
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun ScrollBarLazyColumn(
    modifier: Modifier = Modifier,
    state: LazyListState = rememberLazyListState(),
    showScrollbar: Boolean = true,
    contentPadding: PaddingValues = PaddingValues(0.dp),
    scrollBarContentPadding: PaddingValues = contentPadding,
    reverseLayout: Boolean = false,
    verticalArrangement: Arrangement.Vertical =
        if (!reverseLayout) Arrangement.Top else Arrangement.Bottom,
    horizontalAlignment: Alignment.Horizontal = Alignment.Start,
    flingBehavior: FlingBehavior = ScrollableDefaults.flingBehavior(),
    userScrollEnabled: Boolean = true,
    scrollBarColour: Color = Color.Unspecified,
    verticalAlignment: Alignment.Vertical = Alignment.Top,
    reverseScrollBarLayout: Boolean = false,
    scrollBarSpacing: Dp = 10.dp,
    scrollBarThickness: Dp = 8.dp,
    columnModifier: Modifier = Modifier,
    onScrollDelta: ((delta: Float, continuous: Boolean) -> Unit)? = null,
    content: LazyListScope.() -> Unit
) {
    VerticalScrollBarContainer(
        state = state,
        modifier = modifier,
        showScrollbar = showScrollbar,
        contentPadding = contentPadding,
        scrollBarContentPadding = scrollBarContentPadding,
        horizontalAlignment = horizontalAlignment,
        userScrollEnabled = userScrollEnabled,
        scrollBarColour = scrollBarColour,
        verticalAlignment = verticalAlignment,
        reverseScrollBarLayout = reverseScrollBarLayout,
        scrollBarSpacing = scrollBarSpacing,
        scrollBarThickness = scrollBarThickness,
        onScrollDelta = onScrollDelta
    ) { innerModifier, innerPadding ->
        LazyColumn(
            modifier = columnModifier.then(innerModifier),
            state = state,
            contentPadding = innerPadding,
            reverseLayout = reverseLayout,
            verticalArrangement = verticalArrangement,
            horizontalAlignment = horizontalAlignment,
            flingBehavior = flingBehavior,
            userScrollEnabled = false,
            content = content
        )
    }
}

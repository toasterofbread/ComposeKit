package dev.toastbits.composekit.components.utils.composable.wave

import androidx.compose.ui.graphics.Path
import kotlin.math.PI
import kotlin.math.ceil
import kotlin.math.sin

fun fullWavePath(
    path: Path,
    height: Float,
    length: Float,
    waveLength: Float,
    outerRotationDegrees: Float = 0f,
    offset: Float = 0f
): Path {
    for (direction in listOf(-1, 1)) {
        wavePath(
            path = path,
            direction = direction,
            height = height,
            length = length,
            waveLength = waveLength,
            outerRotationDegrees = outerRotationDegrees,
            offset = offset
        )
    }
    return path
}

fun wavePath(
    path: Path,
    direction: Int,
    height: Float,
    length: Float,
    waveLength: Float,
    outerRotationDegrees: Float = 0f,
    offset: Float = 0f
): Path {
    require(offset in 0f .. 1f) { offset }

    val halfPeriod: Float = waveLength / 2f
    val effectiveWidth: Float = ceil(length / halfPeriod) * halfPeriod

    val rotationAdj: Float = sin(outerRotationDegrees.toRadians())
    val yOffset: Float = -(length * rotationAdj * 0.5f)

    val xOffset: Float = offset * halfPeriod * 2
    val xAdjustedOffset = (xOffset % effectiveWidth) - (if (xOffset > 0f) effectiveWidth else 0f)
    path.moveTo(x = -halfPeriod / 2 + xAdjustedOffset, y = yOffset)

    for (i in 0 until ceil((effectiveWidth * 2) / halfPeriod + 1).toInt()) {
        if ((i % 2 == 0) != (direction == 1)) {
            path.relativeMoveTo(halfPeriod, 0f)
            continue
        }

        path.relativeQuadraticTo(
            dx1 = halfPeriod / 2,
            dy1 = height / 2 * direction,
            dx2 = halfPeriod,
            dy2 = 0f
        )
    }

    return path
}

private fun Float.toRadians(): Float =
    (this * 180f) / PI.toFloat()

package dev.toastbits.composekit.components

import androidx.compose.runtime.ProvidableCompositionLocal
import androidx.compose.runtime.staticCompositionLocalOf
import dev.toastbits.composekit.context.PlatformContext

val LocalContext: ProvidableCompositionLocal<PlatformContext> =
    staticCompositionLocalOf { throw IllegalStateException("LocalContext accessed before provide") }

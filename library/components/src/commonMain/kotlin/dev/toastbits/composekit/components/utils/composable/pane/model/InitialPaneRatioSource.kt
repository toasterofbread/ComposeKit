package dev.toastbits.composekit.components.utils.composable.pane.model

import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.utils.composable.pane.PaneSettingsProvider
import kotlinx.serialization.Serializable

@Serializable(with = InitialPaneRatioSourceSerialiser::class)
sealed interface InitialPaneRatioSource {
    fun getInitialPaneRatio(paneSettingsProvider: PaneSettingsProvider?, availableSpace: Dp): Float
    suspend fun update(paneSettingsProvider: PaneSettingsProvider, ratio: Float, availableSpace: Dp)

    @Serializable
    data class Ratio(val ratio: Float): InitialPaneRatioSource {
        override fun getInitialPaneRatio(
            paneSettingsProvider: PaneSettingsProvider?,
            availableSpace: Dp
        ): Float = ratio

        override suspend fun update(
            paneSettingsProvider: PaneSettingsProvider,
            ratio: Float,
            availableSpace: Dp
        ) {}
    }

    @Serializable
    data class SizeDp(val sizeDp: Float): InitialPaneRatioSource {
        override fun getInitialPaneRatio(
            paneSettingsProvider: PaneSettingsProvider?,
            availableSpace: Dp
        ): Float =
            sizeDp.dp / availableSpace

        override suspend fun update(
            paneSettingsProvider: PaneSettingsProvider,
            ratio: Float,
            availableSpace: Dp
        ) {}
    }

    @Serializable
    data class Remembered(
        val key: String,
        val default: Ratio
    ): InitialPaneRatioSource {
        override fun getInitialPaneRatio(paneSettingsProvider: PaneSettingsProvider?, availableSpace: Dp): Float =
            (
                paneSettingsProvider?.getInitialPaneRatio(key) ?: default
            ).getInitialPaneRatio(paneSettingsProvider, availableSpace)

        override suspend fun update(paneSettingsProvider: PaneSettingsProvider, ratio: Float, availableSpace: Dp) {
            val ratioProvider: InitialPaneRatioSource =
                when (paneSettingsProvider.getPaneRatioRememberMode()) {
                    RememberMode.RATIO -> Ratio(ratio)
                    RememberMode.SIZE_DP -> SizeDp((availableSpace * ratio).value)
                }

            paneSettingsProvider.updatePaneRatio(key, ratioProvider)
        }

        enum class RememberMode {
            RATIO,
            SIZE_DP;

            companion object {
                val DEFAULT: RememberMode = RATIO
            }
        }
    }
}

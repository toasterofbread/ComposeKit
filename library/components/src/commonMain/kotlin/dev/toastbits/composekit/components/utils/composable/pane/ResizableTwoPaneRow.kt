package dev.toastbits.composekit.components.utils.composable.pane

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberDraggableState
import androidx.compose.foundation.hoverable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsHoveredAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.utils.composable.pane.model.InitialPaneRatioSource
import dev.toastbits.composekit.components.utils.composable.pane.model.ResizablePaneContainerParams
import dev.toastbits.composekit.components.utils.composable.pane.model.ResizablePaneContainerParamsData
import dev.toastbits.composekit.components.utils.composable.pane.model.resizeAnimationSpecOrDefault
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent
import dev.toastbits.composekit.util.composable.copy
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
fun ResizableTwoPaneRow(
    startPaneContent: @Composable (PaddingValues) -> Unit,
    endPaneContent: @Composable (PaddingValues) -> Unit,
    modifier: Modifier = Modifier,
    showEndPane: Boolean = true,
    contentPadding: PaddingValues = PaddingValues(),
    initialStartPaneRatioSource: InitialPaneRatioSource = InitialPaneRatioSource.Ratio(0.5f),
    params: ResizablePaneContainerParams = ResizablePaneContainerParamsData()
) {
    val density: Density = LocalDensity.current
    val theme: ThemeValues = LocalComposeKitTheme.current
    val paneSettingsProvider: PaneSettingsProvider? = LocalPaneSettingsProvider.current

    val coroutineScope: CoroutineScope = rememberCoroutineScope()
    val interactionSource: MutableInteractionSource = remember { MutableInteractionSource() }
    val hoveringOverDragHandle: Boolean by interactionSource.collectIsHoveredAsState()
    var dragging: Boolean by remember { mutableStateOf(false) }

    BoxWithConstraints(modifier) {
        val dragHandleHovering: Boolean = hoveringOverDragHandle || dragging
        val dragHandleWidth: Dp by animateDpAsState(if (dragHandleHovering) params.hoverDragHandleSize else params.dragHandleSize)
        val dragHandlePadding: Dp by animateDpAsState(if (dragHandleHovering) params.hoverDragHandlePadding else params.dragHandlePadding)

        val availableWidth: Dp = maxWidth - dragHandleWidth

        var startPaneRatio: Float by remember(initialStartPaneRatioSource) { mutableFloatStateOf(initialStartPaneRatioSource.getInitialPaneRatio(paneSettingsProvider, availableWidth)) }

        Row(Modifier.matchParentSize()) {
            var reset: Boolean by remember { mutableStateOf(false) }
            var animating: Boolean by remember { mutableStateOf(false) }

            val startPaneTargetWidth: Dp =
                if (showEndPane) availableWidth * startPaneRatio
                else this@BoxWithConstraints.maxWidth

            val startPaneWidth: Dp by animateDpAsState(
                startPaneTargetWidth,
                animationSpec = params.resizeAnimationSpecOrDefault,
                finishedListener = {
                    animating = false
                    reset = false
                }
            )

            LaunchedEffect(showEndPane) {
                animating = true
            }
            LaunchedEffect(initialStartPaneRatioSource) {
                reset = true
            }

            Box(
                Modifier
                    .width(
                        if (!reset && (animating || params.resizeAnimationSpec != null)) startPaneWidth
                        else startPaneTargetWidth
                    )
                    .background(theme.background)
            ) {
                startPaneContent(contentPadding.copy(end = dragHandlePadding))
            }

            AnimatedVisibility(
                showEndPane,
                Modifier
                    .fillMaxHeight()
                    .fillMaxWidth()
                    .weight(1f),
                enter = expandHorizontally(),
                exit = shrinkHorizontally()
            ) {
                Row {
                    if (dragHandleWidth > 0.dp) {
                        PaneResizeDragHandle(
                            Orientation.Horizontal,
                            state = rememberDraggableState {
                                with (density) {
                                    val deltaRatio: Float = it.toDp() / availableWidth
                                    val startPaneRatioRange: Float = params.minPaneWidth / availableWidth

                                    startPaneRatio = (startPaneRatio + deltaRatio).coerceIn(startPaneRatioRange .. (1f - startPaneRatioRange))
                                }
                            },
                            onDraggingChanged = {
                                dragging = it

                                if (paneSettingsProvider != null) {
                                    coroutineScope.launch {
                                        initialStartPaneRatioSource.update(paneSettingsProvider, startPaneRatio, availableWidth)
                                    }
                                }
                            },
                            highlightColour = theme.vibrantAccent,
                            modifier = Modifier
                                .fillMaxHeight()
                                .width(dragHandleWidth)
                                .hoverable(interactionSource, enabled = params.hoverable)
                                .background(theme.card, RoundedCornerShape(10.dp)),
                            highlightSize = params.handleHighlightSize,
                            highlightShape = params.handleHighlightShape,
                            dragTimeout = params.dragTimeout
                        )
                    }

                    Box(
                        Modifier
                            .width(availableWidth * (1f - startPaneRatio))
                            .background(theme.background)
                    ) {
                        endPaneContent(contentPadding.copy(start = dragHandlePadding))
                    }
                }
            }
        }
    }
}

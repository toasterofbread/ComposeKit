package dev.toastbits.composekit.components.utils.composable

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.input.TextFieldLineLimits
import androidx.compose.foundation.text.input.TextFieldState
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.godaddy.colorpicker.ClassicColorPicker
import com.godaddy.colorpicker.HsvColor
import dev.toastbits.composekit.components.platform.composable.ScrollBarColumn
import dev.toastbits.composekit.util.composable.thenIf
import dev.toastbits.composekit.util.fromHexString
import dev.toastbits.composekit.util.generatePalette
import dev.toastbits.composekit.util.sorted
import dev.toastbits.composekit.util.toRGBHexString
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlin.time.Duration.Companion.seconds

@Composable
fun ColourPicker(
    key1: Any? = Unit,
    getInitialColour: () -> Color,
    modifier: Modifier = Modifier,
    arrangement: Arrangement.HorizontalOrVertical = Arrangement.spacedBy(10.dp),
    presets: List<Color>? = null,
    bottomRowExtraContent: (@Composable () -> Unit)? = null,
    square: Boolean = true,
    onSelected: (Color) -> Unit
) {
    val density: Density = LocalDensity.current

    var currentColour: Color by remember(key1) { mutableStateOf(getInitialColour()) }

    var colourPresets: List<Color> by remember {
        mutableStateOf((presets ?: currentColour.generatePalette(10, 1f)).sorted(true))
    }

    LaunchedEffect(key1, presets) {
        if (presets != null) {
            colourPresets = presets
            return@LaunchedEffect
        }

        snapshotFlow { currentColour }
            .collectLatest {
                delay(0.1.seconds)
                colourPresets = it.generatePalette(10, 1f).sorted(true)
            }
    }

    Column(
        modifier
            .thenIf(square) {
                width(IntrinsicSize.Min)
            },
        verticalArrangement = arrangement
    ) {
        Row(
            Modifier
                .fillMaxHeight()
                .weight(1f),
            horizontalArrangement = arrangement
        ) {
            var height: Dp by remember { mutableStateOf(0.dp) }

            ScrollBarColumn(
                Modifier.height(height),
                verticalArrangement = arrangement
            ) {
                for (colour in colourPresets) {
                    colour.presetItem {
                        currentColour = colour
                        onSelected(colour)
                    }
                }
            }

            Crossfade(key1) {
                ClassicColorPicker(
                    Modifier
                        .fillMaxHeight()
                        .weight(1f)
                        .thenIf(
                            square,
                            elseAction = {
                                fillMaxWidth()
                            },
                            action = {
                                aspectRatio(1f)
                            }
                        )
                        .onSizeChanged {
                            height = with (density) {
                                it.height.toDp()
                            }
                        },
                    HsvColor.from(currentColour),
                    showAlphaBar = false
                ) { colour ->
                    currentColour = colour.toColor()
                    onSelected(currentColour)
                }
            }
        }

        Row(
            Modifier.fillMaxWidth(),
            horizontalArrangement = arrangement,
            verticalAlignment = Alignment.CenterVertically
        ) {
            val textFieldState: TextFieldState by remember { mutableStateOf(TextFieldState()) }
            val currentTextFieldColour: Color? by remember {
                derivedStateOf {
                    runCatching { Color.fromHexString(textFieldState.text.toString().trim()) }
                        .getOrNull()
                }
            }
            val textFieldIsError: Boolean by remember {
                derivedStateOf {
                    currentTextFieldColour == null && textFieldState.text.isNotBlank()
                }
            }

            OutlinedTextField(
                textFieldState,
                modifier = Modifier.fillMaxWidth().weight(1f),
                lineLimits = TextFieldLineLimits.SingleLine,
                isError = textFieldIsError,
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                onKeyboardAction = {
                    currentTextFieldColour?.also { colour ->
                        currentColour = colour
                        onSelected(colour)
                    }
                },
                placeholder = {
                    Text(currentColour.toRGBHexString(), Modifier.alpha(0.5f))
                }
            )

            bottomRowExtraContent?.invoke()
        }
    }
}

@Composable
private fun Color.presetItem(onSelected: () -> Unit) {
    Spacer(Modifier
        .size(40.dp)
        .background(this, CircleShape)
        .border(1.dp, LocalContentColor.current, CircleShape)
        .clickable(
            interactionSource = remember { MutableInteractionSource() },
            indication = null
        ) {
            onSelected()
        }
    )
}

package dev.toastbits.composekit.components.utils.composable

import androidx.compose.foundation.LocalIndication
import androidx.compose.material3.ripple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.graphics.Color

@Composable
fun NoRipple(content: @Composable () -> Unit) {
    CompositionLocalProvider(LocalIndication provides ripple(color = Color.Transparent)) {
        content()
    }
}

package dev.toastbits.composekit.components.utils.modifier

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.gestures.FlingBehavior
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.verticalScroll
import androidx.compose.ui.Modifier

fun Modifier.scrollWithoutClip(
    state: ScrollState,
    is_vertical: Boolean,
    reverse_scrolling: Boolean = false,
    fling_behaviour: FlingBehavior? = null,
    is_scrollable: Boolean = true
): Modifier =
    // TODO
    if (is_vertical)
        verticalScroll(
            state,
            enabled = is_scrollable,
            reverseScrolling = reverse_scrolling,
            flingBehavior = fling_behaviour
        )
    else
        horizontalScroll(
            state,
            enabled = is_scrollable,
            reverseScrolling = reverse_scrolling,
            flingBehavior = fling_behaviour
        )

package dev.toastbits.composekit.components.utils.composable.pane

import androidx.compose.runtime.Composable
import androidx.compose.runtime.ProvidableCompositionLocal
import androidx.compose.runtime.staticCompositionLocalOf
import dev.toastbits.composekit.components.utils.composable.pane.model.InitialPaneRatioSource
import dev.toastbits.composekit.components.utils.composable.pane.model.ResizablePaneContainerParams

val LocalPaneSettingsProvider: ProvidableCompositionLocal<PaneSettingsProvider?> =
    staticCompositionLocalOf { null }

interface PaneSettingsProvider {
    fun getPaneRatioRememberMode(): InitialPaneRatioSource.Remembered.RememberMode
    fun getInitialPaneRatio(key: String): InitialPaneRatioSource?
    suspend fun updatePaneRatio(key: String, ratio: InitialPaneRatioSource)

    @Composable
    fun getResizablePaneContainerParams(default: ResizablePaneContainerParams): ResizablePaneContainerParams?
}

package dev.toastbits.composekit.components

import androidx.compose.runtime.ProvidableCompositionLocal
import androidx.compose.runtime.staticCompositionLocalOf

val LocalComposeKitDialogWrapper: ProvidableCompositionLocal<ComposeKitDialogWrapper> =
    staticCompositionLocalOf {
        ComposeKitDialogWrapper { show, dialogContent ->
            if (show) {
                dialogContent()
            }
        }
    }

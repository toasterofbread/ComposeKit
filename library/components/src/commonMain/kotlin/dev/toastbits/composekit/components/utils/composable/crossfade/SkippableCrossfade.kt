package dev.toastbits.composekit.components.utils.composable.crossfade

import androidx.compose.animation.core.FiniteAnimationSpec
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import dev.toastbits.composekit.util.composable.AlignableCrossfade

data class SkippableCrossfadeTransition(
    val skipTransition: Boolean,
    val contentChanged: Boolean = true
)

@Composable
fun <T> SkippableCrossfade(
    state: T,
    shouldSkipTransition: (T, T) -> SkippableCrossfadeTransition,
    modifier: Modifier = Modifier,
    animationSpec: FiniteAnimationSpec<Float> = tween(),
    contentAlignment: Alignment = Alignment.TopStart,
    content: @Composable (T) -> Unit
) {
    var crossfadeState: T by remember { mutableStateOf(state) }
    var currentState: T by remember { mutableStateOf(state) }
    var useCurrentState: Boolean by remember { mutableStateOf(false) }

    LaunchedEffect(state) {
        val (skipTransition: Boolean, contentChanged: Boolean) = shouldSkipTransition(currentState, state)

        if (skipTransition) {
            currentState = state
            if (contentChanged) {
                crossfadeState = state
            }
            useCurrentState = true
        }
        else {
            currentState = state
            crossfadeState = state
            useCurrentState = false
        }
    }

    AlignableCrossfade(
        crossfadeState,
        modifier,
        animationSpec = animationSpec,
        contentAlignment = contentAlignment
    ) { s ->
        content(if (useCurrentState) currentState else s)
    }
}

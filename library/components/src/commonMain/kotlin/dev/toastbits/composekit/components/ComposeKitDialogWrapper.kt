package dev.toastbits.composekit.components

import androidx.compose.runtime.Composable

fun interface ComposeKitDialogWrapper {
    @Composable
    fun DialogWrapper(show: Boolean, dialogContent: @Composable () -> Unit)
}

@file:Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
package dev.toastbits.composekit.components.utils.composable

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.RowScope
import androidx.compose.material3.ButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
fun LoadActionButton(
    performLoad: suspend (Boolean) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    uncancellable: Boolean = false,
    loadOnLaunch: Boolean = false,
    hasAltLoadAction: Boolean = false,
    contentPadding: PaddingValues = ButtonDefaults.ContentPadding,
    contentAlignment: Alignment = Alignment.Center,
    content: @Composable RowScope.() -> Unit,
) {
    BaseLoadActionButton(
        performLoad = performLoad,
        modifier = modifier,
        uncancellable = uncancellable,
        loadOnLaunch = loadOnLaunch,
        contentAlignment = contentAlignment
    ) { contentModifier, onClick ->
        PlatformClickableButton(
            onClick = { onClick(false) },
            onAltClick = { onClick(true) }.takeIf { hasAltLoadAction },
            modifier = contentModifier,
            content = content,
            enabled = enabled,
            contentPadding = contentPadding
        )
    }
}

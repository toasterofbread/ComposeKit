package dev.toastbits.composekit.components.utils.composable.pane.model

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.components.utils.composable.pane.LocalPaneSettingsProvider
import dev.toastbits.composekit.components.utils.composable.pane.PaneSettingsProvider

interface ResizablePaneContainerParamsProvider {
    @Composable
    operator fun invoke(): ResizablePaneContainerParams

    companion object {
        fun default(): ResizablePaneContainerParamsProvider =
            object : ResizablePaneContainerParamsProvider {
                @Composable
                override fun invoke(): ResizablePaneContainerParams {
                    val paneSettingsProvider: PaneSettingsProvider? = LocalPaneSettingsProvider.current
                    val default: ResizablePaneContainerParams = ResizablePaneContainerParamsData()
                    return (
                        paneSettingsProvider?.getResizablePaneContainerParams(default)
                        ?: default
                    )
                }
            }
    }
}

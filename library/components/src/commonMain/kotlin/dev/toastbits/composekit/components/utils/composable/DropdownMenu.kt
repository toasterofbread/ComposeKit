package dev.toastbits.composekit.components.utils.composable

import androidx.compose.foundation.clickable
import androidx.compose.foundation.hoverable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsHoveredAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyGridState
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.LocalComposeKitDialogWrapper
import dev.toastbits.composekit.components.platform.composable.ScrollBarLazyVerticalGrid
import dev.toastbits.composekit.components.utils.composable.wave.WaveLineArea
import dev.toastbits.composekit.components.generated.resources.Res
import dev.toastbits.composekit.components.generated.resources.dropdown_popup_button_cancel
import dev.toastbits.composekit.theme.core.cardVibrantAccent
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent
import dev.toastbits.composekit.util.composable.OnChangedEffect
import kotlinx.coroutines.flow.collectLatest
import org.jetbrains.compose.resources.stringResource

@Composable
fun <T> LargeDropdownMenu(
    isOpen: Boolean,
    title: String,
    items: List<T>,
    selectedItem: T?,
    onDismissRequest: () -> Unit,
    onSelected: (Int, T) -> Unit,
    modifier: Modifier = Modifier,
    columns: GridCells = GridCells.Fixed(1),
    additionalTopContent: @Composable ColumnScope.() -> Unit = {},
    onItemHovered: (Pair<Int, T>?) -> Unit = {},
    itemContent: @Composable (T) -> Unit
) {
    LocalComposeKitDialogWrapper.current.DialogWrapper(isOpen) {
        AlertDialog(
            modifier = modifier,
            onDismissRequest = onDismissRequest,
            confirmButton = {
                Button(onDismissRequest) {
                    Text(stringResource(Res.string.dropdown_popup_button_cancel))
                }
            },
            text = {
                var currentHoverItem: Int? by remember { mutableStateOf(null) }
                OnChangedEffect(currentHoverItem) {
                    onItemHovered(currentHoverItem?.let { it to items[it] })
                }
                DisposableEffect(onItemHovered) {
                    onDispose {
                        onItemHovered(null)
                    }
                }

                Column(verticalArrangement = Arrangement.spacedBy(20.dp)) {
                    additionalTopContent()

                    Column(verticalArrangement = Arrangement.spacedBy(15.dp)) {
                        Text(
                            title,
                            style = MaterialTheme.typography.titleMedium,
                            color = LocalComposeKitTheme.current.vibrantAccent
                        )

                        val gridState: LazyGridState = rememberLazyGridState()
                        LaunchedEffect(Unit) {
                            val selectedIndex: Int = items.indexOf(selectedItem)
                            if (selectedIndex != -1) {
                                gridState.scrollToItem(index = selectedIndex)
                            }
                        }

                        ScrollBarLazyVerticalGrid(
                            state = gridState,
                            columns = columns,
                            verticalArrangement = Arrangement.spacedBy(10.dp)
                        ) {
                            itemsIndexed(items) { index, item ->
                                val interactionSource: MutableInteractionSource = remember { MutableInteractionSource() }
                                val hovering: Boolean by interactionSource.collectIsHoveredAsState()

                                LaunchedEffect(Unit) {
                                    snapshotFlow { hovering }
                                        .collectLatest {
                                            if (hovering) {
                                                currentHoverItem = index
                                            }
                                            else if (currentHoverItem == index) {
                                                currentHoverItem = null
                                            }
                                        }
                                }

                                WaveLineArea(
                                    Modifier
                                        .clip(MaterialTheme.shapes.small)
                                        .hoverable(interactionSource)
                                        .clickable {
                                            onSelected(index, item)
                                        }
                                        .fillMaxWidth(),
                                    contentModifier = Modifier.padding(15.dp),
                                    showWaves = item == selectedItem,
                                    lineColour = LocalComposeKitTheme.current.cardVibrantAccent.copy(alpha = 0.2f)
                                ) {
                                    itemContent(item)
                                }
                            }
                        }
                    }
                }
            }
        )
    }
}

@file:Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
package dev.toastbits.composekit.components.utils.composable

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun LoadActionIconButton(
    performLoad: suspend (Boolean) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    uncancellable: Boolean = false,
    loadOnLaunch: Boolean = false,
    hasAltLoadAction: Boolean = false,
    content: @Composable () -> Unit,
) {
    BaseLoadActionButton(
        performLoad = performLoad,
        modifier = modifier,
        uncancellable = uncancellable,
        loadOnLaunch = loadOnLaunch
    ) { contentModifier, onClick ->
        PlatformClickableIconButton(
            onClick = { onClick(false) },
            onAltClick = { onClick(true) }.takeIf { hasAltLoadAction },
            modifier = contentModifier,
            content = content,
            enabled = enabled
        )
    }
}

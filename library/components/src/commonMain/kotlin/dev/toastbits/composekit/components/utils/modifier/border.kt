package dev.toastbits.composekit.components.utils.modifier

import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.drawOutline
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.Dp

fun Modifier.border(width: Dp, shape: Shape, getColour: () -> Color) =
    drawBehind {
        drawOutline(shape.createOutline(size, layoutDirection, this), getColour(), style = Stroke(width.toPx()))
    }

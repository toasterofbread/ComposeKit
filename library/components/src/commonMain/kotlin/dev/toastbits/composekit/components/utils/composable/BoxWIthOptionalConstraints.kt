package dev.toastbits.composekit.components.utils.composable

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.BoxWithConstraintsScope
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
fun BoxWithOptionalConstraints(
    enable_constraints: Boolean,
    modifier: Modifier = Modifier,
    contentAlignment: Alignment = Alignment.TopStart,
    content: @Composable (BoxWithConstraintsScope?) -> Unit
) {
    if (enable_constraints) {
        BoxWithConstraints(modifier, contentAlignment = contentAlignment) {
            content(this)
        }
    }
    else {
        Box(modifier, contentAlignment = contentAlignment) {
            content(null)
        }
    }
}
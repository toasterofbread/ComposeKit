package dev.toastbits.composekit.components.utils.composable.wave

import androidx.compose.animation.core.DurationBasedAnimationSpec
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent

@Composable
fun WaveBorder(
    modifier: Modifier = Modifier,
    waveColour: Color = LocalComposeKitTheme.current.vibrantAccent,
    waveThickness: Dp = 2.dp,
    wavelength: Dp = 50.dp,
    offsetState: State<Float>
) {
    Canvas(
        modifier
            .height(15.dp)
            .clipToBounds()
    ) {
        val path: Path = Path()

        fullWavePath(
            path = path,
            height = size.height,
            length = size.width,
            waveLength = wavelength.toPx(),
            offset = offsetState.value
        )

        translate(top = size.height / 2f) {
            drawPath(path, waveColour, style = Stroke(waveThickness.toPx()))
        }
    }
}

@Composable
fun WaveBorder(
    modifier: Modifier = Modifier,
    waveColour: Color = LocalComposeKitTheme.current.vibrantAccent,
    waveThickness: Dp = 2.dp,
    wavelength: Dp = 50.dp,
    offsetAnimationSpec: DurationBasedAnimationSpec<Float>? = tween(2000, easing = LinearEasing),
) {
    val offsetAnimation: State<Float> =
        if (offsetAnimationSpec != null)
            rememberInfiniteTransition()
                .animateFloat(
                    initialValue = 0f,
                    targetValue = 1f,
                    animationSpec = infiniteRepeatable(
                        animation = offsetAnimationSpec,
                        repeatMode = RepeatMode.Restart
                    )
                )
        else mutableStateOf(0f)

    WaveBorder(modifier, waveColour, waveThickness, wavelength, offsetAnimation)
}

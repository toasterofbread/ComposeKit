package dev.toastbits.composekit.components.utils.composable

import androidx.compose.foundation.Indication
import androidx.compose.foundation.hoverable
import androidx.compose.foundation.indication
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.material.ContentAlpha
import androidx.compose.material.LocalContentAlpha
import androidx.compose.material.minimumInteractiveComponentSize
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.ripple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.platform.composable.platformClickable
import dev.toastbits.composekit.components.utils.modifier.background
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.onAccent
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.vibrantAccent
import dev.toastbits.composekit.util.thenIf

@Composable
fun PlatformClickableFilledIconButton(
    onClick: (() -> Unit)? = null,
    onAltClick: (() -> Unit)? = null,
    onAlt2Click: (() -> Unit)? = null,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    shape: Shape = IconButtonDefaults.filledShape,
    indication: Indication? = ripple(bounded = false, radius = 24.dp),
    apply_minimum_size: Boolean = true,
    content: @Composable () -> Unit
) {
    val theme: ThemeValues = LocalComposeKitTheme.current
    val interactionSource: MutableInteractionSource = remember { MutableInteractionSource() }

    val contentAlpha = if (enabled) LocalContentAlpha.current else ContentAlpha.disabled
    CompositionLocalProvider(
        LocalContentAlpha provides contentAlpha,
        LocalContentColor provides theme.onAccent
    ) {
        Box(
            modifier = modifier
                .hoverable(interactionSource)
                .indication(interactionSource, indication)
                .platformClickable(
                    onClick = onClick,
                    onAltClick = onAltClick,
                    onAlt2Click = onAlt2Click,
                    enabled = enabled,
                    indication = indication
                )
                .background(shape) { theme.vibrantAccent }
                .thenIf(apply_minimum_size) {
                    minimumInteractiveComponentSize()
                },
            contentAlignment = Alignment.Center
        ) {
            content()
        }
    }
}

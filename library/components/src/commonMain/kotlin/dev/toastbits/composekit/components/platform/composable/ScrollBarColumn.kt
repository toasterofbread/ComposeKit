package dev.toastbits.composekit.components.platform.composable

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.gestures.FlingBehavior
import androidx.compose.foundation.gestures.ScrollableDefaults
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun ScrollBarColumn(
    modifier: Modifier = Modifier,
    state: ScrollState = rememberScrollState(),
    showScrollbar: Boolean = true,
    contentPadding: PaddingValues = PaddingValues(0.dp),
    scrollBarContentPadding: PaddingValues = contentPadding,
    verticalArrangement: Arrangement.Vertical = Arrangement.Top,
    horizontalAlignment: Alignment.Horizontal = Alignment.Start,
    flingBehavior: FlingBehavior = ScrollableDefaults.flingBehavior(),
    userScrollEnabled: Boolean = true,
    scrollBarColour: Color = Color.Unspecified,
    verticalAlignment: Alignment.Vertical = Alignment.Top,
    reverseScrollBarLayout: Boolean = false,
    scrollBarSpacing: Dp = 10.dp,
    scrollBarThickness: Dp = 8.dp,
    columnModifier: Modifier = Modifier,
    content: @Composable ColumnScope.() -> Unit
) {
    VerticalScrollBarContainer(
        state = state,
        modifier = modifier,
        showScrollbar = showScrollbar,
        contentPadding = contentPadding,
        scrollBarContentPadding = scrollBarContentPadding,
        horizontalAlignment = horizontalAlignment,
        userScrollEnabled = userScrollEnabled,
        scrollBarColour = scrollBarColour,
        verticalAlignment = verticalAlignment,
        reverseScrollBarLayout = reverseScrollBarLayout,
        scrollBarSpacing = scrollBarSpacing,
        scrollBarThickness = scrollBarThickness
    ) { innerModifier, innerPadding ->
        Column(
            modifier = columnModifier
                .then(innerModifier)
                .verticalScroll(
                    state,
                    enabled = userScrollEnabled,
                    flingBehavior = flingBehavior
                )
                .padding(innerPadding),
            verticalArrangement = verticalArrangement,
            horizontalAlignment = horizontalAlignment,
            content = content
        )
    }
}

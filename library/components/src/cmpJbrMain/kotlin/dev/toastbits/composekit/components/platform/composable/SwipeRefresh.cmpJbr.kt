package dev.toastbits.composekit.components.platform.composable

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
actual fun SwipeRefresh(
    state: Boolean,
    onRefresh: () -> Unit,
    modifier: Modifier,
    swipe_enabled: Boolean,
    indicator: Boolean,
    indicator_padding: PaddingValues,
    content: @Composable () -> Unit
) {
    Box(modifier) {
        content()
    }
}

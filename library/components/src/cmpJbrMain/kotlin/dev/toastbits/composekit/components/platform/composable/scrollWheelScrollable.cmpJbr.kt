package dev.toastbits.composekit.components.platform.composable

import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.gestures.ScrollableState
import androidx.compose.foundation.gestures.animateScrollBy
import androidx.compose.runtime.ProvidableCompositionLocal
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.input.pointer.PointerEventType
import androidx.compose.ui.input.pointer.isShiftPressed
import androidx.compose.ui.input.pointer.onPointerEvent
import dev.toastbits.composekit.util.platform.Platform
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

val LocalScrollWheelDeltaMultiplier: ProvidableCompositionLocal<Float> =
    staticCompositionLocalOf {
        when (Platform.current) {
            Platform.DESKTOP -> 100f
            else -> 1f
        }
    }

actual fun Modifier.scrollWheelScrollable(
    state: ScrollableState,
    reverseDirection: Boolean,
    onlyWhileShifting: Boolean,
    onDelta: (Float) -> Unit
): Modifier = composed {
    val coroutineScope: CoroutineScope = rememberCoroutineScope()
    val scrollWheelDeltaMultiplier: Float = LocalScrollWheelDeltaMultiplier.current

    onPointerEvent(PointerEventType.Scroll) { event ->
        if (onlyWhileShifting && !event.keyboardModifiers.isShiftPressed) {
            return@onPointerEvent
        }

        val distance: Float = event.changes.first().scrollDelta.y * scrollWheelDeltaMultiplier * (if (reverseDirection) -1 else 1)
        onDelta(distance)

        coroutineScope.launch {
            if (state.isScrollInProgress) {
                state.animateScrollBy(distance * 1.5f, tween(durationMillis = 150, easing = LinearOutSlowInEasing))
            }
            else {
                state.animateScrollBy(distance, tween(durationMillis = 200, easing = LinearOutSlowInEasing))
            }
        }
    }
}

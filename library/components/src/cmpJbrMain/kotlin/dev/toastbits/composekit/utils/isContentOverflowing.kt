package dev.toastbits.composekit.utils

import androidx.compose.foundation.gestures.ScrollableState
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.grid.LazyGridState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp

@Composable
fun ScrollableState.isContentOverflowing(startPadding: Dp, endPadding: Dp): State<Boolean> =
    when (this) {
        is LazyListState -> isContentOverflowing(startPadding, endPadding)
        is LazyGridState -> isContentOverflowing(startPadding, endPadding)
        else -> mutableStateOf(true)
    }

@Composable
fun LazyListState.isContentOverflowing(startPadding: Dp, endPadding: Dp): State<Boolean> {
    val density: Density = LocalDensity.current

    val startPaddingPx: Int =
        remember(density, startPadding) {
            with (density) { startPadding.roundToPx() }
        }
    val endPaddingPx: Int =
        remember(density, endPadding) {
            with (density) { endPadding.roundToPx() }
        }

    return remember(this, startPaddingPx, endPaddingPx) {
        derivedStateOf {
            layoutInfo.visibleItemsInfo.firstOrNull()?.also { firstItem ->
                if (
                    firstItem.index > 0
                    || firstItem.offset - startPaddingPx < layoutInfo.viewportStartOffset
                ) {
                    return@derivedStateOf true
                }
            }

            layoutInfo.visibleItemsInfo.lastOrNull()?.also { lastItem ->
                if (
                    lastItem.index < layoutInfo.totalItemsCount - 1
                    || (lastItem.offset + lastItem.size + endPaddingPx) > layoutInfo.viewportEndOffset
                ) {
                    return@derivedStateOf true
                }
            }

            return@derivedStateOf false
        }
    }
}

@Composable
fun LazyGridState.isContentOverflowing(startPadding: Dp, endPadding: Dp): State<Boolean> {
    val density: Density = LocalDensity.current

    val startPaddingPx: Int =
        remember(density, startPadding) {
            with (density) { startPadding.roundToPx() }
        }
    val endPaddingPx: Int =
        remember(density, endPadding) {
            with (density) { endPadding.roundToPx() }
        }

    return remember(this, startPaddingPx, endPaddingPx) {
        derivedStateOf {
            layoutInfo.visibleItemsInfo.firstOrNull()?.also { firstItem ->
                if (
                    firstItem.index > 0
                    || firstItem.offset.y - startPaddingPx < layoutInfo.viewportStartOffset
                ) {
                    return@derivedStateOf true
                }
            }

            if (layoutInfo.visibleItemsInfo.size == 1) {
                return@derivedStateOf false
            }

            layoutInfo.visibleItemsInfo.lastOrNull()?.also { lastItem ->
                if (
                    lastItem.index < layoutInfo.totalItemsCount - 1
                    || (lastItem.offset.y + lastItem.size.height + endPaddingPx) > layoutInfo.viewportEndOffset
                ) {
                    return@derivedStateOf true
                }
            }

            return@derivedStateOf false
        }
    }
}

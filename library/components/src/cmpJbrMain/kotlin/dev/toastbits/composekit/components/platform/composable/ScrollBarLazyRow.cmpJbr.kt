package dev.toastbits.composekit.components.platform.composable

import androidx.compose.foundation.HorizontalScrollbar
import androidx.compose.foundation.LocalScrollbarStyle
import androidx.compose.foundation.ScrollbarStyle
import androidx.compose.foundation.gestures.FlingBehavior
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.rememberScrollbarAdapter
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.isUnspecified
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import dev.toastbits.composekit.util.composable.animateFloatAsState
import dev.toastbits.composekit.util.composable.end
import dev.toastbits.composekit.util.composable.start
import dev.toastbits.composekit.util.thenIf
import dev.toastbits.composekit.util.toFloat
import dev.toastbits.composekit.utils.isContentOverflowing

@Composable
actual fun ScrollBarLazyRow(
    modifier: Modifier,
    state: LazyListState,
    showScrollbar: Boolean,
    contentPadding: PaddingValues,
    scrollBarContentPadding: PaddingValues,
    reverseLayout: Boolean,
    horizontalArrangement: Arrangement.Horizontal,
    verticalAlignment: Alignment.Vertical,
    flingBehavior: FlingBehavior,
    userScrollEnabled: Boolean,
    scrollBarColour: Color,
    horizontalAlignment: Alignment.Horizontal,
    reverseScrollBarLayout: Boolean,
    scrollBarSpacing: Dp,
    scrollBarThickness: Dp,
    rowModifier: Modifier,
    content: LazyListScope.() -> Unit
) {
    Column(modifier.scrollWheelScrollable(state, onlyWhileShifting = true), horizontalAlignment = horizontalAlignment) {
        val isScrollBarShowing: Boolean by
            state.isContentOverflowing(
                startPadding = contentPadding.start,
                endPadding = contentPadding.end
            )
        val scrollBarShowingAnimation: Float by animateFloatAsState { isScrollBarShowing.toFloat() }

        val scrollbarModifier: Modifier =
            Modifier.graphicsLayer { alpha = scrollBarShowingAnimation }

        val scrollbarStyle: ScrollbarStyle =
            LocalScrollbarStyle.current
                .run {
                    if (scrollBarColour.isUnspecified) this
                    else copy(
                        hoverColor = scrollBarColour,
                        unhoverColor = scrollBarColour.copy(alpha = scrollBarColour.alpha * 0.25f)
                    )
                }
                .copy(thickness = scrollBarThickness)

        if (reverseScrollBarLayout && showScrollbar) {
            HorizontalScrollbar(
                rememberScrollbarAdapter(state),
                scrollbarModifier,
                style = scrollbarStyle
            )
            Spacer(Modifier.height(scrollBarSpacing))
        }

        LazyRow(
            rowModifier
                .weight(1f, false)
                .thenIf(showScrollbar) {
                    offset {
                        IntOffset(
                            x = 0,
                            y = (((scrollbarStyle.thickness + scrollBarSpacing) / 2) * (1f - scrollBarShowingAnimation)).roundToPx()
                        )
                    }
                },
            state,
            contentPadding,
            reverseLayout,
            horizontalArrangement,
            verticalAlignment,
            flingBehavior,
            userScrollEnabled
        ) {
            content()
        }

        if (!reverseScrollBarLayout && showScrollbar) {
            Spacer(Modifier.height(scrollBarSpacing))
            HorizontalScrollbar(
                rememberScrollbarAdapter(state),
                scrollbarModifier,
                style = scrollbarStyle
            )
        }
    }
}

package dev.toastbits.composekit.components.platform.composable

import androidx.compose.foundation.LocalScrollbarStyle
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.ScrollbarAdapter
import androidx.compose.foundation.ScrollbarStyle
import androidx.compose.foundation.VerticalScrollbar
import androidx.compose.foundation.focusable
import androidx.compose.foundation.gestures.ScrollableState
import androidx.compose.foundation.gestures.animateScrollBy
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.grid.LazyGridState
import androidx.compose.foundation.text.TextFieldScrollState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.isUnspecified
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.KeyEvent
import androidx.compose.ui.input.key.KeyEventType
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.input.key.type
import androidx.compose.ui.input.pointer.PointerEventType
import androidx.compose.ui.input.pointer.onPointerEvent
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import dev.toastbits.composekit.components.utils.modifier.horizontal
import dev.toastbits.composekit.components.utils.modifier.vertical
import dev.toastbits.composekit.util.composable.animateFloatAsState
import dev.toastbits.composekit.util.composable.bottom
import dev.toastbits.composekit.util.composable.top
import dev.toastbits.composekit.util.thenIf
import dev.toastbits.composekit.util.toFloat
import dev.toastbits.composekit.utils.isContentOverflowing
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds
import kotlin.time.TimeSource

private const val ARROW_KEY_SCROLL_AMOUNT: Float = 75f

@Composable
actual fun <S: ScrollableState> VerticalScrollBarContainer(
    state: S,
    modifier: Modifier,
    showScrollbar: Boolean,
    contentPadding: PaddingValues,
    scrollBarContentPadding: PaddingValues,
    horizontalAlignment: Alignment.Horizontal,
    userScrollEnabled: Boolean,
    scrollBarColour: Color,
    verticalAlignment: Alignment.Vertical,
    reverseScrollBarLayout: Boolean,
    scrollBarSpacing: Dp,
    scrollBarThickness: Dp,
    onScrollDelta: ((delta: Float, continuous: Boolean) -> Unit)?,
    content: @Composable (Modifier, PaddingValues) -> Unit
) {
    val density: Density = LocalDensity.current
    val coroutineScope: CoroutineScope = rememberCoroutineScope()
    val scrollKeyEventFilter: ScrollKeyEventFilter = LocalScrollKeyEventFilter.current

    var height: Int by remember { mutableStateOf(0) }
    val focusRequester: FocusRequester = remember { FocusRequester() }
    var isFocused: Boolean by remember { mutableStateOf(false) }

    Row(
        modifier
            .focusable()
            .focusRequester(focusRequester)
            .onFocusChanged {
                isFocused = it.hasFocus
            }
            .onPointerEvent(PointerEventType.Press) {
                if (!isFocused) {
                    focusRequester.requestFocus()
                }
            }
            .onSizeChanged {
                height = it.height
            }
            .thenIf(userScrollEnabled) {
                scrollWheelScrollable(state) { delta ->
                    if (onScrollDelta == null || delta == 0f) {
                        return@scrollWheelScrollable
                    }

                    val canScroll: Boolean =
                        if (delta > 0) state.canScrollForward
                        else state.canScrollBackward

                    if (canScroll) {
                        onScrollDelta(delta, false)
                    }
                }
            }
            .onKeyEvent { keyEvent ->
                if (keyEvent.type != KeyEventType.KeyDown || !scrollKeyEventFilter(keyEvent)) {
                    return@onKeyEvent false
                }

                return@onKeyEvent coroutineScope.handleScrollContainerKeyEvent(keyEvent, state, onScrollDelta, height)
            }
            .padding(contentPadding.horizontal),
        horizontalArrangement = Arrangement.aligned(horizontalAlignment),
        verticalAlignment = verticalAlignment
    ) {
        val isScrollBarShowing: Boolean by
            state.isContentOverflowing(
                startPadding = contentPadding.top,
                endPadding = contentPadding.bottom
            )

        val launchTime: TimeSource.Monotonic.ValueTimeMark =
            remember { TimeSource.Monotonic.markNow() }
        val scrollBarShowingAnimation: Float by
            animateFloatAsState(
                shouldSnap = { launchTime.elapsedNow() < 0.1.seconds },
                getTargetValue =  { isScrollBarShowing.toFloat() }
            )

        val scrollbarStyle: ScrollbarStyle =
            LocalScrollbarStyle.current
                .run {
                    if (scrollBarColour.isUnspecified) this
                    else copy(
                        hoverColor = scrollBarColour,
                        unhoverColor = scrollBarColour.copy(alpha = scrollBarColour.alpha * 0.25f)
                    )
                }
                .copy(thickness = scrollBarThickness)

        val scrollbarAdapter: androidx.compose.foundation.v2.ScrollbarAdapter =
            remember(state, onScrollDelta) {
                when (state) {
                    is LazyListState ->
                        CustomLazyListScrollbarAdapter(state) { onScrollDelta?.invoke(it, false) }
                    is ScrollState ->
                        ScrollbarAdapter(state)
                    is LazyGridState ->
                        ScrollbarAdapter(state)
                    is TextFieldScrollState ->
                        ScrollbarAdapter(state)
                    else -> throw NotImplementedError(state::class.toString())
                }
            }
        val scrollbarModifier: Modifier =
            Modifier
                .padding(scrollBarContentPadding.vertical)
                .height(
                    with (density) {
                        height.toDp() - scrollBarContentPadding.top - scrollBarContentPadding.bottom
                    }
                )

        if (reverseScrollBarLayout && showScrollbar) {
            VerticalScrollbar(
                scrollbarAdapter,
                scrollbarModifier,
                style = scrollbarStyle
            )
            Spacer(Modifier.width(scrollBarSpacing))
        }

        content(
            Modifier
                .weight(1f, false)
                .thenIf(showScrollbar) {
                    offset {
                        val offset: Int = (
                            ((scrollbarStyle.thickness + scrollBarSpacing) / 2)
                            * (1f - scrollBarShowingAnimation)
                        ).roundToPx()

                        IntOffset(
                            x =
                                if (reverseScrollBarLayout) -offset
                                else offset,
                            y = 0
                        )
                    }
                },
            contentPadding.vertical
        )

        if (!reverseScrollBarLayout && showScrollbar) {
            Spacer(Modifier.width(scrollBarSpacing))
            VerticalScrollbar(
                scrollbarAdapter,
                scrollbarModifier,
                style = scrollbarStyle
            )
        }
    }
}

private fun <S : ScrollableState> CoroutineScope.handleScrollContainerKeyEvent(
    it: KeyEvent,
    state: S,
    onScrollDelta: ((delta: Float, continuous: Boolean) -> Unit)?,
    height: Int,
): Boolean {
    when (it.key) {
        Key.DirectionUp ->
            if (state.canScrollBackward) {
                launch {
                    onScrollDelta?.invoke(-ARROW_KEY_SCROLL_AMOUNT, false)
                    state.animateScrollBy(-ARROW_KEY_SCROLL_AMOUNT)
                }
                return true
            }
        Key.DirectionDown ->
            if (state.canScrollForward) {
                launch {
                    onScrollDelta?.invoke(ARROW_KEY_SCROLL_AMOUNT, false)
                    state.animateScrollBy(ARROW_KEY_SCROLL_AMOUNT)
                }
                return true
            }
        Key.PageUp ->
            if (state.canScrollBackward) {
                launch {
                    onScrollDelta?.invoke(-height.toFloat(), false)
                    state.animateScrollBy(-height.toFloat())
                }
                return true
            }
        Key.PageDown ->
            if (state.canScrollForward) {
                launch {
                    onScrollDelta?.invoke(height.toFloat(), false)
                    state.animateScrollBy(height.toFloat())
                }
                return true
            }
        Key.MoveHome ->
            if (state.canScrollBackward && state is LazyListState) {
                launch {
                    onScrollDelta?.invoke(-ARROW_KEY_SCROLL_AMOUNT * 100, false)
                    state.animateScrollToItem(0)
                }
                return true
            }
        Key.MoveEnd ->
            if (state.canScrollForward && state is LazyListState && state.layoutInfo.totalItemsCount > 0) {
                launch {
                    onScrollDelta?.invoke(ARROW_KEY_SCROLL_AMOUNT * 100, false)
                    state.animateScrollToItem(state.layoutInfo.totalItemsCount - 1)
                }
                return true
            }
    }

    return false
}

@Composable
actual fun areScrollBarsVisible(): Boolean = true

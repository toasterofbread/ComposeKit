
import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget
import util.configureAllKmpTargets

plugins {
    id("compose-noncommon-conventions")
    id("android-library-conventions")
    id("gitlab-publishing-conventions")

    alias(libs.plugins.kotlinx.serialization)
}

kotlin {
    configureAllKmpTargets {
        if (this is KotlinNativeTarget) {
            compilations.getByName("main") {
                cinterops {
                    // Must have unique names
                    create("locale-${this@configureAllKmpTargets.name}") {
                        defFile(project.file("src/nativeInterop/cinterop/locale.def"))
                    }
                }
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                implementation(libs.kotlinx.coroutines.core)
                implementation(libs.kotlinx.serialization.json)
            }
        }
    }
}

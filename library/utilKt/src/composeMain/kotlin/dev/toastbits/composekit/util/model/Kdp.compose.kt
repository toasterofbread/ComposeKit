package dev.toastbits.composekit.util.model

import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

val Kdp.dp: Dp
    get() = value.dp

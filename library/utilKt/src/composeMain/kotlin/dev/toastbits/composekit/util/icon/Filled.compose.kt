package dev.toastbits.composekit.util.icon

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Cloud
import dev.toastbits.composekit.util.model.KImageVectorResource

internal actual object Filled: dev.toastbits.composekit.util.icon.Icons {
    actual override val Cloud: KImageVectorResource
        get() = KImageVectorResource { Icons.Default.Cloud }
}

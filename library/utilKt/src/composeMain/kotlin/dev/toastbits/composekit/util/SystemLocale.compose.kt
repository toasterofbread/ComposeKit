@file:Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
package dev.toastbits.composekit.util

import dev.toastbits.composekit.util.model.Locale
import org.jetbrains.compose.resources.InternalResourceApi
import org.jetbrains.compose.resources.getSystemResourceEnvironment

@OptIn(InternalResourceApi::class)
actual fun getSystemLocaleImpl(): Locale? =
    getSystemResourceEnvironment().let {
        Locale(it.language.language, it.region.region)
    }

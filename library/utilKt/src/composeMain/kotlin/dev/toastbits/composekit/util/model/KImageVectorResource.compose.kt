package dev.toastbits.composekit.util.model

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector

actual fun interface KImageVectorResource {
    @Composable
    fun get(): ImageVector
}

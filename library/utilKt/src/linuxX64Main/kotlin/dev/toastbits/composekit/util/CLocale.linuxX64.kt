package dev.toastbits.composekit.util

import kotlinx.cinterop.ByteVarOf
import kotlinx.cinterop.CPointer
import locale.getLocale

internal actual fun getCLocale(): CPointer<ByteVarOf<Byte>>? =
    getLocale()

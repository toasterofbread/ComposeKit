package dev.toastbits.composekit.util

import dev.toastbits.composekit.util.model.Locale
import kotlinx.browser.window

actual fun getSystemLocaleImpl(): Locale? =
    window.navigator.languages.firstNotNullOfOrNull() {
        try {
            Locale.parse(it)
        }
        catch (_: Throwable) {
            null
        }
    }

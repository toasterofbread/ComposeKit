package dev.toastbits.composekit.util

import dev.toastbits.composekit.util.model.Locale
import kotlinx.cinterop.toKString

actual fun getSystemLocaleImpl(): Locale? {
    val cLocale: String? = getCLocale()?.toKString()
    if (cLocale.isNullOrBlank()) {
        return null
    }

    if (cLocale == "C") {
        return null
    }

    val locale: String =
        cLocale.split('.', limit = 2).first()

    try {
        return Locale.parse(locale)
    }
    catch (_: Throwable) {
        return null
    }
}

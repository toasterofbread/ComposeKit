package dev.toastbits.composekit.util

import kotlinx.cinterop.ByteVarOf
import kotlinx.cinterop.CPointer

internal expect fun getCLocale(): CPointer<ByteVarOf<Byte>>?

package dev.toastbits.composekit.util.icon

import dev.toastbits.composekit.util.model.KImageVectorResource

internal actual object Filled: Icons {
    actual override val Cloud: KImageVectorResource
        get() = object : KImageVectorResource {}
}

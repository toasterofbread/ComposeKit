package dev.toastbits.composekit.util.model

interface LocaleList {
    suspend fun getLocales(): List<Locale>
    suspend fun getReadableLocaleName(locale: Locale): String?
}

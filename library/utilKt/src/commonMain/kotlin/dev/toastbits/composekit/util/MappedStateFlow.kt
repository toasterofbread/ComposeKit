package dev.toastbits.composekit.util

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

private class MappedStateFlow<T>(
    private val flow: Flow<T>,
    private val mapValue: () -> T
): StateFlow<T> {
    override val replayCache: List<T>
        get () = listOf(value)

    override val value: T
        get () = mapValue()

    @InternalCoroutinesApi
    override suspend fun collect(collector: FlowCollector<T>): Nothing {
        coroutineScope { flow.distinctUntilChanged().stateIn(this).collect(collector) }
    }
}

fun <I, O> StateFlow<I>.mapState(map: (a: I) -> O): StateFlow<O> =
    MappedStateFlow(
        flow = this.map { a -> map(a) },
        mapValue = { map(this.value) }
    )

fun <I, O> StateFlow<I>.flatMapLatestState(map: (a: I) -> StateFlow<O>): StateFlow<O> =
    MappedStateFlow(
        flow = this.flatMapLatest { a -> map(a) },
        mapValue = { map(this.value).value }
    )

fun <I1, I2, O> combineStates(flow: StateFlow<I1>, flow2: StateFlow<I2>, map: (a: I1, b: I2) -> O): StateFlow<O> =
    MappedStateFlow(
        flow = combine(flow, flow2) { a, b -> map(a, b) },
        mapValue = { map(flow.value, flow2.value) }
    )

fun <I1, I2, I3, O> combineStates(flow: StateFlow<I1>, flow2: StateFlow<I2>, flow3: StateFlow<I3>, map: (a: I1, b: I2, c: I3) -> O): StateFlow<O> =
    MappedStateFlow(
        flow = combine(flow, flow2, flow3) { a, b, c -> map(a, b, c) },
        mapValue = { map(flow.value, flow2.value, flow3.value) }
    )

fun <I, O> MutableStateFlow<I>.mapMutable(
    coroutineScope: CoroutineScope,
    from: (I) -> O,
    to: (O) -> I
): MutableStateFlow<O> {
    val outState: MutableStateFlow<O> = MutableStateFlow(from(value))
    coroutineScope.launch {
        map(from).collect(outState)
        outState.map(to).collect(this@mapMutable)
    }
    return outState
}

package dev.toastbits.composekit.util.icon

import dev.toastbits.composekit.util.model.KImageVectorResource

interface Icons {
    val Cloud: KImageVectorResource

    companion object
}

val Icons.Companion.Default: Icons
    get() = Filled

val Icons.Companion.Filled: Icons
    get() = dev.toastbits.composekit.util.icon.Filled

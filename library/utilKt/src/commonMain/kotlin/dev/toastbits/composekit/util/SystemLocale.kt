package dev.toastbits.composekit.util

import dev.toastbits.composekit.util.model.Locale

var getSystemLocale: () -> Locale? = { getSystemLocaleImpl() }

internal expect fun getSystemLocaleImpl(): Locale?

package dev.toastbits.composekit.util

import kotlinx.coroutines.CancellationException
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt
import kotlin.reflect.KClass
import kotlin.time.Duration.Companion.seconds

fun Boolean.toInt() = if (this) 1 else 0
fun Boolean.toFloat() = if (this) 1f else 0f

inline fun <R, T: R> T.thenIf(condition: Boolean, action: T.() -> R): R = if (condition) action() else this
inline fun <T> T.thenIf(condition: Boolean, elseAction: T.() -> T, action: T.() -> T): T = if (condition) action() else elseAction()

inline fun <R, T: R, A: Any> T.thenWith(value: A?, nullAction: T.() -> R = { this }, action: T.(A) -> R): R =
	if (value != null) action(value) else nullAction()

inline fun <T, A: Any, B: Any> T.thenWith(valueA: A?, valueB: B?, nullAction: T.() -> T = { this }, action: T.(A, B) -> T): T =
	if (valueA != null && valueB != null) action(valueA, valueB) else nullAction()

fun getInnerSquareSizeOfCircle(radius: Float, corner_percent: Int): Float {
	val C = 1.0 - (corner_percent * 0.02)
	val E = (sqrt(8.0 * radius * radius) / 2.0) - radius
	val I = radius + (E * C)
	return sqrt(I * I * 0.5).toFloat()
}

fun <T> MutableList<T>.addUnique(item: T): Boolean {
	if (!contains(item)) {
		add(item)
		return true
	}
	return false
}

fun <T> MutableList<T>.toggleItemPresence(item: T) {
	val index: Int = indexOf(item)
	if (index == -1) {
		add(item)
	}
	else {
		removeAt(index)
	}
}

fun formatElapsedTime(seconds: Long): String {
	val hours = seconds.seconds.inWholeHours
	val minutes = seconds.seconds.inWholeMinutes % 60
	val remaining_seconds = seconds % 60
	if (hours > 0) {
		return "$hours:${minutes.toString().padStart(2, '0')}:${remaining_seconds.toString().padStart(2, '0')}"
	}
	else {
		return "${minutes.toString().padStart(2, '0')}:${remaining_seconds.toString().padStart(2, '0')}"
	}
}

inline fun <K, V : Any> MutableMap<K, V>.putIfAbsent(key: K, getValue: () -> V): V {
	var v = this[key]
	if (v == null) {
		v = getValue()
		put(key, v)
	}
	return v
}

fun String.indexOfOrNull(string: String, start_index: Int = 0, ignore_case: Boolean = false): Int? =
	indexOf(string, start_index, ignore_case).takeIf { it != -1 }

fun String.indexOfOrNull(char: Char, start_index: Int = 0, ignore_case: Boolean = false): Int? =
	indexOf(char, start_index, ignore_case).takeIf { it != -1 }

fun String.indexOfFirstOrNull(start: Int = 0, predicate: (Char) -> Boolean): Int? {
	for (i in start until length) {
		if (predicate(elementAt(i))) {
			return i
		}
	}
	return null
}

fun Float.roundTo(decimals: Int): Float {
	val multiplier = 10f.pow(decimals)
	return (this * multiplier).roundToInt() / multiplier
}

fun String.substringBetween(start: String, end: String, ignore_case: Boolean = false): String? {
	val start_index = indexOf(start, ignoreCase = ignore_case) + start.length
	if (start_index < start.length) {
		return null
	}
	val end_index = indexOf(end, start_index, ignoreCase = ignore_case)
	if (end_index == -1) {
		return null
	}

	return substring(start_index, end_index)
}

fun String.coerceIn(maxLength: Int, cutoffIndicator: String = "..."): String {
	val over: Int = length - maxLength
	if (over > 0) {
		return dropLast((over + cutoffIndicator.length).coerceAtMost(length)).trimEnd() + cutoffIndicator
	}
	return this
}

fun Throwable.anyCauseIs(cls: KClass<out Throwable>): Boolean {
	var checking: Throwable? = this
	while(checking != null) {
		if (cls.isInstance(checking)) {
			return true
		}
		checking = checking.cause
	}
	return false
}

fun <T, K, V> Iterable<T>.associateNotNull(transform: (T) -> Pair<K, V>?): Map<K, V> =
	mapNotNull(transform).associate { it }

suspend fun <T> runSuspendCatching(block: suspend () -> T): Result<T> {
	try {
		return Result.success(block())
	}
	catch (e: Throwable) {
		if (e is CancellationException) {
			throw e
		}
		return Result.failure(e)
	}
}

package dev.toastbits.composekit.util.model

import kotlin.jvm.JvmInline

@JvmInline
value class Kdp(val value: Float): Comparable<Kdp> {
    override fun compareTo(other: Kdp): Int =
        value.compareTo(other.value)
}

val Float.kdp: Kdp
    get() = Kdp(this)

val Int.kdp: Kdp
    get() = Kdp(this.toFloat())

package dev.toastbits.composekit.util.icon

import dev.toastbits.composekit.util.model.KImageVectorResource

internal expect object Filled: Icons {
    override val Cloud: KImageVectorResource
}

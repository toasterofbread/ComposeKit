package dev.toastbits.composekit.application

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
internal expect fun ApplicationContent(
    modifier: Modifier,
    content: @Composable (Modifier) -> Unit
)

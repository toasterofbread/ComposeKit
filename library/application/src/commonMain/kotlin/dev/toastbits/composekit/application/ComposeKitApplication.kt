package dev.toastbits.composekit.application

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.KeyEvent
import androidx.compose.ui.input.key.KeyEventType
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.type
import dev.toastbits.composekit.commonsettings.impl.ComposeKitSettings
import dev.toastbits.composekit.commonsettings.impl.LocalComposeKitSettings
import dev.toastbits.composekit.commonsettings.impl.group.theme.SettingsThemeManager
import dev.toastbits.composekit.components.LocalContext
import dev.toastbits.composekit.components.platform.composable.BackHandler
import dev.toastbits.composekit.components.platform.composable.onWindowBackPressed
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.navigation.compositionlocal.LocalNavigator
import dev.toastbits.composekit.navigation.navigator.CurrentScreen
import dev.toastbits.composekit.navigation.navigator.Navigator
import dev.toastbits.composekit.navigation.navigator.StandardNavigator
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.util.mapState
import dev.toastbits.composekit.util.platform.Platform
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

abstract class ComposeKitApplication(
    initialScreen: Screen,
    private val context: PlatformContext,
    open val baseUiScale: Float = 1f,
    open val baseFontScale: Float = 1f,
): StandardNavigator(initialScreen = initialScreen) {
    abstract val settings: ComposeKitSettings

    val navigator: Navigator get() = this

    val canNavigateBack: StateFlow<Boolean> =
        navigator.navigateBackwardCount
            .mapState { it > 0 }

    @Composable
    fun Main() {
        val coroutineScope: CoroutineScope = rememberCoroutineScope()
        val theme: SettingsThemeManager =
            remember {
                require(settings.preferences.json.configuration.useArrayPolymorphism) {
                    "ComposeKit settings JSON must have useArrayPolymorphism enabled"
                }

                SettingsThemeManager(settings)
            }

        CompositionLocalProvider(
            LocalContext provides context,
            LocalComposeKitSettings provides settings,
            LocalNavigator provides this
        ) {
            theme.ApplicationTheme(
                context,
                settings,
                baseUiScale = baseUiScale,
                baseFontScale = baseFontScale
            ) {
                Scaffold { padding ->
                    ApplicationContent(Modifier.fillMaxSize()) { innerModifier ->
                        Content(innerModifier, padding)
                    }
                }
            }
        }

        Platform.ANDROID.only {
            val currentCanNavigateBack: Boolean by canNavigateBack.collectAsState()
            BackHandler(currentCanNavigateBack) {
                coroutineScope.launch {
                    navigator.navigateBackward()
                }
            }
        }
    }

    fun onKeyEvent(event: KeyEvent): Boolean {
        if (event.type != KeyEventType.KeyUp) {
            return false
        }

        return when (event.key) {
            Key.Escape -> {
                if (onWindowBackPressed(context)) true
                else if (canNavigateBack.value) {
                    context.coroutineScope.launch {
                        navigator.navigateBackward()
                    }
                    true
                }
                else false
            }
            else -> false
        }
    }

    @Composable
    protected open fun Content(modifier: Modifier, contentPadding: PaddingValues) {
        navigator.CurrentScreen(modifier, contentPadding = contentPadding)
    }
}

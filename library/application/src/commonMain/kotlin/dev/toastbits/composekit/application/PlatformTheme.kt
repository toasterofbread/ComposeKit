package dev.toastbits.composekit.application

import androidx.compose.runtime.Composable
import dev.toastbits.composekit.theme.core.ThemeValues

@Composable
internal expect fun ThemeValues.PlatformTheme(content: @Composable () -> Unit)

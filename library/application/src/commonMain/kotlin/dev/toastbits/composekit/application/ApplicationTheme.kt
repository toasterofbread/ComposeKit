package dev.toastbits.composekit.application

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Shapes
import androidx.compose.material3.Typography
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.commonsettings.impl.ComposeKitSettings
import dev.toastbits.composekit.commonsettings.impl.group.InterfaceSettingsPaneSettingsProvider
import dev.toastbits.composekit.components.LocalContext
import dev.toastbits.composekit.components.utils.composable.pane.LocalPaneSettingsProvider
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.context.getDarkColorScheme
import dev.toastbits.composekit.context.getLightColorScheme
import dev.toastbits.composekit.context.isSystemInDarkTheme
import dev.toastbits.composekit.settingsitem.domain.utils.observe
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.model.ComposeKitFont
import dev.toastbits.composekit.theme.core.model.NamedTheme
import dev.toastbits.composekit.theme.core.model.SerialisableTheme
import dev.toastbits.composekit.theme.core.onAccent
import dev.toastbits.composekit.theme.core.provider.ContextThemeProvider
import dev.toastbits.composekit.theme.core.provider.ThemeProvider
import dev.toastbits.composekit.theme.core.ui.LocalComposeKitTheme
import dev.toastbits.composekit.theme.core.ui.LocalThemeProvider
import dev.toastbits.composekit.theme.core.vibrantAccent
import dev.toastbits.composekit.util.LocalisedComposeEnvironment
import dev.toastbits.composekit.util.amplifyPercent
import dev.toastbits.composekit.util.blendWith
import dev.toastbits.composekit.util.contrastAgainst
import dev.toastbits.composekit.util.getContrasted
import dev.toastbits.composekit.util.model.Locale
import dev.toastbits.composekit.util.platform.Platform

@Composable
fun ThemeValues.ApplicationTheme(
    context: PlatformContext,
    settings: ComposeKitSettings,
    baseUiScale: Float = 1f,
    baseFontScale: Float = 1f,
    content: @Composable () -> Unit
) {
    val darkTheme: Boolean = context.isSystemInDarkTheme() ?: true

    val primaryContainer: Color = accent.blendWith(background, ourRatio = 0.2f).contrastAgainst(background, by = 0.1f)
    val secondaryContainer: Color = accent.blendWith(background, ourRatio = 0.6f).contrastAgainst(background, by = 0.1f)
    val tertiaryContainer: Color = accent.blendWith(background, ourRatio = 0.8f).contrastAgainst(background, by = 0.1f)

    val colourScheme: ColorScheme =
        (if (darkTheme) context.getDarkColorScheme() else context.getLightColorScheme())
            .copy(
                primary = vibrantAccent,
                onPrimary = onAccent,
                inversePrimary = accent,
                secondary = onBackground,
                onSecondary = background,
                tertiary = vibrantAccent,
                onTertiary = vibrantAccent.getContrasted(),

                primaryContainer = primaryContainer,
                onPrimaryContainer = primaryContainer.getContrasted(),
                secondaryContainer = secondaryContainer,
                onSecondaryContainer = secondaryContainer.getContrasted(),
                tertiaryContainer = tertiaryContainer,
                onTertiaryContainer = tertiaryContainer.getContrasted(),

                background = background,
                onBackground = onBackground,

                surface = card.amplifyPercent(0.1f),
                onSurface = card.getContrasted(),
                surfaceVariant = card.amplifyPercent(0.2f),
                onSurfaceVariant = card.getContrasted(),
                surfaceTint = accent.blendWith(background, 0.75f),
                inverseSurface = vibrantAccent,
                inverseOnSurface = vibrantAccent.getContrasted(),

                outline = onBackground,
                outlineVariant = vibrantAccent,

//                error = Color.Unspecified,
//                onError = Color.Unspecified,
//                errorContainer = Color.Unspecified,
//                onErrorContainer = Color.Unspecified,
//                scrim = Color.Unspecified,
                surfaceBright = card.amplifyPercent(0.1f),
                surfaceDim = card,
                surfaceContainer = card.amplifyPercent(0.1f),
                surfaceContainerHigh = card.amplifyPercent(0.025f),
                surfaceContainerHighest = card,
                surfaceContainerLow = card.amplifyPercent(0.2f),
                surfaceContainerLowest = card.amplifyPercent(0.3f),
            )

    val composeKitFont: ComposeKitFont by settings.Interface.FONT.observe()
    val fontFamily: FontFamily = composeKitFont.rememberFontFamily()

    val defaultTypography: Typography = MaterialTheme.typography
    val typography: Typography = remember(defaultTypography, fontFamily) {
        with(defaultTypography) {
            copy(
                displayLarge = displayLarge.copy(fontFamily = fontFamily),
                displayMedium = displayMedium.copy(fontFamily = fontFamily),
                displaySmall = displaySmall.copy(fontFamily = fontFamily),
                headlineLarge = headlineLarge.copy(fontFamily = fontFamily),
                headlineMedium = headlineMedium.copy(fontFamily = fontFamily),
                headlineSmall = headlineSmall.copy(fontFamily = fontFamily),
                titleLarge = titleLarge.copy(fontFamily = fontFamily),
                titleMedium = titleMedium.copy(fontFamily = fontFamily),
                titleSmall = titleSmall.copy(fontFamily = fontFamily),
                bodyLarge = bodyLarge.copy(fontFamily = fontFamily),
                bodyMedium = bodyMedium.copy(fontFamily = fontFamily),
                bodySmall = bodySmall.copy(fontFamily = fontFamily),
                labelLarge = labelLarge.copy(fontFamily = fontFamily),
                labelMedium = labelMedium.copy(fontFamily = fontFamily),
                labelSmall = labelSmall.copy(fontFamily = fontFamily)
            )
        }
    }

    val defaultShapes: Shapes = MaterialTheme.shapes
    val shapes: Shapes = remember(defaultShapes) {
        when (Platform.current) {
            Platform.DESKTOP ->
                defaultShapes.copy(
                    extraSmall = RoundedCornerShape(2.dp),
                    small = RoundedCornerShape(4.dp),
                    medium = RoundedCornerShape(6.dp),
                    large = RoundedCornerShape(8.dp),
                    extraLarge = RoundedCornerShape(10.dp),
                )
            else -> defaultShapes
        }
    }

    val locale: Locale? by settings.Interface.UI_LOCALE.observe()
    val composeEnvironment: LocalisedComposeEnvironment =
        remember { LocalisedComposeEnvironment { locale } }

    val uiScale: Float by settings.Interface.UI_SCALE.observe()
    val fontScale: Float by settings.Interface.FONT_SCALE.observe()

    val customThemes: List<NamedTheme> by settings.Theme.CUSTOM_THEMES.observe()

    val themeProvider: ThemeProvider = remember(context, customThemes) {
        object : ContextThemeProvider(context) {
            override fun getCustomThemes(): List<NamedTheme> =
                customThemes

            override fun getCustomTheme(index: Int): SerialisableTheme? =
                customThemes.getOrNull(index)
        }
    }

    composeEnvironment.WithEnvironment {
        MaterialTheme(
            colorScheme = colourScheme,
            typography = typography,
            shapes = shapes
        ) {
            Box(Modifier.background(background)) {
                CompositionLocalProvider(
                    LocalContext provides context,
                    LocalComposeKitTheme provides this@ApplicationTheme,
                    LocalThemeProvider provides themeProvider,
                    LocalContentColor provides onBackground,
                    LocalPaneSettingsProvider provides InterfaceSettingsPaneSettingsProvider(settings.Interface),
                    LocalDensity provides Density(
                        LocalDensity.current.density * baseUiScale * uiScale,
                        baseFontScale * fontScale
                    ),
                ) {
                    Update()

                    PlatformTheme {
                        content()
                    }
                }
            }
        }
    }
}

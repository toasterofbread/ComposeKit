package dev.toastbits.composekit.application

import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import dev.toastbits.composekit.navigation.compositionlocal.LocalNavigator
import dev.toastbits.composekit.navigation.navigator.Navigator
import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.w3c.dom.PopStateEvent
import org.w3c.dom.events.Event
import org.w3c.dom.events.KeyboardEvent

@Composable
internal actual fun ApplicationContent(
    modifier: Modifier,
    content: @Composable (Modifier) -> Unit
) {
    content(modifier)

    val navigator: Navigator = LocalNavigator.current
    var previousBackCount: Int by remember { mutableIntStateOf(0) }
    val loadTime: Double = remember { getTime() }

    ListenToEvent<PopStateEvent>("popstate") { event ->
        val data: HistoryData? =
            (event.state as? JsString)?.toString()?.let { Json.decodeFromString(it) }

        if (data != null && data.loadTime != loadTime) {
            return@ListenToEvent
        }

        val depth: Int = data?.depth ?: 0
        if (depth < 0) {
            return@ListenToEvent
        }

        val difference: Int = previousBackCount - depth
        previousBackCount = depth

        if (difference > 0) {
            navigator.navigateBackward(difference)
        }
        else if (difference < 0) {
            navigator.navigateForward(-difference)
        }
    }

    ListenToEvent<KeyboardEvent>("keyup") { event ->
        if (event.key == "Escape" && navigator.navigateBackwardCount.value >= 1) {
            navigator.navigateBackward(1)
        }
    }

    LaunchedEffect(navigator.navigateBackwardCount) {
        navigator.navigateBackwardCount.collectLatest { navigateBackwardCount ->
            delay(100)

            val difference: Int = navigateBackwardCount - previousBackCount
            if (difference > 0) {
                for (i in 1 .. difference) {
                    val depth: Int = previousBackCount + i
                    window.history.pushState(
                        data = Json.encodeToString(HistoryData(depth, loadTime)).toJsString(),
                        title = document.title,
                        url = document.location!!.pathname + "#$depth"
                    )
                }
            }
            else if (difference < 0) {
                for (i in 0 until -difference) {
                    window.history.back()
                }
            }
            previousBackCount = navigateBackwardCount
        }
    }
}

private fun getTime(): Double = js("Date.now()")

@Serializable
private data class HistoryData(
    val depth: Int,
    val loadTime: Double
)

@Composable
private inline fun <reified T: Event> ListenToEvent(type: String, key: Any? = Unit, noinline listener: suspend (T) -> Unit) {
    val coroutineScope: CoroutineScope = rememberCoroutineScope()
    DisposableEffect(key) {
        val callback: (Event) -> Unit = { event ->
            if (event is T) {
                coroutineScope.launch {
                    listener(event)
                }
            }
        }

        window.addEventListener(type, callback)

        onDispose {
            window.removeEventListener(type, callback)
        }
    }
}

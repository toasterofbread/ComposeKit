package dev.toastbits.composekit.application

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import dev.toastbits.composekit.navigation.navigator.Navigator

@Composable
internal actual fun ApplicationContent(
    modifier: Modifier,
    content: @Composable (Modifier) -> Unit
) {
    content(modifier)
}

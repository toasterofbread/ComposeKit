package dev.toastbits.composekit.application

import androidx.compose.foundation.LocalScrollbarStyle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.theme.core.ThemeValues
import dev.toastbits.composekit.theme.core.vibrantAccent

@Composable
internal actual fun ThemeValues.PlatformTheme(content: @Composable () -> Unit) {
    CompositionLocalProvider(
        LocalScrollbarStyle provides LocalScrollbarStyle.current.copy(
            hoverColor = vibrantAccent,
            unhoverColor = vibrantAccent.copy(alpha = vibrantAccent.alpha * 0.25f),
            thickness = 6.dp
        )
    ) {
        content()
    }
}

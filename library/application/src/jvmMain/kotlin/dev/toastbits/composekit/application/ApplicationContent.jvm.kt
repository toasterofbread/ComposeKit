package dev.toastbits.composekit.application

import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.PointerButton
import androidx.compose.ui.input.pointer.PointerEventType
import androidx.compose.ui.input.pointer.onPointerEvent
import dev.toastbits.composekit.components.LocalContext
import dev.toastbits.composekit.components.platform.composable.onWindowBackPressed
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.navigation.compositionlocal.LocalNavigator
import dev.toastbits.composekit.navigation.navigator.Navigator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
internal actual fun ApplicationContent(
    modifier: Modifier,
    content: @Composable (Modifier) -> Unit
) {
    val coroutineScope: CoroutineScope = rememberCoroutineScope()
    val context: PlatformContext = LocalContext.current
    val navigator = LocalNavigator.current

    content(
        modifier.onPointerEvent(PointerEventType.Press) { event ->
            val index: Int = event.button?.index ?: return@onPointerEvent
            coroutineScope.launch {
                onButtonPress(index, context, navigator)
            }
        }
    )
}

private suspend fun onButtonPress(button: Int, context: PlatformContext, navigator: Navigator) {
    when (button) {
        PointerButton.Forward.index, 6 -> navigator.navigateForward()
        PointerButton.Back.index, 5 ->
            if (!onWindowBackPressed(context)) {
                navigator.navigateBackward()
            }
    }
}

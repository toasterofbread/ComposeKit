import util.configureAllComposeTargets

plugins {
    id("compose-conventions")
    id("android-library-conventions")
    id("gitlab-publishing-conventions")

    alias(libs.plugins.kotlinx.serialization)
}

kotlin {
    configureAllComposeTargets()

    sourceSets {
        commonMain {
            dependencies {
                implementation(projects.library.context)
                implementation(projects.library.commonsettings)
                implementation(projects.library.settings)
                implementation(projects.library.settingsitem.domain)
                implementation(projects.library.theme.core)
                implementation(projects.library.util)
                implementation(projects.library.utilKt)
                implementation(projects.library.components)
                implementation(projects.library.navigation)
            }
        }
    }
}

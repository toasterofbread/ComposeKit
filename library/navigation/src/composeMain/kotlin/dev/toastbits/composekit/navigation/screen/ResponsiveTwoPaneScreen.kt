package dev.toastbits.composekit.navigation.screen

import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.BoxWithConstraintsScope
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.utils.composable.crossfade.SkippableCrossfade
import dev.toastbits.composekit.components.utils.composable.crossfade.SkippableCrossfadeTransition
import dev.toastbits.composekit.components.utils.composable.pane.ResizableTwoPaneRow
import dev.toastbits.composekit.components.utils.composable.pane.model.InitialPaneRatioSource
import dev.toastbits.composekit.components.utils.composable.pane.model.ResizablePaneContainerParamsProvider
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

abstract class ResponsiveTwoPaneScreen<T: Any>: Screen {
    protected open val initialStartPaneRatioSource: InitialPaneRatioSource = InitialPaneRatioSource.Ratio(0.5f)
    protected open val paneParams: ResizablePaneContainerParamsProvider = ResizablePaneContainerParamsProvider.default()
    protected open val alwaysShowEndPane: Boolean = false

    val _isDisplayingBothPanes: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val isDisplayingBothPanes: StateFlow<Boolean> = _isDisplayingBothPanes.asStateFlow()

    @Composable
    protected abstract fun getCurrentData(): T?

    protected open fun BoxWithConstraintsScope.shouldDisplayBothPanes(): Boolean =
        (maxWidth / 2f) >= 300.dp

    protected open fun shouldSkipFormFactorTransition(from: Boolean, to: Boolean): SkippableCrossfadeTransition =
        SkippableCrossfadeTransition(false)

    @Composable
    final override fun Content(modifier: Modifier, contentPadding: PaddingValues) {
        val currentData: T? = getCurrentData()

        BoxWithConstraints(modifier) {
            _isDisplayingBothPanes.value = shouldDisplayBothPanes()

            SkippableCrossfade(
                isDisplayingBothPanes.collectAsState().value,
                shouldSkipTransition = ::shouldSkipFormFactorTransition,
                modifier = Modifier.fillMaxSize()
            ) { displayBothPanes ->
                if (displayBothPanes) {
                    ResizableTwoPaneRow(
                        startPaneContent = {
                            PrimaryPane(currentData, it, Modifier)
                        },
                        endPaneContent = {
                            SecondaryPane(currentData, it, Modifier)
                        },
                        showEndPane = alwaysShowEndPane || currentData != null,
                        initialStartPaneRatioSource = initialStartPaneRatioSource,
                        contentPadding = contentPadding,
                        modifier = Modifier.fillMaxSize(),
                        params = paneParams()
                    )
                }
                else if (currentData == null) {
                    PrimaryPane(null, contentPadding, Modifier.fillMaxSize())
                }
                else {
                    SecondaryPane(currentData, contentPadding, Modifier.fillMaxSize())
                }
            }
        }
    }

    @Composable
    protected abstract fun PrimaryPane(data: T?, contentPadding: PaddingValues, modifier: Modifier)

    @Composable
    protected abstract fun SecondaryPane(data: T?, contentPadding: PaddingValues, modifier: Modifier)
}
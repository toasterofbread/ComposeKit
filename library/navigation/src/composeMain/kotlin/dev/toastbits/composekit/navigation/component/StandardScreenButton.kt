package dev.toastbits.composekit.navigation.component

import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import dev.toastbits.composekit.components.utils.composable.LoadActionIconButton
import dev.toastbits.composekit.navigation.compositionlocal.LocalNavigator
import dev.toastbits.composekit.navigation.navigator.Navigator
import dev.toastbits.composekit.navigation.screen.ScreenButton

@Composable
fun ScreenButton.StandardButton(modifier: Modifier = Modifier) {
    val navigator: Navigator = LocalNavigator.current
    LoadActionIconButton(
        { onClick(navigator) },
        modifier = modifier
    ) {
        Icon(icon.get(), readableName.getComposable())
    }
}

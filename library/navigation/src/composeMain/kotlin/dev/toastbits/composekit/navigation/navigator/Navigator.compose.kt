package dev.toastbits.composekit.navigation.navigator

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

actual interface Navigator: BaseNavigator {
    @Composable
    fun CurrentScreen(
        modifier: Modifier,
        contentPadding: PaddingValues,
        render: @Composable (Modifier, PaddingValues, @Composable (Modifier, PaddingValues) -> Unit) -> Unit
    )
}

@Composable
fun Navigator.CurrentScreen(modifier: Modifier = Modifier, contentPadding: PaddingValues = PaddingValues()) {
    CurrentScreen(modifier, contentPadding) { innerModifier, innerContentPadding, content ->
        content(innerModifier, innerContentPadding)
    }
}

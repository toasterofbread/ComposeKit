package dev.toastbits.composekit.navigation.util

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import dev.toastbits.composekit.components.platform.composable.BackHandler
import dev.toastbits.composekit.navigation.navigator.Navigator
import kotlinx.coroutines.delay

@Composable
fun Navigator.DialogWrapper(
    show: Boolean,
    dialogContent: @Composable () -> Unit
) {
    var skipBack: Boolean by remember { mutableStateOf(true) }
    BackHandler(skipBack) {
        skipBack = false
    }

    LaunchedEffect(show) {
        if (!show) {
            delay(100)
            skipBack = false
        }
        else {
            skipBack = true
        }
    }

    if (show) {
        dialogContent()
    }
}

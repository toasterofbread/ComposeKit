package dev.toastbits.composekit.navigation.screen

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import dev.toastbits.composekit.components.utils.composable.crossfade.SkippableCrossfadeTransition
import dev.toastbits.composekit.navigation.navigator.CurrentScreen
import dev.toastbits.composekit.navigation.navigator.StandardNavigator
import dev.toastbits.composekit.util.combineStates
import dev.toastbits.composekit.util.mapState
import kotlinx.coroutines.flow.StateFlow

abstract class ResponsiveTwoPaneNavigatorScreen: ResponsiveTwoPaneScreen<Screen>(), NavigatorScreen {
    val currentScreen: StateFlow<Screen?>
        get() = internalNavigator.currentScreen.mapState {
            it.takeUnless { it == Screen.EMPTY }
        }

    fun resetNavigator() {
        internalNavigator.replaceScreenUpTo(Screen.EMPTY) { it == Screen.EMPTY }
    }

    final override val internalNavigator: StandardNavigator =
        object : StandardNavigator(Screen.EMPTY) {
            override val navigateBackwardCount: StateFlow<Int> =
                combineStates(isDisplayingBothPanes, super.navigateBackwardCount) { displayingBothPanes, superNavigateBackwardCount ->
                    if (displayingBothPanes) (superNavigateBackwardCount - 1).coerceAtLeast(0)
                    else superNavigateBackwardCount
                }
        }

    @Composable
    override fun SecondaryPane(data: Screen?, contentPadding: PaddingValues, modifier: Modifier) {
        internalNavigator.CurrentScreen(modifier, contentPadding)
    }

    @Composable
    final override fun getCurrentData(): Screen? =
        currentScreen.collectAsState().value

    override fun shouldSkipFormFactorTransition(from: Boolean, to: Boolean): SkippableCrossfadeTransition =
        SkippableCrossfadeTransition(currentScreen.value == null, false)
}

package dev.toastbits.composekit.navigation.screen

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

actual interface Screen: BaseScreen {
    @Composable
    fun Content(modifier: Modifier, contentPadding: PaddingValues)

    actual companion object {
        actual val EMPTY: Screen =
            object : Screen {
                override fun toString(): String = "Screen.EMPTY"

                @Composable
                override fun Content(
                    modifier: Modifier,
                    contentPadding: PaddingValues
                ) {}
            }
    }
}

package dev.toastbits.composekit.navigation.navigator

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.ui.component.GenericTopBar
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.util.composable.copy
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map

open class NavigatorWithGenericTopBar(
    initialScreen: Screen,
    extraButtonsHandledExternally: Flow<Boolean> = flowOf(false)
): StandardNavigator(
    initialScreen = initialScreen,
    extraButtonsHandledExternally = extraButtonsHandledExternally
) {
    @Composable
    override fun InnerContent(
        modifier: Modifier,
        contentPadding: PaddingValues,
        content: @Composable (Modifier, PaddingValues) -> Unit
    ) {
        Column(modifier) {
            GenericTopBar(
                title = currentScreen.collectAsState().value.title?.getComposable(),
                onBack = { navigateBackward(1) },
                canGoBack = navigateBackwardCount.map { it > 0 },
                modifier =
                    Modifier
                        .fillMaxWidth()
                        .padding(contentPadding.copy(bottom = 0.dp))
            )

            content(Modifier, contentPadding.copy(top = 20.dp))
        }
    }
}

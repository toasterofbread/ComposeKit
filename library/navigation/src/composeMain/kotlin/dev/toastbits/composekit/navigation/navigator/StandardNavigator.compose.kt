package dev.toastbits.composekit.navigation.navigator

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import dev.toastbits.composekit.components.ComposeKitDialogWrapper
import dev.toastbits.composekit.components.LocalComposeKitDialogWrapper
import dev.toastbits.composekit.components.platform.composable.ScrollBarLazyRow
import dev.toastbits.composekit.components.utils.composable.SubtleLoadingIndicator
import dev.toastbits.composekit.components.utils.composable.animatedvisibility.NullableValueAnimatedVisibility
import dev.toastbits.composekit.navigation.component.StandardButton
import dev.toastbits.composekit.navigation.compositionlocal.LocalNavigator
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.navigation.screen.ScreenButton
import dev.toastbits.composekit.navigation.util.DialogWrapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlin.time.Duration.Companion.milliseconds

actual open class StandardNavigator actual constructor(
    initialScreen: Screen,
    extraButtonsHandledExternally: Flow<Boolean>
): BaseStandardNavigator(initialScreen, extraButtonsHandledExternally) {
    private var _coroutineScope: CoroutineScope? = null
    actual override val coroutineScope: CoroutineScope? get() = _coroutineScope

    @Composable
    final override fun CurrentScreen(
        modifier: Modifier,
        contentPadding: PaddingValues,
        render: @Composable (Modifier, PaddingValues, @Composable (Modifier, PaddingValues) -> Unit) -> Unit
    ) {
        val currentCoroutineScope: CoroutineScope = rememberCoroutineScope()
        DisposableEffect(currentCoroutineScope) {
            _coroutineScope = currentCoroutineScope
            onDispose {
                if (coroutineScope == currentCoroutineScope) {
                    _coroutineScope = null
                }
            }
        }

        val composeKitDialogWrapper: ComposeKitDialogWrapper =
            remember {
                ComposeKitDialogWrapper { show, dialogContent ->
                    DialogWrapper(show, dialogContent)
                }
            }

        CompositionLocalProvider(LocalComposeKitDialogWrapper provides composeKitDialogWrapper) {
            OuterContent(modifier, contentPadding, render)
        }
    }

    @Composable
    protected open fun InnerContent(
        modifier: Modifier,
        contentPadding: PaddingValues,
        content: @Composable (Modifier, PaddingValues) -> Unit,
    ) {
        content(modifier, contentPadding)
    }

    @OptIn(InternalCoroutinesApi::class)
    @Composable
    private fun ScreenContent(screenWrapper: ScreenWrapper, modifier: Modifier, contentPadding: PaddingValues) {
        LaunchedEffect(screenWrapper.beforeOpenJob) {
            screenWrapper.beforeOpenJob.collectLatest { job ->
                if (job == null) {
                    return@collectLatest
                }

                job.join()

                val exception: Throwable? = job.getCancellationException().cause
                if (exception != null) {
                    RuntimeException("Screen ${screenWrapper.screen} beforeOpenJob failed", exception).printStackTrace()
                }

                screenWrapper.beforeOpenException.value = exception
                screenWrapper.beforeOpenJob.value = null
            }
        }

        val beforeOpenJob: Job? by screenWrapper.beforeOpenJob.collectAsState()
        val beforeOpenException: Throwable? by screenWrapper.beforeOpenException.collectAsState()

        if (beforeOpenJob != null) {
            SubtleLoadingIndicator(
                modifier.padding(contentPadding),
                appearAfter = 200.milliseconds,
                initialOffset = 0f
            )
        }
        else if (beforeOpenException != null) {
            Box(modifier.padding(contentPadding)) {
                SelectionContainer {
                    Text(remember(beforeOpenException) {
                        beforeOpenException?.stackTraceToString().toString()
                    })
                }
            }
        }
        else {
            screenWrapper.screen.Content(modifier, contentPadding)
        }
    }

    @Composable
    private fun OuterContent(
        modifier: Modifier,
        contentPadding: PaddingValues,
        render: @Composable (Modifier, PaddingValues, @Composable (Modifier, PaddingValues) -> Unit) -> Unit
    ) {
        CompositionLocalProvider(LocalNavigator provides this) {
            InnerContent(modifier, contentPadding) { innerModifier, innerContentPadding ->
                Column(innerModifier) {
                    render(
                        Modifier.weight(1f),
                        innerContentPadding
                    ) { innerModifier, innerContentPadding ->
                        val currentScreenWrapperValue: ScreenWrapper by currentScreenWrapper.collectAsState()
                        ScreenContent(currentScreenWrapperValue, innerModifier, innerContentPadding)
                    }

                    val currentExtraButtonsValue: List<ScreenButton> by currentExtraButtons.collectAsState(emptyList())
                    val extraButtons: List<ScreenButton>? =
                        if (extraButtonsHandledExternally.collectAsState(false).value) null
                        else currentExtraButtonsValue.ifEmpty { null }

                    NullableValueAnimatedVisibility(extraButtons) { buttons ->
                        ScrollBarLazyRow(verticalAlignment = Alignment.CenterVertically) {
                            items(buttons.orEmpty()) { button ->
                                button.StandardButton()
                            }
                        }
                    }
                }
            }
        }
    }

}

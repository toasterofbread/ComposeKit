package dev.toastbits.composekit.navigation.screen

import dev.toastbits.composekit.navigation.navigator.Navigator
import dev.toastbits.composekit.util.model.KImageVectorResource
import dev.toastbits.kmpresources.library.model.KmpStringResource

interface ScreenButton {
    val icon: KImageVectorResource
    val readableName: KmpStringResource

    suspend fun onClick(navigator: Navigator)
}

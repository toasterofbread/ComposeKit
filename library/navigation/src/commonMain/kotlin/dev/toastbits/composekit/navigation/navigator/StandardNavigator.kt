package dev.toastbits.composekit.navigation.navigator

import dev.toastbits.composekit.navigation.navigator.BaseNavigator.HistoryResetListener
import dev.toastbits.composekit.navigation.screen.NavigatorScreen
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.navigation.screen.ScreenButton
import dev.toastbits.composekit.navigation.screen.ScreenWithExtraButtons
import dev.toastbits.composekit.util.combineStates
import dev.toastbits.composekit.util.flatMapLatestState
import dev.toastbits.composekit.util.mapState
import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.update

expect open class StandardNavigator(
    initialScreen: Screen,
    extraButtonsHandledExternally: Flow<Boolean> = flowOf(false)
): BaseStandardNavigator {
    override val coroutineScope: CoroutineScope?
}

abstract class BaseStandardNavigator(
    initialScreen: Screen,
    open val extraButtonsHandledExternally: Flow<Boolean> = flowOf(false)
): Navigator {
    protected class ScreenWrapper(val screen: Screen) {
        var historyResetListener: HistoryResetListener? = null
        val beforeOpenJob: MutableStateFlow<Job?> = MutableStateFlow(null)
        val beforeOpenException: MutableStateFlow<Throwable?> = MutableStateFlow(null)
        
        fun release() {
            historyResetListener?.also { resetListener ->
                (screen as? NavigatorScreen)?.internalNavigator?.removeHistoryResetListener(resetListener)
            }
            runCatching { 
                beforeOpenJob.value?.cancel()
            }
            screen.release()
        }
    }

    private val stack: MutableStateFlow<List<ScreenWrapper>> = MutableStateFlow(emptyList())
    private val screenIndex: MutableStateFlow<Int> = MutableStateFlow(0)
    private val historyResetListeners: MutableList<HistoryResetListener> = mutableListOf()

    protected abstract val coroutineScope: CoroutineScope?

    private fun checkStack() {
        check(stack.value.indices.contains(screenIndex.value)) { "${screenIndex.value} | ${stack.value.toList()}" }
    }

    private inline fun updateCurrentScreenIndex(value: Int, withUpdatedValue: (Int) -> Unit = {}) {
        val newIndex: Int = value.coerceIn(0 until stack.value.size)
        val newScreen: ScreenWrapper = stack.value[newIndex]

        val oldIndex: Int = screenIndex.value
        val oldScreen: ScreenWrapper = stack.value[oldIndex]

        val movingBackward: Boolean = newIndex < oldIndex

        val canNavigateFromOldScreen: Boolean =
            if (movingBackward) oldScreen.screen.canNavigateBackwardFrom
            else oldScreen.screen.canNavigateForwardFrom

        if (!canNavigateFromOldScreen) {
            return
        }

        val screenChanging: Boolean = oldScreen.screen != newScreen.screen
        if (screenChanging) {
            oldScreen.beforeOpenJob.value?.cancel()
            newScreen.beforeOpenJob.value?.cancel()
            beforeScreenOpened(newScreen)
        }

        screenIndex.value = newIndex
        checkStack()
        withUpdatedValue(screenIndex.value)
        checkStack()

        if (screenChanging) {
            oldScreen.screen.onClosed(movingBackward = movingBackward)
        }
    }

    init {
        addScreenToStack(initialScreen, 0)
    }

    protected val currentScreenWrapper: StateFlow<ScreenWrapper> =
        combineStates(stack, screenIndex) { currentStack, currentScreenIndex ->
            currentStack[currentScreenIndex]
        }

    final override val currentScreen: StateFlow<Screen> =
        currentScreenWrapper.mapState {
            it.screen
        }

    override val currentTitle: Flow<KmpStringResource?> =
        combine(screenIndex, stack) { screenIndex, stack ->
            (screenIndex downTo 0).firstNotNullOfOrNull { stack[it].screen.title }
        }

    override val currentInfo: Flow<KmpStringResource?> =
        combine(screenIndex, stack) { screenIndex, stack ->
            (screenIndex downTo 0).firstNotNullOfOrNull { stack[it].screen.info }
        }

    override val currentExtraButtons: Flow<List<ScreenButton>> =
        currentScreen.flatMapLatest { screen ->
            (screen as? ScreenWithExtraButtons)?.extraButtons ?: flowOf()
        }

    override fun pushScreen(screen: Screen) {
        addScreenToStack(screen, screenIndex.value + 1)
    }

    override fun replaceScreen(screen: Screen, inPlace: Boolean) {
        addScreenToStack(screen, screenIndex.value, inPlace)
    }

    override fun replaceScreenUpTo(screen: Screen, inPlace: Boolean, isLastScreenToReplace: (Screen) -> Boolean) {
        for (i in screenIndex.value downTo 0) {
            if (isLastScreenToReplace(stack.value[i].screen)) {
                addScreenToStack(screen, i, inPlace)
                return
            }
        }

        pushScreen(screen)
    }

    override fun removeScreens(amount: Int) {
        if (amount <= 0) {
            return
        }

        val originalScreenIndex: Int = screenIndex.value
        updateCurrentScreenIndex(originalScreenIndex - amount) { screenIndex ->
            val removedAmount: Int = originalScreenIndex - screenIndex
            clearStack(
                keepFirst = originalScreenIndex + 1 - removedAmount, // 0
                keepLast = stack.value.size - originalScreenIndex - 1, // 1
                notifyListeners = false
            )
        }
    }

    private val childNavigator: StateFlow<Navigator?> =
        currentScreen.mapState {
            (it as? NavigatorScreen)?.internalNavigator
        }

    private fun getCurrentChildNavigator(): Navigator? =
        (currentScreen.value as? NavigatorScreen)?.internalNavigator

    override val navigateForwardCount: StateFlow<Int> =
        combineStates(
            stack,
            screenIndex,
            childNavigator.flatMapLatestState { it?.navigateForwardCount ?: MutableStateFlow(0) }
        ) { currentStack, currentScreenIndex, childNavigateForwardCount ->
            (currentStack.size - currentScreenIndex - 1).coerceAtLeast(0) + childNavigateForwardCount
        }

    override val navigateBackwardCount: StateFlow<Int> =
        combineStates(
            screenIndex,
            childNavigator.flatMapLatestState { it?.navigateForwardCount ?: MutableStateFlow(0) }
        ) { currentScreenIndex, childNavigateForwardCount ->
            currentScreenIndex.coerceAtLeast(0) + childNavigateForwardCount
        }

    override suspend fun navigateForward(by: Int) {
        if (by <= 0) {
            return
        }

        val child: Navigator? = childNavigator.first()
        val childNavigationCount: Int = child?.navigateForwardCount?.first() ?: 0
        if (childNavigationCount > 0) {
            child?.navigateForward(by.coerceAtMost(childNavigationCount))

            if (by > childNavigationCount) {
                updateCurrentScreenIndex(screenIndex.value + by - childNavigationCount)
            }
        }
        else {
            updateCurrentScreenIndex(screenIndex.value + by)
        }
    }

    override suspend fun navigateBackward(by: Int) {
        if (by <= 0) {
            return
        }

        val child: Navigator? = childNavigator.first()
        val childNavigationCount: Int = child?.navigateBackwardCount?.first() ?: 0
        if (childNavigationCount > 0) {
            child?.navigateBackward(by.coerceAtMost(childNavigationCount))

            if (by > childNavigationCount) {
                updateCurrentScreenIndex(screenIndex.value - by + childNavigationCount)
            }
        }
        else {
            updateCurrentScreenIndex(screenIndex.value - by)
        }
    }

    override fun peekRelative(offset: Int): Flow<Screen?> =
        combine(stack, screenIndex) { currentStack, currentScreenIndex ->
            currentStack.getOrNull(currentScreenIndex + offset)?.screen
        }

    override fun getMostRecentOfOrNull(predicate: (Screen) -> Boolean): Screen? {
        val currentStack: List<ScreenWrapper> = stack.value
        for (i in screenIndex.value downTo 0) {
            if (predicate(currentStack[i].screen)) {
                return currentStack[i].screen
            }
        }
        return null
    }

    override fun clearForwardNavigation() {
        clearStack(screenIndex.value + 1)
        getCurrentChildNavigator()?.clearForwardNavigation()
    }

    override fun addHistoryResetListener(listener: HistoryResetListener) {
        historyResetListeners.add(listener)
    }

    override fun removeHistoryResetListener(listener: HistoryResetListener) {
        historyResetListeners.remove(listener)
    }

    override fun visualise(): String =
        buildString {
            val topLine: String = "---${this@BaseStandardNavigator}---"
            appendLine(topLine)

            for ((index, item) in stack.value.withIndex()) {
                append("$index - ${item.screen}")
                if (index == screenIndex.value) {
                    append(" <-")
                }
                appendLine()
            }

            appendLine("-".repeat(topLine.length))
        }

    private fun addScreenToStack(screen: Screen, index: Int, inPlace: Boolean = false) {
        val currentStack: List<ScreenWrapper> = stack.value
        if (currentStack.getOrNull(index)?.screen != screen) {
            val screenWrapper: ScreenWrapper = ScreenWrapper(screen)

            if (screen is NavigatorScreen) {
                val historyResetListener: HistoryResetListener =
                    HistoryResetListener {
                        val screenIndex: Int = currentStack.indexOf(screenWrapper)
                        if (screenIndex != -1) {
                            clearStack(screenIndex + 1)
                        }
                    }
                screenWrapper.historyResetListener = historyResetListener
                screen.internalNavigator.addHistoryResetListener(historyResetListener)
            }

            stack.value = currentStack.toMutableList().also { newStack ->
                if (index in newStack.indices) {
                    newStack[index] = screenWrapper
                }
                else {
                    newStack.add(index, screenWrapper)
                }
            }

            checkStack()
        }

        updateCurrentScreenIndex(index) {
            if (!inPlace) {
                clearStack(keepFirst = it + 1)
            }
        }
    }

    private fun clearStack(
        keepFirst: Int,
        keepLast: Int = 0,
        notifyListeners: Boolean = true
    ) {
        stack.update { currentStack ->
            val newStack: MutableList<ScreenWrapper> = currentStack.toMutableList()
            for (i in 0 until newStack.size - keepFirst - keepLast) {
                newStack.removeAt(newStack.size - 1 - keepLast).release()
                checkStack()
            }
            return@update newStack
        }
        if (notifyListeners) {
            historyResetListeners.forEach(HistoryResetListener::invoke)
        }
    }

    private fun beforeScreenOpened(screenWrapper: ScreenWrapper) {
        with (screenWrapper.screen) {
            screenWrapper.beforeOpenException.value = null
            screenWrapper.beforeOpenJob.value = coroutineScope?.beforeOpen()
        }
    }
}

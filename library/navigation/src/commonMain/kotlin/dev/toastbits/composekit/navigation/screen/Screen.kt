package dev.toastbits.composekit.navigation.screen

import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job

expect interface Screen: BaseScreen {
    companion object {
        val EMPTY: Screen
    }
}

interface BaseScreen {
    val title: KmpStringResource? get() = null
    val info: KmpStringResource? get() = null

    val canNavigateBackwardFrom: Boolean get() = true
    val canNavigateForwardFrom: Boolean get() = true

    fun CoroutineScope.beforeOpen(): Job? = null
    fun onClosed(movingBackward: Boolean) {}
    fun release() {}
}

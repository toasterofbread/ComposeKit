package dev.toastbits.composekit.navigation.screen

import dev.toastbits.composekit.navigation.navigator.Navigator

interface NavigatorScreen: Screen {
    val internalNavigator: Navigator
}

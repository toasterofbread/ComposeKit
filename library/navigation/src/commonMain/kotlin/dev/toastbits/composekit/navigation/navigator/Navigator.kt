package dev.toastbits.composekit.navigation.navigator

import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.navigation.screen.ScreenButton
import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlin.reflect.KClass

expect interface Navigator: BaseNavigator

interface BaseNavigator {
    val currentScreen: Flow<Screen>
    val currentTitle: Flow<KmpStringResource?>
    val currentInfo: Flow<KmpStringResource?>
    val currentExtraButtons: Flow<List<ScreenButton>>

    val navigateForwardCount: StateFlow<Int>
    val navigateBackwardCount: StateFlow<Int>

    fun pushScreen(screen: Screen)
    fun replaceScreen(screen: Screen, inPlace: Boolean = false)
    fun replaceScreenUpTo(screen: Screen, inPlace: Boolean = false, isLastScreenToReplace: (Screen) -> Boolean)
    fun removeScreens(amount: Int)

    suspend fun navigateForward(by: Int = 1)
    suspend fun navigateBackward(by: Int = 1)

    fun peekRelative(offset: Int): Flow<Screen?>
    fun getMostRecentOfOrNull(predicate: (Screen) -> Boolean): Screen?

    fun clearForwardNavigation()

    fun addHistoryResetListener(listener: HistoryResetListener)
    fun removeHistoryResetListener(listener: HistoryResetListener)

    fun visualise(): String

    fun interface HistoryResetListener {
        operator fun invoke()
    }
}

fun Navigator.replaceScreenUpTo(screen: Screen, lastScreenToReplace: KClass<out Screen>) {
    replaceScreenUpTo(screen) { lastScreenToReplace.isInstance(it) }
}

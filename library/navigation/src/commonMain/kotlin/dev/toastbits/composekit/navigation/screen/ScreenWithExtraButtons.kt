package dev.toastbits.composekit.navigation.screen

import kotlinx.coroutines.flow.StateFlow

interface ScreenWithExtraButtons: Screen {
    val extraButtons: StateFlow<List<ScreenButton>>
}

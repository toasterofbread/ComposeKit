package dev.toastbits.composekit.navigation.screen

actual interface Screen: BaseScreen {
    actual companion object {
        actual val EMPTY: Screen =
            object : Screen {}
    }
}

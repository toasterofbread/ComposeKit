package dev.toastbits.composekit.navigation.navigator

import dev.toastbits.composekit.navigation.screen.Screen
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

actual open class StandardNavigator actual constructor(
    initialScreen: Screen,
    extraButtonsHandledExternally: Flow<Boolean>
): BaseStandardNavigator(initialScreen, extraButtonsHandledExternally) {
    actual override val coroutineScope: CoroutineScope? = null
}

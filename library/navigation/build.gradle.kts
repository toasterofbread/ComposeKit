import util.configureAllKmpTargets

plugins {
    id("android-library-conventions")
    id("compose-noncommon-conventions")
    id("gitlab-publishing-conventions")
    alias(libs.plugins.kotlinx.serialization)
}

kotlin {
    configureAllKmpTargets()

    sourceSets {
        commonMain {
            dependencies {
                api(libs.kmpresources)
                implementation(libs.kotlinx.coroutines.core)
                implementation(projects.library.utilKt)
            }
        }

        val composeMain by getting {
            dependencies {
                implementation(projects.library.components)
                implementation(projects.library.util)
            }
        }
    }
}

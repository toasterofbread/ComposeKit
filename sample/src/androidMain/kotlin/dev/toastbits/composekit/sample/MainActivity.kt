package dev.toastbits.composekit.sample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import dev.toastbits.composekit.context.ApplicationContext
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settings.PlatformSettingsImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel

class MainActivity : ComponentActivity() {
    private val coroutineScope: CoroutineScope = CoroutineScope(SupervisorJob())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val context: PlatformContext =
            PlatformContext(
                this,
                coroutineScope,
                ApplicationContext(this)
            )

        val preferences: PlatformSettings =
            PlatformSettingsImpl.getInstance(this)

        val application: ComposeKitSampleApplication =
            ComposeKitSampleApplication(context, preferences)

        enableEdgeToEdge()

        setContent {
            application.Main()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        coroutineScope.cancel()
    }
}
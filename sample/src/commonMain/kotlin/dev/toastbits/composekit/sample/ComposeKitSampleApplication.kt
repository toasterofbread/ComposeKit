package dev.toastbits.composekit.sample

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.application.ComposeKitApplication
import dev.toastbits.composekit.commonsettings.impl.ComposeKitSettings
import dev.toastbits.composekit.commonsettings.impl.ComposeKitSettingsImpl
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.sample.generated.resources.Res
import dev.toastbits.composekit.sample.generated.resources.language_name
import dev.toastbits.composekit.sample.screen.ComposeKitSampleScreen
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.util.composable.plus

class ComposeKitSampleApplication(
    private val context: PlatformContext,
    preferences: PlatformSettings
): ComposeKitApplication(ComposeKitSampleScreen(), context) {
    override val settings: ComposeKitSettings =
        ComposeKitSettingsImpl(preferences, Res.string.language_name)

    @Composable
    override fun Content(modifier: Modifier, contentPadding: PaddingValues) {
        super.Content(modifier, contentPadding + PaddingValues(20.dp))
    }
}

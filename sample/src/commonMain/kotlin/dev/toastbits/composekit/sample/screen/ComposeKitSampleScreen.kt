package dev.toastbits.composekit.sample.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.commonsettings.impl.ComposeKitSettings
import dev.toastbits.composekit.commonsettings.impl.LocalComposeKitSettings
import dev.toastbits.composekit.components.utils.composable.pane.model.InitialPaneRatioSource
import dev.toastbits.composekit.navigation.compositionlocal.LocalNavigator
import dev.toastbits.composekit.navigation.navigator.Navigator
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.sample.generated.resources.Res
import dev.toastbits.composekit.sample.generated.resources.app_name
import dev.toastbits.composekit.settings.ui.screen.PlatformSettingsScreen
import org.jetbrains.compose.resources.stringResource

internal class ComposeKitSampleScreen: Screen {
    private var opened: Boolean = false

    @Composable
    override fun Content(modifier: Modifier, contentPadding: PaddingValues) {
        val navigator: Navigator = LocalNavigator.current

        val settings: ComposeKitSettings? = LocalComposeKitSettings.current

        val screens: List<Screen> =
            remember {
                listOfNotNull(
                    settings?.let {
                        PlatformSettingsScreen(
                            it.preferences,
                            it.allGroups,
                            initialStartPaneRatioSource =
                                InitialPaneRatioSource.Remembered(
                                    "dev.toastbits.composekit.sample.screen.ComposeKitSampleScreen",
                                    InitialPaneRatioSource.Ratio(0.4f)
                                )
                        )
                    },
                    SettingsItemTestScreen()
                )
            }

        LaunchedEffect(Unit) {
            if (opened) {
                return@LaunchedEffect
            }

            val singleScreen: Screen? = screens.singleOrNull()
            if (singleScreen != null) {
                navigator.pushScreen(singleScreen)
            }

            opened = true
        }

        Column(
            modifier.padding(contentPadding),
            verticalArrangement = Arrangement.spacedBy(10.dp)
        ) {
            Text(
                stringResource(Res.string.app_name),
                style = MaterialTheme.typography.titleLarge
            )

            Column {
                for (screen in screens) {
                    Button({ navigator.pushScreen(screen) }) {
                        Text(screen.title?.getComposable() ?: screen.toString())
                    }
                }
            }
        }
    }
}
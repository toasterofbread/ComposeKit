package dev.toastbits.composekit.sample.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.toastbits.composekit.components.platform.composable.ScrollBarLazyColumn
import dev.toastbits.composekit.navigation.screen.Screen
import dev.toastbits.composekit.settingsitem.domain.MutableStateFlowSettingsProperty
import dev.toastbits.composekit.settingsitem.presentation.ui.component.item.TextFieldSettingsItem
import dev.toastbits.kmpresources.library.model.KmpStringResource
import kotlinx.coroutines.flow.MutableStateFlow

class SettingsItemTestScreen: Screen {
    @Composable
    override fun Content(modifier: Modifier, contentPadding: PaddingValues) {
        ScrollBarLazyColumn(
            modifier,
            contentPadding = contentPadding
        ) {
            item {
                val textInput: MutableStateFlow<String> = remember { MutableStateFlow("Hello!") }
                Column(
                    verticalArrangement = Arrangement.spacedBy(10.dp)
                ) {
                    TextFieldSettingsItem(
                        remember(textInput) {
                            MutableStateFlowSettingsProperty(
                                textInput,
                                propertyName = KmpStringResource.ofString("Text input"),
                                propertyDescription = KmpStringResource.ofString("Description")
                            )
                        }
                    ).Item(Modifier)

                    val textInputValue: String by textInput.collectAsState()
                    Text("External value: $textInputValue")

                    Button({ textInput.value = "" }) {
                        Text("Clear input")
                    }
                }
            }
        }
    }
}

import androidx.compose.material3.Text
import androidx.compose.ui.window.CanvasBasedWindow
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.sample.ComposeKitSampleApplication
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settings.cookies.BrowserCookies
import dev.toastbits.composekit.settings.cookies.CookiesPlatformSettings
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel

fun main() {
    val coroutineScope: CoroutineScope = CoroutineScope(SupervisorJob())

    val context: PlatformContext = PlatformContext(coroutineScope)
    val preferences: PlatformSettings = CookiesPlatformSettings(BrowserCookies())

    val application: ComposeKitSampleApplication =
        ComposeKitSampleApplication(context, preferences)

    CanvasBasedWindow(canvasElementId = "ComposeTarget") {
        Text("Hello World!")
        application.Main()
    }

    coroutineScope.cancel()
}

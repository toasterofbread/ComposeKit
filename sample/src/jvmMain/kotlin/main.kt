import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.window.singleWindowApplication
import dev.toastbits.composekit.context.PlatformContext
import dev.toastbits.composekit.sample.ComposeKitSampleApplication
import dev.toastbits.composekit.settings.PlatformSettings
import dev.toastbits.composekit.settings.PlatformSettingsJson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.job
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import java.awt.Dimension
import kotlin.time.Duration.Companion.seconds

fun main() {
    val coroutineScope: CoroutineScope = CoroutineScope(SupervisorJob())

    val context: PlatformContext =
        PlatformContext("ComposeKitSample", coroutineScope)
    val preferences: PlatformSettings =
        runBlocking {
            PlatformSettingsJson.load(
                context.getFilesDir()!!.resolve("settings.json"),
                Dispatchers.IO
            )
        }

    val application: ComposeKitSampleApplication =
        ComposeKitSampleApplication(context, preferences)

    singleWindowApplication(
        onKeyEvent = application::onKeyEvent
    ) {
        LaunchedEffect(Unit) {
            window.size = Dimension(1280, 900)
        }
        application.Main()
    }

    runBlocking {
        withTimeout(2.seconds) {
            coroutineScope.coroutineContext.job.cancelAndJoin()
        }
    }
}

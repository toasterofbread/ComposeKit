import org.jetbrains.kotlin.gradle.targets.js.dsl.KotlinWasmJsTargetDsl
import util.configureAllComposeTargets

plugins {
    id("android-application-conventions")
    id("compose-conventions")

    alias(libs.plugins.kotlin)
    alias(libs.plugins.gradleremoterunner)
}

kotlin {
    configureAllComposeTargets {
        when (this) {
            is KotlinWasmJsTargetDsl -> {
                browser {
                    commonWebpackConfig {
                        outputFileName = "client.js"
                    }
                }
                binaries.executable()
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                implementation(projects.library.application)
                implementation(projects.library.commonsettings)
                implementation(projects.library.components)
                implementation(projects.library.context)
                implementation(projects.library.navigation)
                implementation(projects.library.settings)
                implementation(projects.library.settingsitem.presentation)
                implementation(projects.library.util)
            }
        }

        val jvmMain by getting {
            dependencies {
                implementation(compose.desktop.currentOs)
                runtimeOnly(libs.kotlinx.coroutines.swing)
            }
        }
    }
}

compose.desktop {
    application {
        mainClass = "MainKt"
    }
}

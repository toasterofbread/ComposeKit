# ComposeKit

A collection of common code for use in my Kotlin/Compose Multiplatform projects.

This library is intended for personal use only. There is no documentation, and there will probably never be any documentation. You're welcome to use it if you want to though.

## Used in

- [SpMp](https://github.com/toasterofbread/spmp) (original code source)
- [LifeLog](https://gitlab.com/toasterofbread/lifelog)

## Installation

1. Add the repository to your dependency resolution configuration.
```kotlin
repositories {
  maven("https://maven.toastbits.dev")
}
```

2. Include the desired modules. Available module versions can be found [here](https://gitlab.com/toastbits/composekit/-/packages).

```kotlin
implementation("dev.toastbits:composekit-<module>:<version>")
```

## Compilation

###### Publish to local Maven repository
```shell
./gradlew publishAllModulesToMavenLocal
```

###### Publish to GitLab repository (see [gitlab-publishing-conventions](build-logic/src/main/kotlin/gitlab-publishing-conventions.gradle.kts))
```shell
./gradlew publishAllModulesToGitLabRepository
```

## Running the sample

### Desktop

```shell
./gradlew sample:run
```

### Android

```shell
./gradlew sample:installDebug
```

### Web

```shell
./gradlew sample:wasmJsBrowserRun
```

## Modules

Unless otherwise specified, modules are available for JVM, Android, and WASM.

### [Application](library/application)
// TODO

### [Commonsettings](library/commonsettings)
// TODO

### [Components](library/components)
// TODO

### [Context](library/context)
// TODO

### [File](library/file)
// TODO

### [Filechooser](library/filechooser) (JVM-only)

Code from [spookygames/gdx-nativefilechooser](https://github.com/spookygames/gdx-nativefilechooser)

<details>

<summary>LICENSE</summary>
<br>
The MIT License (MIT)

Copyright (c) 2016-2024 Spooky Games

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


</details>

### [Navigation](library/navigation)
// TODO

### [Settings](library/settings)
// TODO

### [Settingsitem](library/settingsitem)
// TODO

### [Theme](library/theme)
// TODO

### [Util](library/util)
// TODO

### [UtilKt](library/utilKt) (+Kotlin/ative)
// TODO

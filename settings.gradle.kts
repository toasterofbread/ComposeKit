@file:Suppress("UnstableApiUsage")

pluginManagement {
    includeBuild("build-logic")

    repositories {
        mavenLocal()
        maven("https://maven.toastbits.dev")

        gradlePluginPortal()
        mavenCentral()
        google()
    }
}

dependencyResolutionManagement {
    repositories {
        mavenLocal()
        maven("https://maven.toastbits.dev")

        mavenCentral()
        google()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}

rootProject.name = "ComposeKit"

include(":library:application")
include(":library:commonsettings")
include(":library:components")
include(":library:context")
include(":library:file")
include(":library:filechooser")
include(":library:navigation")
include(":library:settings")
include(":library:settingsitem:domain")
include(":library:settingsitem:presentation")
include(":library:settingsitem:presentation")
include(":library:settingsitem:resources")
include(":library:theme:config")
include(":library:theme:core")
include(":library:themetypes")
include(":library:util")
include(":library:utilKt")

include(":sample")
